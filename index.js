import App from './App';
import React from 'React';
import {ScreenOrientation} from 'expo';

export default class Index extends React.component {

async componentWillMount() {
	// Lock to portrait
    await ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT);
}

render() {
return(<App />);
}

}