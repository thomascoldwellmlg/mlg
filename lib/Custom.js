import { days,
		 suffixDate,
		 monthsArray } from '../components/reusable/common';

export const setObj = async (obj, value, parent, state) => {
	obj = value;
	parent.setState({[state]: obj});
}

export const getNeatDate = async () => {
	var d = new Date();
	var date = d.getDate();
	var suffixedDate = suffixDate[date - 1];
	var day = d.getDay();
	day = days[day];
	var month = d.getMonth();
	month = monthsArray[month];
	var neatDate = day + ' ' + suffixedDate + ' ' + month;
	return(neatDate);
}

export const getNeatShortDate = async () => {
	var d = new Date();
	var date = d.getDate();
	var suffixedDate = suffixDate[date - 1];
	var day = d.getDay();
	day = days[day];
	var month = d.getMonth();
	month = monthsArray[month];
	var neatDate = suffixedDate + ' ' + month;
	return(neatDate);
}

export const getNeatTime = async () => {
	var t = new Date();
	var hour = parseInt(t.getHours());
	if (hour < 10) {
		hour = '0' + hour.toString();
	}
	else {
		hour = hour.toString();
	}
	var minute = parseInt(t.getMinutes());
	if (minute < 10) {
		minute = '0' + minute.toString();
	}
	else {
		minute = minute.toString();
	}
	var neatTime = hour + ':' + minute;
	return(neatTime);
}

export const insertionSort = (arr) => {
  var minIdx, temp
      var len = arr.length;
  for(var i = 0; i < len; i++){
    minIdx = i;
    for(var  j = i+1; j<len; j++){
       if(arr[j]<arr[minIdx]){
          minIdx = j;
       }
    }
    temp = arr[i];
    arr[i] = arr[minIdx];
    arr[minIdx] = temp;
  }
  return arr;
}

export const objectToArray = (obj) => {
	var keys = Object.keys(obj);
	// Insertion Sort the keys to make sure they are nurmenically in order
	keys = insertionSort(keys);
	console.log(keys);
	var array = [];
	for (var i = 0; i <= keys.length - 1; i++) {
		array.push(obj[keys[i]]);
	}
	console.log('array == ', array)
	return(array);
}

