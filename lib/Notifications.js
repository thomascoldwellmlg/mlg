import { MemberData,
		 UserData,
		 UserId,
		 Append,
		 FamilyData } from './Firebase';
import { getNeatDate, getNeatTime } from './Custom';

export const sendGameLaunchedNotification = async (gameId, creatorId) => {
	// Construct Notification info
	const uniqueNotificationId = Date.now();
	var senderData = await UserData();
	var senderId = await UserId();
	var creatorData = await MemberData(creatorId);
	var gameTitle = creatorData.createdGames[gameId].title;
	var title = senderData.name.split(' ')[0] + ' started playing ' +  gameTitle;
	var neatDate = await getNeatDate();
	var neatTime = await getNeatTime();
	var subTitle = 'at ' + neatTime + ' on ' + neatDate;
	// Send Notification info
	var firebasePath = 'families/' + senderData.familyId + '/notifications/' + uniqueNotificationId;
	var data = {
		title: title,
		subTitle: subTitle,
		senderId: senderId,
		attachedMedia: null
	}
	await Append(data, firebasePath);
}

export const sendAddFamilyPhotoNotification = async (photoId) => {
	// Construct notification info
	const uniqueNotificationId = Date.now();
	var senderData = await UserData();
	var senderId = await UserId();
	var title = senderData.name.split(' ')[0] + ' added to the family album!';
	var neatDate = await getNeatDate();
	var neatTime = await getNeatTime();
	var subTitle = 'at ' + neatTime + ' on ' + neatDate;
	// Get image Url
	var familyData = await FamilyData();
	var imageUrl = familyData.familyAlbum[photoId].mainImageUrl;
	// Send notification info
	var firebasePath = 'families/' + senderData.familyId + '/notifications/' + uniqueNotificationId;
	var data = {
		title: title,
		subTitle: subTitle,
		senderId: senderId,
		attachedMedia: imageUrl
	}
	await Append(data, firebasePath);
}
