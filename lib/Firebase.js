// Contains all js functions that may be repeatedly called e.g. userdata, familydata
import * as firebase from 'firebase';

export const UserId = async () => {
	var userId = firebase.auth().currentUser.uid;
	return(userId);
}

export const UserEmail = async () => {
	var userEmail = firebase.auth().currentUser.email;
	return(userEmail);
}

export const UserData = async () => {
	var userId = firebase.auth().currentUser.uid;
	var data;
	await firebase.database().ref('users/' + userId).once('value').then(function(snapshot) {
		data = snapshot.val();
	});
	return(data);
}

export const FamilyData = async () => {
	var userId = firebase.auth().currentUser.uid;
	var userData;
	var familyData;
	await firebase.database().ref('users/' + userId).once('value').then(function(snapshot) {
		userData = snapshot.val();
	});
	await firebase.database().ref('families/' + userData.familyId).once('value').then(function(snapshot) {
		familyData = snapshot.val();
	});
	return(familyData);
}

export const FamilyId = async () => {
	var userId = firebase.auth().currentUser.uid;
	var userData;
	var familyData;
	await firebase.database().ref('users/' + userId).once('value').then(function(snapshot) {
		userData = snapshot.val();
	});
	return(userData.familyId);
}

export const ExternalFamilyData = async (familyId) => {
	await firebase.database().ref('families/' + familyId).once('value').then(function(snapshot) {
		familyData = snapshot.val();
	});
	return(familyData);
}

export const MemberData = async (memberId) => {
	var data;
	await firebase.database().ref('users/' + memberId).once('value').then(function(snapshot) {
		data = snapshot.val();
	});
	return(data);
}

export const Append = async (data, path) => {
	await firebase.database().ref(path).update(data);
}

export const FetchFamilyIds = async () => {
	var familyIds;
	await firebase.database().ref('families/').once('value').then(function(snapshot) {
		familyIds = Object.keys(snapshot.val());
	});
	return(familyIds);
}

