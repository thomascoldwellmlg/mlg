const util = require('util');

var json = {
 "families" : {
    "Miller" : {
      "familyAdmin" : "KZWmydxAWidcnw0dXQyy3bQUG572",
      "familyAlbum" : {
        "1537225657834" : {
          "dateTaken" : "",
          "description" : "",
          "location" : "",
          "mainImageUrl" : "https://s3-eu-west-1.amazonaws.com/testingmlg/families%2FVfdvbdfgb%2FfamilyAlbum%2F1537225657834%2Fmain-1537225658003.png",
          "title" : "",
          "uploadedBy" : "KZWmydxAWidcnw0dXQyy3bQUG572"
        }
      },
      "members" : {
        "KC5Do7mhFXNX3LOp7DKnNfXivmB3" : 0,
        "KZWmydxAWidcnw0dXQyy3bQUG572" : 0,
        "Hr3ApcPo2UWl1jcPjFPpozDj9fr2" : 0
      },
    },
  },
  "users" : {
    "Hr3ApcPo2UWl1jcPjFPpozDj9fr2" : {
      "creatorMode" : true,
      "email" : "a@gmail.com",
      "familyId" : "Miller",
      "familyState" : "in-group",
      "name" : "John Miller",
      "profilePicture" : "https://thumbs.dreamstime.com/b/vector-irregular-polygon-background-triangle-pattern-sky-blue-sand-orange-ice-white-color-vector-abstract-irregular-106505522.jpg",
      "relations" : {
        "KZWmydxAWidcnw0dXQyy3bQUG572" : "Wife"
      }
    },
    "KC5Do7mhFXNX3LOp7DKnNfXivmB3" : {
      "creatorMode" : true,
      "email" : "techtreereview2@gmail.com",
      "familyId" : "Vfdvbdfgb",
      "familyState" : "in-group",
      "name" : "Mavis Miller",
      "profilePicture" : "https://thumbs.dreamstime.com/b/vector-irregular-polygon-background-triangle-pattern-sky-blue-sand-orange-ice-white-color-vector-abstract-irregular-106505522.jpg",
      "receivedGames" : {
        "1536848256899" : "KZWmydxAWidcnw0dXQyy3bQUG572"
      },
      "relations" : {
        "KZWmydxAWidcnw0dXQyy3bQUG572" : "Daughter-in-law",
        "Hr3ApcPo2UWl1jcPjFPpozDj9fr2" : "Husband"
      }
    },
    "KZWmydxAWidcnw0dXQyy3bQUG572" : {
      "creatorMode" : true,
      "email" : "techtreereview@gmail.com",
      "familyId" : "Vfdvbdfgb",
      "familyState" : "in-group",
      "name" : "Jan Miller",
      "profilePicture" : "https://s3-eu-west-1.amazonaws.com/testingmlg/users%2FKZWmydxAWidcnw0dXQyy3bQUG572%2FprofilePicture.png",
      "relations" : {
        "KC5Do7mhFXNX3LOp7DKnNfXivmB3" : "Mother-in-law",
        "Hr3ApcPo2UWl1jcPjFPpozDj9fr2" : "Husband"
      }
    }
  }
}

var clean = JSON.stringify(json);
clean = JSON.parse(clean)
console.log(util.inspect(clean, {showHidden: false, depth: null}))
