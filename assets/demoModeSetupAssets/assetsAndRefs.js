// A file that contains all assets and url references to assets to include when initializing MLG
import {FileSystem} from 'expo';

const DEMO_DIR = `${FileSystem.documentDirectory}mlgDemoModeAssets/`;
var date = Date.now();

export const demoModeImageUrls = [
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/0.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/1.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/2.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/3.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/4.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/5.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/6.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/7.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/8.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/9.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/10.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/11.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/12.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/13.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/14.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockProfilePictures/15.jpg', // Mavis Miller PP = DEMO_DIR + '15.jpg'
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockProfilePictures/16.jpg', // Tom Miller PP = DEMO_DIR + '16.jpg'
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockProfilePictures/17.jpg', // John Miller PP = DEMO_DIR + '17.jpg'
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockProfilePictures/18.jpg', // Jan Miller PP = DEMO_DIR + '18.jpg'
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/19.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/20.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/21.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/22.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/23.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/24.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/25.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/26.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/27.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/28.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/29.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/30.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/31.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/32.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/33.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/34.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/35.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/36.jpg',
    'https://s3-eu-west-1.amazonaws.com/testingmlg/stockFamilyPhotos/37.jpg'
];

const demoModeProfilePictures = [
    DEMO_DIR + '17.jpg',
    DEMO_DIR + '15.jpg',
    DEMO_DIR + '16.jpg',
    DEMO_DIR + '18.jpg'
];

export const demoModeBaseImages = {
    ['0']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '0.jpg',
        title: ''
    },
    ['1']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '1.jpg',
        title: ''
    },
    ['2']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '2.jpg',
        title: ''
    },
    ['3']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '3.jpg',
        title: ''
    },
    ['4']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '4.jpg',
        title: ''
    },
    ['5']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '5.jpg',
        title: ''
    },
    ['6']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '6.jpg',
        title: ''
    },
    ['7']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '7.jpg',
        title: ''
    },
    ['8']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '8.jpg',
        title: ''
    },
    ['9']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '9.jpg',
        title: ''
    },
    ['10']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '10.jpg',
        title: ''
    },
    ['11']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '11.jpg',
        title: ''
    },
    ['12']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '12.jpg',
        title: ''
    },
    ['13']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '13.jpg',
        title: ''
    },
    ['14']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '14.jpg',
        title: ''
    },
    ['19']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '19.jpg',
        title: ''
    },
    ['20']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '20.jpg',
        title: ''
    },
    ['21']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '21.jpg',
        title: ''
    },
    ['22']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '22.jpg',
        title: ''
    },
    ['23']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '23.jpg',
        title: ''
    },
    ['24']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '24.jpg',
        title: ''
    },
    ['25']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '25.jpg',
        title: ''
    },
    ['26']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '26.jpg',
        title: ''
    },
    ['27']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '27.jpg',
        title: ''
    },
    ['28']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '28.jpg',
        title: ''
    },
    ['29']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '29.jpg',
        title: ''
    },
    ['30']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '30.jpg',
        title: ''
    },
    ['31']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '31.jpg',
        title: ''
    },
    ['32']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '32.jpg',
        title: ''
    },
    ['33']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '33.jpg',
        title: ''
    },
    ['34']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '34.jpg',
        title: ''
    },
    ['35']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '35.jpg',
        title: ''
    },
    ['36']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '36.jpg',
        title: ''
    },
    ['37']: {
        dateTaken: '23rd May 2012',
        location: 'London',
        mainImageUrl: DEMO_DIR + '37.jpg',
        title: ''
    }
};

const cGame1Id = (date + 1).toString();
const cGame2Id = (date + 2).toString();
const cGame3Id = (date + 3).toString();
export const demoModeBaseCreatedGames = {
    [cGame1Id]: {
        attachedMessage: 'Hey guys hope you enjoy this game I put together, its about flags from around the world!',
        title: 'Guess the flags!',
        uploadDate: '12th June 2018',
        questions: [
            {
                image: demoModeBaseImages['12'],
                title: 'Which country is this flag from?',
                correctAnswer: 'France',
                dummyAnswer1: 'Luxembourg',
                dummyAnswer2: 'Italy',
                dummyAnswer3: 'Argentina',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['13'],
                title: 'Which country is this flag from?',
                correctAnswer: 'Canada',
                dummyAnswer1: 'USA',
                dummyAnswer2: 'China',
                dummyAnswer3: 'Monaco',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['14'],
                title: 'Which country is this flag from?',
                correctAnswer: 'Sweden',
                dummyAnswer1: 'Norway',
                dummyAnswer2: 'Australia',
                dummyAnswer3: 'Germany',
                questionHint: ''
            }
        ],
        recipients: [
            'KZWmydxAWidcnw0dXQyy3bQUG572',
            'KY5Do7mhFXNX3LOp7DKnNfXivmB3'
        ]
    },
    [cGame2Id]: {
        attachedMessage: 'Hey Mum and Dad, this is a game all about your wedding day together I really hope you enjoy it!',
        title: "Mum and Dad's wedding day!",
        uploadDate: '20th June 2018',
        questions: [
            {
                image: demoModeBaseImages['19'],
                title: 'Which church did you get married in?',
                correctAnswer: 'St.Lukes',
                dummyAnswer1: 'St.Ninians',
                dummyAnswer2: 'Peel Cathedral',
                dummyAnswer3: 'All Saints',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['20'],
                title: "What colour was dad's tie on the day?",
                correctAnswer: 'Grey',
                dummyAnswer1: 'Pink',
                dummyAnswer2: 'Yellow',
                dummyAnswer3: 'Teal',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['21'],
                title: "Who was Mum's Bride of honour?",
                correctAnswer: 'Maria',
                dummyAnswer1: 'Emily',
                dummyAnswer2: 'Joan',
                dummyAnswer3: 'Susan',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['22'],
                title: "Where did you go on honeymoon?",
                correctAnswer: 'Spain',
                dummyAnswer1: 'America',
                dummyAnswer2: 'Italy',
                dummyAnswer3: 'France',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['23'],
                title: "What was your first dance to?",
                correctAnswer: 'Frank Sinatra',
                dummyAnswer1: 'David Bowie',
                dummyAnswer2: 'Elvis',
                dummyAnswer3: 'Sam Cooke',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['24'],
                title: "Where was this photo taken?",
                correctAnswer: 'Colden',
                dummyAnswer1: 'Whinfell Forest',
                dummyAnswer2: 'Archallaghan',
                dummyAnswer3: "St.John's",
                questionHint: ''
            },
        ],
        recipients: [
            'KC5Do7mhFXNX3LOp7DKnNfXivmB3',
            'KY5Do7mhFXNX3LOp7DKnNfXivmB3'
        ]
    },
    // [cGame3Id]: {
    //     attachedMessage: 'Hey guys hope you enjoy this game I put together, its about flags from around the world!',
    //     title: 'Guess the flags!',
    //     uploadDate: '12th June 2018',
    //     questions: [
    //         {
    //             image: demoModeBaseImages['12'],
    //             title: 'Which country is this flag from?',
    //             correctAnswer: 'France',
    //             dummyAnswer1: 'Luxembourg',
    //             dummyAnswer2: 'Italy',
    //             dummyAnswer3: 'Argentina',
    //             questionHint: ''
    //         },
    //         {
    //             image: demoModeBaseImages['13'],
    //             title: 'Which country is this flag from?',
    //             correctAnswer: 'Canada',
    //             dummyAnswer1: 'USA',
    //             dummyAnswer2: 'China',
    //             dummyAnswer3: 'Monaco',
    //             questionHint: ''
    //         },
    //         {
    //             image: demoModeBaseImages['14'],
    //             title: 'Which country is this flag from?',
    //             correctAnswer: 'Sweden',
    //             dummyAnswer1: 'Norway',
    //             dummyAnswer2: 'Australia',
    //             dummyAnswer3: 'Germany',
    //             questionHint: ''
    //         }
    //     ],
    //     recipients: [
    //         'KZWmydxAWidcnw0dXQyy3bQUG572',
    //         'KY5Do7mhFXNX3LOp7DKnNfXivmB3'
    //     ]
    // }
};

const rGame1Id = (date + 10).toString();
const rGame2Id = (date + 11).toString();
const rGame3Id = (date + 12).toString();
export const demoModeBaseReceivedGames = {
    [rGame1Id]: 'KZWmydxAWidcnw0dXQyy3bQUG572',
    [rGame2Id]: 'KY5Do7mhFXNX3LOp7DKnNfXivmB3',
    [rGame3Id]: 'KZWmydxAWidcnw0dXQyy3bQUG572'
}

const demoModeJanGames = {
    [rGame1Id]: {
        attachedMessage: "Hey John could you let me know what you think of this game for your Dad? Thanks, Jan xx",
        title: "John and Tom's South America trip!",
        uploadDate: '12th June 2018',
        questions: [
            {
                image: demoModeBaseImages['25'],
                title: 'What year did we go on our trip around South America?',
                correctAnswer: '2012',
                dummyAnswer1: '2004',
                dummyAnswer2: '2017',
                dummyAnswer3: '2008',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['26'],
                title: 'Where did we see these iconic peaks?',
                correctAnswer: 'Patagonia',
                dummyAnswer1: 'Brazil',
                dummyAnswer2: 'Peru',
                dummyAnswer3: 'Bolivia',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['27'],
                title: 'What colour was my backpack on that trip?',
                correctAnswer: 'Green',
                dummyAnswer1: 'Red',
                dummyAnswer2: 'Black',
                dummyAnswer3: 'Blue',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['28'],
                title: 'Where did we see pink dolphins?',
                correctAnswer: 'Amazon River',
                dummyAnswer1: 'Rio Negro',
                dummyAnswer2: 'Orinoco',
                dummyAnswer3: 'Paraguay',
                questionHint: ''
            }
        ],
        recipients: [
            'Hr3ApcPo2UWl1jcPjFPpozDj9fr2'
        ]
    },
    [rGame3Id]: {
        attachedMessage: 'Hi John, I was looking through the attic and found these lovely photos of all the kids tucked away in an album - I thought they would be perfect for a game!',
        title: 'Early photos of the kids!',
        uploadDate: '21st June 2018',
        questions: [
            {
                image: demoModeBaseImages['29'],
                title: 'Who is the baby in this photo?',
                correctAnswer: 'Emily',
                dummyAnswer1: 'Lucy',
                dummyAnswer2: 'Amelia',
                dummyAnswer3: 'Freya',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['30'],
                title: 'How old is Lucy here?',
                correctAnswer: '7 years old',
                dummyAnswer1: '6 years old',
                dummyAnswer2: '8 years old',
                dummyAnswer3: '9 years old',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['31'],
                title: "What was Uncle Dan's nickname for Lucy?",
                correctAnswer: 'Smartie Pants',
                dummyAnswer1: 'Angel',
                dummyAnswer2: 'Nugget',
                dummyAnswer3: 'Professor',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['32'],
                title: 'What sport did Uncle Dan teach Emily?',
                correctAnswer: 'Football',
                dummyAnswer1: 'Tennis',
                dummyAnswer2: 'Badminton',
                dummyAnswer3: 'Archery',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['33'],
                title: 'Where on the IOM did Lucy and Emily hike up?',
                correctAnswer: 'Peel Hill',
                dummyAnswer1: 'South Barrule',
                dummyAnswer2: 'North Barrule',
                dummyAnswer3: 'Skiddaw',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['34'],
                title: 'Who is Lucy feeding cake to?',
                correctAnswer: 'Cousin Billy',
                dummyAnswer1: 'Simon Sheath',
                dummyAnswer2: 'Tom Benn',
                dummyAnswer3: 'Sam Cowley',
                questionHint: ''
            }
        ],
        recipients: [
            'Hr3ApcPo2UWl1jcPjFPpozDj9fr2'
        ]
    }
}

const demoModeTomGames = {
    [rGame2Id]: {
        attachedMessage: "Hey John I thought I would give you a bit of an insight into the work I did as an engineer at NASA during the Apollo era - it features some of my favourtite photos and I'm sure you'll enjoy them just as much as me!",
        title: "Space Questions!",
        uploadDate: '17th June 2018',
        questions: [
            {
                image: demoModeBaseImages['35'],
                title: 'Who took this famous photo of Apollo 12?',
                correctAnswer: 'Alan Bean',
                dummyAnswer1: 'Buzz Aldrin',
                dummyAnswer2: 'Chris Hatfield',
                dummyAnswer3: 'Don Pettit',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['36'],
                title: 'What type of star is shown here?',
                correctAnswer: 'Neutron',
                dummyAnswer1: 'Dwarf',
                dummyAnswer2: 'Red Giant',
                dummyAnswer3: 'Pulsar',
                questionHint: ''
            },
            {
                image: demoModeBaseImages['37'],
                title: 'How far away is Mars from Earth?',
                correctAnswer: '40M miles',
                dummyAnswer1: '120M miles',
                dummyAnswer2: '1000 miles',
                dummyAnswer3: '243M miles',
                questionHint: ''
            }
        ],
        recipients: [
            'Hr3ApcPo2UWl1jcPjFPpozDj9fr2'
        ]
    }
}

export const demoModeInitialDatabase = { 
    families: { 
        Miller: { 
            familyAdmin: 'Hr3ApcPo2UWl1jcPjFPpozDj9fr2',
            familyAlbum: demoModeBaseImages,
            members: { 
                KC5Do7mhFXNX3LOp7DKnNfXivmB3: 0,
                KZWmydxAWidcnw0dXQyy3bQUG572: 0,
                Hr3ApcPo2UWl1jcPjFPpozDj9fr2: 0,
                KY5Do7mhFXNX3LOp7DKnNfXivmB3: 0 
            } 
        } 
    },
    users: { 
        Hr3ApcPo2UWl1jcPjFPpozDj9fr2: {
            createdGames: demoModeBaseCreatedGames,
            receivedGames: demoModeBaseReceivedGames,
            creatorMode: true,
            email: 'a@gmail.com',
            familyId: 'Miller',
            familyState: 'in-group',
            name: 'John Miller',
            profilePicture: demoModeProfilePictures[0]
        },
        KC5Do7mhFXNX3LOp7DKnNfXivmB3: { 
            receivedGames: {},
            creatorMode: true,
            email: 'b@gmail.com',
            familyId: 'Miller',
            familyState: 'in-group',
            name: 'Mavis Miller',
            profilePicture: demoModeProfilePictures[1]
        },
        KY5Do7mhFXNX3LOp7DKnNfXivmB3: { 
            receivedGames: {},
            createdGames: demoModeTomGames,
            creatorMode: true,
            email: 'b@gmail.com',
            familyId: 'Miller',
            familyState: 'in-group',
            name: 'Tom Miller',
            profilePicture: demoModeProfilePictures[2]
        },
        KZWmydxAWidcnw0dXQyy3bQUG572: {
            receivedGames: {},
            createdGames: demoModeJanGames,
            creatorMode: true,
            email: 'c@gmail.com',
            familyId: 'Miller',
            familyState: 'in-group',
            name: 'Jan Miller',
            profilePicture: demoModeProfilePictures[3]
        } 
    } 
}