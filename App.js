import React from 'react';
import { StackNavigator, TabNavigator } from 'react-navigation';
import { StyleSheet, 
         Text, 
         View,
         TouchableOpacity, 
         Image,
         Alert, 
         Dimensions,
         StatusBar,
         Animated } from 'react-native';
import Expo, {FileSystem, BlurView} from 'expo';
import { AppLoading, ScreenOrientation, Asset, Permissions } from 'expo';
import {CacheManager, setupCacheDirectory} from "react-native-aws-cache";
import 'react-native-console-time-polyfill';

// Aws imports
import Amplify, { Storage } from 'aws-amplify';
import aws_exports from './aws-exports';
Amplify.configure(aws_exports);

var overlayWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

// All screen imports for the app router
import SplashScreen from './components/screens/SplashScreen/SplashScreen';
import LoginScreen from './components/screens/LoginScreen/LoginScreen';
import AccountCreationScreen from './components/screens/AccountCreationScreen/AccountCreationScreen';
import ActivityScreen from './components/screens/HomeScreens/ActivityScreen/ActivityScreen';
import CreatedGamesScreen from './components/screens/HomeScreens/CreatedGamesScreen/CreatedGamesScreen';
import FamilyGamesScreen from './components/screens/HomeScreens/FamilyGamesScreen/FamilyGamesScreen';
import FamilyAlbumScreen from './components/screens/HomeScreens/FamilyAlbumScreen/FamilyAlbumScreen';
import MyProfileScreen from './components/screens/HomeScreens/MyProfileScreen/MyProfileScreen';
import AddFamilyAlbumPhotoScreen from './components/reusable/AddFamilyAlbumPhotoScreen/AddFamilyAlbumPhotoScreen';
import SettingsScreen from './components/screens/SettingsScreen/SettingsScreen';
import SmartProfileScreen from './components/screens/SmartProfileScreen/SmartProfileScreen';
import MyFamilyGroupScreen from './components/screens/MyFamilyGroupScreen/MyFamilyGroupScreen';
import CreateGameScreen from './components/screens/CreateGameScreen/CreateGameScreen';
import MainGameActivityScreen from './components/screens/MainGameActivityScreen/MainGameActivityScreen';
import MessagingHomeScreen from './components/screens/MessagingHomeScreen/MessagingHomeScreen';
import MessagingChatScreen from './components/screens/MessagingChatScreen/MessagingChatScreen';
// Any Player Specific Screens
import PlayerGamesScreen from './components/screens/Player/PlayerGamesScreen/PlayerGamesScreen';
import PlayerFamilyAlbumScreen from './components/screens/Player/PlayerFamilyAlbumScreen/PlayerFamilyAlbumScreen';
import PlayerProfileScreen from './components/screens/Player/PlayerProfileScreen/PlayerProfileScreen';

// Import any Tab Navigators
import CreatorHomeNavBar from './components/TabNavBars/CreatorHomeNavBar/CreatorHomeNavBar';
import PlayerHomeNavBar from './components/TabNavBars/PlayerHomeNavBar/PlayerHomeNavBar';

import * as firebase from 'firebase';

// Icons etc. to be natively bundled - require no online dependancy ever
const assetsToCache = [
    require('./assets/icons/png/Memory-Lane-Games.png'),
    require('./assets/icons/png/account-multiple-plus.png'),
    require('./assets/icons/png/account-plus.png'),
    require('./assets/icons/png/account-settings-variant.png'),
    require('./assets/icons/png/account.png'),
    require('./assets/icons/png/arrow-left.png'),
    require('./assets/icons/png/arrow-right.png'),
    require('./assets/icons/png/brush.png'),
    require('./assets/icons/png/camera-iris.png'),
    require('./assets/icons/png/check.png'),
    require('./assets/icons/png/chevron-down.png'),
    require('./assets/icons/png/chevron-left.png'),
    require('./assets/icons/png/chevron-right.png'),
    require('./assets/icons/png/close.png'),
    require('./assets/icons/png/creation.png'),
    require('./assets/icons/png/delete-forever.png'),
    require('./assets/icons/png/exit-to-app.png'),
    require('./assets/icons/png/gamepad.png'),
    require('./assets/icons/png/gift.png'),
    require('./assets/icons/png/help-circle-outline.png'),
    require('./assets/icons/png/human-greeting.png'),
    require('./assets/icons/png/image-multiple.png'),
    require('./assets/icons/png/image-plus.png'),
    require('./assets/icons/png/library-plus.png'),
    require('./assets/icons/png/menu.png'),
    require('./assets/icons/png/message.png'),
    require('./assets/icons/png/newspaper.png'),
    require('./assets/icons/png/noun_Family_1106970.png'),
    require('./assets/icons/png/noun_design_1537634.png'),
    require('./assets/icons/png/play.png'),
    require('./assets/icons/png/plus.png'),
    require('./assets/icons/png/presentation-play.png'),
    require('./assets/icons/png/rotate-3d.png'),
    require('./assets/icons/png/send.png'),
    require('./assets/icons/png/square-edit-outline.png'),
    require('./assets/icons/png/text.png'),
    require('./assets/icons/png/voice.png'),
    require('./assets/images/animations/questionImage1.jpg'),
    require('./assets/images/animations/questionImage2.jpg'),
    require('./assets/images/animations/questionImage3.jpg'),
    require('./assets/images/animations/completeQuestion1.png'),
    require('./assets/images/animations/completeQuestion2.png'),
    require('./assets/images/animations/completeQuestion3.png')
];

// import any assets that need loading in e.g. demo mode assets and icons to be bundled
import { demoModeImageUrls,
         demoModeInitialDatabase } from './assets/demoModeSetupAssets/assetsAndRefs';
// Demo mode assets to grab
const DEMO_DIR = `${FileSystem.documentDirectory}mlgDemoModeAssets/`;

// Initialize Firebase
var config = {
    apiKey: "AIzaSyB6lZ0Fne0We2pxlBBJcP5WWSgSaxCC5gA",
    authDomain: "memory-lane-games.firebaseapp.com",
    databaseURL: "https://memory-lane-games.firebaseio.com",
    projectId: "memory-lane-games",
    storageBucket: "memory-lane-games.appspot.com",
    messagingSenderId: "66793158527"
};

firebase.initializeApp(config);

const HomeScreenTabOptions = {
    header: null, 
    swipeEnabled: false,
    animationEnabled: true
}

const AddPhotoTabOptions = {
    header: null, 
    swipeEnabled: false,
    animationEnabled: true,
    tabBarVisible: false
}

const HomeScreenTabBarStyle = {
    showLabel: false
}

// Defines the App Navigation
const AppRouter = StackNavigator({
    SplashScreen: {screen: SplashScreen, navigationOptions: ({navigation}) => ({ header: null })},
    LoginScreen: {screen: LoginScreen, navigationOptions: ({navigation}) => ({ header: null })},
    AccountCreationScreen: {screen: AccountCreationScreen, navigationOptions: ({navigation}) => ({ header: null })},
    CreatorHomeScreen: {screen: TabNavigator({
        ActivityScreen: {screen: ActivityScreen, navigationOptions: ({ navigation }) => (HomeScreenTabOptions)},
        FamilyGamesScreen: {screen: FamilyGamesScreen, navigationOptions: ({ navigation }) => (HomeScreenTabOptions)},
        CreatedGamesScreen: {screen: CreatedGamesScreen, navigationOptions: ({ navigation }) => (HomeScreenTabOptions)},
        FamilyAlbumScreen: {screen: FamilyAlbumScreen, navigationOptions: ({ navigation }) => (HomeScreenTabOptions)},
        MyProfileScreen: {screen: MyProfileScreen, navigationOptions: ({ navigation }) => (HomeScreenTabOptions)},
        AddFamilyAlbumPhotoScreen: {screen: AddFamilyAlbumPhotoScreen, navigationOptions: ({ navigation }) => (AddPhotoTabOptions)},
    }, {tabBarOptions: HomeScreenTabBarStyle, tabBarComponent: CreatorHomeNavBar})},
    SettingsScreen: {screen: SettingsScreen, navigationOptions: ({navigation}) => ({ header: null })},
    SmartProfileScreen: {screen: SmartProfileScreen, navigationOptions: ({navigation}) => ({ header: null })},
    MyFamilyGroupScreen: {screen: MyFamilyGroupScreen, navigationOptions: ({navigation}) => ({ header: null })},
    CreateGameScreen: {screen: CreateGameScreen, navigationOptions: ({navigation}) => ({ header: null })},
    MainGameActivityScreen: {screen: MainGameActivityScreen, navigationOptions: ({navigation}) => ({ header: null })},
    MessagingHomeScreen: {screen: MessagingHomeScreen, navigationOptions: ({navigation}) => ({ header: null })},
    MessagingChatScreen: {screen: MessagingChatScreen, navigationOptions: ({navigation}) => ({ header: null })},
    PlayerHomeScreen: {screen: TabNavigator({
        PlayerGamesScreen: {screen: PlayerGamesScreen, navigationOptions: ({ navigation }) => (HomeScreenTabOptions)},
        PlayerFamilyAlbumScreen: {screen: PlayerFamilyAlbumScreen, navigationOptions: ({ navigation }) => (HomeScreenTabOptions)},
        PlayerProfileScreen: {screen: PlayerProfileScreen, navigationOptions: ({ navigation }) => (HomeScreenTabOptions)},
    }, {tabBarOptions: HomeScreenTabBarStyle, tabBarComponent: PlayerHomeNavBar})},
});

export default class App extends React.Component {

    state = {
        assetsLoaded: false,
        fontLoaded: false,
        alphaBarWidth: overlayWidth,
        alphaBarHeight: 50,
        loadingScreenIntensity: new Animated.Value(60)
    }

    async componentDidMount() { 
        console.log('Loaded!')
        // Start loading screen animations
        this.startLoadingScreenAnimation();
        //Testing
        // await CacheManager.clearCache();
        // await CacheManager.init();
        // Ensures we have a makeDirAsync set up without clearing the previously cached data if it doesn't need to
        var successfulInit = await Expo.SecureStore.getItemAsync('aws-init');
        if (successfulInit != 'success') {
            await CacheManager.clearCache();
            await Expo.SecureStore.setItemAsync('aws-init', 'success');
        }
        // Load in any Async assets
        await Expo.Font.loadAsync({
            'Futura': require("./assets/fonts/Futura_Medium_Bt.ttf")
        });
        await Asset.loadAsync(assetsToCache);
        this.setState({fontLoaded: true});
        // Get any of the demo mode assets we require and save them to the demo mode async dir
        await this.getDemoModeAssets();
        this.setState({assetsLoaded: true});
        // Lock to portrait
        ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT);
        // Add a listener for the Alpha info bar 
        Dimensions.addEventListener('change', this.handler);
    }

    startLoadingScreenAnimation() {
        Animated.loop(
            Animated.sequence([
                Animated.timing(
                    this.state.loadingScreenIntensity,
                    {
                        toValue: 90,
                        duration: 1000
                    }
                ),
                Animated.timing(
                    this.state.loadingScreenIntensity,
                    {
                        toValue: 60,
                        duration: 1000
                    }
                )
            ])
        ).start();
    }

    handler = dims => {
        this.setState({alphaBarWidth: dims.window.width});
    }

    async getDemoModeAssets() {
        // Check SecureStore to see if we have already downloaded the images
        // await Expo.SecureStore.setItemAsync('demo-mode-init', 'false'); // Testing
        var successfulInit = await Expo.SecureStore.getItemAsync('demo-mode-init');
        if (successfulInit != 'true') {
            // Initialize the directory
            await FileSystem.deleteAsync(DEMO_DIR, { idempotent: true });
            await FileSystem.makeDirectoryAsync(DEMO_DIR, {intermediates: true});
            var dir = await FileSystem.getInfoAsync(DEMO_DIR);
            console.log(dir);
            // Pull assets into that directory
            this.setState({demoModeAssetsCached: false});
            // Create a timeout function that fires after 120 seconds if all assets are not downloaded
            setTimeout(() => {
                if (this.state.demoModeAssetsCached == false) {
                    Alert.alert("Please check your internet connection and restart Memory Lane Games!")
                }
            }, 120000);
            for (var i = 0; i <= demoModeImageUrls.length - 1; i++) {
                await FileSystem.downloadAsync(
                    demoModeImageUrls[i],
                    DEMO_DIR + i.toString() + '.jpg'
                ).then((response) => console.log(response));
                if (i == demoModeImageUrls.length - 1) {
                    this.setState({demoModeAssetsCached: true});
                }
            }
            // Add a clean JSON file to manage any additional assets the user may add
            await FileSystem.writeAsStringAsync(
                DEMO_DIR + 'manager.json',
                JSON.stringify(demoModeInitialDatabase)
            );
            var json = await FileSystem.readAsStringAsync(DEMO_DIR + 'manager.json');
            console.log(JSON.parse(json))
            // Set secure store key to show the images were successfully downloaded
            await Expo.SecureStore.setItemAsync('demo-mode-init', 'true');
        }
    }

    async hardResetApp() {
        await Expo.SecureStore.setItemAsync('demo-mode-init', 'false');
        Expo.SecureStore.deleteItemAsync('userName');
        Expo.SecureStore.deleteItemAsync('userPassword');
    }

    uiBuildInfoOverlay() {
        if (this.state.buildNotesOpened == true) {
            return(
                <TouchableOpacity style={{ position: "absolute", backgroundColor: 'rgba(100,100,100,0.8)', height: 320, width: this.state.alphaBarWidth, justifyContent: 'flex-start', alignItems: 'center', paddingTop: 10}}
                                  onPress={() => this.setState({buildNotesOpened: false})}>
                  <Text style={{color: '#ccc', fontSize: 22}}>
                      Memory Lane Games Alpha || 
                      Version: <Text style={{color: '#fff'}}>0.2.0</Text> || 
                      Build: <Text style={{color: '#fff'}}>1</Text>
                  </Text>
                  <Text style={{color: '#ccc', fontSize: 22}}>
                      {"\n "}<Text style={{color: '#fff'}}>Build notes:</Text>
                      Hi there Peter and Bruce, this latest version should be bug free in demo mode. We also have some basic games as placeholders but if you think of anything
                      in particular we can replace them with send me the structure for some games! Cheers!
                      Date = 6/10/2018
                  </Text>
                  <TouchableOpacity style={{height: screenHeight * 0.1, width: screenWidth * 0.4, backgroundColor: '#1289A7', justifyContent: 'center', alignItems: 'center'}}
                                    onPress={ async () => Alert.alert(
                                                          "Hard reset the app?",
                                                          null,
                                                          [
                                                            {text: "Confirm", onPress: async () => this.hardResetApp()},
                                                            {text: "Cancel"}
                                                          ])}>
                        <Text style={{fontSize: screenHeight * 0.02, color: '#fff'}}>Hard Reset</Text>
                  </TouchableOpacity>
                </TouchableOpacity>
            );
        }
        else {
            return(
                <TouchableOpacity style={{ position: "absolute", backgroundColor: 'rgba(100,100,100,0.4)', height: 50, width: this.state.alphaBarWidth, justifyContent: 'center', alignItems: 'center'}}
                                  onPress={() => this.setState({buildNotesOpened: true})}>
                  <Text style={{color: '#ccc', fontSize: 22}}>
                      Memory Lane Games Alpha || 
                      Version: <Text style={{color: '#fff'}}>0.2.0</Text> || 
                      Build: <Text style={{color: '#fff'}}>1</Text>
                  </Text>
                </TouchableOpacity>
            );
        }
    }

    uiLoadingText() {
        if (this.state.fontLoaded == true) {
            return(
                <Text style={{fontFamily: 'Futura', color: '#fff', fontSize: screenHeight * 0.03, textAlign: 'center'}}>
                    Performing initial Memory Lane Games setup...
                </Text>
            );
        }
        else {

        }
    }

    // Conditional render function to display placeholder while fonts and other assets get loaded in
    render() {
        if (this.state.assetsLoaded == false) {
            return (
                <AnimatedBlurView style={{
                    height: screenHeight,
                    width: screenWidth,
                    backgroundColor: '#000000',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    paddingVertical: screenHeight * 0.3
                }} intensity={this.state.loadingScreenIntensity} tint='dark'>
                    <Image style={{height: screenHeight * 0.15, width: screenHeight * 0.15}}
                           source={require('./assets/icons/png/Memory-Lane-Games.png')} />
                    {this.uiLoadingText()}
                </AnimatedBlurView>
            );
        }

        // Returns the App Router for navigation through the app - defaults to display the first screen
        // In the app router (login Screen)
        return (
            <View style={{flex: 1}}>
                <AppRouter />
                {this.uiBuildInfoOverlay()}
                <StatusBar hidden={true} />
            </View>
        );

    }

}

export class HomeScreenTabBar extends React.Component {

    render() {
        return(
            <View />
        );
    }

}

const AnimatedBlurView = Animated.createAnimatedComponent(BlurView);
