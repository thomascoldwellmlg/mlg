//Import React Components
import React from 'react';
import {View, 
        Text, 
        Image, 
        TextInput, 
        TouchableOpacity, 
        Alert, 
        StyleSheet, 
        Dimensions, 
        KeyboardAvoidingView,
        Animated,
        Platform,
        Easing,
        ActivityIndicator} from 'react-native';
import { Button } from '../../reusable/rui';
import Expo from 'expo';
import {Permissions, BlurView} from 'expo';
import * as firebase from 'firebase';
import styles from './LoginScreenStyle';
import gstyles from '../../reusable/GlobalStyles';
import { UserData,
         UserId,
         UserEmail,
         Append } from '../../../lib/Firebase';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CardFlip from 'react-native-card-flip';

var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

// Ensure style sheet is given as portrait
if (screenHeight < screenWidth) {
    var temp = screenHeight;
    screenHeight = screenWidth;
    screenWidth = temp;
}

const animationImages = [
    {
        image: require('../../../assets/images/animations/questionImage1.jpg'),
        question: require('../../../assets/images/animations/completeQuestion1.png')
    },
    {
        image: require('../../../assets/images/animations/questionImage2.jpg'),
        question: require('../../../assets/images/animations/completeQuestion2.png')
    },
    {
        image: require('../../../assets/images/animations/questionImage3.jpg'),
        question: require('../../../assets/images/animations/completeQuestion3.png')
    }
]

export default class LoginScreen extends React.Component {

    state = {
        username: '',
        password: '',
        renderState: 'splash',
        showTutorial: true,
        animationCard: {
            marginLeft: new Animated.Value(0),
            opacity: new Animated.Value(0)
        },
        animationCaption: 'Turning your photos...',
        animationImageIndex: 0,
        buffering: false
    }

    async componentDidMount() {
        // Lock the screen orientation to portrait mode
        await Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);
        // Fetch the saved user login credentials from Secure Store
        let userName =  await Expo.SecureStore.getItemAsync('userName');
        let userPassword =  await Expo.SecureStore.getItemAsync('userPassword');
        await this.setState({username: userName});
        await this.setState({password: userPassword});
        if (this.state.username != null) {
        // Try to automatically log the user in with these saved credentials
        //await this.autoLogin();
        }
        else {
        // Else the user is registering for the first time - do nothing and let them select
        // if they want to login to an existing account or register for one.
        }
    }

    async autoLogin() {
        // If the user credentials don't exist then login / register else auto-login
        if (this.state.username.includes('@')) {
            await firebase.auth().signInWithEmailAndPassword(this.state.username, this.state.password).catch(function(error) {
                // Catch any errors here from the login
            });
            // Handle any errors here if not then navigate to the builder home screen
            // Sets an async value for the tab nav bar so activity and profile screens cannot be accessed
            await Expo.SecureStore.setItemAsync('demo-mode', 'false');
            this.props.navigation.navigate('CreatorHomeScreen');
        }
    }

    async loginExistingUser() {
        var errorCode = '';
        var errorMessage = '';
        // Attempt to log them in and set the buffering screen to render
        this.setState({buffering: true});
        await firebase.auth().signInWithEmailAndPassword(this.state.username, this.state.password).catch(function(error) {
            // Catch errors here
            errorCode = error.code;
            errorMessage = error.message;
        });
        var userData = await UserData();
        // Handle errors here
        if (errorCode === 'auth/wrong-password') {
            Alert.alert("Incorrect Password!");
            this.setState({buffering: false});
        }
        else if (errorCode === 'auth/invalid-email'){
            Alert.alert("Invalid Email Address");
            this.setState({buffering: false});
        }
        else if (errorCode === 'auth/user-not-found') {
            Alert.alert("Please create an account first!");
            this.setState({buffering: false});
        }
        // In case the app crashes or some other terminating event (e.g. no wifi) causes the account to be
        // created and able to sign into but none of the req fileds have been filled - direct them to do so
        else if (userData.familyState == 'none' || userData.name == '' || userData.dateOfBirth == '') {
            this.props.navigation.navigate('AccountCreationScreen');
            // Secure store the username and password for auto - login later - called on component did mount
            Expo.SecureStore.setItemAsync('userName', this.state.username);
            Expo.SecureStore.setItemAsync('userPassword', this.state.password);
        }
        else {
            // If there is no errors thrown then take them to the home screen
            var userData = await UserData();
            if (userData.creatorMode == true) {
                // Sets an async value for the tab nav bar so activity and profile screens cannot be accessed
                await Expo.SecureStore.setItemAsync('demo-mode', 'false');
                this.props.navigation.navigate('CreatedGamesScreen');
            }
            else {
                this.props.navigation.navigate('PlayerHomeScreen');
            }
            // Secure store the username and password for auto - login later - called on component did mount
            Expo.SecureStore.setItemAsync('userName', this.state.username);
            Expo.SecureStore.setItemAsync('userPassword', this.state.password);
        }

    }

    async registerNewUser() {
        var errorCode = '';
        var errorMessage = '';
        // Attempt to create a user account and set the buffer screen to render
        this.setState({buffering: true});
        await firebase.auth().createUserWithEmailAndPassword(this.state.username, this.state.password).catch(function(error) {
            // Catch any errors that are thrown
            errorCode = error.code;
            errorMessage = error.message;
        });
        // Handle errors here and present the user with appropriate alert.
        if (errorCode == 'auth/email-already-in-use') {
            Alert.alert('This email address is already in use. Please login to your existing account or create a new one.');
            this.setState({buffering: false});
        }
        else if (errorCode == 'auth/invalid-email') {
            Alert.alert('This email address is invalid. Please check it is spelt correctly and try again.');
            this.setState({buffering: false});
        }
        else if (errorCode == 'auth/operation-not-allowed') {
            Alert.alert('Oh no! It looks like you found a glitch in the matrix!');
            this.setState({buffering: false});
        }
        else if (errorCode == 'auth/weak-password') {
            Alert.alert('Please increase the length of your password. Minimum of 6 characters long.');
            this.setState({buffering: false});
        }
        else {
            // If there is no errors...
            // Create the user's firebase directory
            const userId = await UserId();
            const userEmail = await UserEmail();
            const path = 'users/' + userId;
            const data = {
                name: '',
                email: userEmail,
                familyId: '',
                familyState: 'none'
            }
            await Append(data, path);
            // Once a user creates an account navigate them to the settings screen to fill in
            // critical information
            // Sets an async value for the tab nav bar so activity and profile screens cannot be accessed
            await Expo.SecureStore.setItemAsync('demo-mode', 'false');
            this.props.navigation.navigate('AccountCreationScreen');
            // Secure store the username and password for auto - login later - called on component did mount
            Expo.SecureStore.setItemAsync('userName', this.state.username);
            Expo.SecureStore.setItemAsync('userPassword', this.state.password);
        }
    }

    forgotPassword() {
        this.setState({renderState: 'forgotPassword'});
    }

    async resetPassword() {
        if (this.state.username != '') {
            await firebase.auth().sendPasswordResetEmail(this.state.username).then(function() {
                Alert.alert(
                    'Password reset email successfully sent!', 
                    'Check your email address and then attempt to log in with your new password.');
            }).catch(function(error) {
                if (error.code == 'auth/invalid-email') {
                    Alert.alert('Please enter a valid email address!');
                }
                else {
                    this.setState({forgotPassword: false});
                }
            });
        }
        else {
            Alert.alert('Please enter an email address for your password reset to be sent to!');
        }
    }

    uiSplash() {
        return(
            <View style={styles.container}>
                <Image style={[gstyles.logoPortrait, styles.logo]}
                       source={require('../../../assets/images/Memory-Lane-Games.png')}/>
                <Text style={[gstyles.largeText, {textAlign: 'center'}]}>Welcome to Memory Lane Games!</Text>
                <TouchableOpacity style={[styles.button, styles.splashScreenButton1]}
                                  onPress={() => this.setState({renderState: 'registering'})}>
                    <Text style={[gstyles.mediumText, {textAlign: 'center'}]}>I am a new user!</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.button, styles.splashScreenButton2]}
                                  onPress={() => this.setState({renderState: 'login'})}>
                    <Text style={[gstyles.mediumText, {textAlign: 'center'}]}>I already have an account!</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.navBackButton}
                                  onPress={() => this.props.navigation.navigate('SplashScreen')}>
                    <Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/arrow-left.png')} />
                </TouchableOpacity>
            </View>
        );
    }

    uiLogin() {
        return(
            <KeyboardAwareScrollView>
                    <View style={styles.container}>
                    <Image style={styles.logo}
                           source={require('../../../assets/images/Memory-Lane-Games.png')} />
                    <Text style={[gstyles.largeText, {textAlign: 'center'}]}>Turning memories into games!</Text>
                    <TextInput style={styles.textInput}
                               placeholder='Enter your email address'
                               value={this.state.username}
                               autoCapatalize='none'
                               onChangeText={(username) => this.setState({username})}/>
                    <TextInput style={styles.textInput}
                               placeholder='Enter your password'
                               value={this.state.password}
                               autoCapatalize='none'
                               secureTextEntry={true}
                               onChangeText={(password) => this.setState({password})}/>
                    <TouchableOpacity style={[styles.button, styles.loginScreenButton1]}
                                      onPress={() => this.loginExistingUser()} >
                        <Text style={gstyles.mediumText}>Login</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.button, styles.loginScreenButton2]}
                                      onPress={() => this.forgotPassword()}>
                        <Text style={gstyles.smallText}>Forgot Password?</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({renderState: 'registering'})}>
                        <Text style={[gstyles.smallText, {textAlign: 'center'}]}>
                            <Text>Don't have an account yet? </Text>
                            <Text style={gstyles.bold}>Sign up here</Text>
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.navBackButton}
                                      onPress={() => this.setState({renderState: 'splash'})}>
                        <Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/arrow-left.png')} />
                    </TouchableOpacity>
                </View>
            </KeyboardAwareScrollView>
        );
    }

    uiRegistering() {
        return(
            <KeyboardAwareScrollView>
                <View style={styles.container}>
                    <Image style={styles.logo}
                           source={require('../../../assets/images/Memory-Lane-Games.png')} />
                    <Text style={[gstyles.largeText, {textAlign: 'center'}]}>Register your free account!</Text>
                    <TextInput style={styles.textInput}
                               placeholder='Enter your email address'
                               value={this.state.username}
                               onChangeText={(username) => this.setState({username})}/>
                    <TextInput style={styles.textInput}
                               placeholder='Enter a password at least 6 characters long!'
                               value={this.state.password}
                               onChangeText={(password) => this.setState({password})}/>
                    <TouchableOpacity style={[styles.button, styles.registerScreenButton1]}
                                      onPress={() => this.registerNewUser()} >
                        <Text style={gstyles.mediumText}>Register</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({renderState: 'login'})}>
                        <Text style={gstyles.smallText}>
                            <Text>Already have an account? </Text>
                            <Text style={gstyles.bold}>Login here</Text>
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.navBackButton}
                                      onPress={() => this.setState({renderState: 'splash'})}>
                        <Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/arrow-left.png')} />
                    </TouchableOpacity>
                </View>
            </KeyboardAwareScrollView> 
        );
    }

    uiForgotPassword() {
        return(
            <KeyboardAwareScrollView>
                <View style={styles.container}>
                    <Image style={styles.logo}
                           source={require('../../../assets/images/Memory-Lane-Games.png')} />
                    <Text style={gstyles.largeText}>Password Reset</Text>
                    <TextInput style={styles.textInput}
                               placeholder='Enter your email address'
                               value={this.state.username}
                               onChangeText={(username) => this.setState({username})}/>
                    <TouchableOpacity style={styles.button}
                                      onPress={() => this.resetPassword()} >
                        <Text style={gstyles.mediumText}>Reset Password</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({renderState: 'login'})}>
                        <Text style={gstyles.smallText}>
                            <Text style={gstyles.bold}>Back to login?</Text>
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.navBackButton}
                                      onPress={() => this.setState({renderState: 'login'})}>
                        <Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/arrow-left.png')} />
                    </TouchableOpacity>
                </View>
            </KeyboardAwareScrollView>
        );
    }

    uiBufferBlur() {
        if (this.state.buffering == true) {
            return(
                <BlurView tint="dark" intensity={90} style={styles.bufferBlur}>
                    <Image style={styles.bufferLogo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
                    <Text style={[{textAlign: 'center'}, gstyles.mediumText]}>Connecting to Memory Lane Games...</Text>
                    <ActivityIndicator size="large" color='#3498db' />
                </BlurView>
            );
        }
    }

    render() {
        if (this.state.renderState == 'splash') {
            // Allows a user to initially select whether they want to register a new account
            // or sign into a pre-existing account
            return(<View>{this.uiSplash()}</View>);
        }
        if (this.state.renderState == 'login') {
            return(<View>{this.uiLogin()}{this.uiBufferBlur()}</View>);
        }
        if (this.state.renderState == 'registering') {
            return(<View>{this.uiRegistering()}{this.uiBufferBlur()}</View>);
        }
        if (this.state.renderState == 'forgotPassword') {
            return(<View>{this.uiForgotPassword()}</View>);
        }
    }

}


