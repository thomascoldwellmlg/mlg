import React from 'react';
import {StyleSheet, 
        Dimensions} from 'react-native';
import { color } from '../../reusable/common';

var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

// Ensure style sheet is given as portrait
if (screenHeight < screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const styles = StyleSheet.create({

	/*
		Contents:
			0. General
			1. Splash Screen
			2. Login Screen
			3. Registeration Screen
			4. Forgot Password Screen
	*/

	// 0. General
	container: {
		height: screenHeight,
		width: screenWidth,
		backgroundColor: color.darkBlue,
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingTop: '8%',
		paddingBottom: '2%'
	},
	keyboardScrollView: {
		height: screenHeight,
		width: screenWidth,
		justifyContent: 'space-around'
	},
	logo: {
		height: screenHeight * 0.3,
		width: screenHeight * 0.3,
		resizeMode: 'contain',
		shadowOffset: {  width: 2,  height: 5,  },
		shadowColor: 'black',
		shadowOpacity: 0.35
	},
	button: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.65,
		backgroundColor: color.green,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 20,
		shadowOffset: {  width: 2,  height: 2,  },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	textInput: {
		height: screenHeight * 0.06,
		width: screenWidth * 0.7,
		fontFamily: 'Futura',
		fontSize: screenHeight * 0.023,
		color: color.grey,
		backgroundColor: '#fff',
		borderRadius: screenWidth * 0.01,
		padding: screenHeight * 0.01,
		shadowOffset: {  width: 2,  height: 2,  },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	navBackButton: {
		position: 'absolute',
		top: 0,
		left: 0,
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		borderBottomRightRadius: screenHeight * 0.05,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'rgba(0, 0, 0, 0.4)'
	},
	navBackImage: {
		height: screenHeight * 0.05,
		width: screenHeight * 0.05
	},

	//1. Splash Screen
	splashScreenButton1: {
		backgroundColor: color.green
	},

	splashScreenButton2: {
		backgroundColor: color.yellow
	},
	tutorialBlur: {
		position: 'absolute',
		height: screenHeight,
		width: screenWidth,
		paddingVertical: screenHeight * 0.03,
		paddingHorizontal: screenWidth * 0.1,
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	tutorialLogo: {
		height: screenHeight * 0.2,
		width: screenHeight * 0.2
	},
	tutorialGIF: {
		height: screenHeight * 0.35,
		width: screenWidth * 0.9,
		alignItems: 'flex-start'
	},
	tutorialButtonRow: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.8,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	tutorialDemoButton: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.25,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: screenWidth * 0.01,
		backgroundColor: color.yellow
	},
	tutorialHowButton: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.25,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: screenWidth * 0.01,
		backgroundColor: color.darkBlue
	},
	tutorialContinueButton: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.25,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: screenWidth * 0.01,
		backgroundColor: color.green
	},
	animatedView: {
		height: screenHeight * 0.4,
		width: screenWidth * 0.63,
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	animatedCard: {
		height: screenHeight * 0.40,
		width: screenWidth * 0.63,
		justifyContent: 'center',
		alignItems: 'center'
	},
	animatedImage1: {
		marginTop: screenHeight * 0.05,
		height: screenHeight * 0.35,
		width: screenWidth * 0.63,
		resizeMode: 'contain'
	},
	animatedImage2: {
		marginTop: screenHeight * 0.05,
		height: screenHeight * 0.32,
		width: screenWidth * 0.63,
		resizeMode: 'contain'
	},

	//2. Login Screen
	loginScreenButton1: {
		backgroundColor: color.green,
		height: screenHeight * 0.1
	},

	loginScreenButton2: {
		backgroundColor: color.yellow,
		height: screenHeight * 0.08,
		width: screenWidth * 0.42
	},

	//3. Registration Screen
	registerScreenButton1: {
		backgroundColor: color.green,
		height: screenHeight * 0.1
	},

	//4. Forgot Password

	//5. Buffer Screen
	bufferBlur: {
		position: 'absolute',
		height: screenHeight,
		width: screenWidth,
		paddingVertical: screenHeight * 0.2,
		paddingHorizontal: screenWidth * 0.1,
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	bufferLogo: {
		height: screenHeight * 0.15,
		width: screenHeight * 0.15
	}

});

export default styles;