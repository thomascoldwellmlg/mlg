import React from 'react';
import { View, 
		 Text,
		 Image,
		 TouchableOpacity,
		 ScrollView,
		 FlatList,
		 RefreshControl,
		 TextInput } from 'react-native';
import gstyles from '../../reusable/GlobalStyles';
import { color } from '../../reusable/common';
import styles from './MessagingChatScreenStyles';
import * as firebase from 'firebase';
import { objectToArray, getNeatTime, getNeatShortDate } from '../../../lib/Custom';
import { UserId,
		 MemberData,
		 Append } from '../../../lib/Firebase';
import { UploadAudio } from '../../../lib/AWS';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import VoiceRecorder from '../../reusable/VoiceRecorder/VoiceRecorder';
import { Audio } from 'expo';
import { Image as CacheableImage} from 'react-native-aws-cache';

export default class MessagingChatScreen extends React.Component {

	state = {
		text: '',
		isRefreshing: false,
		messageData: [],
		contact: '',
		contactData: {},
		typedMessage: '',
		recordingUri: '',
		enableVoiceRecorder: false,
		voiceRecorderStage: 'start'
	}

	async componentDidMount() {
		var contact = this.props.navigation.state.params.contactUid;
		await this.setState({contact: contact});
		this.getContactData();
		this.getNewMessages();
	}

	async getContactData() {
		var contactData = await MemberData(this.state.contact);
		this.setState({contactData: contactData});
	}

	async getNewMessages() {
		var userId = await UserId();
		// Firebase logic to get the messages from the contact
		await firebase.database().ref('users/' + userId + '/messages/' + this.state.contact).on('value', async (snapshot) => {
			var receivedMessages = snapshot.val();
			// Firebase logic to get the messages from the current user to the contact
			var sentMessages;
			await firebase.database().ref('users/' + this.state.contact + '/messages/' + userId).once('value').then((snapshot2) => {
				sentMessages = snapshot2.val();
			});
			console.log(sentMessages)
			// Now we have the latest messages from both recipient and sender
			// for each set of messages, add an object to each message to show
			// whether its from the recipient or sender - 'fromSender true/false'
			if (receivedMessages != null) {
				for (var r = 0; r <= Object.keys(receivedMessages).length - 1; r++) {
					receivedMessages[Object.keys(receivedMessages)[r]].fromSender = false;
				}
			}
			if (sentMessages != null) {
				for (var s = 0; s <= Object.keys(sentMessages).length - 1; s++) {
					sentMessages[Object.keys(sentMessages)[s]].fromSender = true;
				}
			}
			// Now they all have that flag we can merge the 2 message arrays
			if (receivedMessages != null && sentMessages != null) {
				var allMessages = Object.assign(receivedMessages, sentMessages);
				console.log(allMessages);
				var formatMessages = objectToArray(allMessages);
			}
			else if (receivedMessages != null) {
				var allMessages = receivedMessages;
				var formatMessages = objectToArray(allMessages);
			}
			else if (sentMessages != null) {
				var allMessages = sentMessages;
				var formatMessages = objectToArray(allMessages);
			}
			else {
				var formatMessages = [];
			}
			this.setState({messageData: formatMessages});
		});
		console.log('Message Data ==>>> ', this.state.messageData)
	}

	async refreshFeed() {
		this.setState({isRefreshing: true});
		await this.getNewMessages();
		this.setState({isRefreshing: false});
	}

	getMessageSenderName(item) {
		if (item.fromSender == true) {
			return('You');
		}
		else {
			return(this.state.contactData.name);
		}
	}

	getAdditionalQuestionText(item) {
		if (item.fromSender == false) {
			return(this.state.contactData.name + ' replied to your question!');
		}
		else {
			return('You replied to a question!');
		}
	}

	async sendTextMessage() {
		if (this.state.typedMessage != '') {
			const messageType = 'text';
			var time = await getNeatTime();
			var date = await getNeatShortDate();
			const messageId = Date.now();
			const sentBy = await UserId();
			const firebaseData = {
				messageType: messageType,
				data: this.state.typedMessage,
				sentTime: time,
				sentDate: date,
				sentBy: sentBy
			};
			const firebasePath = 'users/' + this.state.contact + '/messages/' + sentBy + '/' + messageId;
			await Append(firebaseData, firebasePath);
			this.setState({typedMessage: ''});
			this.getNewMessages();
		}
	}

	async uploadAndSendVoiceMessage() {
		var time = await getNeatTime();
		var date = await getNeatShortDate();
		const messageId = Date.now();
		const sentBy = await UserId();
		const messageType = 'voice-message';
		// Perform AWS upload
		const AWSPath = 'users/' + sentBy + '/messages/' + messageId + '/';
		const name = 'voiceMessage';
		var audioUrl = await UploadAudio(this.state.recordingUri, name, AWSPath);
		// Push reply as message to Firebase
		const firebaseData = {
			messageType: messageType,
			data: [audioUrl],
			sentTime: time,
			sentDate: date,
			sentBy: sentBy
		};
		const firebasePath = 'users/' + this.state.contact + '/messages/' + sentBy + '/' + messageId;
		await Append(firebaseData, firebasePath);
	}

	async playAudio(uri) {
		const audioObject = new Audio.Sound();
		try {
			await audioObject.loadAsync({uri: uri});
			await audioObject.playAsync();
		} catch(error) {
			Alert.alert('Sorry it appears we are unable to play your hint right now!, Please check your network connection!');
		}
	}

	uiTitleBar() {
		return(
			<View style={styles.titleBar}>
				<TouchableOpacity style={gstyles.shadow}
							      onPress={() => this.props.navigation.navigate('MessagingHomeScreen')}>
					<Image style={styles.titleBarIcon} source={require('../../../assets/icons/png/chevron-left.png')} />
				</TouchableOpacity>
				<Text style={gstyles.largeText}>{this.state.contactData.name}</Text>
				<TouchableOpacity style={styles.titleBarIconButton}
								  onPress={() => this.props.navigation.navigate('MessagingHomeScreen')}>
				    <View style={gstyles.shadow}>
						<CacheableImage source={{uri: this.state.contactData.profilePicture}}
							   style={gstyles.profilePicturePortrait} />
				    </View>
			    </TouchableOpacity>
			</View>
		);
	}

	uiMessage(item) {
		if (item.messageType == 'voice-message') {
			return(
				<View style={[gstyles.cardRow, this.messageCardStyle(item.fromSender), styles.voiceMessageCard]}>
					<View style={styles.voiceMessageText}>
						<Text style={gstyles.smallText}>
							{this.getMessageSenderName(item)} sent a voice message!
						</Text>
					</View>
					<TouchableOpacity style={[styles.voiceMessagePlayButton, gstyles.shadow]}
									  onPress={() => this.playAudio(item.data[0])}>
						<Text style={gstyles.smallText}>Play</Text>
					</TouchableOpacity>
				</View>
			);
		}
		else if (item.messageType == 'additional-question-reply') {
			return(
				<View style={[gstyles.cardRow, this.messageCardStyle(item.fromSender), styles.questionReplyCard]}>
					<View style={styles.questionReplyText}>
						<Text style={gstyles.smallText}>
							{this.getAdditionalQuestionText(item)}
						</Text>
					</View>
					<TouchableOpacity style={[styles.questionPlayButton, gstyles.shadow]}
									  onPress={() => this.playAudio(item.data[0])}>
						<Text style={gstyles.smallText}>Question</Text>
					</TouchableOpacity>
					<TouchableOpacity style={[styles.answerPlayButton, gstyles.shadow]}
									  onPress={() => this.playAudio(item.data[1])}>
						<Text style={gstyles.smallText}>Answer</Text>
					</TouchableOpacity>
				</View>
			);
		}
		else {
			return(
				<View style={[gstyles.cardRow, this.messageCardStyle(item.fromSender), styles.textMessageCard]}>
					<View style={styles.textMessageText}>
						<Text style={gstyles.smallText}>
							{item.data}
						</Text>
					</View>
				</View>
			);
		}
	}

	messageCardStyle(sender) {
		if (sender == true) {
			return({backgroundColor: color.cardBlue, alignSelf: 'flex-end'});
		}
		else {
			return({backgroundColor: color.cardBlue, alignSelf: 'flex-start'});
		}
	}

	uiMessagesBody() {
		return(
			<ScrollView style={styles.scrollView}
			            ref={ref => this.scrollView = ref}
					    onContentSizeChange={(contentWidth, contentHeight)=>{        
					        this.scrollView.scrollToEnd({animated: true});
					    }}>
				<FlatList data={this.state.messageData}
						  style={styles.flatList}
						  extraData={this.state}
					      keyExtractor={(item, index) => index.toString()}
                          renderItem={({item}) => <View style={styles.messageRow}>
                          						      {this.uiMessage(item)}
                      							  </View>}/>
			</ScrollView>
		);
	}

	uiUserInput() {
		return(
			<View style={[styles.userInputContainer, gstyles.shadow]}>
				<TouchableOpacity style={[styles.recordButton, gstyles.shadow]}
								  onPress={() => this.setState({enableVoiceRecorder: true})}>
					<Image style={gstyles.smallIcon} source={require('../../../assets/icons/png/voice.png')}/>
				</TouchableOpacity>
				<TextInput style={styles.messageInput}
						   multiline={true}
                           placeholder='Type a new message...'
                           value={this.state.typedMessage}
                           onChangeText={(typedMessage) => this.setState({typedMessage})}/>
                <TouchableOpacity style={[styles.sendButton, gstyles.shadow]}
                				  onPress={() => this.sendTextMessage()}>
                	<Image style={gstyles.smallIcon} source={require('../../../assets/icons/png/send.png')} />
				</TouchableOpacity>
			</View>
		);
	}

	uiVoiceRecorder() {
		if (this.state.enableVoiceRecorder == true) {
			return(<VoiceRecorder parent={this} recordingType={'voice-message'} stagePreset={this.state.voiceRecorderStage} />);
		}
		else {
			return(null);
		}
	}

	render() {
		return(
			<View style={styles.container}>
				<KeyboardAwareScrollView scrollEnabled={false} extraHeight={5}>
					{this.uiTitleBar()}
					{this.uiMessagesBody()}
					{this.uiUserInput()}
					{this.uiVoiceRecorder()}
				</KeyboardAwareScrollView>
			</View>
		);
	}
	
}