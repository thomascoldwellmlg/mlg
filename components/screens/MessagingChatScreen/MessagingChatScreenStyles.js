import { StyleSheet, Dimensions} from 'react-native';
import { color } from '../../reusable/common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

// Ensure style sheet is given as portrait
if (screenHeight < screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const styles = StyleSheet.create({

	/*
		Contents:
			0. Title Bar
			1. Messages
	*/

	// 0. Title Bar
	titleBar: {
		height: screenHeight * 0.15,
		width: screenWidth,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		backgroundColor: color.darkBlue,
		paddingLeft: screenWidth * 0.05,
		paddingRight: screenWidth * 0.05,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	logo: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		resizeMode: 'contain'
	},
	titleBarIconButton: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	titleBarIcon: {
		height: screenHeight * 0.08,
		width: screenHeight * 0.08,
		resizeMode: 'contain',
		tintColor: '#fff'
	},

	// 1. Body
	container: {
		height: screenHeight,
		width: screenWidth,
		backgroundColor: color.lightBlue,
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	cardRowText: {
		marginLeft: screenWidth * 0.05,
		width: screenWidth * 0.47,
	},
	scrollView: {
		paddingTop: screenHeight * 0.03,
		height: screenHeight * 0.7,
		width: screenWidth,
		paddingLeft: screenWidth * 0.05,
		paddingRight: screenWidth * 0.05
	},
	flatList: {
		paddingBottom: screenHeight * 0.05
	},
	voiceMessageCard: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.75,
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	voiceMessageText: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.3,
		justifyContent: 'center'
	},
	voiceMessagePlayButton: {
		height: screenHeight * 0.08,
		width: screenWidth * 0.2,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: screenWidth * 0.01,
		backgroundColor: color.yellow
	},
	questionReplyCard: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.75,
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	questionReplyText: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.25,
		justifyContent: 'center'
	},
	questionPlayButton: {
		height: screenHeight * 0.08,
		width: screenWidth * 0.15,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: screenWidth * 0.01,
		backgroundColor: color.yellow
	},
	answerPlayButton: {
		height: screenHeight * 0.08,
		width: screenWidth * 0.15,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: screenWidth * 0.01,
		backgroundColor: color.green
	},
	textMessageCard: {
		height: undefined,
		width: screenWidth * 0.75,
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingTop: screenHeight * 0.02,
		paddingBottom: screenHeight * 0.02
	},
	textMessageText: {
		width: screenWidth * 0.75,
		flex: 1,
		flexWrap: 'wrap'
	},

	userInputContainer: {
		height: screenHeight * 0.15,
		width: screenWidth,
		backgroundColor: color.darkBlue,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	recordButton: {
		height: screenHeight * 0.08,
		width: screenHeight * 0.12,
		borderRadius: screenHeight * 0.01,
		backgroundColor: color.red,
		justifyContent: 'center',
		alignItems: 'center'
	},
	messageInput: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.5,
		borderRadius: screenWidth * 0.01,
		padding: screenWidth * 0.03,
		backgroundColor: '#fff',
		color: '#555',
		fontSize: screenHeight * 0.023,
		fontFamily: 'Futura'
	},
	sendButton: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		borderRadius: screenHeight * 0.05,
		backgroundColor: color.green,
		justifyContent: 'center',
		alignItems: 'center'
	},

});

export default styles;