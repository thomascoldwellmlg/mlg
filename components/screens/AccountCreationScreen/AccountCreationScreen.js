import React from 'react';
import { View,
		 Text,
		 TextInput,
		 Image,
		 Alert,
		 TouchableOpacity,
		 FlatList } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import gstyles from '../../reusable/GlobalStyles';
import styles from './AccountCreationScreenStyles';
import { UserId,
		 Append,
		 MemberData,
		 FetchFamilyIds,
		 ExternalFamilyData } from '../../../lib/Firebase';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BlurView } from 'expo';

export default class AccountCreationScreen extends React.Component {

	state = {
		stage: 'name',
		name: '',
		familyId: '',
		creatorMode: true,
		familyIds: []
	}

	async componentDidMount() {
		var familyIds = await FetchFamilyIds();
		await this.setState({familyIds: familyIds});
	}

	async addName() {
		const currentUser = await UserId();
		const path = 'users/' + currentUser + '/'
		const data = {name: this.state.name}
		await Append(data, path);
		this.setState({stage: 'family-group-select'});
	}

	async createFamilyGroup() {
		if (this.state.familyId != '') {
			var exists = this.doesFamilyIdExist();
			if (exists == true) {
				Alert.alert('This family group ID already exists! If you are sure this is your group go to "join" or enter a different, unique ID.');
			}
			else {
				// Create the family group
				// Get current userId and create family group
				const currentUser = await UserId();
				const familyPath = 'families/' + this.state.familyId;
				const data = {
					familyAdmin: currentUser,
					members: { [currentUser]: 0 }
				}
				await Append(data, familyPath);
				// Then update users db to reflect changes
				const userPath = 'users/' + currentUser;
				await Append({familyState: 'in-group', familyId: this.state.familyId}, userPath);
				// Continue to the completion stage which confirms the creation of the family group
				this.setState({stage: 'family-group-create-confirmation'});
			}
		}
		else {
			Alert.alert('Please enter a family ID');
		}
	}

	async deleteFamilyGroup() {
		// Get current userId and null member in family group
		const currentUser = await UserId();
		const familyPath = 'families/';
		const data = {[this.state.familyId]: null};
		await Append(data, familyPath);
		// Then update users db to reflect changes
		const userPath = 'users/' + currentUser;
		await Append({familyState: 'none', familyId: '', pendingFamilyId: ''}, userPath);
		this.setState({familyId: ''});
		this.setState({stage: 'family-group-select'});
	}

	async joinFamilyGroup() {
		if (this.state.familyId) {
			var exists = this.doesFamilyIdExist();
			if (exists == true) {
				// Send a join request to family group
				// Set current user as pending member in target family group
				const currentUser = await UserId();
				const familyPath = 'families/' + this.state.familyId + '/pendingMembers/';
				await Append({[currentUser]: 0}, familyPath);
				// Then set the family state on the current users db, also set familyId to 'pending'
				// giving access to a placeholder family group till approval
				const userPath = 'users/' + currentUser;
				await Append({familyState: 'pending-approval', familyId: 'Pending', pendingFamilyId: this.state.familyId}, userPath);
				// Continue to the completion stage which confirms the joining of the family group
				this.setState({stage: 'family-group-join-confirmation'});
			}
			else {
				Alert.alert('This group does not exist! Check you entered the correct ID or create a family group if you do not have one yet.');
			}
		}
		else {
			Alert.alert('Please enter a family ID');
		}
	}

	async leaveFamilyGroup() {
		// Set current user to null as pending member in target family group removing from db
		const currentUser = await UserId();
		const familyPath = 'families/' + this.state.familyId + '/pendingMembers/';
		await Append({[currentUser]: null}, familyPath);
		// Then set the family state on the current users db, also set familyId to 'pending'
		// giving access to a placeholder family group till approval
		const userPath = 'users/' + currentUser;
		await Append({familyState: 'none', familyId: '', pendingFamilyId: ''}, userPath);
		// Finally update the UI to reflect the changes
		this.setState({familyId: ''});
		this.setState({stage: 'family-group-select'});
	}

	doesFamilyIdExist() {
		var exists = false;
		var familyIds = this.state.familyIds;
		for (var i = 0; i <= familyIds.length - 1; i++) {
			if (familyIds[i] == this.state.familyId) {
				exists = true;
			}
		}
		return(exists);
	}

	async setAccountType(type) {
		var creatorMode = false;
		if (type == 'creator') {
			creatorMode = true;
		}
		const currentUser = await UserId();
		const path = 'users/' + currentUser + '/'
		const data = {creatorMode: creatorMode}
		await Append(data, path);
		// Add a placeholder profile picture
		const profilePicture = {profilePicture: 'https://thumbs.dreamstime.com/b/vector-irregular-polygon-background-triangle-pattern-sky-blue-sand-orange-ice-white-color-vector-abstract-irregular-106505522.jpg'}
		await Append(profilePicture, path);
		if (type == 'creator') {
			this.props.navigation.navigate('CreatedGamesScreen');
		}
		else {
			this.props.navigation.navigate('PlayerGamesScreen');
		}
	}

	uiEnterName() {
		if (this.state.stage == 'name') {
			return(
				<KeyboardAwareScrollView extraHeight={150}>
					<View style={styles.body}>
						<View style={gstyles.shadow}>
							<Image style={styles.logo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
						</View>
						<View style={styles.enterNameBodyText}>
							<Text style={[gstyles.largeText, {textAlign: 'center'}]}>
								Nearly there!
							</Text>
							<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>
								{"\n"}{"\n"}Let us know your full name so family and friends know who you are!
							</Text>
						</View>
						<TouchableOpacity style={styles.nameTextInputContainer}
		        					      onPress={() => this.refs.nameInput.focus()}>
		        			<Text style={gstyles.subTitle}>My name is...</Text>
		        			<TextInput style={styles.nameTextInput}
		        					   ref='nameInput'
		        					   placeholder='Enter your full name'
		        					   value={this.state.name}
		        					   returnKeyType='next'
		        					   onSubmitEditing={() => { 
		        					   	if(this.state.name != '') {
		        					   		this.addName();
		        					   }
		        					   else {
		        					   	Alert.alert('Please let us know your name before continuing!');
		        					   }}}
		        					   onChangeText={(name) => this.setState({name})} />
		        		</TouchableOpacity>
		        		{this.uiContinueButton()}
					</View>
				</KeyboardAwareScrollView>
			);
		}
	}

	uiFamilyGroup() {
		if (this.state.stage == 'family-group-select') {
			return(
				<KeyboardAwareScrollView extraHeight={150}>
					<View style={styles.body}>
						<View style={gstyles.shadow}>
							<Image style={styles.logo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
						</View>
						<View style={styles.familyGroupBodyText}>
							<Text style={[gstyles.largeText, {textAlign: 'center'}]}>
								Find my family!
							</Text>
							<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>
								{"\n"}{"\n"}Memory lane uses family groups to allow you to share games
								and photos between family members. If your family hasn't created a group
								yet you can create one below (this will make you in charge of the family group)
								or enter your family group ID to join an existing family group!
							</Text>
						</View>
		        		<View style={styles.familyActionsContainer}>
							<View style={styles.familyActionButtons}>
								<TouchableOpacity style={styles.familyActionSelectCreateButton}
										          onPress={() => this.setState({stage: 'family-group-create'})}>
									<Text style={[gstyles.smallText, {textAlign: 'center'}]}>Create new family group</Text>
								</TouchableOpacity>
								<TouchableOpacity style={styles.familyActionSelectJoinButton}
										          onPress={() => this.setState({stage: 'family-group-join'})}>
									<Text style={[gstyles.smallText, {textAlign: 'center'}]}>Join existing family group</Text>
								</TouchableOpacity>
							</View>
						</View>
					</View>
				</KeyboardAwareScrollView>
			);
		}
		else if (this.state.stage == 'family-group-create') {
			return(
				<KeyboardAwareScrollView extraHeight={150}>
					<View style={styles.body}>
						<View style={gstyles.shadow}>
							<Image style={styles.logo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
						</View>
						<View style={styles.familyGroupBodyText}>
							<Text style={[gstyles.largeText, {textAlign: 'center'}]}>
								Create my family group!
							</Text>
							<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>
								{"\n"}{"\n"}Enter a unique family ID below e.g. 'Coldwell212'. Once you press
								create you will be made the family group admin and will be able to accept other
								members to this group when they enter this ID during their sign up!
							</Text>
						</View>
							<View style={styles.familyGroupEntry}>
								<TouchableOpacity style={styles.familyTextInputContainer}
				        					      onPress={() => this.refs.familyInput.focus()}>
				        			<Text style={gstyles.subTitle}>My family ID will be...</Text>
				        			<TextInput style={styles.familyTextInput}
				        					   ref='familyInput'
				        					   placeholder='Enter an ID for your family group...'
				        					   value={this.state.familyId}
				        					   onSubmitEditing={() => this.createFamilyGroup()}
				        					   returnKeyType='next'
				        					   onChangeText={(familyId) => this.setState({familyId})} />
				        		</TouchableOpacity>
								<TouchableOpacity style={styles.familyActionCreateButton}
										          onPress={() => this.createFamilyGroup()}>
									<Text style={[gstyles.smallText, {textAlign: 'center'}]}>Create</Text>
								</TouchableOpacity>
							</View>
							<TouchableOpacity onPress={() => this.setState({stage: 'family-group-join'})}>
								<Text style={gstyles.subTitle}>Already have a family group?</Text>
							</TouchableOpacity>
						</View>
				</KeyboardAwareScrollView>
			);
		}
		else if (this.state.stage == 'family-group-join') {
			return(
				<KeyboardAwareScrollView extraHeight={150}>
					<View style={styles.body}>
						<View style={gstyles.shadow}>
							<Image style={styles.logo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
						</View>
						<View style={styles.familyGroupBodyText}>
							<Text style={[gstyles.largeText, {textAlign: 'center'}]}>
								Join a family group!
							</Text>
							<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>
								{"\n"}{"\n"}To join an existing family group simply enter the ID of the family
								group you want to join and press join to send a request! If you don't know your
								family ID ask your family admin and they will help you enter it! 
							</Text>
						</View>
						<View style={styles.familyGroupEntry}>
							<TouchableOpacity style={styles.familyTextInputContainer}
			        					      onPress={() => this.refs.familyInput.focus()}>
			        			<Text style={gstyles.subTitle}>My family ID is...</Text>
			        			<TextInput style={styles.familyTextInput}
			        					   ref='familyInput'
			        					   placeholder='Enter your existing family ID...'
			        					   value={this.state.familyId}
			        					   onSubmitEditing={() => this.joinFamilyGroup()}
			        					   returnKeyType='next'
			        					   onChangeText={(familyId) => this.setState({familyId})} />
			        		</TouchableOpacity>
							<TouchableOpacity style={styles.familyActionJoinButton}
									          onPress={() => this.joinFamilyGroup()}>
								<Text style={[gstyles.smallText, {textAlign: 'center'}]}>Join</Text>
							</TouchableOpacity>
						</View>
						<TouchableOpacity onPress={() => this.setState({stage: 'family-group-create'})}>
							<Text style={gstyles.subTitle}>Don't have a family group yet?</Text>
						</TouchableOpacity>
					</View>
				</KeyboardAwareScrollView>
			);
		}
		else if (this.state.stage == 'family-group-create-confirmation') {
			return(
				<View style={styles.confirmationBody}>
					<View style={gstyles.shadow}>
						<Image style={styles.logo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
					</View>
					<View style={[gstyles.shadow, styles.familyGroupTickCircle]}>
						<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/check.png')} />
					</View>
					<Text style={[gstyles.largeText, {textAlign: 'center'}]}>
						Created family group:
					</Text>
					<View style={[styles.familyIdHighlight, gstyles.shadow]}>
						<Text style={gstyles.mediumText}>
							{this.state.familyId}
						</Text>
					</View>
					<View style={styles.familyGroupBodyCompletionText}>
						<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>
							Congrats you have now successfully created your family group with the ID
							shown above - share this with your family so they can join your family group with it
							when they sign up!
						</Text>
					</View>
	        		{this.uiContinueButton()}
	        		<TouchableOpacity onPress={() => Alert.alert('Are you sure you want to delete this group and start again?',
	        													  null,
	        													  [{text: 'Confirm', onPress: () => this.deleteFamilyGroup()},
	        													   {text: 'Cancel'}])}>
						<Text style={gstyles.subTitle}>Made a mistake?</Text>
					</TouchableOpacity>
				</View>
			);
		}
		else if (this.state.stage == 'family-group-join-confirmation') {
			return(
				<View style={styles.confirmationBody}>
					<View style={gstyles.shadow}>
						<Image style={styles.logo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
					</View>
					<View style={[gstyles.shadow, styles.familyGroupTickCircle]}>
						<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/check.png')} />
					</View>
					
						<Text style={[gstyles.largeText, {textAlign: 'center'}]}>
							Request sent to join:
						</Text>
						<View style={[styles.familyIdHighlight, gstyles.shadow]}>
							<Text style={gstyles.mediumText}>
								{this.state.familyId}
							</Text>
						</View>
					<View style={styles.familyGroupBodyCompletionText}>
						<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>
							Congrats you have now sent a request to join a family group!
							The admin of this family group should now open the 'My Family Group' section 
							of their app to accept your request to join!
						</Text>
					</View>
	        		{this.uiContinueButton()}
	        		<TouchableOpacity onPress={() => Alert.alert('Are you sure you want to leave this group and start again?',
	        													  null,
	        													  [{text: 'Confirm', onPress: () => this.leaveFamilyGroup()},
	        													   {text: 'Cancel'}])}>
						<Text style={gstyles.subTitle}>Made a mistake?</Text>
					</TouchableOpacity>
				</View>
			);
		}
	}

	uiAccountType() {
		if (this.state.stage == 'account-select') {
			return(
				<KeyboardAwareScrollView extraHeight={150}>
					<View style={styles.accountSelectBody}>
						<View style={gstyles.shadow}>
							<Image style={styles.logo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
						</View>
						<View style={styles.accountTypeBodyText}>
							<Text style={[gstyles.largeText, {textAlign: 'center'}]}>
								{"\n"}Select your account type{"\n"}
							</Text>
							<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>
								Memory Lane has two specific account types:{"\n\n"}
								Player - this allows you to simply play the games and view the family album
								(this mode is recommended for those with dementia or the elderly){"\n\n"}
								Creator - this allows you to play the games, but also build them and add photos
								to the family album (this mode is recommended for the parents and kids to help build games through)
							</Text>
						</View>
						<View style={styles.accountTypeButtons}>
							<TouchableOpacity style={styles.accountTypeCreatorButton}
									          onPress={() => this.setAccountType('creator')}>
								<Text style={[gstyles.smallText, {textAlign: 'center'}]}>Creator account</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.accountTypePlayerButton}
									          onPress={() => this.setAccountType('player')}>
								<Text style={[gstyles.smallText, {textAlign: 'center'}]}>Player account</Text>
							</TouchableOpacity>
						</View>
					</View>
				</KeyboardAwareScrollView>
			);
		}
	}

	uiContinueButton() {
		if (this.state.stage == 'name') {
			if (this.state.name == '') {
				return(
					<View style={styles.inactiveContinueButton}>
						<Text style={gstyles.smallText}>Continue</Text>
					</View>
				);
			}
			else {
				return(
					<TouchableOpacity style={styles.activeContinueButton}
									  onPress={() => this.addName()}>
						<Text style={gstyles.smallText}>Continue</Text>
					</TouchableOpacity>
				);
			}
		}
		else if (this.state.stage == 'family-group-join-confirmation' || this.state.stage == 'family-group-create-confirmation') {
			return(
				<TouchableOpacity style={styles.activeContinueButton}
								  onPress={() => this.setState({stage: 'account-select'})}>
					<Text style={gstyles.smallText}>Continue</Text>
				</TouchableOpacity>
			);
		}
		else {

		}
	}
	
	render() {
		return(
			<View style={styles.container}>
				<BlurView style={styles.blur} intensity={0} tint='dark'>
					{this.uiEnterName()}
					{this.uiFamilyGroup()}
					{this.uiAccountType()}
				</BlurView>
			</View>
		);
	}

}