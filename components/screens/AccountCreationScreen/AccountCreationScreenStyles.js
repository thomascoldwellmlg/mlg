import { StyleSheet, Dimensions} from 'react-native';
import { color } from '../../reusable/common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

if (screenHeight < screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const styles = StyleSheet.create({

	/*
		Contents:
			0. Main
			1. Body
	*/

	// 0. Main
	container: {
		height: screenHeight,
		width: screenWidth,
		backgroundColor: color.darkBlue,
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	body: {
		height: screenHeight,
		width: screenWidth,
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingVertical: screenHeight * 0.1
	},
	confirmationBody: {
		height: screenHeight,
		width: screenWidth,
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingVertical: screenHeight * 0.05
	},
	accountSelectBody: {
		height: screenHeight,
		width: screenWidth,
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingVertical: screenHeight * 0.05
	},
	logo: {
		height: screenHeight * 0.15,
		width: screenHeight * 0.15
	},
	blur: {
		position: 'absolute',
		height: screenHeight,
		width: screenWidth,
		justifyContent: 'space-around',
		alignItems: 'center',
		backgroundColor: '#000000'
	},
	backgroundVideo: {
		height: screenHeight,
		width: screenWidth
	},

	// 1. Body
	nameTextInput: {
		fontFamily: 'Futura',
		fontSize: 20,
		color: '#fff',
		height: screenHeight * 0.06,
		width: screenWidth * 0.6,
		borderBottomWidth: 3,
		borderColor: color.lightBlue
	},
	nameTextInputContainer: {
		height: screenHeight * 0.07,
		width: screenWidth * 0.6,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginBottom: screenHeight * 0.01
	},
	enterNameBodyText: {
		height: screenHeight * 0.15,
		width: screenWidth * 0.7,
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	familyGroupBodyText: {
		height: screenHeight * 0.25,
		width: screenWidth * 0.7,
		justifyContent: 'center',
		alignItems: 'center'
	},
	activeContinueButton: {
		height: screenHeight * 0.1,
		width: screenWidth * 0.42,
		borderRadius: screenWidth * 0.01,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.green
	},
	inactiveContinueButton: {
		height: screenHeight * 0.1,
		width: screenWidth * 0.42,
		borderRadius: screenWidth * 0.01,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.grey
	},
	familyActionsContainer: {
		height: screenHeight * 0.25,
		width: screenWidth * 0.8,
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	familyActionButtons: {
		height: screenHeight * 0.25,
		width: screenWidth * 0.8,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	familyActionSelectJoinButton: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.3,
		borderRadius: screenWidth * 0.01,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.green,
		paddingHorizontal: screenWidth * 0.04
	},
	familyActionSelectCreateButton: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.3,
		borderRadius: screenWidth * 0.01,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.yellow,
		paddingHorizontal: screenWidth * 0.04
	},
	familyGroupEntry: {
		height: screenHeight * 0.15,
		width: screenWidth * 0.8,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	familyActionCreateButton: {
		height: screenHeight * 0.1,
		width: screenWidth * 0.2,
		borderRadius: screenWidth * 0.01,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.yellow,
		paddingHorizontal: screenWidth * 0.04
	},
	familyActionLeaveButton: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.3,
		borderRadius: screenWidth * 0.01,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.yellow,
		paddingHorizontal: screenWidth * 0.04
	},
	familyActionRemoveButton: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.3,
		borderRadius: screenWidth * 0.01,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.yellow,
		paddingHorizontal: screenWidth * 0.04
	},
	familyActionJoinButton: {
		height: screenHeight * 0.1,
		width: screenWidth * 0.2,
		borderRadius: screenWidth * 0.01,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.green,
		paddingHorizontal: screenWidth * 0.04
	},
	familyTextInput: {
		fontFamily: 'Futura',
		fontSize: 20,
		color: '#fff',
		height: screenHeight * 0.06,
		width: screenWidth * 0.5,
		borderBottomWidth: 3,
		borderColor: color.lightBlue
	},
	familyTextInputContainer: {
		height: screenHeight * 0.07,
		width: screenWidth * 0.5,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginBottom: screenHeight * 0.01
	},
	familyGroupTickCircle: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		borderRadius: screenHeight * 0.05,
		backgroundColor: color.green,
		justifyContent: 'center',
		alignItems: 'center'
	},
	familyIdHighlight: {
		height: screenHeight * 0.08,
		width: screenWidth * 0.5,
		borderRadius: screenWidth * 0.03,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.grey
	},
	familyGroupBodyCompletionText: {
		height: screenHeight * 0.15,
		width: screenWidth * 0.7,
		justifyContent: 'center',
		alignItems: 'center'
	},
	accountTypeBodyText: {
		height: screenHeight * 0.32,
		width: screenWidth * 0.7,
		justifyContent: 'center',
		alignItems: 'center'
	},
	accountTypeButtons: {
		height: screenHeight * 0.2,
		width: screenWidth * 0.8,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		marginTop: screenHeight * 0.03
	},
	accountTypePlayerButton: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.3,
		borderRadius: screenWidth * 0.01,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.green,
		paddingHorizontal: screenWidth * 0.04
	},
	accountTypeCreatorButton: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.3,
		borderRadius: screenWidth * 0.01,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.yellow,
		paddingHorizontal: screenWidth * 0.04
	},

	// Picker Styles
	pickerContainer: {
		height: screenHeight * 0.06,
		width: screenWidth * 0.25,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		borderColor: color.grey,
		borderBottomWidth: screenHeight * 0.003,
		alignSelf: 'center'
	},
	picker: {
		height: screenHeight * 0.06,
		width: screenWidth * 0.25,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	pickerText: {
		fontFamily: 'Futura',
		fontSize: screenHeight * 0.02,
		color: '#fff'
	},
	pickerChevron: {
		position: 'relative',
		height: screenHeight * 0.03,
		width: screenHeight * 0.03,
		tintColor: '#444'
	}

});

export default styles;