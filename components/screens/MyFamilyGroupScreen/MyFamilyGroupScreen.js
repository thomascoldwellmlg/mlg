import React from 'react';
import { Text,
		 View,
		 ScrollView,
		 FlatList,
		 Image,
		 TextInput,
		 Alert,
		 TouchableOpacity,
		 Picker } from 'react-native';
import styles from './MyFamilyGroupScreenStyles';
import gstyles from '../../reusable/GlobalStyles';
import { UserData,
		 FamilyData,
		 FamilyId,
		 UserId,
		 Append,
		 MemberData } from '../../../lib/Firebase';
import RNPickerSelect from 'react-native-picker-select';
import { Image as CacheableImage} from 'react-native-aws-cache';

const relationPickerOptions = [
	"Grandmother",
	"Grandfather",
	"Mother",
	"Father",
	"Brother",
	"Sister",
	"Husband",
	"Wife",
	"Partner",
	"Daughter",
	"Son",
	"Other"
];
 
export default class MyFamilyGroupScreen extends React.Component {

	static navigationOptions = {
		header: null
	}

	state = {
		familyId: '',
		familyMembers: [],
		pendingMembers: [],
		relationItems: [
			{label: 'Grandmother', value:'Grandmother'},
			{label: 'Grandfather', value:'Grandfather'},
			{label: 'Mother', value:'Mother'},
			{label: 'Father', value:'Father'},
			{label: 'Brother', value:'Brother'},
			{label: 'Sister', value:'Sister'},
			{label: 'Husband', value:'Husband'},
			{label: 'Wife', value:'Wife'},
			{label: 'Partner', value:'Partner'},
			{label: 'Daughter', value:'Daughter'},
			{label: 'Son', value:'Son'},
			{label: 'Other', value:'Other'},
		],
		loadedRelations: false
	}

	async componentDidMount() {
		await this.getFamilyGroupData();
	}

	async getFamilyGroupData() {
		// Get familyId and then subsequently get the familyData inc. member uids
		// then using member uids get their name and pp
		const familyData = await FamilyData();
		// Set the family Id for the UI
		const familyId = await FamilyId();
		this.setState({familyId: familyId});
		const userId = await UserId();
		var mainMembers = Object.keys(familyData.members);
		var currentUserIndex = mainMembers.indexOf(userId);
		mainMembers.splice(currentUserIndex, 1);
		var familyMembers = [];
		for (var i = 0; i <= mainMembers.length - 1; i++) {
			var memberData = await MemberData(mainMembers[i]);
			familyMembers.push({name: memberData.name, 
							    profilePicture: memberData.profilePicture,
							    uid: mainMembers[i],
							    creatorMode: memberData.creatorMode});
			await this.getMemberRelation(mainMembers[i]);
		}
		this.setState({familyMembers: familyMembers});
		console.log(this.state.familyMembers);
		// If there are pending members do the exact same process
		if (familyData.pendingMembers != undefined) {
			var pendingMembers = Object.keys(familyData.pendingMembers);
			var pendingMemberData = [];
			for (var j = 0; j <= pendingMembers.length - 1; j++) {
				var memberData = await MemberData(pendingMembers[j]);
				pendingMemberData.push({name: memberData.name, 
										profilePicture: memberData.profilePicture,
									    uid: pendingMembers[j],
							    		creatorMode: memberData.creatorMode});
			}
			this.setState({pendingMembers: pendingMemberData});
			console.log(this.state.pendingMembers);
		}
		else {
			this.setState({pendingMembers: []});
		}
	}

	async getMemberRelation(uid) {
		var userData = await UserData();
		if (userData.relations != null) {
			for (var i = 0; i <= Object.keys(userData.relations).length - 1; i++) {
				if (Object.keys(userData.relations)[i] == uid) {
					this.setState({['relation_' + uid]: userData.relations[uid]});
					console.log(this.state['relation_' + uid]);
				}
			}
		}
		this.setState({loadedRelations: true});
	}

	async saveAndExit() {
		var userId = await UserId();
		// Save the member relations if set
		for (var i = 0; i <= this.state.familyMembers.length - 1; i++) {
			if (this.state['relation_' + this.state.familyMembers[i].uid] != null) {
				const data = {[this.state.familyMembers[i].uid]: this.state['relation_' + this.state.familyMembers[i].uid]};
				const path = 'users/' + userId + '/relations';
				await Append(data, path);
			}
		}
		this.props.navigation.navigate('MyProfileScreen');
	}

	selectedValue(item) {
		setTimeout(() => this.selectedValue(item), 10);
		if (this.state.loadedRelations == true) {
			return(this.state['relation_' + item.uid]);
		}
		else {
			return('None')
		}
	}

	async acceptPendingMember(uid) {
		// Edit the family group to add them as a member
		var userData = await UserData();
		var familyId = userData.familyId;
		const mainPath = 'families/' + familyId;
		const pendingPath = mainPath + '/pendingMembers/';
		await Append({[uid]: null}, pendingPath);
		const membersPath = mainPath + '/members/';
		await Append({[uid]: 0}, membersPath);
		// Then update their personal db
		const newMemberPath = 'users/' + uid;
		await Append({familyState: 'in-group',
					  pendingFamilyId: null,
					  familyId: familyId}, newMemberPath);
		// Finally trigger the UI to update by refreshing
		this.getFamilyGroupData();
	}

	async declinePendingMember(uid) {
		// Edit the family group to remove them as pending
		var userData = await UserData();
		var familyId = userData.familyId;
		const mainPath = 'families/' + familyId;
		const pendingPath = mainPath + '/pendingMembers/';
		await Append({[uid]: null}, pendingPath);
		// Then update their personal db
		const newMemberPath = 'users/' + uid;
		await Append({familyState: 'none',
					  pendingFamilyId: null,
					  familyId: ''}, newMemberPath);
		// Finally trigger the UI to update by refreshing
		this.getFamilyGroupData();
	}

	getMemberType(item) {
		if (item.creatorMode == true) {
			return('Creator');
		}
		else {
			return('Player');
		}
	}

	uiTitleBar() {
		return(
			<View style={styles.titleBar}>
				<TouchableOpacity style={styles.titleBarIconButton}
								  onPress={() => this.saveAndExit()}>
					<View style={gstyles.shadow}>
						<Image style={styles.titleBarIcon} source={require('../../../assets/icons/png/chevron-left.png')} />
					</View>
				</TouchableOpacity>
				<Text style={gstyles.largeText}>My Family Group</Text>
				<TouchableOpacity style={styles.titleBarIconButton}
								  onPress={() => null}>
				    <View style={gstyles.shadow}>
						<Image source={require('../../../assets/icons/png/account-plus.png')}
							   style={styles.titleBarIcon} />
				    </View>
			    </TouchableOpacity>
			</View>
		);
	}

	uiBody() {
		return(
			<ScrollView style={styles.scrollView}>
				<TouchableOpacity style={styles.changeFamilyGroup} onPress={() => this.props.navigation.navigate('SettingsScreen', {navvedFrom: 'profile'})}>
					<View style={gstyles.shadow}>
						<Image style={gstyles.largeIcon} source={require('../../../assets/icons/png/noun_Family_1106970.png')} />
					</View>
					<View style={styles.changeFamilyGroupText}>
						<Text style={gstyles.largeText}>{this.state.familyId}</Text>
						<Text style={gstyles.subTitle}>Change family group?</Text>
					</View>
				</TouchableOpacity>
				{this.uiFamilyMembersCard()}
				{this.uiPendingMembersCard()}
            </ScrollView>
		);
	}

	uiFamilyMembersCard() {
		return(
			<View style={[gstyles.cardColumn, styles.familyMembersCard, {height: 100 + (this.state.familyMembers.length * 115)}]}>
        		<Text style={gstyles.smallText}>My Family Members</Text>
        		<View style={[styles.familyMembers, {flex: 1}]}>
            		<FlatList data={this.state.familyMembers}
					          keyExtractor={(item, index) => index.toString()}
					          scrollEnabled={false}
                              renderItem={({item}) => <View style={styles.familyMemberRow}>
                              							<View style={gstyles.shadow}>
                              								<CacheableImage style={[gstyles.profilePicturePortrait, styles.profilePictures]} source={{uri: item.profilePicture}}/>
                              							</View>
                              							<View style={styles.familyMemberText}>
								                			<Text style={gstyles.smallText}>{item.name}</Text>
								                			<Text style={gstyles.subTitle}>{this.getMemberType(item)}</Text>
								                	    </View>
								                	    <RNPickerSelect style={{viewContainer: styles.pickerContainer,
								                	    						inputIOSContainer: styles.picker,
								                	    					    inputIOS: styles.pickerText,
								                	    						icon: styles.pickerChevron}}
								                	    			    selectedValue={this.selectedValue(item)}
								                	    			    value={this.selectedValue(item)}
								                	    		        onValueChange={(d, i) => { this.setState({['relation_' + item.uid]: d})}}
							                	    					items={this.state.relationItems} />
							                	    </View>}/>
        	    </View>
        	</View>
		);
	}

	uiPendingMembersCard() {
		return(
			<View style={[gstyles.cardColumn, styles.familyMembersCard, {height: 100 + (this.state.pendingMembers.length * 115)}]}>
        		<Text style={gstyles.smallText}>Pending Members</Text>
        		<View style={[styles.familyMembers, {flex: 1}]}>
            		<FlatList data={this.state.pendingMembers}
					          keyExtractor={(item, index) => index.toString()}
					          scrollEnabled={false}
                              renderItem={({item}) => <View style={styles.familyMemberRow}>
                              							<View style={gstyles.shadow}>
                              								<CacheableImage style={[gstyles.profilePicturePortrait, styles.profilePictures]} source={{uri: item.profilePicture}}/>
                              							</View>
                              							<View style={styles.familyMemberText}>
								                			<Text style={gstyles.smallText}>{item.name}</Text>
								                			<Text style={gstyles.subTitle}>Creator</Text>
								                	    </View>
								                	    <TouchableOpacity style={styles.pendingMemberButton} onPress={() => this.acceptPendingMember(item.uid)}>
								                	    	<Image style={[gstyles.mediumIcon, styles.pendingMemberTick]} source={require('../../../assets/icons/png/check.png')} />
								                	    </TouchableOpacity>
								                	    <TouchableOpacity style={styles.pendingMemberButton} onPress={() => this.declinePendingMember(item.uid)}>
								                	    	<Image style={[gstyles.mediumIcon, styles.pendingMemberCross]} source={require('../../../assets/icons/png/close.png')} />
								                	    </TouchableOpacity>
							                	    </View>}/>
        	    </View>
        	</View>
		);
	}

	render() {
		return(
			<View style={styles.container}>
				{this.uiTitleBar()}
				{this.uiBody()}
			</View>
		);
	}

}