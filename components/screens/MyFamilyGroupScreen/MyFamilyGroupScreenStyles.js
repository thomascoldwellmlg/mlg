import { StyleSheet, Dimensions} from 'react-native';
import { color } from '../../reusable/common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

if (screenHeight < screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const styles = StyleSheet.create({

	/*
		Contents:
			0. Title Bar
			1. Body
			2. Cards
	*/

	// 0. Title Bar
	titleBar: {
		height: screenHeight * 0.15,
		width: screenWidth,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		backgroundColor: color.darkBlue,
		paddingLeft: screenWidth * 0.05,
		paddingRight: screenWidth * 0.05,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	titleBarIconButton: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	titleBarIcon: {
		height: screenHeight * 0.07,
		width: screenHeight * 0.07,
		resizeMode: 'contain',
		tintColor: '#fff'
	},

	// 1. Body
	container: {
		height: screenHeight,
		width: screenWidth,
		backgroundColor: color.lightBlue,
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	scrollView: {
		flex: 1,
		width: screenWidth
	},
	changeFamilyGroup: {
		height: screenHeight * 0.25,
		width: screenWidth * 0.6,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		alignSelf: 'center'
	},
	changeFamilyGroupText: {
		height: screenHeight * 0.08,
		width: screenWidth * 0.4,
		justifyContent: 'space-around',
		alignItems: 'center'
	},

	// 2. Cards
	familyMembersCard: {
		height: screenHeight * 0.45,
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
		paddingLeft: screenWidth * 0.05,
		alignSelf: 'center'
	},
	familyMembers: {
		height: screenHeight * 0.35,
		width: screenWidth * 0.8,
		marginLeft: screenWidth * 0.03,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginTop: screenHeight * 0.02
	},
	familyMemberText: {
		height: screenHeight * 0.055,
		width: screenWidth * 0.35,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginBottom: screenHeight * 0.01
	},
	familyMemberRow: {
		flexDirection: 'row',
		height: screenHeight * 0.12,
		width: screenWidth * 0.8,
		alignItems: 'center',
		justifyContent: 'flex-start'
	},
	profilePictures: {
		marginRight: screenWidth * 0.03
	},
	pendingMemberButton: {
		justifyContent: 'center',
		alignItems: 'center'
	},
	pendingMemberTick: {
		tintColor: color.green
	},
	pendingMemberCross: {
		tintColor: color.yellow,
		marginLeft: screenWidth * 0.07
	},
	pickerContainer: {
		height: screenHeight * 0.06,
		width: screenWidth * 0.25,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		borderColor: color.lightBlue,
		borderBottomWidth: screenHeight * 0.002,
		alignSelf: 'center'
	},
	picker: {
		height: screenHeight * 0.06,
		width: screenWidth * 0.25,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	pickerText: {
		fontFamily: 'Futura',
		fontSize: screenHeight * 0.02
	},
	pickerChevron: {
		position: 'relative',
		height: screenHeight * 0.03,
		width: screenHeight * 0.03,
		tintColor: '#444'
	}

});

export default styles;