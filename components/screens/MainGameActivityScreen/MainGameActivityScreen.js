import React from 'react';
import { View,
         Text,
         Image,
         TouchableOpacity,
         Alert } from 'react-native';
import gstyles from '../../reusable/GlobalStyles';
import { color } from '../../reusable/common';
import styles from './MainGameActivityScreenStyles';
import { MemberData,
		 Append,
		 UserId,
		 UserData } from '../../../lib/Firebase';
import { UploadAudio } from '../../../lib/AWS';
import Expo, { Audio, BlurView, ScreenOrientation, FileSystem } from 'expo';
import VoiceRecorder from '../../reusable/VoiceRecorder/VoiceRecorder';
import { sendGameLaunchedNotification } from '../../../lib/Notifications';
import { getNeatShortDate, getNeatTime } from '../../..//lib/Custom';
import { Image as CacheableImage} from 'react-native-aws-cache';

const DEMO_DIR = `${FileSystem.documentDirectory}mlgDemoModeAssets/`;

export default class MainGameActivityScreen extends React.Component {

	state = {
		gameData: [],
		creatorData: {},
		titleBarText: 'Welcome to your game!',
		stage: 'splash',
		additionalRecordingStage: 1,
		currentQuestion: 0,
		attemptNumber: 1,
		shuffledAnswers: ['', '', '', ''],
		completionButtonText: '',
		additionalQuestionReply: '',
		replyMessage: '',
		enableVoiceRecorder: false,
		voiceRecorderStage: 'start',
		enableMainMenu: false,
		relation: '',
		personalGame: false,
		demoMode: null
	}

	async componentDidMount() {
		// Firstly check if we are in demo mode or not
		var demoMode = await Expo.SecureStore.getItemAsync('demo-mode');
		if (demoMode == 'true') {
			console.log('demoMode ==== > ', demoMode)
			this.setState({demoMode: true});
		}
		if (this.state.demoMode == null) {
			// Fetch the game data from the given props - set the state of game data
			const gameId = this.props.navigation.state.params.gameId;
			const senderId = this.props.navigation.state.params.creatorId;
			var userId = await UserId();
			// Check if launched by the creator themselves - if so skip the splash screen
			if (senderId == userId) {
				this.setState({personalGame: true});
			}
			var senderData = await MemberData(senderId);
			this.setState({creatorData: senderData});
			const gameData = senderData.createdGames[gameId];
			this.setState({gameData: gameData});
			console.log(this.state.gameData);
			this.shuffleAnswers();
			// Set any default states
			this.setState({completionButtonText: 'Send ' + this.state.creatorData.name.split(' ')[0] + ' a message!'});
			// Send a notification telling the family the game has been launched
			await sendGameLaunchedNotification(gameId, senderId);
			// Get the relation data if there from who sent the game and set the title bar text
			// to 'welcome to the game' + relation
			if (senderData.relations != null) {
				var relation = senderData.relations[userId];
				if (relation != null) {
					this.setState({titleBarText: 'Welcome to your game, ' + relation + '!',
						           relation: relation});
				} 
			}
			if (this.state.personalGame == true) {
				this.setState({stage: 'main-question'});
			}
		}
		else {
			// Else get the game data from the demo mode 
			var demoData = await FileSystem.readAsStringAsync(DEMO_DIR + 'manager.json');
			demoData = JSON.parse(demoData);
			const gameId = this.props.navigation.state.params.gameId;
			const senderId = this.props.navigation.state.params.creatorId;
			// Check if launched by the creator themselves - if so skip the splash screen
			const userId = 'Hr3ApcPo2UWl1jcPjFPpozDj9fr2' // In demo mode default user is John Miller (Hr3ApcPo2UWl1jcPjFPpozDj9fr2)
			if (senderId == userId) {
				this.setState({personalGame: true});
			}
			var senderData = demoData.users[senderId];
			this.setState({creatorData: senderData});
			const gameData = senderData.createdGames[gameId];
			this.setState({gameData: gameData});
			console.log(this.state.gameData);
			this.shuffleAnswers();
			// Set any default states
			this.setState({completionButtonText: 'Send ' + this.state.creatorData.name.split(' ')[0] + ' a message!'});
			// Get the relation data if there from who sent the game and set the title bar text
			// to 'welcome to the game' + relation
			if (senderData.relations != null) {
				var relation = senderData.relations[userId];
				if (relation != null) {
					this.setState({titleBarText: 'Welcome to your game, ' + relation + '!',
						           relation: relation});
				} 
			}
			if (this.state.personalGame == true) {
				this.setState({stage: 'main-question'});
			}
		}
	}

	checkAnswer(answer) {
		// On presisng one of the answer buttons check the answer with the correct answer
		// if they match advance to correct evluation, if not check attempt number and
		// set the stage appropriately
		var correctAnswer = this.state.gameData.questions[this.state.currentQuestion].correctAnswer;
		if (answer == correctAnswer) {
			this.setState({stage: 'correct-eval'});
		}
		else {
			if (this.state.attemptNumber < 3) {
				this.setState({stage: 'incorrect-eval'});
				this.setState({attemptNumber: this.state.attemptNumber + 1});
			}
			else {
				this.setState({stage: 'final-attempt-eval'});
			}
		}
	}

	checkForAdditionalQuestion() {
 		// Check to see if there is an additional question recording -
 		// if so then set the stage to for that, else nav to next q
 		if (this.state.gameData.questions[this.state.currentQuestion].additionalQuestion != ''
 			&& this.state.gameData.questions[this.state.currentQuestion].additionalQuestion != null
 			&& this.state.gameData.questions[this.state.currentQuestion].additionalQuestion != undefined) {
 			this.setState({stage: 'additional-question'});
 		}
 		else {
 			this.goToNextQuestion();
 		}
	}

	async goToNextQuestion() {
		// Check if the user recorded a additional question reply
		if (this.state.additionalQuestionReply != '') {
			await this.uploadAndSendQuestionReply();
		}
		// Check to see if we have reached the last question, if so set the
		// stage to completion, else nav to the next q and reset q specifics
		if ((this.state.currentQuestion + 1) == this.state.gameData.questions.length) {
			if (this.state.personalGame == false) {
				this.setState({stage: 'complete'});
			}
			else {
				this.props.navigation.navigate('CreatedGamesScreen', {demoMode: this.state.demoMode});
			}
		}
		else {
			// Reset question number and additional recording sub stage
			// Change title bar to next q
			await this.setState({currentQuestion: this.state.currentQuestion + 1});
			this.setState({attemptNumber: 1});
			await this.shuffleAnswers();
			this.setState({titleBarText: 'Question ' + (this.state.currentQuestion + 1).toString()});
			this.setState({stage: 'main-question'});
			this.setState({additionalRecordingStage: 1});
			this.setState({additionalQuestionReply: ''});
		}
	}

	async uploadAndSendQuestionReply() {
		var time = await getNeatTime();
		var date = await getNeatShortDate();
		const messageId = Date.now();
		const sentBy = await UserId();
		const messageType = 'additional-question-reply';
		// Perform AWS upload
		const AWSPath = 'users/' + sentBy + '/messages/' + messageId + '/';
		const name = 'reply';
		var audioUrl = await UploadAudio(this.state.additionalQuestionReply, name, AWSPath);
		// Push reply as message to Firebase
		const firebaseData = {
			messageType: messageType,
			data: [this.state.gameData.questions[this.state.currentQuestion].additionalQuestion, audioUrl],
			sentTime: time,
			sentDate: date,
			sentBy: sentBy
		};
		const firebasePath = 'users/' + this.props.navigation.state.params.creatorId + '/messages/' + sentBy + '/' + messageId;
		await Append(firebaseData, firebasePath);
	}

	shuffleAnswers() {
		var answers = [
			this.state.gameData.questions[this.state.currentQuestion].correctAnswer,
			this.state.gameData.questions[this.state.currentQuestion].dummyAnswer1,
			this.state.gameData.questions[this.state.currentQuestion].dummyAnswer2,
			this.state.gameData.questions[this.state.currentQuestion].dummyAnswer3
		];
		// Fisher-Yates Shuffle Algorithm
		for (var i = 0; i <= answers.length - 1; i++) {
	        var j = Math.floor(Math.random() * (i + 1));
	        var x = answers[i];
	        answers[i] = answers[j];
	        answers[j] = x;
	    }
	    this.setState({shuffledAnswers: answers});
	}

	async playQuestionHint() {
		if (this.state.gameData.questions[this.state.currentQuestion].questionHint != '') {
			const audioObject = new Audio.Sound();
			try {
				await audioObject.loadAsync({uri: this.state.gameData.questions[this.state.currentQuestion].questionHint});
				await audioObject.playAsync();
			} catch(error) {
				Alert.alert('Sorry it appears we are unable to play your hint right now!, Please check your network connection!');
			}
		}
	}

	async playAdditionalQuestion() {
		const audioObject = new Audio.Sound();
		try {
			await audioObject.loadAsync({uri: this.state.gameData.questions[this.state.currentQuestion].additionalQuestion});
			await audioObject.playAsync();
			this.setState({additionalRecordingStage: 2});
		} catch(error) {
			Alert.alert('Sorry it appears we are unable to play their question right now!, Please check your network connection!');
		}
	}

	recordAdditionalQuestionReply() {
		this.setState({enableVoiceRecorder: true});
		this.setState({additionalRecordingStage: 3});
	}

	recordGameReplyMessage() {
		if (this.state.demoMode != true) {
			this.setState({enableVoiceRecorder: true});
		}
		else {
			Alert.alert("Please create an account and use the full version of Memory Lane Games to enable this feature!");
		}
	}

	checkAdditionalQuestionPresent() {
		if (this.state.additionalQuestionReply == '') {
			this.setState({additionalRecordingStage: 2});
		}
		else {
			this.setState({additionalRecordingStage: 3});
		}
	}

	checkGameReplyPresent() {
		if (this.state.replyMessage == '') {
			this.setState({completionButtonText: 'Send ' + this.state.creatorData.name.split(' ')[0] + ' a message!'});
		}
		else {
			this.setState({completionButtonText: 'Message saved! Change your reply?'});
		}
	}

	async navToHomeScreen() {
		if (this.state.demoMode != true) {
			var userData = await UserData();
			// Check if they left a reply if so upload to AWS and send to recips firebase
			if (this.state.replyMessage != '') {
				var time = await getNeatTime();
				var date = await getNeatShortDate();
				const messageId = Date.now();
				const sentBy = await UserId();
				const messageType = 'voice-message';
				// Perform AWS upload
				const AWSPath = 'users/' + sentBy + '/messages/' + messageId + '/';
				const name = 'reply';
				var audioUrl = await UploadAudio(this.state.replyMessage, name, AWSPath);
				// Push reply as message to Firebase
				const firebaseData = {
					messageType: messageType,
					data: [audioUrl],
					sentTime: time,
					sentDate: date,
					sentBy: sentBy
				};
				const firebasePath = 'users/' + this.props.navigation.state.params.creatorId + '/messages/' + sentBy + '/' + messageId;
				await Append(firebaseData, firebasePath);
				ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT);
				if (userData.creatorMode == true) {
					this.props.navigation.navigate('FamilyGamesScreen', {demoMode: this.state.demoMode});
				}
				else {
					this.props.navigation.navigate('PlayerGamesScreen');
				}
			}
			else {
				ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT);
				if (userData.creatorMode == true) {
					this.props.navigation.navigate('FamilyGamesScreen', {demoMode: this.state.demoMode});
				}
				else {
					this.props.navigation.navigate('PlayerGamesScreen');
				}
			}
		}
		else {
			ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT);
			this.props.navigation.navigate('FamilyGamesScreen');
		}
	}

	uiTitleBar() {
		return(
			<View style={[gstyles.shadow, styles.navBar]}>
				<TouchableOpacity style={gstyles.shadow} onPress={() => Alert.alert('Warning!',
																					'Are you sure you want to retun to the home screen?',
																					[
																						{text: 'Confirm', onPress: () => this.navToHomeScreen()},
																						{text: 'Cancel'}
																					]
																					)}>
					<Image style={styles.navBarLogo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
				</TouchableOpacity>
				<Text style={gstyles.mediumText}>{this.state.titleBarText}</Text>
				<TouchableOpacity onPress={() => this.setState({enableMainMenu: true})}>
					<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/menu.png')} />
				</TouchableOpacity>
			</View>
		);
	}

	uiSplashScreen() {
		if (this.state.stage == 'splash') {
			return(
				<View style={styles.splashBody}>
					<View style={[gstyles.cardColumn, styles.splashCard]}>
						<View style={styles.splashCardHeader}>
							<View style={gstyles.shadow}>
								{this.uiSpalshScreenProfilePicture()}
							</View>
							<View style={styles.splashHeaderText}>
								<Text style={gstyles.smallText}>{this.state.gameData.title}</Text>
								<Text style={gstyles.subTitle}>Sent by {this.state.creatorData.name} on {this.state.gameData.uploadDate}</Text>
							</View>
						</View>
						<View style={styles.splashCardText}>
							<Text style={gstyles.smallText}>{this.state.gameData.attachedMessage}</Text>
						</View>
						{/*<TouchableOpacity style={styles.secondaryActionButton} onPress={() => this.playAttachedVoiceMessage()}>
							<Text style={gstyles.smallText}>Play Attached Message</Text>
						</TouchableOpacity>*/}
					</View>
					<TouchableOpacity style={styles.primaryActionButton} onPress={() => this.setState({stage: 'main-question',
																									   titleBarText: 'Question 1'})}>
						<Text style={gstyles.smallText}>Ready to take a trip down Memory Lane?</Text>
					</TouchableOpacity> 
				</View>
			);
		}
	}

	uiSpalshScreenProfilePicture() {
		if (this.state.demoMode != true) {
			return(
				<CacheableImage style={styles.profilePicture} source={{uri: this.state.creatorData.profilePicture}} />
			);
		}
		else {
			return(
				<Image style={styles.profilePicture} source={{uri: this.state.creatorData.profilePicture}} />
			);
		}
	}

	uiMainQuestion() {
		if (this.state.stage == 'main-question') {
			return(
				<View style={styles.mainQuestionBody}>
					<Text style={[gstyles.largeText, {textAlign: 'center'}]}>{this.state.gameData.questions[this.state.currentQuestion].title}</Text>
					<View style={styles.mainQuestionRow}>
						<View style={gstyles.shadow}>
							{this.uiMainQuestionImage()}
						</View>
							<TouchableOpacity style={styles.mainQuestionPlayHint}
											  onPress={() => this.playQuestionHint()}>
							    <Text style={[gstyles.smallText, {textAlign: 'center'}]}>Play{"\n"}hint</Text>
						    </TouchableOpacity>
					</View>
					<View style={styles.answerButtonsRow}>
						<TouchableOpacity style={[gstyles.shadow, styles.answerButton]}
										  onPress={() => this.checkAnswer(this.state.shuffledAnswers[0])}>
							<Text style={styles.answerButtonText}>{this.state.shuffledAnswers[0]}</Text>
						</TouchableOpacity>
						<TouchableOpacity style={[gstyles.shadow, styles.answerButton]}
										  onPress={() => this.checkAnswer(this.state.shuffledAnswers[1])}>
							<Text style={styles.answerButtonText}>{this.state.shuffledAnswers[1]}</Text>
						</TouchableOpacity>
						<TouchableOpacity style={[gstyles.shadow, styles.answerButton]}
										  onPress={() => this.checkAnswer(this.state.shuffledAnswers[2])}>
							<Text style={styles.answerButtonText}>{this.state.shuffledAnswers[2]}</Text>
						</TouchableOpacity>
						<TouchableOpacity style={[gstyles.shadow, styles.answerButton]}
										  onPress={() => this.checkAnswer(this.state.shuffledAnswers[3])}>
							<Text style={styles.answerButtonText}>{this.state.shuffledAnswers[3]}</Text>
						</TouchableOpacity>
					</View>
				</View>
			);
		}
	}

	uiMainQuestionImage() {
		if (this.state.demoMode != true) {
			return(
				<CacheableImage style={styles.mainImage} source={{uri: this.state.gameData.questions[this.state.currentQuestion].image.mainImageUrl}} />
			);
		}
		else {
			return(
				<Image style={styles.mainImage} source={{uri: this.state.gameData.questions[this.state.currentQuestion].image.mainImageUrl}} />
			);
		}
	}

	uiCorrectAnswerEval() {
		if (this.state.stage == 'correct-eval') {
			return(
				<View style={styles.evaluationBody}>
					<View style={styles.evaluationHeader}>
						<View style={[styles.evalutionTickContainer, gstyles.shadow]}>
							<Image style={gstyles.smallIcon} source={require('../../../assets/icons/png/check.png')} />
						</View>
						<Text style={gstyles.mediumText}>That's Correct {this.state.relation}! Your answer was...</Text>
					</View>
					<View style={styles.evaluationCenterRow}>
						<View style={gstyles.shadow}>
							{this.uiEvaluationImage()}
						</View>
						<View style={[gstyles.cardColumn, styles.evaluationCard]}>
							<Text style={gstyles.mediumText}>{this.state.gameData.questions[this.state.currentQuestion].correctAnswer}</Text>
						</View>
					</View>
					<TouchableOpacity style={styles.primaryActionButton} onPress={() => this.checkForAdditionalQuestion()}>
						<Text style={gstyles.mediumText}>Continue to next question!</Text>
					</TouchableOpacity> 
				</View>
			);
		}
	}

	uiIncorrectAnswerEval() {
		if (this.state.stage == 'incorrect-eval') {
			return(
				<View style={styles.evaluationBody}>
					<View style={styles.evaluationHeader}>
						<View style={[styles.evalutionCrossContainer, gstyles.shadow]}>
							<Image style={gstyles.smallIcon} source={require('../../../assets/icons/png/close.png')} />
						</View>
						<Text style={gstyles.mediumText}>Not quite! Listen to the hint and try again!</Text>
					</View>
					<View style={styles.evaluationCenterRow}>
						<View style={gstyles.shadow}>
							{this.uiEvaluationImage()}
						</View>
						<TouchableOpacity style={[gstyles.cardColumn, styles.playHintButton]}
										  onPress={() => this.playQuestionHint()}>
							<Text style={gstyles.mediumText}>Play Hint</Text>
						</TouchableOpacity>
					</View>
					<TouchableOpacity style={styles.primaryActionButton} onPress={() => this.setState({stage: 'main-question'})}>
						<Text style={gstyles.mediumText}>Try again!</Text>
					</TouchableOpacity> 
				</View>
			);
		}
	}

	uiFinalAttemptEval() {
		if (this.state.stage == 'final-attempt-eval') {
			return(
				<View style={styles.evaluationBody}>
					<View style={styles.evaluationHeader}>
						<Text style={gstyles.largeText}>The correct answer was...</Text>
					</View>
					<View style={styles.evaluationCenterRow}>
						<View style={gstyles.shadow}>
							{this.uiEvaluationImage()}
						</View>
						<View style={[gstyles.cardColumn, styles.evaluationCard]}>
							<Text style={gstyles.mediumText}>{this.state.gameData.questions[this.state.currentQuestion].correctAnswer}</Text>
						</View>
					</View>
					<TouchableOpacity style={styles.primaryActionButton} onPress={() => this.checkForAdditionalQuestion()}>
						<Text style={gstyles.mediumText}>Continue to next question!</Text>
					</TouchableOpacity> 
				</View>
			);
		}
	}

	uiEvaluationImage() {
		if (this.state.demoMode != true) {
			return(
				<CacheableImage style={styles.evaluationImage} source={{uri: this.state.gameData.questions[this.state.currentQuestion].image.mainImageUrl}} />
			);
		}
		else {
			return(
				<Image style={styles.evaluationImage} source={{uri: this.state.gameData.questions[this.state.currentQuestion].image.mainImageUrl}} />
			);
		}
	}

	uiAdditionalQuestion() {
		if (this.state.stage == 'additional-question') {
			if (this.state.additionalRecordingStage == 1) {
				return(
					<View style={styles.additionalQBody1}>
						<View style={gstyles.shadow}>
							<CacheableImage style={styles.additionalQuestionProfile} source={{uri: this.state.creatorData.profilePicture}} />
						</View>
						<View style={styles.additionalQPrompt}>
							<Text style={[gstyles.mediumText, {textAlign: 'center' }]}>
								{this.state.creatorData.name.split(' ')[0]} has another question they'd
								like to ask you!{"\n"}Press below to hear their question!
							</Text>
						</View>
						<TouchableOpacity style={styles.primaryActionButton}
									      onPress={() => this.playAdditionalQuestion()}>
				      		<Text style={gstyles.mediumText}>Play Additional Question</Text>
			      		</TouchableOpacity>
			      		<TouchableOpacity style={styles.additionalQuestionSkip} onPress={() => this.goToNextQuestion()}>
			      			<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>Skip to next question</Text>
			      		</TouchableOpacity>
					</View>
				);
			}
			else if (this.state.additionalRecordingStage == 2) {
				return(
					<View style={styles.additionalQBody2}>
						<View style={styles.additionalQPrompt}>
							<Text style={[gstyles.mediumText, {textAlign: 'center' }]}>
								Let {this.state.creatorData.name.split(' ')[0]} know your answer by recording
								your response below!
							</Text>
						</View>
						<TouchableOpacity style={styles.primaryActionButton}
									      onPress={() => this.recordAdditionalQuestionReply()}>
				      		<Text style={gstyles.mediumText}>Record your answer</Text>
			      		</TouchableOpacity>
						<TouchableOpacity style={styles.secondaryActionButton}
									      onPress={() => this.playAdditionalQuestion()}>
				      		<Text style={gstyles.smallText}>Play Question Again</Text>
			      		</TouchableOpacity>
			      		<TouchableOpacity style={styles.additionalQuestionSkip} onPress={() => this.goToNextQuestion()}>
			      			<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>Skip to next question</Text>
			      		</TouchableOpacity>
					</View>
				);
			}
			else {
				return(
					<View style={styles.additionalQBody2}>
						<View style={styles.additionalQPrompt}>
							<Text style={[gstyles.mediumText, {textAlign: 'center' }]}>
								Thank you for replying to {this.state.creatorData.name.split(' ')[0]}'s question!
								{"\n"}To continue press the green button below!
							</Text>
						</View>
						<TouchableOpacity style={styles.secondaryActionButton}
									      onPress={() => this.recordAdditionalQuestionReply()}>
				      		<Text style={gstyles.smallText}>Change my answer</Text>
			      		</TouchableOpacity>
						<TouchableOpacity style={styles.primaryActionButton}
									      onPress={() => this.goToNextQuestion()}>
				      		<Text style={gstyles.mediumText}>Continue to next question!</Text>
			      		</TouchableOpacity>
					</View>
				);
			}
		}
	}

	uiCompletion() {
		if (this.state.stage == 'complete') {
			return(
				<View style={styles.completionBody}>
					<View style={styles.completionHeader}>
						<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>
							Thank you for playing {this.state.relation}! Can you tell me
							what you thought of the game by recording a message below!
						</Text>
						<View style={styles.creatorSignitureRow}>
							{this.uiCompletionProfilePicture()}
							<Text style={gstyles.smallText}>From {this.state.creatorData.name.split(' ')[0]}</Text>
						</View>
					</View>
					{this.uiCompletionRecordButton()}
					<TouchableOpacity style={styles.primaryActionButton}
									  onPress={() => this.navToHomeScreen()}>
				  		<Text style={gstyles.mediumText}>Back to home screen</Text>
					</TouchableOpacity>
				</View>
			);
		}
	}

	uiCompletionProfilePicture() {
		if (this.state.demoMode != true) {
			return(
				<CacheableImage style={[styles.creatorSignitureProfilePicture, gstyles.shadow]} source={{uri: this.state.creatorData.profilePicture}} />
			);
		}
		else {
			return(
				<Image style={[styles.creatorSignitureProfilePicture, gstyles.shadow]} source={{uri: this.state.creatorData.profilePicture}} />
			);
		}	
	}

	uiCompletionRecordButton() {
		if (this.state.replyMessage == '') {
			return(
				<TouchableOpacity style={styles.secondaryActionButton}
								  onPress={() => this.recordGameReplyMessage()}>
			  		<Text style={gstyles.smallText}>{this.state.completionButtonText}</Text>
				</TouchableOpacity>
			);
		}
		else {
			return(
				<TouchableOpacity style={styles.secondaryActionButton}>
			  		<Text style={gstyles.smallText}>Message Sent!</Text>
				</TouchableOpacity>
			);
		}
	}

	uiVoiceRecorder() {
		if (this.state.enableVoiceRecorder == true) {
			return(<VoiceRecorder parent={this} recordingType={this.state.stage} stagePreset={this.state.voiceRecorderStage} />);
		}
		else {
			return(null);
		}
	}

	uiMainMenu() {
		if (this.state.enableMainMenu == true) {
			return(
				<BlurView style={styles.mainMenuBlur} intensity={90} tint='dark'>
					<View style={gstyles.shadow}>
						<Image style={styles.mainMenuLogo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
					</View>
					<TouchableOpacity style={[styles.mainMenuExitButton, gstyles.shadow]}
								      onPress={() => this.navToHomeScreen()}>
						<Text style={gstyles.smallText}>Back to Home</Text>
					</TouchableOpacity>
			        <TouchableOpacity style={[styles.closeIcon, gstyles.shadow]} 
			        				  onPress={() => this.setState({enableMainMenu: false})}>
                    	<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/close.png')} />
                	</TouchableOpacity>
				</BlurView>
			);
		}
		else {
			return(null);
		}
	}

	render() {
		return(
			<View style={styles.container}>
				{this.uiTitleBar()}
				{this.uiSplashScreen()}
				{this.uiMainQuestion()}
				{this.uiCorrectAnswerEval()}
				{this.uiIncorrectAnswerEval()}
				{this.uiFinalAttemptEval()}
				{this.uiAdditionalQuestion()}
				{this.uiCompletion()}
				{this.uiVoiceRecorder()}
				{this.uiMainMenu()}
			</View>
		);
	}
}