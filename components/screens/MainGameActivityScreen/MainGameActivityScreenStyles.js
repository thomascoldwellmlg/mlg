import { StyleSheet, Dimensions } from 'react-native';
import { color } from '../../reusable/common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

if (screenHeight > screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const styles = StyleSheet.create({

	/*
		Contents:
			0. General
			1. Splash Screen
			2. Main question
			3. Evaluation
			4. Reply
			5. Completion
            6. Main Menu
	*/

	// 0. General
	container: {
		height: screenHeight,
		width: screenWidth,
		backgroundColor: color.lightBlue
	},
    navBar: {
    	height: screenHeight * 0.16,
    	width: screenWidth,
    	flexDirection: 'row',
    	justifyContent: 'space-between',
    	alignItems: 'center',
    	backgroundColor: color.darkBlue,
    	paddingLeft: screenWidth * 0.07,
    	paddingRight: screenWidth * 0.07,
    	marginBottom: screenHeight * 0.02
    },
    navBarLogo: {
    	height: screenHeight * 0.11,
    	width: screenHeight * 0.11
    },
    primaryActionButton: {
    	height: screenHeight * 0.15,
    	width: screenWidth * 0.5,
    	justifyContent: 'center',
    	alignItems: 'center',
    	borderRadius: screenWidth * 0.01,
    	backgroundColor: color.green,
    	shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
    },
    secondaryActionButton: {
    	height: screenHeight * 0.12,
    	width: screenWidth * 0.4,
    	justifyContent: 'space-around',
    	alignItems: 'center',
    	borderRadius: screenWidth * 0.01,
    	backgroundColor: color.yellow,
    	shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
    },

    // 1. Splash Screen
    splashBody: {
    	height: screenHeight * 0.84,
    	width: screenWidth,
    	justifyContent: 'space-around',
    	alignItems: 'center',
    	paddingBottom: screenHeight * 0.07
    },
    splashCard: {
    	height: screenHeight * 0.5,
    	width: screenWidth * 0.7,
    	justifyContent: 'flex-start',
    	alignItems: 'center',
    	paddingTop: screenHeight * 0.05
    },
    splashCardHeader: {
    	height: screenHeight * 0.15,
    	width: screenWidth * 0.6,
    	flexDirection: 'row',
    	justifyContent: 'flex-start',
    	alignItems: 'center',
    	marginBottom: screenHeight * 0.04
    },
    profilePicture: {
    	height: screenHeight * 0.14,
    	width: screenHeight * 0.14,
    	borderRadius: screenHeight * 0.07
    },
    splashHeaderText: {
    	height: screenHeight * 0.08,
    	width: screenWidth * 0.5,
    	justifyContent: 'space-around',
    	alignItems: 'flex-start',
    	paddingLeft: screenWidth * 0.05
    },
    splashCardText: {
    	height: screenHeight * 0.3,
    	width: screenWidth * 0.6
    },

    // 2. Main Question
    mainQuestionBody: {
    	height: screenHeight * 0.84,
    	width: screenWidth,
    	alignItems: 'center',
    	justifyContent: 'space-around',
    	paddingBottom: screenHeight * 0.07,
    	paddingTop: screenHeight * 0.00
    },
    mainImage: {
    	height: screenHeight * 0.4,
		width: screenWidth * 0.5,
		alignSelf: 'center',
		marginBottom: screenHeight * 0.01,
		borderRadius: screenWidth * 0.02
    },
    answerButtonsRow: {
    	height: screenHeight * 0.15,
    	width: screenWidth * 0.95,
    	flexDirection: 'row',
    	justifyContent: 'space-around',
    	alignItems: 'center'
    },
    answerButton: {
    	height: screenHeight * 0.15,
    	width: screenWidth * 0.2,
    	justifyContent: 'center',
    	alignItems: 'center',
    	borderRadius: screenWidth * 0.01,
    	backgroundColor: color.cardBlue
    },
    answerButtonText: {
    	fontFamily: 'Futura',
    	fontSize: screenHeight * 0.03,
    	color: '#fff',
    	textAlign: 'center'
    },
    mainQuestionPlayHint: {
        position: 'absolute',
        height: screenHeight * 0.12,
        width: screenWidth * 0.12,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: screenWidth * 0.01,
        backgroundColor: color.green,
        end: screenWidth * 0.075
    },
    mainQuestionRow: {
        height: screenHeight * 0.4,
        width: screenWidth,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },

    // 3. Evaluation
    evaluationBody: {
    	height: screenHeight * 0.84,
    	width: screenWidth,
    	justifyContent: 'space-around',
    	alignItems: 'center',
    	paddingBottom: screenHeight * 0.07,
    	paddingTop: screenHeight * 0.02
    },
    evaluationHeader: {
    	height: screenHeight * 0.1,
    	width: screenWidth * 0.8,
        flexDirection: 'row',
    	justifyContent: 'space-around',
    	alignItems: 'center'
    },
    evaluationCenterRow: {
    	height: screenHeight * 0.4,
    	width: screenWidth * 0.9,
    	justifyContent: 'space-around', 
    	flexDirection: 'row',
    	alignItems: 'center'
    },
    evaluationImage: {
    	height: screenHeight * 0.3,
    	width: screenWidth * 0.4,
    	borderRadius: screenWidth * 0.01
    },
    evaluationCard: {
    	height: screenHeight * 0.3,
    	width: screenWidth * 0.4,
    	justifyContent: 'center',
    	alignItems: 'center',
    	marginBottom: 0
    },
    playHintButton: {
    	backgroundColor: color.yellow,
    	height: screenHeight * 0.2,
    	width: screenWidth * 0.3,
    	marginBottom: 0
    },
    evalutionTickContainer: {
        height: screenHeight * 0.1,
        width: screenHeight * 0.1,
        borderRadius: screenHeight * 0.05,
        backgroundColor: color.green,
        justifyContent: 'center',
        alignItems: 'center'
    },
    evalutionCrossContainer: {
        height: screenHeight * 0.1,
        width: screenHeight * 0.1,
        borderRadius: screenHeight * 0.05,
        backgroundColor: color.yellow,
        justifyContent: 'center',
        alignItems: 'center'
    },

    // 4. Reply
    additionalQBody1: {
    	height: screenHeight * 0.84,
    	width: screenWidth,
    	justifyContent: 'space-around',
    	alignItems: 'center',
    	paddingTop: screenHeight * 0.05,
    	paddingBottom: screenHeight * 0.1
    },
    additionalQBody2: {
    	height: screenHeight * 0.84,
    	width: screenWidth,
    	justifyContent: 'space-around',
    	alignItems: 'center',
        paddingTop: screenHeight * 0.05,
        paddingBottom: screenHeight * 0.1
    },
    additionalQuestionProfile: {
        height: screenHeight * 0.2,
        width: screenHeight * 0.2,
        borderRadius: screenHeight * 0.1
    },
    additionalQPrompt: {
    	height: screenHeight * 0.15,
    	width: screenWidth * 0.8,
    	justifyContent: 'center',
    	alignItems: 'center'
    },
    additionalQuestionSkip: {
        position: 'absolute',
        height: screenHeight * 0.1,
        width: screenWidth * 0.15,
        justifyContent: 'center',
        alignItems: 'center',
        end: '1%',
        bottom: '5%'
    },

    // 5. Completion
    completionBody: {
    	height: screenHeight * 0.84,
    	width: screenWidth,
    	justifyContent: 'space-around',
    	alignItems: 'center',
    	paddingBottom: screenHeight * 0.07
    },
    completionHeader: {
    	height: screenHeight * 0.3,
    	width: screenWidth * 0.8,
    	justifyContent: 'space-around',
    	alignItems: 'center'
    },
    creatorSignitureRow: {
        height: screenHeight * 0.1,
        width: screenWidth * 0.4,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    creatorSignitureProfilePicture: {
        height: screenHeight * 0.15,
        width: screenHeight * 0.15,
        borderRadius: screenHeight * 0.075,
        marginRight: screenWidth * 0.03
    },

    // 11. Main Menu
    mainMenuBlur: {
        height: screenHeight,
        width: screenWidth,
        position: 'absolute',
        backgroundColor: '#000',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingTop: screenHeight * 0.2,
        paddingBottom: screenHeight * 0.2
    },
    mainMenuLogo: {
        height: screenHeight * 0.2,
        width: screenHeight * 0.2,
        marginBottom: screenHeight * 0.05
    },
    mainMenuButton: {
        height: screenHeight * 0.12,
        width: screenWidth * 0.35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: screenWidth * 0.01,
        backgroundColor: color.green
    },
    closeIcon: {
        position: 'absolute',
        right: '5%',
        top: '5%'
    },
    mainMenuExitButton: {
        height: screenHeight * 0.12,
        width: screenWidth * 0.3,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: screenWidth * 0.01,
        backgroundColor: color.green
    }

});

export default styles;