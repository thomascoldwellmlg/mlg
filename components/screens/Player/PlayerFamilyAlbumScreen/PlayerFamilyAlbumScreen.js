import React from 'react';
import { Text,
		 View,
		 ScrollView,
		 FlatList,
		 Image,
		 TouchableOpacity,
		 Dimensions,
		 ActionSheetIOS,
		 Alert } from 'react-native';
import GridView from 'react-native-super-grid';
import styles from './PlayerFamilyAlbumScreenStyles';
import gstyles from '../../../reusable/GlobalStyles';
import { UserData,
		 MemberData,
		 FamilyData } from '../../../../lib/Firebase';
import { BlurView, ImagePicker, Camera } from 'expo';
import { color } from '../../../reusable/common';
import { Image as CacheableImage} from 'react-native-aws-cache';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

export default class PlayerFamilyAlbumScreen extends React.Component {

	state = {
		userData: {},
		familyAlbum: [],
		isEnlarged: false,
		enlargedImageIndex: 0,
		cameraEnabled: false,
		cameraDirection: Camera.Constants.Type.back,
		isRefreshing: false
	}

	async componentDidMount() {
		// Set up navigation listener to refresh screen every time
		const willFocusNavListener = this.props.navigation.addListener('willFocus', () => {
			console.log('Navigated to Family Album Screen!');
			this.getFamilyAlbum();
		});
		// Refresh family album
		this.getFamilyAlbum();
	}

	async getFamilyAlbum() {
		// Use Firebase to get user data 
		var userData = await UserData();
		this.setState({userData: userData});
		// Then using the familyId get the whole family album library
		if (userData.familyId != 'Pending') {
			var familyData = await FamilyData();
			if (familyData.familyAlbum != null) {
				var keys = Object.keys(familyData.familyAlbum);
				var familyAlbum = [];
				for (var i = keys.length - 1; i >= 0; i--) {
					var image = familyData.familyAlbum[keys[i]];
					image.index = i;
					familyAlbum.push(image);
				}
				this.setState({familyAlbum: familyAlbum});
			}
		}
	}

	async onPressAddPhotoIcon() {
		var userData = await UserData();
		if (userData.familyState != 'pending-approval') {
			// Open up IOS action sheet with options to choose where to add image from
			ActionSheetIOS.showActionSheetWithOptions({
				options: ['Choose from Camera Roll', 'Take New Photo', 'Cancel'],
				cancelButtonIndex: 3,
				destructiveButtonIndex: 2,
				tintColor: color.darkBlue
			},
			(buttonIndex) => {
				console.log(buttonIndex);
				if (buttonIndex == 0) {
					this.addNewImage('library');
				}
				else if (buttonIndex == 1) {
					this.addNewImage('camera');
				}
				else {

				}
			});
		}
		else {
			Alert.alert('Once your family admin accepts your request you will be able to access this feature!');
		}
	}

	async addNewImage(source) {
		// Now we have the source we can await for the image uri and data to be returned
		if (source == 'library') {
			// In case we naved here from the camera - disable it
			this.setState({cameraEnabled: false});
			this.props.navigation.setParams({visible: false});
			//
			var newImageData = await this.openImagePicker();
			if (newImageData.cancelled == false) {
				// Once we get the image data we pass it as a parameter and navigate to the AddPhotoScreen
				this.props.navigation.navigate('AddFamilyAlbumPhotoScreen', {data: newImageData});
			}
		}
		if (source == 'camera') {
			// The rest is handled by the onShutterPress of the camera component
			this.setState({cameraEnabled: true});
			this.props.navigation.setParams({visible: false});
		}
	}

	async openImagePicker() {
		let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: false,
            aspect: [4, 3],
            exif: true
        });
        return(result);
	}

	rotateCamera() {
		if (this.state.cameraDirection == Camera.Constants.Type.back) {
			this.setState({cameraDirection: Camera.Constants.Type.front});
		}
		else {
			this.setState({cameraDirection: Camera.Constants.Type.back});
		}
	}

	onShutterPress = async () => {
	    if (this.camera) {
	    	var newImageData = await this.camera.takePictureAsync({exif: true});
	    	this.setState({cameraEnabled: false});
	    	this.props.navigation.navigate('AddFamilyAlbumPhotoScreen', {data: newImageData});
    	}
	}

	enlargeImage(index) {
		this.setState({enlargedImageIndex: index});
		this.setState({isEnlarged: true});
		this.props.navigation.setParams({visible: false});
	}

	collapseImage() {
		this.setState({isEnlarged: false});
		this.props.navigation.setParams({visible: true});
	}

	async refreshAlbum() {
		this.setState({isRefreshing: true});
		await this.getFamilyAlbum();
		this.setState({isRefreshing: false});
	}


	async shouldActivateTitleBarButton() {
		var userData = await UserData();
		if (userData.familyState != 'pending-approval') {
			this.props.navigation.navigate('MessagingHomeScreen');
		}
		else {
			Alert.alert('Once your family admin accepts your request you will be able to access this feature!');
		}
	}

	uiTitleBar() {
		return(
			<View style={styles.titleBar}>
				<View style={gstyles.shadow}>
					<Image style={styles.logo} source={require('../../../../assets/images/Memory-Lane-Games.png')} />
				</View>
				<Text style={gstyles.largeText}>Family Album</Text>
				<TouchableOpacity style={styles.titleBarIconButton}
								  onPress={() => this.shouldActivateTitleBarButton()}>
				    <View style={gstyles.shadow}>
						<Image source={require('../../../../assets/icons/png/message.png')}
							   style={styles.titleBarIcon} />
				    </View>
			    </TouchableOpacity>
			</View>
		);
	}

	uiFamilyAlbumGrid() {
		// Check if it should render a place holder or the present content
		if (this.state.familyAlbum.length != 0 && this.state.familyAlbum.length != undefined) {
			return(
				<View>
					<GridView itemDimension={screenWidth * 0.23}
							  items={this.state.familyAlbum}
							  style={styles.familyAlbumGridContainer}
							  renderItem={(item) => <TouchableOpacity onPress={() => this.enlargeImage(item.index)}
							  	  			    					  style={styles.gridViewItem}>
									  		  			<CacheableImage style={styles.gridViewItem}
									  	  		               source={{uri: item.mainImageUrl}} />
							  		  				</TouchableOpacity>
							  }
							  onRefresh={() => this.refreshAlbum()}
	                  		  refreshing={this.state.isRefreshing}/>

				</View>
			);
		}
		else {
			return(
				<BlurView style={styles.placeholderBlur} tint='dark' intensity={90}>
					<View style={gstyles.shadow}>
						<Image style={gstyles.mediumIcon} source={require('../../../../assets/icons/png/creation.png')} />
					</View>
					<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>
						It looks like your family album is empty!
						Get your family members to add photos and they will appear here!
					</Text>
				</BlurView>
			);
		}
	}

	uiEnlargedImage() {
		if (this.state.isEnlarged == true) {
			return(
				<BlurView tint="dark" intensity={90} style={styles.blur}>
					<TouchableOpacity style={styles.closeImageCross} onPress={() => this.collapseImage()}>
						<Image style={gstyles.mediumIcon} source={require('../../../../assets/icons/png/close.png')} />
					</TouchableOpacity>
					<CacheableImage style={styles.enlargedImage} source={{uri: this.state.familyAlbum[this.state.enlargedImageIndex].mainImageUrl}} />
				</BlurView>
			);
		}
		else {
			return(null);
		}
	}

	uiCamera() {
		if (this.state.cameraEnabled == true) {
			return(
				<Camera type={this.state.cameraDirection} style={styles.camera} ref={ref => {this.camera = ref;}}>
					<BlurView style={styles.cameraActionBar} tint="dark" intensity={90}>
						<TouchableOpacity style={styles.cameraShutterButton} onPress={() => this.rotateCamera()}>
							<View style={gstyles.shadow}>
								<Image style={gstyles.smallIcon} source={require('../../../../assets/icons/png/rotate-3d.png')} />
							</View>
						</TouchableOpacity>
						<TouchableOpacity style={styles.cameraShutterButton} onPress={() => this.onShutterPress()}>
							<View style={gstyles.shadow}>
								<Image style={gstyles.largeIcon} source={require('../../../../assets/icons/png/camera-iris.png')} />
							</View>
						</TouchableOpacity>
						<TouchableOpacity style={styles.cameraShutterButton} onPress={() => this.addNewImage('library')}>
							<View style={gstyles.shadow}>
								<Image style={gstyles.smallIcon} source={require('../../../../assets/icons/png/image-multiple.png')} />
							</View>
						</TouchableOpacity>
					</BlurView>
				</Camera>
			);
		}
		else {
			return(null);
		}
	}

	render() {
		return(
			<View style={styles.container}>
				{this.uiTitleBar()}
				{this.uiFamilyAlbumGrid()}
				{this.uiEnlargedImage()}
				{this.uiCamera()}
			</View>
		);
	}

}