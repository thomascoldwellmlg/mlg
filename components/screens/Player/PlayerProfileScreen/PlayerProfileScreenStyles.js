import { StyleSheet, Dimensions} from 'react-native';
import { color } from '../../../reusable/common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

if (screenHeight < screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const styles = StyleSheet.create({

	/*
		Contents:
			0. Top Bar
			1. Cards
	*/

	container: {
		height: screenHeight * 0.88,
		width: screenWidth,
		backgroundColor: color.lightBlue,
		justifyContent: 'flex-start',
		alignItems: 'center'
	},

	// 0. Top Bar
	titleBar: {
		height: screenHeight * 0.4,
		width: screenWidth,
		justifyContent: 'space-between',
		alignItems: 'flex-start',
		flexDirection: 'row',
		backgroundColor: color.darkBlue,
		paddingLeft: screenWidth * 0.05,
		paddingRight: screenWidth * 0.05,
		paddingTop: screenHeight * 0.025,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	logo: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		resizeMode: 'contain'
	},
	titleBarMid: {
		height: screenHeight * 0.35,
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	largeProfilePicture: {
		height: screenHeight * 0.2,
		width: screenHeight * 0.2,
		borderRadius: screenHeight * 0.1
	},
	titleBarNameContainer: {
		alignItems: 'center'
	},
	titleBarIconButton: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	titleBarIcon: {
		height: screenHeight * 0.06,
		width: screenHeight * 0.06,
		resizeMode: 'contain',
		tintColor: '#fff'
	},

	// 1. Cards
	body: {
		height: screenHeight * 0.48,
		width: screenWidth,
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingTop: screenHeight * 0.03
	},
	card: {
		height: screenHeight * 0.18,
		alignItems: 'center'
	},
	cardColumn: {
		height: screenHeight * 0.14,
		width: screenWidth * 0.75
	},
	cardArrow: {
		width: screenWidth * 0.05,
		justifyContent: 'center',
		alignItems: 'center'
	}

});

export default styles;