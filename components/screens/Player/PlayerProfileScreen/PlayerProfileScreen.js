import React from 'react';
import { Text,
		 View,
		 ScrollView,
		 FlatList,
		 Image,
		 Alert,
		 TouchableOpacity } from 'react-native';
import styles from './PlayerProfileScreenStyles';
import gstyles from '../../../reusable/GlobalStyles';
import { UserData,
		 MemberData,
		 Append } from '../../../../lib/Firebase';
import { Image as CacheableImage} from 'react-native-aws-cache';

export default class PlayerProfileScreen extends React.Component {

	state = {
		userData: {}
	}

	async componentDidMount() {
		// Get and make the user data accessible for the component
		var userData = await UserData();
		this.setState({userData: userData});

		// //////TESTTTTTTTINNNNNNGGGGGG
		// await Append({familyState: 'pending-approval', pendingFamilyId: 'Coldwell2', familyId: '', name: 'Goaty McGoatFace', profilePicture: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Hausziege_04.jpg/1200px-Hausziege_04.jpg'}, 'users/goat');
		// await Append({pendingMembers: {goat: 0}}, 'families/Coldwell2');
	}

	uiTitleBar() {
		return(
			<View style={styles.titleBar}>
				<View style={gstyles.shadow}>
					<Image style={styles.logo} source={require('../../../../assets/images/Memory-Lane-Games.png')} />
				</View>
				<View style={styles.titleBarMid}>
					<View style={gstyles.shadow}>
						<CacheableImage style={styles.largeProfilePicture} source={{uri: this.state.userData.profilePicture}} />
					</View>
					<View style={styles.titleBarNameContainer}>
						<Text style={gstyles.largeText}>{this.state.userData.name}</Text>
						<Text style={gstyles.subTitle}>Game Player</Text>
					</View>
				</View>
				<TouchableOpacity style={styles.titleBarIconButton}
								  onPress={() => this.props.navigation.navigate('SettingsScreen', {navvedFrom: 'profile'})}>
				    <View style={gstyles.shadow}>
						<Image source={require('../../../../assets/icons/png/account-settings-variant.png')}
							   style={styles.titleBarIcon} />
				    </View>
			    </TouchableOpacity>
			</View>
		);
	}

	uiBody() {
		return(
			<View />
		);
	}

	render() {
		return(
			<View style={styles.container}>
				{this.uiTitleBar()}
				{this.uiBody()}
			</View>
		);
	}

}