import React from 'react';
import { Text,
		 View,
		 ScrollView,
		 FlatList,
		 Image,
		 TouchableOpacity,
		 RefreshControl,
		 Alert } from 'react-native';
import styles from './PlayerGamesScreenStyles';
import gstyles from '../../../reusable/GlobalStyles';
import { UserData,
		 MemberData } from '../../../../lib/Firebase';
import { ScreenOrientation, BlurView } from 'expo';
import { Image as CacheableImage} from 'react-native-aws-cache';

export default class PlayerGamesScreen extends React.Component {

	state = {
		userData: {},
		familyGameData: [],
		isRefreshing: false
	}

	async componentDidMount() {
		// Refresh feed
		this.getReceivedGames();
	}

	async getReceivedGames() {
		// Use Firebase to get user data 
		var userData = await UserData();
		this.setState({userData: userData});
		// Get the user's activity, fetch sender's PP and form notification data
		var receivedGames = userData.receivedGames;
		console.log(receivedGames);
		var familyGameData = [];
		if (receivedGames != undefined && receivedGames != null) {
			var gameIds = Object.keys(receivedGames);
			for (var i = gameIds.length - 1; i >= 0; i--) {
				var sentFromUID = receivedGames[gameIds[i]];
				var memberData = await MemberData(sentFromUID);
				var gameData = memberData.createdGames[gameIds[i]];
				var soleGameData = {
					gameId: gameIds[i],
					title: gameData.title,
					subTitle: 'Sent by ' + memberData.name + ' on ' + gameData.uploadDate,
					profilePicture: memberData.profilePicture,
					sentFrom: sentFromUID
				}
				familyGameData.push(soleGameData);
			}
			this.setState({familyGameData: familyGameData});
		}
	}

	async refreshGames() {
		this.setState({isRefreshing: true});
		await this.getReceivedGames();
		this.setState({isRefreshing: false});
	}

	async shouldActivateTitleBarButton() {
		var userData = await UserData();
		if (userData.familyState != 'pending-approval') {
			this.props.navigation.navigate('MessagingHomeScreen')
		}
		else {
			Alert.alert('Once your family admin accepts your request you will be able to access this feature!');
		}
	}

	uiTitleBar() {
		return(
			<View style={styles.container}>
				<View style={styles.titleBar}>
					<View style={gstyles.shadow}>
						<Image style={styles.logo} source={require('../../../../assets/images/Memory-Lane-Games.png')} />
					</View>
					<Text style={gstyles.largeText}>My Games</Text>
					<TouchableOpacity style={styles.titleBarIconButton}
									  onPress={() => this.shouldActivateTitleBarButton()}>
					    <View style={gstyles.shadow}>
							<Image source={require('../../../../assets/icons/png/message.png')}
								   style={styles.titleBarIcon} />
					    </View>
				    </TouchableOpacity>
				</View>
				{this.uiReceivedGamesBody()}
			</View>
		);
	}

	uiReceivedGamesBody() {
		// Check if it should render a place holder or the present content
		if (this.state.familyGameData.length != 0 && this.state.familyGameData.length != undefined) {
			return(
				<ScrollView style={styles.scrollView}
							refreshControl={
								<RefreshControl 
									onRefresh={() => this.refreshGames()}
	                          		refreshing={this.state.isRefreshing}
								/>
							}>
					<FlatList data={this.state.familyGameData}
							  extraData={this.state}
						      keyExtractor={(item, index) => index.toString()}
	                          renderItem={({item}) => <ListItem data={item} parent={this}/>}/>
				</ScrollView>
			);
		}
		else {
			return(
				<BlurView style={styles.placeholderBlur} tint='dark' intensity={90}>
					<View style={gstyles.shadow}>
						<Image style={gstyles.mediumIcon} source={require('../../../../assets/icons/png/creation.png')} />
					</View>
					<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>
						It looks like you haven't received any games yet!
						Get your family members to create some games and they will appear here!
					</Text>
				</BlurView>
			);
		}
	}

	render() {
		return(
			<View>
				{this.uiTitleBar()}
			</View>
		);
	}

}

class ListItem extends React.Component {

	state = {
		menuOpen: false
	}

	launchGame(gameId, creatorUid) {
		console.log(gameId, creatorUid);
		ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE);
		this.props.parent.props.navigation.navigate('MainGameActivityScreen', {gameId: gameId, creatorId: creatorUid});
	}

	uiProfilePicture() {
		return(
			<View style={styles.recipientProfileContainer}>
				<View style={gstyles.shadow}>
					<CacheableImage style={gstyles.profilePicturePortrait} source={{uri: this.props.data.profilePicture}} />
				</View>
			</View>
		);
	}

	render() {
		return(
			<TouchableOpacity style={[gstyles.cardRow, styles.menuClosedCard]}
							  onPress={() => this.setState({menuOpen: true})}>
			    {this.uiProfilePicture()}
				<View style={styles.cardRowText}>
					<Text style={gstyles.smallText}>{this.props.data.title}</Text>
					<Text style={gstyles.subTitle}>{this.props.data.subTitle}</Text>
				</View>
				<TouchableOpacity style={styles.playButton} onPress={() => this.launchGame(this.props.data.gameId, this.props.data.sentFrom)}>
					<Text style={gstyles.smallText}>Launch</Text>
				</TouchableOpacity>
			</TouchableOpacity>
		);
	}

}