import { StyleSheet, Dimensions} from 'react-native';
import { color } from '../../reusable/common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

if (screenHeight < screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const styles = StyleSheet.create({

	/*
		Contents:
			0. Title Bar
			1. Body
	*/

	// 0. Title Bar
	titleBar: {
		height: screenHeight * 0.15,
		width: screenWidth,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		backgroundColor: color.darkBlue,
		paddingLeft: screenWidth * 0.05,
		paddingRight: screenWidth * 0.05,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	titleBarIconButton: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	titleBarIcon: {
		height: screenHeight * 0.07,
		width: screenHeight * 0.07,
		resizeMode: 'contain',
		tintColor: '#fff'
	},
	confirmIcon: {
		tintColor: color.green
	},

	// 1. Body
	container: {
		height: screenHeight,
		width: screenWidth,
		backgroundColor: color.lightBlue,
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	textInput: {
		fontFamily: 'Futura',
		fontSize: 20,
		color: '#fff',
		height: screenHeight * 0.06,
		width: screenWidth * 0.4,
		borderBottomWidth: 3,
		borderColor: color.lightBlue
	},
	flatlist: {
		height: screenHeight * 0.84,
		width: screenWidth,
		paddingTop: screenHeight * 0.03
	},

	// Card Styles
	cardItem: {
		justifyContent: 'space-around',
		alignSelf: 'center'
	},

	// Picker Styles
	pickerContainer: {
		height: screenHeight * 0.06,
		width: screenWidth * 0.25,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		borderColor: color.grey,
		borderBottomWidth: screenHeight * 0.003,
		alignSelf: 'center'
	},
	picker: {
		height: screenHeight * 0.06,
		width: screenWidth * 0.25,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	pickerText: {
		fontFamily: 'Futura',
		fontSize: screenHeight * 0.02,
		color: '#fff'
	},
	pickerChevron: {
		position: 'relative',
		height: screenHeight * 0.03,
		width: screenHeight * 0.03,
		tintColor: '#444'
	},

	// Placeholder stuff
    placeholderBlur: {
    	height: screenHeight * 0.86,
    	width: screenWidth,
    	justifyContent: 'space-around',
    	alignItems: 'center',
    	backgroundColor: '#000000',
    	paddingHorizontal: screenWidth * 0.1,
    	paddingVertical: screenHeight * 0.1
    },
    placeholderActionButton: {
    	height: screenHeight * 0.12,
    	width: screenWidth * 0.3,
    	borderRadius: screenWidth * 0.01,
    	justifyContent: 'center',
    	alignItems: 'center',
    	backgroundColor: color.green
    }

});

export default styles;