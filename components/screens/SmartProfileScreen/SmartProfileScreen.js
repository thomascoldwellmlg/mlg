import React from 'react';
import { View,
		 Text,
		 TextInput,
		 Image,
		 TouchableOpacity,
		 FlatList } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import gstyles from '../../reusable/GlobalStyles';
import styles from './SmartProfileScreenStyles';
import { UserId, UserData, Append } from '../../../lib/Firebase';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BlurView } from 'expo';

export default class SmartProfileScreen extends React.Component {

	state = {
		loadedProfile: false,
		smartProfileData: [],
		profileOptions: [
			{label: 'Favourite Food', value: 'favouriteFood'},
			{label: 'Favourite Sport', value: 'favouriteSport'},
			{label: 'Favourite Place', value: 'favouritePlace'},
			{label: 'Occuptation', value: 'occupation'},
			{label: 'Home Town', value: 'homeTown'},
		]
	}

	componentDidMount() {
		this.loadSmartProfile();
	}

	async loadSmartProfile() {
		var userData = await UserData();
		if (userData.smartProfile != null) {
			var smartProfileData = [];
			var options = Object.keys(userData.smartProfile);
			for (var i = 0; i <= options.length - 1; i++) {
				var item = {
					option: options[i],
					value: userData.smartProfile[options[i]]
				}
				smartProfileData.push(item);
				this.setState({
					smartProfileData: smartProfileData,
					['option_' + i.toString()]: options[i],
					['field_' + i.toString()]: userData.smartProfile[options[i]]
				});
			}
		}
		this.setState({loadedProfile: true});
	}

	async saveSmartProfile() {
		var smartProfile = this.state.smartProfileData;
		var data = {};
		var userId = await UserId();
		const path = 'users/' + userId + '/smartProfile';
		if (smartProfile.length != undefined) {
			for (var i = 0; i <= smartProfile.length - 1; i++) {
				console.log(this.state['option_' + i.toString()], this.state['field_' + i.toString()])
				if (this.state['option_' + i.toString()] != '' && this.state['field_' + i.toString()] != '') {
					var data = {[this.state['option_' + i.toString()]]: this.state['field_' + i.toString()]};
					await Append(data, path);
				}
			}
		}
	}

	async onPressBackButton() {
		await this.saveSmartProfile();
		this.props.navigation.navigate('MyProfileScreen');
	}

	addNewField() {
		var smartProfile = this.state.smartProfileData;
		if (smartProfile.length != undefined) {
			var nextIndex = smartProfile.length;
		}
		else {
			var nextIndex = 0;
		}
		smartProfile.push({option: null, value: ''});
		this.setState({
			smartProfileData: smartProfile,
			['option_' + nextIndex]: null,
			['field_' + nextIndex]: ''
		});
	}

	selectedValue(item) {
		setTimeout(() => this.selectedValue(item), 10);
		if (this.state.loadedProfile == true) {
			return(this.state['option_' + item.index]);
		}
		else {
			return('None')
		}
	}

	uiTitleBar() {
		return(
			<View style={styles.titleBar}>
				<TouchableOpacity style={styles.titleBarIconButton}
								  onPress={() => this.onPressBackButton()}>
					<View style={gstyles.shadow}>
						<Image style={styles.titleBarIcon} source={require('../../../assets/icons/png/chevron-left.png')} />
					</View>
				</TouchableOpacity>
				<Text style={gstyles.largeText}>Smart Profile</Text>
				<TouchableOpacity style={styles.titleBarIconButton}
								  onPress={() => this.addNewField()}>
				    <View style={gstyles.shadow}>
						<Image source={require('../../../assets/icons/png/plus.png')}
							   style={[styles.titleBarIcon, styles.confirmIcon]} />
				    </View>
			    </TouchableOpacity>
			</View>
		);
	}

	uiList() {
		// Check if it should render a place holder or the present content
		if (this.state.smartProfileData.length != 0 && this.state.smartProfileData.length != undefined) {
			return(
				<KeyboardAwareScrollView extraHeight={150}>
				<FlatList data={this.state.smartProfileData}
						  style={styles.flatlist}
						  extraData={this.state}
					      keyExtractor={(item, index) => index.toString()}
	                      renderItem={(item) => <TouchableOpacity style={[gstyles.cardRow, styles.cardItem]}
																	onPress={() => this.itemRefs[item.index.toString()].focus()}>
													  <RNPickerSelect style={{viewContainer: styles.pickerContainer,
							                	    						  inputIOSContainer: styles.picker,
							                	    					      inputIOS: styles.pickerText,
							                	    						  icon: styles.pickerChevron}}
							                	    			      selectedValue={this.selectedValue(item)}
							                	    			      value={this.selectedValue(item)}
							                	    		          onValueChange={(d, i) => { this.setState({['option_' + item.index.toString()]: d})}}
						                	    					  items={this.state.profileOptions} />
												      <TextInput style={styles.textInput}
												      			 ref={(ref) => this.itemRefs = {...this.itemRefs, [item.index.toString()]: ref}}
												      			 placeholder='Enter your answer...'
							                					 value={this.state['field_' + item.index.toString()]}
		                                 					     onChangeText={(text) => this.setState({['field_' + item.index.toString()]: text})}/>
												   </TouchableOpacity>}/>
			    </KeyboardAwareScrollView>
			);
		}
		else {
			return(
				<BlurView style={styles.placeholderBlur} tint='dark' intensity={90}>
					<View style={gstyles.shadow}>
						<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/creation.png')} />
					</View>
					<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>
						It looks like you haven't added anything to your profile yet!
						To add a fact press the button below!
					</Text>
					<TouchableOpacity style={styles.placeholderActionButton}
								      onPress={() => this.addNewField()}>
						<Text style={gstyles.smallText}>Add a fact</Text>
					</TouchableOpacity>
				</BlurView>
			);
		}
	}
	
	render() {
		return(
			<View style={styles.container}>
				{this.uiTitleBar()}
				{this.uiList()}
			</View>
		);
	}

}