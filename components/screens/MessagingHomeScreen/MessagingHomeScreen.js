import React from 'react';
import { View, 
		 Text,
		 Image,
		 TouchableOpacity,
		 ScrollView,
		 FlatList,
		 RefreshControl } from 'react-native';
import gstyles from '../../reusable/GlobalStyles';
import styles from './MessagingHomeScreenStyles';
import * as firebase from 'firebase';
import { insertionSort } from '../../../lib/Custom';
import { BlurView } from 'expo';
import { UserId,
		 MemberData,
		 FamilyData,
		 UserData } from '../../../lib/Firebase';
import { Image as CacheableImage} from 'react-native-aws-cache';

export default class MessagingHomeScreen extends React.Component {

	state = {
		text: '',
		isRefreshing: false,
		messageData: [],
		newContactMenuEnabled: false,
		familyMembers: []
	}

	async componentDidMount() {
		await this.getFamilyData();
		await this.getNewMessages();
	}

	async getFamilyData() {
		// Get the PP, name and uid for each family member for the recipients flatlist
		// Ensure they have a valid family group to be able to continue
		var userData = await UserData();
		if (userData.familyId != 'Pending') {
			var familyData = await FamilyData();
			var userId = await UserId();
			var uids = Object.keys(familyData.members);
			// Remove current userId
			await uids.splice(uids.indexOf(userId), 1);
			var familyMembers = [];
			for (var i = 0; i <= uids.length - 1; i++) {
				var memberData = await MemberData(uids[i]);
				familyMembers.push({
					name: memberData.name,
					profilePicture: memberData.profilePicture,
					uid: uids[i]
				});
			}
			this.setState({familyMembers: familyMembers});
		}
	}

	// async getNewMessages() {
	// 	var userId = await UserId();
	// 	// Firebase logic to get the latest message from each party
	// 	firebase.database().ref('users/' + userId + '/messages/').on('value', async (snapshot) => {
	// 		var messages = snapshot.val();
	// 		// Check the user actually has messages
	// 		if (messages != null) {
	// 			// Go through all message recipients and add to our output data for the flatlist
	// 			// their relative information + last message information
	// 			var memberUids = Object.keys(messages);
	// 			var messageData = [];
	// 			for (var i = 0; i <= memberUids.length - 1; i++) {
	// 				var userData = await MemberData(memberUids[i]);
	// 				var userMessages = Object.keys(messages[memberUids[i]]);
	// 				var messagesLength = userMessages.length;
	// 				var latestMessage = messages[memberUids[i]][userMessages[messagesLength - 1]];
	// 				var data = {
	// 					contactUid: memberUids[i],
	// 					contactName: userData.name,
	// 					contactProfilePicture: userData.profilePicture,
	// 					latestMessageData: latestMessage,
	// 					sendTime: Object.keys(messages[memberUids[i]])[messagesLength - 1]
	// 				}
	// 				messageData.push(data);
	// 			}
	// 			// Sort array by send time, by firstly creating array of relative send times
	// 			var sendTimes = [];
	// 			for (var j = 0; j <= messageData.length - 1; j++) {
	// 				sendTimes.push(messageData[j].sendTime);
	// 			}
	// 			var sortedSendTimes = await insertionSort(sendTimes);
	// 			var sortedMessageData = [];
	// 			for (var n = 0; n <= messageData.length - 1; n++) {
	// 				for (var m = 0; m <= sortedSendTimes.length - 1; m++) {
	// 					if (messageData[n].sendTime == sortedSendTimes[m]) {
	// 						sortedMessageData.splice(m, 0, messageData[n]);
	// 					}
	// 				}
	// 			}
	// 			this.setState({messageData: sortedMessageData});
	// 		}
	// 	});
	// }

	async getNewMessages() {
		// Check the current users db for messages sent to them
		// Additionally check all of the family members db's for
		// messages sent from this user to them, if either is present
		// compare time stamps and take the latest message - form contact
		// data for flatlist
		var userId = await UserId();
		var familyData = await FamilyData();
		var familyMemberUids = Object.keys(familyData.members);
		var userIdIndex = familyMemberUids.indexOf(userId);
		for (var n = 0; n <= familyMemberUids.length - 1; n++) {
			await firebase.database().ref('users/' + familyMemberUids[n] + '/messages/').once('value').then( async (snapshot) => {
				var memberData = await MemberData(familyMemberUids[n]);
				console.log('md = >', memberData);
				var messages = snapshot.val();
				if (messages != null) {
					var contacts = Object.keys(messages);
					for (var m = 0; m <= contacts.length - 1; m++) {
						// If we see that WE are a contact of any of the family members
						// get the latest message from each
						var data = null;
						if (userId == contacts[m]) {
							var userMessages = messages[contacts[m]];
							var latestMessage = userMessages[Object.keys(userMessages)[Object.keys(userMessages).length - 1]];
							data = {
								contactUid: familyMemberUids[n],
								contactName: memberData.name,
								contactProfilePicture: memberData.profilePicture,
								latestMessageData: latestMessage,
								sendTime: Object.keys(userMessages)[Object.keys(userMessages).length - 1],
								fromSender: true
							}
							// Pass as a state for the whole component so we can check for
							// changes in the parallel self db check
							console.log('State name 1: ', familyMemberUids[n]);
							if (this.state[familyMemberUids[n] + '_message'] == undefined || this.state[familyMemberUids[n] + '_message'] == null) {
								this.setState({[familyMemberUids[n] + '_message']: data});
							}
							else {
								var oldMessageTime = parseInt(this.state[familyMemberUids[n] + '_message'].sendTime);
								var newMessageTime = parseInt(data.sendTime);
								if (newMessageTime > oldMessageTime) {
									this.setState({[familyMemberUids[n] + '_message']: data});
								}
							}
						}
					}
					this.setMessageData();
				}
			});
		}

		await firebase.database().ref('users/' + userId + '/messages/').once('value').then( async (snapshot) => {
			var messages = snapshot.val();
			if (messages != null) {
				var contacts = Object.keys(messages);
				for (var i = 0; i <= contacts.length - 1; i++) {
					var memberData = await MemberData(contacts[i]);
					console.log('md = >', memberData);
					var userMessages = messages[contacts[i]];
					var latestMessage = userMessages[Object.keys(userMessages)[Object.keys(userMessages).length - 1]];
					data = {
						contactUid: contacts[i],
						contactName: memberData.name,
						contactProfilePicture: memberData.profilePicture,
						latestMessageData: latestMessage,
						sendTime: Object.keys(userMessages)[Object.keys(userMessages).length - 1],
						fromSender: false
					}
					console.log('State name 2: ', contacts[i]);
					if (this.state[contacts[i] + '_message'] == undefined || this.state[contacts[i] + '_message'] == null) {
						this.setState({[contacts[i] + '_message']: data});
					}
					else {
						var oldMessageTime = parseInt(this.state[contacts[i] + '_message'].sendTime);
						var newMessageTime = parseInt(data.sendTime);
						if (newMessageTime > oldMessageTime) {
							this.setState({[contacts[i] + '_message']: data});
						}
					}
				}
				this.setMessageData();
			}
		});
	}

	async setMessageData() {
		// Get family member uids, check through them all to see if their message
		// state is not null, if not null then pus into message data array, set
		// message data array as the data for the flatlist

		var familyData = await FamilyData();
		var familyMemberUids = Object.keys(familyData.members);
		var messageData = [];
		for (var i = 0; i <= familyMemberUids.length - 1; i++) {
			var messageState = this.state[familyMemberUids[i] + '_message'];
			if (messageState != null) {
				messageData.push(this.state[familyMemberUids[i] + '_message']);
			}
		}
		// Sort array by send time, by firstly creating array of relative send times
		var sendTimes = [];
		for (var j = 0; j <= messageData.length - 1; j++) {
			sendTimes.push(messageData[j].sendTime);
		}
		var sortedSendTimes = await insertionSort(sendTimes);
		var sortedMessageData = [];
		for (var n = 0; n <= messageData.length - 1; n++) {
			for (var m = 0; m <= sortedSendTimes.length - 1; m++) {
				if (messageData[n].sendTime == sortedSendTimes[m]) {
					sortedMessageData.splice(m, 0, messageData[n]);
				}
			}
		}
		this.setState({messageData: sortedMessageData});
	}

	async refreshFeed() {
		this.setState({isRefreshing: true});
		await this.getNewMessages();
		this.setState({isRefreshing: false});
	}

	messageSubTitle(item) {
		if (item.latestMessageData.messageType == 'voice-message') {
			if (item.fromSender == true) {
				return('You sent you a voice message!');
			}
			else {
				var firstName = item.contactName.split(' ')[0];
				return(firstName + ' sent you a voice message!');
			}	
		}
		if (item.latestMessageData.messageType == 'additional-question-reply') {
			if (item.fromSender == true) {
				return('You replied to an additional question!');
			}
			else {
				var firstName = item.contactName.split(' ')[0];
				return(firstName + ' replied to an additional question!');
			}	
		}
		else {
			if (item.fromSender == true) {
				return('You: ' + item.latestMessageData.data);
			}
			else {
				return(item.latestMessageData.data);
			}
		}
	}

	async onPressBackArrow() {
		var userData = await UserData();
		if (userData.creatorMode == true) {
			this.props.navigation.navigate('ActivityScreen');
		}
		else {
			this.props.navigation.navigate('PlayerHomeScreen')
		}
	}

	uiTitleBar() {
		return(
			<View style={styles.titleBar}>
				<TouchableOpacity style={gstyles.shadow}
							      onPress={() => this.onPressBackArrow()}>
					<Image style={styles.titleBarIcon} source={require('../../../assets/icons/png/chevron-left.png')} />
				</TouchableOpacity>
				<Text style={gstyles.largeText}>Messages</Text>
				<TouchableOpacity style={styles.titleBarIconButton}
								  onPress={() => this.setState({newContactMenuEnabled: true})}>
				    <View style={gstyles.shadow}>
						<Image source={require('../../../assets/icons/png/square-edit-outline.png')}
							   style={styles.titleBarIcon} />
				    </View>
			    </TouchableOpacity>
			</View>
		);
	}

	uiMessagesBody() {
		// Check if it should render a place holder or the present content
		if (this.state.messageData.length != 0 && this.state.messageData.length != undefined) {
			return(
				<ScrollView style={styles.scrollView}>
					<FlatList data={this.state.messageData}
							  extraData={this.state}
						      keyExtractor={(item, index) => index.toString()}
						      inverted={true}
	                          renderItem={({item}) => <TouchableOpacity style={gstyles.cardRow}
	                          											onPress={() => this.props.navigation.navigate('MessagingChatScreen', {contactUid: item.contactUid})}>
													      <View style={gstyles.shadow}>
														      <CacheableImage style={gstyles.profilePicturePortrait} source={{uri: item.contactProfilePicture}} />
														  </View>
														  <View style={styles.cardRowText}>
														      <Text style={gstyles.smallText}>{item.contactName}</Text>
															  <Text style={gstyles.subTitle} numberOfLines={1}>{this.messageSubTitle(item)}</Text>
														  </View>
														  <View style={styles.cardRowTimestamps}>
														      <Text style={gstyles.subTitle}>{item.latestMessageData.sentTime}</Text>
															  <Text style={gstyles.subTitle}>{item.latestMessageData.sentDate}</Text>
														  </View>
													  </TouchableOpacity>}/>
				</ScrollView>
			);
		}
		else {
			return(
				<BlurView style={styles.placeholderBlur} tint='dark' intensity={90}>
					<View style={gstyles.shadow}>
						<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/creation.png')} />
					</View>
					<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>
						It looks like you haven't messaged anyone yet!
						To create a message press the button below!
					</Text>
					<TouchableOpacity style={styles.placeholderActionButton}
								      onPress={() => this.setState({newContactMenuEnabled: true})}>
						<Text style={[gstyles.smallText, {textAlign: 'center'}]}>Create New Message</Text>
					</TouchableOpacity>
				</BlurView>
			);
		}
	}

	uiNewContactMenu() {
		if (this.state.newContactMenuEnabled == true) {
			// Check if it should render a place holder or the present content
			if (this.state.familyMembers.length != 0 && this.state.familyMembers.length != undefined) {
				return(
					<BlurView style={styles.newContactBlur} tint='dark' intensity={90}>
						<View style={[gstyles.cardColumn, styles.recipientCard]}>
							<FlatList data={this.state.familyMembers}
								      keyExtractor={(item, index) => index.toString()}
								      extraData={this.state}
			                          renderItem={(item) => <TouchableOpacity style={styles.memberRow}
		                              										  onPress={() => this.props.navigation.navigate('MessagingChatScreen', {contactUid: item.item.uid})}>
		                              							<View style={gstyles.shadow}>
		                              								<CacheableImage style={styles.profilePictures} source={{uri: item.item.profilePicture}} />
		                              							</View>
		                              							<Text style={gstyles.smallText}>{item.item.name}</Text>
		                              							<Image style={gstyles.smallIcon} source={require('../../../assets/icons/png/square-edit-outline.png')} />
									                	    </TouchableOpacity>}/>
	            	    </View>
						<TouchableOpacity style={styles.closeImageCross} onPress={() => this.setState({newContactMenuEnabled: false})}>
							<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/close.png')} />
						</TouchableOpacity>
					</BlurView>
				);
			}
			else {
				return(
					<BlurView style={styles.newContactBlur} tint='dark' intensity={90}>
						<View style={[gstyles.cardColumn, styles.recipientCard]}>
							<Text style={[gstyles.smallText, {textAlign: 'center'}]}>
								No family members found! Add new family members to
								begin messaging!
							</Text>
	            	    </View>
						<TouchableOpacity style={styles.closeImageCross} onPress={() => this.setState({newContactMenuEnabled: false})}>
							<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/close.png')} />
						</TouchableOpacity>
					</BlurView>
				);
			}
		}
	}

	render() {
		return(
			<View style={styles.container}>
				{this.uiTitleBar()}
				{this.uiMessagesBody()}
				{this.uiNewContactMenu()}
			</View>
		);
	}

}