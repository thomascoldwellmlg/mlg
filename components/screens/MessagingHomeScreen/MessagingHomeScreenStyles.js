import { StyleSheet, Dimensions} from 'react-native';
import { color } from '../../reusable/common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

// Ensure style sheet is given as portrait
if (screenHeight < screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const styles = StyleSheet.create({

	/*
		Contents:
			0. Title Bar
			1. Body
			2. Add Contact
	*/

	// 0. Title Bar
	titleBar: {
		height: screenHeight * 0.15,
		width: screenWidth,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		backgroundColor: color.darkBlue,
		paddingLeft: screenWidth * 0.05,
		paddingRight: screenWidth * 0.05,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	logo: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		resizeMode: 'contain'
	},
	titleBarIconButton: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	titleBarIcon: {
		height: screenHeight * 0.06,
		width: screenHeight * 0.06,
		resizeMode: 'contain',
		tintColor: '#fff'
	},

	// 1. Body
	container: {
		height: screenHeight,
		width: screenWidth,
		backgroundColor: color.lightBlue,
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	cardRowText: {
		marginLeft: screenWidth * 0.05,
		width: screenWidth * 0.47,
	},
	scrollView: {
		paddingTop: screenHeight * 0.03
	},
	cardRowTimestamps: {
		width: screenWidth * 0.2,
		marginLeft: screenWidth * 0.05
	},

	// 2. Add Contacts
    newContactBlur: {
    	height: screenHeight,
		width: screenWidth,
		position: 'absolute',
		backgroundColor: '#000',
		justifyContent: 'center',
		alignItems: 'center',
		paddingTop: screenHeight * 0.05
    },
    recipientCard: {
    	height: screenHeight * 0.6,
    	width: screenWidth * 0.7,
    	alignItems: 'center'
    },
    memberRow: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.6,
		alignSelf: 'center',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	profilePictures: {
		height: screenHeight * 0.08,
		width: screenHeight * 0.08,
		borderRadius: screenHeight * 0.04
	},
	closeImageCross: {
    	position: 'absolute',
    	end: '3%',
    	top: '2%'
    },

    // Placeholder stuff
    placeholderBlur: {
    	height: screenHeight * 0.86,
    	width: screenWidth,
    	justifyContent: 'space-around',
    	alignItems: 'center',
    	backgroundColor: '#000000',
    	paddingHorizontal: screenWidth * 0.1,
    	paddingVertical: screenHeight * 0.1
    },
    placeholderActionButton: {
    	height: screenHeight * 0.12,
    	width: screenWidth * 0.3,
    	borderRadius: screenWidth * 0.01,
    	justifyContent: 'center',
    	alignItems: 'center',
    	backgroundColor: color.green,
    	paddingHorizontal: screenWidth * 0.01
    }

});

export default styles;