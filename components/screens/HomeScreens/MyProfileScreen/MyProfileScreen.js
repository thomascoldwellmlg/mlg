import React from 'react';
import { Text,
		 View,
		 ScrollView,
		 FlatList,
		 Image,
		 Alert,
		 TouchableOpacity } from 'react-native';
import styles from './MyProfileScreenStyles';
import gstyles from '../../../reusable/GlobalStyles';
import { UserData,
		 UserId,
		 MemberData,
		 Append } from '../../../../lib/Firebase';
import { UploadImage } from '../../../../lib/AWS';
import {Image as CacheableImage} from 'react-native-aws-cache';
import { BlurView, ImagePicker } from 'expo';

export default class MyProfileScreen extends React.Component {

	state = {
		userData: {},
		profilePicture: {uri: ''},
		allowProfilePictureUpdate: false
	}

	async componentDidMount() {
		// Get and make the user data accessible for the component
		var userData = await UserData();
		console.log(userData)
		this.setState({userData: userData});
		this.setState({profilePicture: {uri: userData.profilePicture}});
		// //////TESTTTTTTTINNNNNNGGGGGG
		// await Append({familyState: 'pending-approval', pendingFamilyId: 'Coldwell2', familyId: '', name: 'Goaty McGoatFace', profilePicture: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Hausziege_04.jpg/1200px-Hausziege_04.jpg'}, 'users/goat');
		// await Append({pendingMembers: {goat: 0}}, 'families/Coldwell2');
	}

	async shouldOpenMyFamilyGroup() {
		var userData = await UserData();
		if (userData.familyState != 'pending-approval') {
			this.props.navigation.navigate('MyFamilyGroupScreen');
		}
		else {
			Alert.alert('Once your family admin accepts your request you will be able to access this feature!');
		}
	}

	async changeProfilePicture() {
		this.setState({allowProfilePictureUpdate: true});
		let photo = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: false,
            aspect: [4, 3],
            exif: true
        });
        if (photo.cancelled != true) {
        	const currentUser = await UserId();
        	const uri = photo.uri;
        	const name = 'profilePicture';
        	const path = 'users/' + currentUser + '/';
        	var profilePictureUrl = await UploadImage(uri, name, path);
        	await Append({profilePicture: profilePictureUrl}, path);
        	this.setState({profilePicture: {uri: profilePictureUrl}});
        }
        this.setState({allowProfilePictureUpdate: false});
	}

	uiTitleBar() {
		return(
			<View style={styles.titleBar}>
				<View style={gstyles.shadow}>
					<Image style={styles.logo} source={require('../../../../assets/images/Memory-Lane-Games.png')} />
				</View>
				<View style={styles.titleBarMid}>
					<View style={gstyles.shadow}>
						<View style={styles.largeProfilePicture}>
							<CacheableImage style={styles.largeProfilePicture} source={this.state.profilePicture} allowsUpdate={this.state.allowProfilePictureUpdate}/>
							<TouchableOpacity style={styles.profilePictureOverlay}
										      onPress={() => this.changeProfilePicture()}>
								<BlurView style={styles.profilePictureBlur} intensity={90} tint='dark'>
									<Text style={gstyles.subTitle}>Edit</Text>
								</BlurView>
							</TouchableOpacity>
						</View>
					</View>
					<View style={styles.titleBarNameContainer}>
						<Text style={gstyles.largeText}>{this.state.userData.name}</Text>
						<Text style={gstyles.subTitle}>Game Creator</Text>
					</View>
				</View>
				<TouchableOpacity style={styles.titleBarIconButton}
								  onPress={() => this.props.navigation.navigate('SettingsScreen', {navvedFrom: 'profile'})}>
				    <View style={gstyles.shadow}>
						<Image source={require('../../../../assets/icons/png/account-settings-variant.png')}
							   style={styles.titleBarIcon} />
				    </View>
			    </TouchableOpacity>
			</View>
		);
	}

	uiBody() {
		return(
			<View style={styles.body}>
				<TouchableOpacity style={[gstyles.cardRow, styles.card]}
							      onPress={() => this.props.navigation.navigate('SmartProfileScreen')}>
			        <View style={styles.cardColumn}>
			        	<View>
			        		<Text style={gstyles.smallText}>Smart Profile</Text>
			        		<Text style={gstyles.subTitle}>Tell us some key facts about you to help build games!</Text>
		        		</View>
			        </View>
			        <View style={styles.cardArrow}>
			        	<Image style={gstyles.mediumIcon} source={require('../../../../assets/icons/png/chevron-right.png')} />
			        </View>
				</TouchableOpacity>
				<TouchableOpacity style={[gstyles.cardRow, styles.card]}
							      onPress={() => this.shouldOpenMyFamilyGroup()}>
			        <View style={styles.cardColumn}>
			        	<View>
			        		<Text style={gstyles.smallText}>My Family Group</Text>
			        		<Text style={gstyles.subTitle}>View and manage your family group!</Text>
		        		</View>
			        </View>
			        <View style={styles.cardArrow}>
			        	<Image style={gstyles.mediumIcon} source={require('../../../../assets/icons/png/chevron-right.png')} />
			        </View>
				</TouchableOpacity>
			</View>
		);
	}

	render() {
		return(
			<View style={styles.container}>
				{this.uiTitleBar()}
				{this.uiBody()}
			</View>
		);
	}

}