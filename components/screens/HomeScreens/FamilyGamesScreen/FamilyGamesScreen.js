import React from 'react';
import { Text,
		 View,
		 ScrollView,
		 FlatList,
		 Image,
		 TouchableOpacity,
		 RefreshControl } from 'react-native';
import styles from './FamilyGamesScreenStyles';
import gstyles from '../../../reusable/GlobalStyles';
import { UserData,
		 MemberData } from '../../../../lib/Firebase';
import Expo, { ScreenOrientation, BlurView, FileSystem } from 'expo';
import { Image as CacheableImage} from 'react-native-aws-cache';

const DEMO_DIR = `${FileSystem.documentDirectory}mlgDemoModeAssets/`;

export default class FamilyGamesScreen extends React.Component {

	state = {
		userData: {},
		familyGameData: [],
		isRefreshing: false,
		demoMode: null
	}

	async componentDidMount() {
		// Check if we are in demo mode
		await this.checkIfInDemoMode();
		// Refresh feed
		await this.getReceivedGames();
	}

	async checkIfInDemoMode() {
		var demoMode = await Expo.SecureStore.getItemAsync('demo-mode');
		if (demoMode == 'true') {
			console.log('demoMode ==== > ', demoMode)
			this.setState({demoMode: true});
		}
	}

	async getReceivedGames() {
		// Check whether we are in demo mode or not
		if (this.state.demoMode != true) {
			// Use Firebase to get user data 
			var userData = await UserData();
			this.setState({userData: userData});
			// Get the user's activity, fetch sender's PP and form notification data
			var receivedGames = userData.receivedGames;
			console.log(receivedGames);
			var familyGameData = [];
			if (receivedGames != undefined && receivedGames != null) {
				var gameIds = Object.keys(receivedGames);
				for (var i = gameIds.length - 1; i >= 0; i--) {
					var sentFromUID = receivedGames[gameIds[i]];
					var memberData = await MemberData(sentFromUID);
					var gameData = memberData.createdGames[gameIds[i]];
					var soleGameData = {
						gameId: gameIds[i],
						title: gameData.title,
						subTitle: 'Sent by ' + memberData.name + ' on ' + gameData.uploadDate,
						profilePicture: memberData.profilePicture,
						sentFrom: sentFromUID
					}
					familyGameData.push(soleGameData);
				}
				this.setState({familyGameData: familyGameData});
			}
		}
		else {
			// Use demo manager to get user data
			var demoData = await FileSystem.readAsStringAsync(DEMO_DIR + 'manager.json');
			demoData = JSON.parse(demoData);
			// The default user is John Miller (Hr3ApcPo2UWl1jcPjFPpozDj9fr2) in demo mode
			var userData = demoData.users.Hr3ApcPo2UWl1jcPjFPpozDj9fr2;
			this.setState({userData: userData});
			// Get the user's activity, fetch sender's PP and form notification data
			var receivedGames = userData.receivedGames;
			console.log(receivedGames);
			var familyGameData = [];
			if (receivedGames != undefined && receivedGames != null) {
				var gameIds = Object.keys(receivedGames);
				for (var i = gameIds.length - 1; i >= 0; i--) {
					var sentFromUID = receivedGames[gameIds[i]];
					var memberData = demoData.users[sentFromUID];
					var gameData = memberData.createdGames[gameIds[i]];
					var soleGameData = {
						gameId: gameIds[i],
						title: gameData.title,
						subTitle: 'Sent by ' + memberData.name + ' on ' + gameData.uploadDate,
						profilePicture: memberData.profilePicture,
						sentFrom: sentFromUID
					}
					familyGameData.push(soleGameData);
				}
				this.setState({familyGameData: familyGameData});
			}
		}
	}

	async refreshGames() {
		this.setState({isRefreshing: true});
		await this.getReceivedGames();
		this.setState({isRefreshing: false});
	}

	uiTitleBar() {
		return(
			<View style={styles.container}>
				<View style={styles.titleBar}>
					<View style={gstyles.shadow}>
						<Image style={styles.logo} source={require('../../../../assets/images/Memory-Lane-Games.png')} />
					</View>
					<Text style={gstyles.largeText}>Received Games</Text>
					<TouchableOpacity style={styles.titleBarIconButton}
									  onPress={() => this.props.navigation.navigate('CreatedGamesScreen')}>
					    <View style={gstyles.shadow}>
							<Image source={require('../../../../assets/icons/png/brush.png')}
								   style={styles.titleBarIcon} />
					    </View>
				    </TouchableOpacity>
				</View>
				{this.uiReceivedGamesBody()}
			</View>
		);
	}

	uiReceivedGamesBody() {
		// Check if it should render a place holder or the present content
		if (this.state.familyGameData.length != 0 && this.state.familyGameData.length != undefined) {
			return(
				<ScrollView style={styles.scrollView}
							refreshControl={
								<RefreshControl 
									onRefresh={() => this.refreshGames()}
	                          		refreshing={this.state.isRefreshing}
								/>
							}>
					<FlatList data={this.state.familyGameData}
							  extraData={this.state}
						      keyExtractor={(item, index) => index.toString()}
	                          renderItem={({item}) => <ListItem data={item} parent={this}/>}/>
				</ScrollView>
			);
		}
		else {
			return(
				<BlurView style={styles.placeholderBlur} tint='dark' intensity={90}>
					<View style={gstyles.shadow}>
						<Image style={gstyles.mediumIcon} source={require('../../../../assets/icons/png/creation.png')} />
					</View>
					<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>
						It looks like you haven't received a game yet!
						Get a family member to create and send you a game!
					</Text>
				</BlurView>
			);
		}
	}

	render() {
		return(
			<View>
				{this.uiTitleBar()}
			</View>
		);
	}

}

class ListItem extends React.Component {

	state = {
		menuOpen: false
	}

	launchGame(gameId, creatorUid) {
		console.log(gameId, creatorUid);
		ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE);
		this.props.parent.props.navigation.navigate('MainGameActivityScreen', {gameId: gameId, creatorId: creatorUid, demoMode: this.props.parent.state.demoMode});
	}

	uiProfilePicture() {
		if (this.props.parent.state.demoMode != true) {
			return(
				<View style={styles.recipientProfileContainer}>
					<View style={gstyles.shadow}>
						<CacheableImage style={gstyles.profilePicturePortrait} source={{uri: this.props.data.profilePicture}} />
					</View>
				</View>
			);
		}
		else {
			return(
				<View style={styles.recipientProfileContainer}>
					<View style={gstyles.shadow}>
						<Image style={gstyles.profilePicturePortrait} source={{uri: this.props.data.profilePicture}} />
					</View>
				</View>
			);
		}
	}

	render() {
		return(
			<TouchableOpacity style={[gstyles.cardRow, styles.menuClosedCard]}
							  onPress={() => this.setState({menuOpen: true})}>
			    {this.uiProfilePicture()}
				<View style={styles.cardRowText}>
					<Text style={gstyles.smallText}>{this.props.data.title}</Text>
					<Text style={gstyles.subTitle}>{this.props.data.subTitle}</Text>
				</View>
				<TouchableOpacity style={styles.playButton} onPress={() => this.launchGame(this.props.data.gameId, this.props.data.sentFrom)}>
					<Text style={gstyles.smallText}>Launch</Text>
				</TouchableOpacity>
			</TouchableOpacity>
		);
	}

}