import { StyleSheet, Dimensions} from 'react-native';
import { color } from '../../../reusable/common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

if (screenHeight < screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const styles = StyleSheet.create({

	/*
		Contents:
			0. Title Bar
			1. Body
	*/

	// 0. Title Bar
	titleBar: {
		height: screenHeight * 0.15,
		width: screenWidth,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		backgroundColor: color.darkBlue,
		paddingLeft: screenWidth * 0.05,
		paddingRight: screenWidth * 0.05,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	logo: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		resizeMode: 'contain'
	},
	titleBarIconButton: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	titleBarIcon: {
		height: screenHeight * 0.06,
		width: screenHeight * 0.06,
		resizeMode: 'contain',
		tintColor: '#fff'
	},

	// 1. Body
	container: {
		height: screenHeight * 0.88,
		width: screenWidth,
		backgroundColor: color.lightBlue,
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	scrollView: {
		paddingTop: screenHeight * 0.03
	},
	cardRowText: {
		marginLeft: screenWidth * 0.05
	},
	greetingIcon: {
		marginLeft: screenWidth * 0.05
	},
	cardRowHeader: {
		height: screenHeight * 0.15,
        width: screenWidth * 0.85,
        backgroundColor: color.cardBlue,
        flexDirection: 'row',
        alignItems: 'center'
	},
	cardExpanded: {
		height: screenHeight * 0.35,
		justifyContent: 'space-around',

	},
	menuClosedCard: {
		paddingLeft: 0.0,
		paddingRight: screenWidth * 0.03,
		justifyContent: 'space-around' 
	},
	recipientProfileContainer: {
		height: screenHeight * 0.09,
		width: screenWidth * 0.09,
		justifyContent: 'center',
		alignItems: 'center',
		paddingLeft: screenWidth * 0.07
	},
	playButton: {
		height: screenHeight * 0.08,
		width: screenWidth * 0.18,
		borderRadius: screenWidth * 0.01,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.green,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},

	// Placeholder stuff
    placeholderBlur: {
    	height: screenHeight * 0.74,
    	width: screenWidth,
    	justifyContent: 'space-around',
    	alignItems: 'center',
    	backgroundColor: '#000000',
    	paddingHorizontal: screenWidth * 0.1,
    	paddingVertical: screenHeight * 0.1
    },
    placeholderActionButton: {
    	height: screenHeight * 0.12,
    	width: screenWidth * 0.3,
    	borderRadius: screenWidth * 0.01,
    	justifyContent: 'center',
    	alignItems: 'center',
    	backgroundColor: color.green
    }

});

export default styles;