import { StyleSheet, Dimensions} from 'react-native';
import { color } from '../../../reusable/common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

if (screenHeight < screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const styles = StyleSheet.create({

	/*
		Contents:
			0. Title Bar
			1. Body
			2. Preview
			3. Add Recipients
	*/

	// 0. Title Bar
	titleBar: {
		height: screenHeight * 0.15,
		width: screenWidth,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		backgroundColor: color.darkBlue,
		paddingLeft: screenWidth * 0.05,
		paddingRight: screenWidth * 0.05,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	logo: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		resizeMode: 'contain'
	},
	titleBarIconButton: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	titleBarIcon: {
		height: screenHeight * 0.06,
		width: screenHeight * 0.06,
		resizeMode: 'contain',
		tintColor: '#fff'
	},

	// 1. Body
	container: {
		height: screenHeight,
		width: screenWidth,
		backgroundColor: color.lightBlue,
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	scrollView: {
		paddingTop: screenHeight * 0.03
	},
	cardRowText: {
		marginLeft: screenWidth * 0.05,
		width: screenWidth * 0.55,
		height: screenHeight * 0.07,
		justifyContent: 'space-between'
	},
	greetingIcon: {
		marginLeft: screenWidth * 0.05
	},
	cardRowHeader: {
		height: screenHeight * 0.15,
        width: screenWidth * 0.9,
        backgroundColor: color.cardBlue,
        flexDirection: 'row',
        paddingRight: screenWidth * 0.05,
        alignItems: 'center'
	},
	cardExpanded: {
		height: screenHeight * 0.35,
		justifyContent: 'space-around',

	},
	menuClosedCard: {
		paddingLeft: 0.0
	},
	recipientProfileContainer: {
		position: 'absolute',
		end: screenWidth * 0.06,
		height: screenHeight * 0.09,
		width: screenWidth * 0.19,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	profilePicture1: {
		position: 'absolute',
		start: 0
	},
	profilePicture2: {
		position: 'absolute',
		end: 0
	},
	menuRow: {
		height: screenHeight * 0.18,
		width: screenWidth * 0.9,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	menuItem: {
		height: screenHeight * 0.18,
		width: screenWidth * 0.225,
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingTop: screenHeight * 0.03,
		paddingBottom: screenHeight * 0.03
	},
	openMenuDividingBar: {
		height: screenHeight * 0.001,
		width: screenWidth * 0.8,
		backgroundColor: color.lightBlue,
		marginVertical: screenHeight * 0.02
	},

	// 2. Preview
	previewBlur: {
    	height: screenHeight,
		width: screenWidth,
		position: 'absolute',
		backgroundColor: '#000',
		justifyContent: 'center',
		alignItems: 'center',
		paddingTop: screenHeight * 0.05
    },
    closeImageCross: {
    	position: 'absolute',
    	end: '3%',
    	top: '2%'
    },
    previewContainer: {
    	height: screenWidth * 0.56,
    	width: screenWidth * 0.75,
    	alignItems: 'center',
    	backgroundColor: color.lightBlue,
    	marginBottom: screenHeight * 0.05
    },
    previewNavBar: {
    	height: screenHeight * 0.09,
    	width: screenWidth * 0.75,
    	flexDirection: 'row',
    	justifyContent: 'space-between',
    	alignItems: 'center',
    	backgroundColor: color.darkBlue,
    	paddingLeft: screenWidth * 0.03,
    	paddingRight: screenWidth * 0.03,
    	marginBottom: screenHeight * 0.02
    },
    previewLogo: {
    	height: screenHeight * 0.06,
    	width: screenHeight * 0.06
    },
    previewImage: {
    	height: screenHeight * 0.15,
		width: screenWidth * 0.35,
		alignSelf: 'center',
		marginBottom: screenHeight * 0.01,
		borderRadius: screenWidth * 0.02,
		marginTop: screenHeight * 0.01,
		marginBottom: screenHeight * 0.02
    },
    previewButtonRow: {
    	height: screenHeight * 0.08,
    	width: screenWidth * 0.7,
    	flexDirection: 'row',
    	justifyContent: 'space-around',
    	alignItems: 'center'
    },
    previewButton: {
    	height: screenHeight * 0.08,
    	width: screenWidth * 0.15,
    	justifyContent: 'center',
    	alignItems: 'center',
    	borderRadius: screenWidth * 0.01,
    	backgroundColor: color.cardBlue
    },
    previewButtonText: {
    	fontFamily: 'Futura',
    	fontSize: screenHeight * 0.015,
    	color: '#fff',
    	textAlign: 'center'
    },

    // 3. Add Recipients
    recipientsBlur: {
    	height: screenHeight,
		width: screenWidth,
		position: 'absolute',
		backgroundColor: '#000',
		justifyContent: 'center',
		alignItems: 'center',
		paddingTop: screenHeight * 0.05
    },
    recipientCard: {
    	height: screenHeight * 0.6,
    	width: screenWidth * 0.7,
    	alignItems: 'center'
    },
    memberRow: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.6,
		alignSelf: 'center',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	profilePictures: {
		height: screenHeight * 0.08,
		width: screenHeight * 0.08,
		borderRadius: screenHeight * 0.04
	},
	unselectedRecipient: {
		height: screenHeight * 0.07,
		width: screenHeight * 0.07,
		tintColor: color.grey,
		opacity: 0.7
	},
	selectedRecipient: {
		height: screenHeight * 0.07,
		width: screenHeight * 0.07,
		tintColor: color.green
	},
	recipientSendButton: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.35,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: screenWidth * 0.01,
		backgroundColor: color.yellow
	},

	// Placeholder stuff
    placeholderBlur: {
    	height: screenHeight * 0.74,
    	width: screenWidth,
    	justifyContent: 'space-around',
    	alignItems: 'center',
    	backgroundColor: '#000000',
    	paddingHorizontal: screenWidth * 0.1,
    	paddingVertical: screenHeight * 0.1
    },
    placeholderActionButton: {
    	height: screenHeight * 0.12,
    	width: screenWidth * 0.3,
    	borderRadius: screenWidth * 0.01,
    	justifyContent: 'center',
    	alignItems: 'center',
    	backgroundColor: color.green
    }

});

export default styles;