import React from 'react';
import { Text,
		 View,
		 ScrollView,
		 FlatList,
		 Image,
		 TouchableOpacity,
		 Alert,
		 Dimensions } from 'react-native';
import styles from './CreatedGamesScreenStyles';
import gstyles from '../../../reusable/GlobalStyles';
import { UserData,
		 MemberData,
		 FamilyData,
		 Append,
		 UserId } from '../../../../lib/Firebase';
import Expo, { ScreenOrientation, BlurView, FileSystem } from 'expo';
import { color } from '../../../reusable/common';
import { Image as CacheableImage} from 'react-native-aws-cache';

const DEMO_DIR = `${FileSystem.documentDirectory}mlgDemoModeAssets/`;

const testFeedData = {
	createdGames: {
		gamerhvrgb4u5ti598: {
			title: 'Family Holiday 2017',
			subTitle: 'Hey mum and Dad hope everything is...',
			recipientIds: {
				0: 'hl557wopfHXMltgApr9fD5wTdlJ2',
				1: 'hl557wopfHXMltgApr9fD5wTdlJ2'
			}
		},
		game2vfjgreigh4g84: {
			title: 'Family Holiday 2017',
			subTitle: 'Hey mum and Dad hope everything is...',
			recipientIds: {
				0: 'hl557wopfHXMltgApr9fD5wTdlJ2'
			}
		},
	}
}

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

export default class CreatedGamesScreen extends React.Component {

	state = {
		userData: {},
		createdGamesData: {},
		createdGames: '',
		previewData: [],
		familyMembers: [],
		enableAddRecipients: false,
		selectedGame: '',
		demoMode: null
	}

	async componentDidMount() {
		ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT);
		// Check if we are in demo mode
		var demoMode = await Expo.SecureStore.getItemAsync('demo-mode');
		if (demoMode == 'true') {
			console.log('demoMode ==== > ', demoMode)
			this.setState({demoMode: true});
		}
		// Refresh feed
		this.getCreatedGames();
		if (this.state.demoMode == null) {
			await this.getFamilyData();
			console.log(this.state.familyMembers);
		}
	}

	async getCreatedGames() {
		console.log('Getting created Games');
		// Check whether we are in demo mode if so pull from the manager json file else retrieve from firebase
		if (this.state.demoMode == null) {
			// Use Firebase to get user data 
			var userData = await UserData();
			this.setState({userData: userData});
			console.log(userData);
			// Get the user's activity, fetch sender's PP and form notification data
			var createdGames = userData.createdGames;
			this.setState({createdGames: createdGames});
			console.log(createdGames);
			if (createdGames != undefined) {
				var gameKeys = Object.keys(createdGames);
				var feedData = [];
				for (var i = 0; i <= gameKeys.length - 1; i++) {
					var recipientData = [];
					if (createdGames[gameKeys[i]].recipients != undefined) {
						var recipientKeys = createdGames[gameKeys[i]].recipients;
						for (var j = 0; j <= recipientKeys.length - 1; j++) {
							var soleRecipientData = await MemberData(createdGames[gameKeys[i]].recipients[j]);
							recipientData.push(soleRecipientData.profilePicture);
						}
					}
					else {
						recipientData.push(userData.profilePicture);
					}
					feedData.push({
						gameId: gameKeys[i],
						title: createdGames[gameKeys[i]].title,
						subTitle: createdGames[gameKeys[i]].attachedMessage,
						recipientProfilePictures: recipientData
					});
					recipientData = [];
				}
				await this.setState({createdGamesData: feedData});
				console.log('created game data = ', this.state.createdGamesData);
			}
			else {
				this.setState({createdGamesData: []});
			}
		}
		else {
			// Load the data in from the demo mode json manager file - loading as if you are John Miller (Hr3ApcPo2UWl1jcPjFPpozDj9fr2)
			var demoData = await FileSystem.readAsStringAsync(DEMO_DIR + 'manager.json');
			demoData = JSON.parse(demoData);
            var createdGames = demoData.users.Hr3ApcPo2UWl1jcPjFPpozDj9fr2.createdGames;
            if (createdGames != null) {
	            this.setState({createdGames: createdGames});
	            var feedData = [];
	            for (var i = 0; i <= Object.keys(createdGames).length - 1; i++) {
	            	var key = Object.keys(createdGames)[i];
	            	var gameData = createdGames[key];
	            	var recipientProfilePictures = [];
	            	for (var j = 0; j <= gameData.recipients.length - 1; j++) {
	            		recipientProfilePictures.push(
	            			demoData.users[gameData.recipients[j]].profilePicture
            			);
	            	}
	            	feedData.push({
	            		gameId: key,
	            		title: createdGames[key].title,
	            		subTitle: createdGames[key].attachedMessage,
	            		recipientProfilePictures: recipientProfilePictures
	            	});
	            }
	            this.setState({createdGamesData: feedData});
            }
            else {
            	this.setState({createdGamesData: []});
            }
		}
	}

	async getFamilyData() {
		// Get the PP, name and uid for each family member for the recipients flatlist
		// Ensure they have a valid family group to be able to continue
		var userData = await UserData();
		if (userData.familyId != 'Pending') {
			var familyData = await FamilyData();
			var userId = await UserId();
			var uids = Object.keys(familyData.members);
			// Remove current userId
			await uids.splice(uids.indexOf(userId), 1);
			var familyMembers = [];
			for (var i = 0; i <= uids.length - 1; i++) {
				var memberData = await MemberData(uids[i]);
				familyMembers.push({
					name: memberData.name,
					profilePicture: memberData.profilePicture,
					uid: uids[i],
					selected: false,
					styleStatus: {
						height: screenHeight * 0.07,
						width: screenHeight * 0.07,
						tintColor: color.grey,
						opacity: 0.7
					}
				});
			}
			this.setState({familyMembers: familyMembers});
		}
	}

	async createNewGame() {
		// Check first whether we are in demo mode or not
		if (this.state.demoMode != true) {
			// Ensure they have a valid family group to be able to continue
			var userData = await UserData();
			if (userData.familyState != 'pending-approval') {
				await ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE);
				this.props.navigation.navigate('CreateGameScreen');
			}
			else {
				Alert.alert('Once your family admin accepts your request you will be able to access this feature!');
			}
		}
		else {
			await ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE);
			this.props.navigation.navigate('CreateGameScreen', {demoMode: true});
		}
	}

	async previewGame(gameId) {
		var gameIds = Object.keys(this.state.createdGames);
		for (var i = 0; i <= gameIds.length - 1; i++) {
			console.log('gameIds', gameId, gameIds[i])
			if (gameIds[i] == gameId) {
				await this.setState({previewData: this.state.createdGames[gameIds[i]].questions});
				console.log('preview', this.state.previewData)
			}
		}
		this.setState({enablePreview: true});
		this.props.navigation.setParams({visible: false});
	}

	closePreview() {
		this.setState({enablePreview: false});
		this.props.navigation.setParams({visible: true});
	}

	closeAddRecipients() {
		this.setState({enableAddRecipients: false});
		this.props.navigation.setParams({visible: true});
	}

	editGame(gameId) {

	}

	async launchGame(gameId) {
		var userId = '';
		var demoMode = null;
		if (this.state.demoMode == null) {
			userId = await UserId();
			demoMode = false;
		}
		else {
			// Launch created game by default user John Miller (Hr3ApcPo2UWl1jcPjFPpozDj9fr2)
			userId = 'Hr3ApcPo2UWl1jcPjFPpozDj9fr2';
		    demoMode = true;
		}
		await ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE);
		this.props.navigation.navigate('MainGameActivityScreen', {gameId: gameId, creatorId: userId, demoMode: demoMode});
	}

	async deleteGame(gameId) {
		if (this.state.demoMode == null) {
			// Get recipient IDs and remove the game reference from their firebases
			var gameData = this.state.userData.createdGames[gameId];
			if (gameData.recipients != undefined) {
				var recipientIds = gameData.recipients;
				for (var i = 0; i <= recipientIds.length - 1; i++) {
					var gameRefPath = 'users/' + recipientIds[i] + '/receivedGames';
					await Append({[gameId]: null}, gameRefPath);
				}
			}
			// Then remove the game from current users firebase
			var userId = await UserId();
			var localGameRefPath = 'users/' + userId + '/createdGames/';
			await Append({[gameId]: null}, localGameRefPath);
			await this.getCreatedGames();
		}
	}

	addRecipients(gameId) {
		if (this.state.demoMode == null) {
			this.props.navigation.setParams({visible: false});
			this.setState({selectedGame: gameId,
					       enableAddRecipients: true});
		}
	}

	async onToggleRecipient(index, item) {
		console.log('familyMember 3', item);
		var selectedStatus = this.state.familyMembers[index].selected;
		if (selectedStatus == true) {
			var members = this.state.familyMembers;
			members[index].selected = false;
			members[index].styleStatus = {
				height: screenHeight * 0.07,
				width: screenHeight * 0.07,
				tintColor: color.grey,
				opacity: 0.7
			};
			await this.setState({familyMembers: members});
		}
		else {
			var members = this.state.familyMembers;
			members[index].selected = true;
			members[index].styleStatus = {
				height: screenHeight * 0.07,
				width: screenHeight * 0.07,
				tintColor: color.green
			};
			await this.setState({familyMembers: members});
		}
	}

	async sendGameToRecipients() {
		var selectedRecipients = [];
		var familyMembers = this.state.familyMembers;
		var gameId = this.state.selectedGame;
		var userId = await UserId();
		for (var i = 0; i <= familyMembers.length - 1; i++) {
			if (familyMembers[i].selected == true) {
				var path = 'users/' + familyMembers[i].uid + '/receivedGames';
				await Append({[gameId]: userId}, path);
			}
		}
		this.setState({enableAddRecipients: false});
		this.props.navigation.setParams({visible: true});
		this.getFamilyData();
	}

	uiTitleBar() {
		return(
			<View style={styles.container}>
				<View style={styles.titleBar}>
					<View style={gstyles.shadow}>
						<Image style={styles.logo} source={require('../../../../assets/images/Memory-Lane-Games.png')} />
					</View>
					<Text style={gstyles.largeText}>My Games</Text>
					<TouchableOpacity style={styles.titleBarIconButton}
									  onPress={() => this.createNewGame()}>
					    <View style={gstyles.shadow}>
							<Image source={require('../../../../assets/icons/png/library-plus.png')}
								   style={styles.titleBarIcon} />
					    </View>
				    </TouchableOpacity>
				</View>
				{this.uiCreatedGamesBody()}
			</View>
		);
	}

	uiCreatedGamesBody() {
		// Check if it should render a place holder or the present content
		if (this.state.createdGamesData.length != 0 && this.state.createdGamesData.length != undefined) {
			return(
				<ScrollView style={styles.scrollView}>
					<FlatList data={this.state.createdGamesData}
						      extraData={this.state}
						      keyExtractor={(item, index) => index.toString()}
	                          renderItem={({item}) => <ListItem data={item} parent={this}/>}/>
				</ScrollView>
			);
		}
		else {
			return(
				<BlurView style={styles.placeholderBlur} tint='dark' intensity={90}>
					<View style={gstyles.shadow}>
						<Image style={gstyles.mediumIcon} source={require('../../../../assets/icons/png/creation.png')} />
					</View>
					<Text style={[gstyles.largeText, {textAlign: 'center'}]}>Welcome to Memory Lane Games!</Text>
					<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>
						Ready to take a trip down memory lane?{"\n"}
						To create a game press the button below!
					</Text>
					<TouchableOpacity style={styles.placeholderActionButton}
								      onPress={() => this.createNewGame()}>
						<Text style={gstyles.smallText}>Create New Game</Text>
					</TouchableOpacity>
				</BlurView>
			);
		}
	}

	uiPreviewGame() {
		if (this.state.enablePreview == true) {
			return(
				<BlurView style={styles.previewBlur} intensity={90} tint='dark'>
					<FlatList data={this.state.previewData}
						      keyExtractor={(item, index) => index.toString()}
	                          renderItem={(item) => <View style={[gstyles.shadow, styles.previewContainer]}>
														<View style={[gstyles.shadow, styles.previewNavBar]}>
															<View style={gstyles.shadow}>
																<Image style={styles.previewLogo} source={require('../../../../assets/images/Memory-Lane-Games.png')} />
															</View>
															<Text style={gstyles.smallText}>Question {parseInt(item.index) + 1}</Text>
															<Image style={gstyles.smallIcon} source={require('../../../../assets/icons/png/menu.png')} />
														</View>
														<Text style={gstyles.smallText}>{item.item.title}</Text>
														<View style={gstyles.shadow}>
															{this.uiPreviewGameImage(item)}
														</View>
														<View style={styles.previewButtonRow}>
															<View style={[gstyles.shadow, styles.previewButton]}>
																<Text style={styles.previewButtonText}>{item.item.correctAnswer}</Text>
															</View>
															<View style={[gstyles.shadow, styles.previewButton]}>
																<Text style={styles.previewButtonText}>{item.item.dummyAnswer1}</Text>
															</View>
															<View style={[gstyles.shadow, styles.previewButton]}>
																<Text style={styles.previewButtonText}>{item.item.dummyAnswer2}</Text>
															</View>
															<View style={[gstyles.shadow, styles.previewButton]}>
																<Text style={styles.previewButtonText}>{item.item.dummyAnswer3}</Text>
															</View>
														</View>
													</View>}/>
					<TouchableOpacity style={styles.closeImageCross} onPress={() => this.closePreview()}>
						<Image style={gstyles.mediumIcon} source={require('../../../../assets/icons/png/close.png')} />
					</TouchableOpacity>
				</BlurView>
			);
		}
		else {
			return(null);
		}
	}

	uiPreviewGameImage(item) {
		if (this.state.demoMode == null) {
			return(
				<CacheableImage style={styles.previewImage} source={{uri: item.item.image.mainImageUrl}} />
			);
		}
		else {
			return(
				<Image style={styles.previewImage} source={{uri: item.item.image.mainImageUrl}} />
			);
		}
	}

	uiAddRecipients() {
		if (this.state.enableAddRecipients == true) {
			return(
				<BlurView style={styles.recipientsBlur} intensity={90} tint='dark'>
					<View style={[gstyles.cardColumn, styles.recipientCard]}>
						<FlatList data={this.state.familyMembers}
							      keyExtractor={(item, index) => index.toString()}
							      extraData={this.state}
		                          renderItem={(item) => <TouchableOpacity style={styles.memberRow}
	                              										  onPress={() => this.onToggleRecipient(item.index, item.item)}>
	                              							<View style={gstyles.shadow}>
	                              								<CacheableImage style={styles.profilePictures} source={{uri: item.item.profilePicture}} />
	                              							</View>
	                              							<Text style={gstyles.smallText}>{item.item.name}</Text>
	                              							<Image style={[item.item.styleStatus]} source={require('../../../../assets/icons/png/check.png')}
	                              						           onLayout={() => console.log(item)} />
								                	    </TouchableOpacity>}/>
            	    </View>
            	    <TouchableOpacity style={styles.recipientSendButton} onPress={() => this.sendGameToRecipients()}>
						<Text style={gstyles.mediumText}>Send</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.closeImageCross} onPress={() => this.closeAddRecipients()}>
						<Image style={gstyles.mediumIcon} source={require('../../../../assets/icons/png/close.png')} />
					</TouchableOpacity>
				</BlurView>
			);
		}
	}

	render() {
		return(
			<View>
				{this.uiTitleBar()}
				{this.uiPreviewGame()}
				{this.uiAddRecipients()}
			</View>
		);
	}

}

class ListItem extends React.Component {

	state = {
		menuOpen: false
	}

	uiProfilePictures() {
		if (this.props.parent.state.demoMode != true) {
			if (this.props.data.recipientProfilePictures.length == 1) {
				return(
					<View style={styles.recipientProfileContainer}>
						<View style={gstyles.shadow}>
							<CacheableImage style={gstyles.profilePicturePortrait} source={{uri: this.props.data.recipientProfilePictures[0]}} />
						</View>
					</View>
				);
			}
			else {
				return(
					<View style={styles.recipientProfileContainer}>
						<View style={[gstyles.shadow, styles.profilePicture1]}>
							<CacheableImage style={gstyles.profilePicturePortrait} source={{uri: this.props.data.recipientProfilePictures[0]}} />
						</View>
						<View style={[gstyles.shadow, styles.profilePicture2]}>
							<CacheableImage style={gstyles.profilePicturePortrait} source={{uri: this.props.data.recipientProfilePictures[1]}} />
						</View>
					</View>
				);
			}
		}
		else {
			if (this.props.data.recipientProfilePictures.length == 1) {
				return(
					<View style={styles.recipientProfileContainer}>
						<View style={gstyles.shadow}>
							<Image style={gstyles.profilePicturePortrait} source={{uri: this.props.data.recipientProfilePictures[0]}} />
						</View>
					</View>
				);
			}
			else {
				return(
					<View style={styles.recipientProfileContainer}>
						<View style={[gstyles.shadow, styles.profilePicture1]}>
							<Image style={gstyles.profilePicturePortrait} source={{uri: this.props.data.recipientProfilePictures[0]}} />
						</View>
						<View style={[gstyles.shadow, styles.profilePicture2]}>
							<Image style={gstyles.profilePicturePortrait} source={{uri: this.props.data.recipientProfilePictures[1]}} />
						</View>
					</View>
				);
			}
		}
	}

	render() {
		if (this.state.menuOpen == true) {
			return(
				<View style={[gstyles.cardColumn, styles.cardExpanded]}>
					<TouchableOpacity style={styles.cardRowHeader}
									  onPress={() => this.setState({menuOpen: false})}>
						<View style={styles.cardRowText}>
							<Text style={gstyles.smallText} numberOfLines={1}>{this.props.data.title}</Text>
							<Text style={gstyles.subTitle} numberOfLines={1}>{this.props.data.subTitle}</Text>
						</View>
						{this.uiProfilePictures()}
					</TouchableOpacity>
					<View style={styles.openMenuDividingBar} />
					<View style={styles.menuRow}>
						<TouchableOpacity style={styles.menuItem}
										  onPress={() => this.props.parent.launchGame(this.props.data.gameId)}>
						    <View style={gstyles.shadow}>
								<Image style={gstyles.smallIcon} source={require('../../../../assets/icons/png/play.png')} />
							</View>
							<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>Play{"\n"}Game</Text>
						</TouchableOpacity>
						<TouchableOpacity style={styles.menuItem}
										  onPress={() => this.props.parent.editGame(this.props.data.gameId)}>
						    <View style={gstyles.shadow}>
								<Image style={gstyles.smallIcon} source={require('../../../../assets/icons/png/square-edit-outline.png')} />
							</View>
							<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>Edit{"\n"}Game</Text>
						</TouchableOpacity>
						<TouchableOpacity style={styles.menuItem}
										  onPress={() => this.props.parent.addRecipients(this.props.data.gameId)}>
						    <View style={gstyles.shadow}>
								<Image style={gstyles.smallIcon} source={require('../../../../assets/icons/png/account-multiple-plus.png')} />
							</View>
							<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>Add{"\n"}Recipients</Text>
						</TouchableOpacity>
						<TouchableOpacity style={styles.menuItem}
										  onPress={() => this.props.parent.previewGame(this.props.data.gameId)}>
						    <View style={gstyles.shadow}>
								<Image style={gstyles.smallIcon} source={require('../../../../assets/icons/png/presentation-play.png')} />
							</View>
							<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>Preview{"\n"}Game</Text>
						</TouchableOpacity>
						<TouchableOpacity style={styles.menuItem}
										  onPress={() => Alert.alert('Warning!',
										  							 'Are you sure you want to permanently delete this game?',
										  							 [
										  							 	{text: 'Confirm', onPress: () => this.props.parent.deleteGame(this.props.data.gameId)},
										  							 	{text: 'Cancel'}
										  							 ]
										  	                         )}>
						    <View style={gstyles.shadow}>
								<Image style={gstyles.smallIcon} source={require('../../../../assets/icons/png/delete-forever.png')} />
							</View>
							<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>Delete{"\n"}Game</Text>
						</TouchableOpacity>
					</View>
				</View>
			);
		}
		else {
			return(
				<TouchableOpacity style={[gstyles.cardRow, styles.menuClosedCard]}
								  onPress={() => this.setState({menuOpen: true})}>
					<View style={styles.cardRowText}>
						<Text style={gstyles.smallText} numberOfLines={1}>{this.props.data.title}</Text>
						<Text style={gstyles.subTitle} numberOfLines={1}>{this.props.data.subTitle}</Text>
					</View>
					{this.uiProfilePictures()}
				</TouchableOpacity>
			);
		}
	}

}