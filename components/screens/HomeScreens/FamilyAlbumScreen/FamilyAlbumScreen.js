import React from 'react';
import { Text,
		 View,
		 ScrollView,
		 FlatList,
		 Image,
		 TextInput,
		 TouchableOpacity,
		 Dimensions,
		 ActionSheetIOS,
		 Alert } from 'react-native';
import GridView from 'react-native-super-grid';
import styles from './FamilyAlbumScreenStyles';
import gstyles from '../../../reusable/GlobalStyles';
import { UserData,
		 MemberData,
		 FamilyData,
		 Append } from '../../../../lib/Firebase';
import Expo, { BlurView, ImagePicker, Camera, FileSystem } from 'expo';
import { color } from '../../../reusable/common';
import { Image as CacheableImage} from 'react-native-aws-cache';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

const DEMO_DIR = `${FileSystem.documentDirectory}mlgDemoModeAssets/`;

export default class FamilyAlbumScreen extends React.Component {

	state = {
		userData: {},
		familyAlbum: [],
		isEnlarged: false,
		enlargedImageIndex: 0,
		cameraEnabled: false,
		cameraDirection: Camera.Constants.Type.back,
		isRefreshing: false,

		// Enalrged Image states
		allowsEditing: false,
		name: '',
		location: '',
		dateTaken: '',
		demoMode: null
	}

	async componentDidMount() {
		// Check if we are in demo mode
		var demoMode = await Expo.SecureStore.getItemAsync('demo-mode');
		if (demoMode == 'true') {
			console.log('demoMode ==== > ', demoMode)
			this.setState({demoMode: true});
		}
		// Set up navigation listener to refresh screen every time
		const willFocusNavListener = this.props.navigation.addListener('willFocus', () => {
			console.log('Navigated to Family Album Screen!');
			this.getFamilyAlbum();
		});
		// Refresh family album
		await this.getFamilyAlbum();
	}

	async getFamilyAlbum() {
		// Firstly determine whether we are in demo mode, if so pull from the local cache instead of firebase
		if (this.state.demoMode != true) {
			// Use Firebase to get user data 
			var userData = await UserData();
			this.setState({userData: userData});
			// Then using the familyId get the whole family album library
			if (userData.familyId != 'Pending') {
				var familyData = await FamilyData();
				if (familyData.familyAlbum != null) {
					var keys = Object.keys(familyData.familyAlbum);
					var familyAlbum = [];
					for (var i = keys.length - 1; i >= 0; i--) {
						var image = familyData.familyAlbum[keys[i]];
						image.index = i;
						image.id = Object.keys(familyData.familyAlbum)[i];
						familyAlbum.push(image);
					}
					this.setState({familyAlbum: familyAlbum});
				}
			}
		}
		else {
			// Read from manager.json and load in all images
			var demoData = await FileSystem.readAsStringAsync(DEMO_DIR + 'manager.json');
			demoData = JSON.parse(demoData);
			var familyAlbum = demoData.families.Miller.familyAlbum;
			var keys = Object.keys(familyAlbum);
			// Now we need to reverse the array so it is in revesrse time order like above with FB
			var timeReversedArray = [];
			for (var i = keys.length - 1; i >= 0; i--) {
				familyAlbum[keys[i]].index = i;
				familyAlbum[keys[i]].id = keys[i];
				timeReversedArray.push(familyAlbum[keys[i]]);
			}
			this.setState({familyAlbum: timeReversedArray});
			console.log(this.state.familyAlbum);
		}
	}

	async onPressAddPhotoIcon() {
		if (this.state.demoMode != true) {
			var userData = await UserData();
			if (userData.familyState != 'pending-approval') {
				// Open up IOS action sheet with options to choose where to add image from
				ActionSheetIOS.showActionSheetWithOptions({
					options: ['Choose from Camera Roll', 'Take New Photo', 'Cancel'],
					cancelButtonIndex: 3,
					destructiveButtonIndex: 2,
					tintColor: color.darkBlue
				},
				(buttonIndex) => {
					console.log(buttonIndex);
					if (buttonIndex == 0) {
						this.addNewImage('library');
					}
					else if (buttonIndex == 1) {
						this.addNewImage('camera');
					}
					else {

					}
				});
			}
			else {
				Alert.alert('Once your family admin accepts your request you will be able to access this feature!');
			}
		}
		else {
			// Open up IOS action sheet with options to choose where to add image from
				ActionSheetIOS.showActionSheetWithOptions({
					options: ['Choose from Camera Roll', 'Take New Photo', 'Cancel'],
					cancelButtonIndex: 3,
					destructiveButtonIndex: 2,
					tintColor: color.darkBlue
				},
				(buttonIndex) => {
					console.log(buttonIndex);
					if (buttonIndex == 0) {
						this.addNewImage('library');
					}
					else if (buttonIndex == 1) {
						this.addNewImage('camera');
					}
					else {

					}
				});
		}
	}

	async addNewImage(source) {
		// Now we have the source we can await for the image uri and data to be returned
		if (source == 'library') {
			// In case we naved here from the camera - disable it
			this.setState({cameraEnabled: false});
			//
			var newImageData = await this.openImagePicker();
			if (newImageData.cancelled == false) {
				// Once we get the image data we pass it as a parameter and navigate to the AddPhotoScreen
				this.props.navigation.navigate('AddFamilyAlbumPhotoScreen', {data: newImageData});
			}
		}
		if (source == 'camera') {
			// The rest is handled by the onShutterPress of the camera component
			this.setState({cameraEnabled: true});
			this.props.navigation.setParams({visible: false});
		}
	}

	async openImagePicker() {
		let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: false,
            aspect: [4, 3],
            exif: true
        });
        this.props.navigation.setParams({visible: true});
        return(result);
	}

	rotateCamera() {
		if (this.state.cameraDirection == Camera.Constants.Type.back) {
			this.setState({cameraDirection: Camera.Constants.Type.front});
		}
		else {
			this.setState({cameraDirection: Camera.Constants.Type.back});
		}
	}

	onShutterPress = async () => {
	    if (this.camera) {
	    	this.props.navigation.setParams({visible: true});
	    	var newImageData = await this.camera.takePictureAsync({exif: true});
	    	this.setState({cameraEnabled: false});
	    	this.props.navigation.navigate('AddFamilyAlbumPhotoScreen', {data: newImageData});
    	}
	}

	enlargeImage(index) {
		var n = this.state.familyAlbum.length - 1 - index;
		console.log(index, n)
		this.setState({enlargedImageIndex: n});
		this.setState({isEnlarged: true, allowsEditing: false});
		this.props.navigation.setParams({visible: false});
		// Set up all the photo info fields with selected photo data
		var imageData = this.state.familyAlbum[n];
		this.setState({
			name: imageData.title,
			location: imageData.location,
			dateTaken: imageData.dateTaken
		});
		if (imageData.faces != null) {
			for (var i = 0; i <= imageData.faces.length - 1; i++) {
				this.setState({['faceLabel_' + i.toString()]: imageData.faces[i].name});
			}
		}
	}

	async collapseImage() {
		this.setState({isEnlarged: false});
		this.props.navigation.setParams({visible: true});
		if (this.state.demoMode != true) {
			// Save any of the enlarged photos data back to the server
			var imageData = this.state.familyAlbum[this.state.enlargedImageIndex];
			console.log(imageData)
			await Append({
				title: this.state.name,
				location: this.state.location,
				dateTaken: this.state.dateTaken
			}, 'families/' + this.state.userData.familyId + '/familyAlbum/' + imageData.id);
			if (imageData.faces != null) {
				for (var i = 0; i <= imageData.faces.length - 1; i++) {
					await Append({
						name: this.state['faceLabel_' + i.toString()]
					}, 'families/' + this.state.userData.familyId + '/familyAlbum/' + imageData.id + '/faces/' + i.toString());
				}
			}
		}
		else {
			// Read from the manager json file and append the data for the edited image
			var demoData = await FileSystem.readAsStringAsync(DEMO_DIR + 'manager.json');
			demoData = JSON.parse(demoData);
			var familyAlbum = demoData.families.Miller.familyAlbum;
			var index = this.state.enlargedImageIndex;
			console.log("index to save to = ", index)
			var id = this.state.familyAlbum[index].id;
			familyAlbum[id].title = this.state.name;
			familyAlbum[id].location = this.state.location;
			familyAlbum[id].dateTaken = this.state.dateTaken;
			demoData.families.Miller.familyAlbum = familyAlbum;
			await FileSystem.writeAsStringAsync(
				DEMO_DIR + 'manager.json',
				JSON.stringify(demoData)
			);
		}
		// Then finally get the latest data again
		this.getFamilyAlbum();
	}

	async refreshAlbum() {
		this.setState({isRefreshing: true});
		await this.getFamilyAlbum();
		this.setState({isRefreshing: false});
	}

	savePhotoInfo() {
		this.collapseImage();
	}

	uiTitleBar() {
		return(
			<View style={styles.titleBar}>
				<View style={gstyles.shadow}>
					<Image style={styles.logo} source={require('../../../../assets/images/Memory-Lane-Games.png')} />
				</View>
				<Text style={gstyles.largeText}>Family Album</Text>
				<TouchableOpacity style={[styles.titleBarIconButton, gstyles.shadow]}
								  onPress={() => this.onPressAddPhotoIcon()}>
				    <Text style={[gstyles.smallText, {textAlign: 'center'}]}>Add a photo</Text>
			    </TouchableOpacity>
			</View>
		);
	}

	uiFamilyAlbumGrid() {
		// Check if it should render a place holder or the present content
		if (this.state.familyAlbum.length != 0 && this.state.familyAlbum.length != undefined) {
			return(
				<View style={styles.familyAlbumGridContainer}>
					<GridView itemDimension={screenWidth * 0.23}
							  items={this.state.familyAlbum}
							  style={styles.familyAlbumGrid}
							  renderItem={(item) => <TouchableOpacity onPress={() => this.enlargeImage(item.index)}
							  	  			    					  style={styles.gridViewItem}>
									  		  			{this.uiFamilyAlbumImage(item)}
							  		  				</TouchableOpacity>
							  }
							  onRefresh={() => this.refreshAlbum()}
	                  		  refreshing={this.state.isRefreshing}/>

				</View>
			);
		}
		else {
			return(
				<BlurView style={styles.placeholderBlur} tint='dark' intensity={90}>
					<View style={gstyles.shadow}>
						<Image style={gstyles.mediumIcon} source={require('../../../../assets/icons/png/creation.png')} />
					</View>
					<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>
						It looks like you haven't added any photos yet! 
						To get started press the button below!
					</Text>
					<TouchableOpacity style={styles.placeholderActionButton}
								      onPress={() => this.onPressAddPhotoIcon()}>
						<Text style={gstyles.smallText}>Add New Photo</Text>
					</TouchableOpacity>
				</BlurView>
			);
		}
	}

	uiFamilyAlbumImage(item) {
		// Check if we are in demo mode - return just a std image comp if so
		if (this.state.demoMode == null) {
			return(
				<CacheableImage style={styles.gridViewItem}
	  	  		                source={{uri: item.mainImageUrl}} />
            );
		}
		else {
			return(
				<Image style={styles.gridViewItem}
		               source={{uri: item.mainImageUrl}} />
            );
		}
	}

	uiEnlargedImage() {
		if (this.state.isEnlarged == true) {
			return(
				<BlurView tint="dark" intensity={90} style={styles.blur}>
					<KeyboardAwareScrollView>
						<View style={styles.keyboardScrollView}>
							{this.uiEnlargedImageContent()}
							<View style={[gstyles.cardColumn, styles.mainDetailsCard]}>
			                	<Text style={gstyles.smallText}>Main Photo Details</Text>
			                	<View style={styles.mainDetailsInputs}>
			                		<TouchableOpacity style={styles.textInputContainer}
			                		                  onPress={() => this.refs.nameInput.focus()}>
			                			<Text style={gstyles.subTitle}>Name</Text>
			                			<TextInput style={styles.textInput}
			                					   ref='nameInput'
			                					   placeholder='Enter a name for your photo'
			                					   value={this.state.name}
			                					   onChangeText={(name) => this.setState({name})}
			                					   editable={this.state.allowsEditing} />
			                		</TouchableOpacity>
			                		{/*<TouchableOpacity style={styles.textInputContainer}
			                		                  onPress={() => this.refs.descriptionInput.focus()}>
			                			<Text style={gstyles.subTitle}>Description</Text>
			                			<TextInput style={styles.textInput}
			                					   ref='descriptionInput'
			                					   placeholder='Enter a short description'
			                					   value={this.state.description}
			                					   onChangeText={(description) => this.setState({description})} />
			                		</TouchableOpacity>*/}
			                		<TouchableOpacity style={styles.textInputContainer}
			                		                  onPress={() => this.refs.locationInput.focus()}>
			                			<Text style={gstyles.subTitle}>Location</Text>
			                			<TextInput style={styles.textInput}
			                					   ref='locationInput'
			                					   placeholder='Enter the photo location'
			                					   value={this.state.location}
			                					   onChangeText={(location) => this.setState({location})}
			                					   editable={this.state.allowsEditing} />
			                		</TouchableOpacity>
			                		<TouchableOpacity style={styles.textInputContainer}
			                		                  onPress={() => this.refs.dateInput.focus()}>
			                			<Text style={gstyles.subTitle}>Date Taken</Text>
			                			<TextInput style={styles.textInput}
			                					   ref='dateInput'
			                					   placeholder='Enter the date taken'
			                					   value={this.state.dateTaken}
			                					   onChangeText={(dateTaken) => this.setState({dateTaken})}
			                					   editable={this.state.allowsEditing} />
			                		</TouchableOpacity>
			                	</View>
		                	</View>
		                	{this.uiFaceDetectionCard()}
	                	</View>
		            </KeyboardAwareScrollView>
		            <TouchableOpacity style={styles.closeImageCross} onPress={() => this.collapseImage()}>
						<Image style={gstyles.mediumIcon} source={require('../../../../assets/icons/png/close.png')} />
					</TouchableOpacity>
					{this.uiEnlargedImageFab()}
				</BlurView>
			);
		}
		else {
			return(null);
		}
	}

	uiEnlargedImageContent() {
		if (this.state.demoMode != true) {
			return(<CacheableImage style={styles.enlargedImage} source={{uri: this.state.familyAlbum[this.state.enlargedImageIndex].mainImageUrl}} />);
		}
		else {
			return(<Image style={styles.enlargedImage} source={{uri: this.state.familyAlbum[this.state.enlargedImageIndex].mainImageUrl}} />);
		}
	}

	uiFaceDetectionCard() {
		var imageData = this.state.familyAlbum[this.state.enlargedImageIndex];
		console.log(imageData.faces)
		if (imageData.faces != null) {
			return(
				<View style={[gstyles.cardColumn, styles.mainDetailsCard, {height: 100 + (imageData.faces.length * 115)}]}>
            		<Text style={gstyles.smallText}>Face Detection</Text>
            		<View style={[styles.mainDetailsInputs, {flex: 1}]}>
	            		<FlatList data={imageData.faces}
	            				  extraData={this.state}
						          keyExtractor={(item, index) => index.toString()}
						          scrollEnabled={false}
	                              renderItem={(item) => <View style={styles.faceDetectionRow}>
	                              							<View style={gstyles.shadow}>
	                              								{this.uiFaceDetectionCardProfilePicture(item)}
	                              							</View>
	                              							<TouchableOpacity style={styles.textInputContainer}
	                              											  onPress={() => this.itemRefs['faceInput' + item.index.toString()].focus()}>
									                		   <Text style={gstyles.subTitle}>Who is this?</Text>
									                		   <TextInput style={[styles.textInput, styles.faceDetectionInputs]}
									                		   		      ref={(ref) => this.itemRefs = {...this.itemRefs, ['faceInput' + item.index.toString()]: ref}}
									                					  placeholder='Their name is...'
									                					  value={this.state['faceLabel_' + item.index.toString()]}
				                                 					      onChangeText={(text) => this.setState({['faceLabel_' + item.index.toString()]: text})}
				                                 					      editable={this.state.allowsEditing} />
									                	    </TouchableOpacity>
								                	    </View>}/>
            	    </View>
            	</View>
			);
		}
		else {
			return(null);
		}
	}

	uiFaceDetectionCardProfilePicture(item) {
		console.log('face = ', item.item.faceImageUrl)
		if (this.state.demoMode != true) {
			return(
				<CacheableImage style={[gstyles.profilePicturePortrait, styles.profilePictures]} source={{uri: item.item.faceImageUrl}} />
			);
		}
		else {
			return(
				<Image style={[gstyles.profilePicturePortrait, styles.profilePictures]} source={{uri: item.item.faceImageUrl}} />
			);
		}
	}

	uiEnlargedImageFab() {
		if (this.state.allowsEditing == true) {
			return(
				<TouchableOpacity style={[gstyles.shadow, styles.saveInfoButton]}
								  onPress={() => this.collapseImage()}>
					<Image style={gstyles.smallIcon} source={require('../../../../assets/icons/png/check.png')} />
				</TouchableOpacity>
			);
		}
		else {
			return(
				<TouchableOpacity style={[gstyles.shadow, styles.editInfoButton]}
								  onPress={() => this.setState({allowsEditing: true})}>
					<Image style={gstyles.smallIcon} source={require('../../../../assets/icons/png/square-edit-outline.png')} />
				</TouchableOpacity>
			);
		}
	}

	uiCamera() {
		if (this.state.cameraEnabled == true) {
			return(
				<Camera type={this.state.cameraDirection} style={styles.camera} ref={ref => {this.camera = ref;}}>
					<BlurView style={styles.cameraActionBar} tint="dark" intensity={90}>
						<TouchableOpacity style={styles.cameraShutterButton} onPress={() => this.rotateCamera()}>
							<View style={gstyles.shadow}>
								<Image style={gstyles.smallIcon} source={require('../../../../assets/icons/png/rotate-3d.png')} />
							</View>
						</TouchableOpacity>
						<TouchableOpacity style={styles.cameraShutterButton} onPress={() => this.onShutterPress()}>
							<View style={gstyles.shadow}>
								<Image style={gstyles.largeIcon} source={require('../../../../assets/icons/png/camera-iris.png')} />
							</View>
						</TouchableOpacity>
						<TouchableOpacity style={styles.cameraShutterButton} onPress={() => this.addNewImage('library')}>
							<View style={gstyles.shadow}>
								<Image style={gstyles.smallIcon} source={require('../../../../assets/icons/png/image-multiple.png')} />
							</View>
						</TouchableOpacity>
					</BlurView>
				</Camera>
			);
		}
		else {
			return(null);
		}
	}

	render() {
		return(
			<View style={styles.container}>
				{this.uiTitleBar()}
				{this.uiFamilyAlbumGrid()}
				{this.uiEnlargedImage()}
				{this.uiCamera()}
			</View>
		);
	}

}