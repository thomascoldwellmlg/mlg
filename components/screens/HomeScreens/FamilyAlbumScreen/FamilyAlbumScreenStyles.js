import { StyleSheet, Dimensions} from 'react-native';
import { color } from '../../../reusable/common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

if (screenHeight < screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const styles = StyleSheet.create({

	/*
		Contents:
			0. Title Bar
			1. Body
			2. Enlarged Image
			3. Camera
	*/

	// 0. Title Bar
	titleBar: {
		height: screenHeight * 0.15,
		width: screenWidth,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		backgroundColor: color.darkBlue,
		paddingLeft: screenWidth * 0.05,
		paddingRight: screenWidth * 0.05,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	logo: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		marginRight: screenWidth * 0.05,
		resizeMode: 'contain'
	},
	titleBarIconButton: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.15,
		borderRadius: screenWidth * 0.01,
		padding: screenWidth * 0.02,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.green
	},
	titleBarIcon: {
		height: screenHeight * 0.06,
		width: screenHeight * 0.06,
		resizeMode: 'contain',
		tintColor: '#fff'
	},

	// 1. Body
	container: {
		height: screenHeight,
		width: screenWidth,
		backgroundColor: color.lightBlue,
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	scrollView: {
		paddingTop: screenHeight * 0.03
	},
	cardRowText: {
		marginLeft: screenWidth * 0.05
	},
	greetingIcon: {
		marginLeft: screenWidth * 0.05
	},
	cardRowHeader: {
		height: screenHeight * 0.15,
        width: screenWidth * 0.9,
        backgroundColor: color.cardBlue,
        flexDirection: 'row',
        paddingRight: screenWidth * 0.05,
        alignItems: 'center'
	},
	cardExpanded: {
		height: screenHeight * 0.3
	},
	menuClosedCard: {
		paddingLeft: 0.0
	},
	recipientProfileContainer: {
		position: 'absolute',
		end: screenWidth * 0.06,
		height: screenHeight * 0.09,
		width: screenWidth * 0.19,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	profilePicture1: {
		position: 'absolute',
		start: 0
	},
	profilePicture2: {
		position: 'absolute',
		end: 0
	},
	menuRow: {
		height: screenHeight * 0.15,
		width: screenWidth * 0.9,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	menuItem: {
		height: screenHeight * 0.15,
		width: screenWidth * 0.225,
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingTop: screenHeight * 0.03,
		paddingBottom: screenHeight * 0.03
	},

	gridViewItem: {
    	height: screenWidth * 0.21,
    	width: screenWidth * 0.21,
    	alignSelf: 'center',
    	marginBottom: screenHeight * 0.015,
    	backgroundColor: '#ccc',
    	shadowColor: 'black',
    	shadowOpacity: 0.3,
    	shadowOffset: {
    		width: 0,
    		height: 5
    	}
    },
    familyAlbumGridContainer: {
    	height: screenHeight * 0.73,
    	width: screenWidth
    },
    familyAlbumGrid: {
    	paddingTop: screenHeight * 0.02
    },

    // 2. Enlarged Image
    blur: {
    	height: screenHeight,
		width: screenWidth,
		position: 'absolute',
		backgroundColor: '#000',
		justifyContent: 'center',
		alignItems: 'center'
    },
    closeImageCross: {
    	position: 'absolute',
    	end: '3%',
    	top: '2%'
    },
    enlargedImage: {
    	height: screenHeight * 0.35,
		width: screenWidth * 0.65,
		alignSelf: 'center',
		marginBottom: screenHeight * 0.03,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3,
		borderRadius: 10
    },
    keyboardScrollView: {
		flex: 1,
		width: screenWidth,
		alignItems: 'center',
		paddingTop: screenHeight * 0.03
	},
	mainDetailsCard: {
		height: screenHeight * 0.45,
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
		paddingLeft: screenWidth * 0.05
	},
	mainDetailsInputs: {
		height: screenHeight * 0.35,
		width: screenWidth * 0.8,
		marginLeft: screenWidth * 0.03,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginTop: screenHeight * 0.02
	},
	textInputContainer: {
		height: screenHeight * 0.07,
		width: screenWidth * 0.7,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginBottom: screenHeight * 0.01
	},
	textInput: {
		fontFamily: 'Futura',
		fontSize: 20,
		color: '#fff',
		height: screenHeight * 0.05,
		width: screenWidth * 0.7,
		borderBottomWidth: 3,
		borderColor: color.lightBlue
	},
	faceDetectionInputs: {
		width: screenWidth * 0.5
	},
	faceDetectionRow: {
		flexDirection: 'row',
		height: screenHeight * 0.12,
	},
	profilePictures: {
		marginRight: screenWidth * 0.03
	},
	saveInfoButton: {
		position: 'absolute',
		bottom: '5%',
		right: '5%',
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		borderRadius: screenHeight * 0.05,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.green
	},
	editInfoButton: {
		position: 'absolute',
		bottom: '5%',
		right: '5%',
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		borderRadius: screenHeight * 0.05,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.yellow
	},

    // 3. Camera
    camera: {
    	position: 'absolute',
    	height: screenHeight,
    	width: screenWidth,
    	justifyContent: 'flex-end'
    },
    cameraActionBar: {
    	height: screenHeight * 0.1,
    	width: screenWidth,
    	justifyContent: 'space-around',
    	alignItems: 'center',
    	flexDirection: 'row',
    	backgroundColor: '#000',
    	opacity: 0.7
    },
    cameraRotateButton: {
    	height: screenHeight * 0.1,
    	width: screenHeight * 0.1,
    	justifyContent: 'center',
    	alignItems: 'center'
    },
    cameraShutterButton: {
    	height: screenHeight * 0.08,
    	width: screenHeight * 0.08,
    	borderRadius: screenHeight * 0.1,
    	marginLeft: screenWidth * 0.25,
    	marginRight: screenWidth * 0.25,
    	justifyContent: 'center',
    	alignItems: 'center'
    },

    // Placeholder stuff
    placeholderBlur: {
    	height: screenHeight * 0.74,
    	width: screenWidth,
    	justifyContent: 'space-around',
    	alignItems: 'center',
    	backgroundColor: '#000000',
    	paddingHorizontal: screenWidth * 0.1,
    	paddingVertical: screenHeight * 0.1
    },
    placeholderActionButton: {
    	height: screenHeight * 0.12,
    	width: screenWidth * 0.3,
    	borderRadius: screenWidth * 0.01,
    	justifyContent: 'center',
    	alignItems: 'center',
    	backgroundColor: color.green
    }

});

export default styles;