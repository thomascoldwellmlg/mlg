import React from 'react';
import { Text,
		 View,
		 ScrollView,
		 FlatList,
		 Image,
		 Alert,
		 TouchableOpacity,
		 RefreshControl } from 'react-native';
import styles from './ActivityScreenStyles';
import gstyles from '../../../reusable/GlobalStyles';
import { UserData,
		 MemberData,
		 FamilyData } from '../../../../lib/Firebase';
import { Permissions, BlurView } from 'expo';
import { Image as CacheableImage} from 'react-native-aws-cache';

const testFeedData = {
	activity: {
		notification0000: {
			title: 'Grandma started playing Family Holiday 2017',
			subTitle: 'at 12:32 pm on Tuesday 6th June',
			senderId: 'yPEtt94mQ5Oxkm79Zs4motvqlTl2',
			attachedMedia: null
		},
		notification0001: {
			title: 'Grandma started playing Family Holiday 2017',
			subTitle: 'at 12:32 pm on Tuesday 6th June',
			senderId: 'yPEtt94mQ5Oxkm79Zs4motvqlTl2',
			attachedMedia: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Hausziege_04.jpg/1200px-Hausziege_04.jpg'
		},
	}
}

export default class ActivityScreen extends React.Component {

	state = {
		userData: {},
		activityFeedData: {},
		isRefreshing: false
	}

	async componentDidMount() {
		// Refresh feed
		this.getActivity();
	}

	async getActivity() {
		// Use Firebase to get user data 
		var userData = await UserData();
		this.setState({userData: userData});
		// Then access the family notifications
		var familyData = await FamilyData();
		if (familyData != null) {
			var notifications = familyData.notifications;
			if (notifications != null) {
				// Get the user's activity, fetch sender's PP and form notification data
				var activity = notifications;
				var activityKeys = Object.keys(activity);
				var feedData = [];

				for (var i = activityKeys.length - 1; i >= 0; i--) {
					var senderData = await MemberData(activity[activityKeys[i]].senderId);
					feedData.push({
						title: activity[activityKeys[i]].title,
						subTitle: activity[activityKeys[i]].subTitle,
						senderProfilePicture: senderData.profilePicture,
						attachedMedia: activity[activityKeys[i]].attachedMedia
					});
				}

				this.setState({activityFeedData: feedData});
				// console.log(this.state.activityFeedData);
			}
		}
	}

	async refreshFeed() {
		this.setState({isRefreshing: true});
		await this.getActivity();
		this.setState({isRefreshing: false});
	}

	async shouldActivateTitleBarButton() {
		var userData = await UserData();
		if (userData.familyState != 'pending-approval') {
			this.props.navigation.navigate('MessagingHomeScreen');
		}
		else {
			Alert.alert('Once your family admin accepts your request you will be able to access this feature!');
		}
	}

	uiTitleBar() {
		return(
			<View style={styles.container}>
				<View style={styles.titleBar}>
					<View style={gstyles.shadow}>
						<Image style={styles.logo} source={require('../../../../assets/images/Memory-Lane-Games.png')} />
					</View>
					<Text style={gstyles.largeText}>Activity Feed</Text>
					<TouchableOpacity style={styles.titleBarIconButton}
									  onPress={() => this.shouldActivateTitleBarButton()}>
					    <View style={gstyles.shadow}>
							<Image source={require('../../../../assets/icons/png/message.png')}
								   style={styles.titleBarIcon} />
					    </View>
				    </TouchableOpacity>
				</View>
				{this.uiActivityFeedBody()}
			</View>
		);
	}

	uiActivityFeedBody() {
		// Check if it should render a place holder or the present content
		if (this.state.activityFeedData.length != 0 && this.state.activityFeedData.length != undefined) {
			return(
				<ScrollView style={styles.scrollView}
							refreshControl={
								<RefreshControl 
									onRefresh={() => this.refreshFeed()}
	                          		refreshing={this.state.isRefreshing}
								/>
							}>
					<View style={[gstyles.cardRow, styles.greetingCard]}>
						<CacheableImage style={gstyles.profilePicturePortrait} source={{uri: this.state.userData.profilePicture}} />
						<Text style={[gstyles.smallText, styles.cardRowText]}>Welcome back, {this.state.userData.name}!</Text>
						<Image style={[gstyles.mediumIcon, styles.greetingIcon]} source={require('../../../../assets/icons/png/human-greeting.png')} />
					</View>
					<FlatList data={this.state.activityFeedData}
							  extraData={this.state}
						      keyExtractor={(item, index) => index.toString()}
	                          renderItem={({item}) => this.uiActivityCards(item)}/>
				</ScrollView>
			);
		}
		else {
			return(
				<ScrollView style={styles.scrollView}
							refreshControl={
								<RefreshControl 
									onRefresh={() => this.refreshFeed()}
	                          		refreshing={this.state.isRefreshing}
								/>
							}>
					<View style={[gstyles.cardRow, styles.greetingCard]}>
						<View style={gstyles.shadow}>
							<CacheableImage style={gstyles.profilePicturePortrait} source={{uri: this.state.userData.profilePicture}} />
						</View>
						<Text style={[gstyles.smallText, styles.cardRowText]}>Welcome back, {this.state.userData.name}!</Text>
						<Image style={[gstyles.mediumIcon, styles.greetingIcon]} source={require('../../../../assets/icons/png/human-greeting.png')} />
					</View>
					<BlurView style={styles.placeholderBlur} tint='dark' intensity={90}>
					<View style={gstyles.shadow}>
						<Image style={gstyles.mediumIcon} source={require('../../../../assets/icons/png/creation.png')} />
					</View>
					<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>
						There is presently no family activity, but as
						you play and create games you will see your family's
						activity here!
					</Text>
				</BlurView>
				</ScrollView>
			);
		}
	}

	uiActivityCards(data) {
		// Check if the activity has media or not
		if (data.attachedMedia == null) {
			return(
				<View style={gstyles.cardRow}>
					<CacheableImage style={gstyles.profilePicturePortrait} source={{uri: data.senderProfilePicture}} />
					<View style={styles.cardRowText}>
						<Text style={gstyles.smallText}>{data.title}</Text>
						<Text style={gstyles.subTitle}>{data.subTitle}</Text>
					</View>
				</View>
			);
		}
		else {
			return(
				<View style={[gstyles.cardColumn, styles.mediaCardColumn]}>
					<View style={styles.cardRowHeader}>
						<CacheableImage style={gstyles.profilePicturePortrait} source={{uri: data.senderProfilePicture}} />
						<View style={styles.cardRowText}>
							<Text style={gstyles.smallText}>{data.title}</Text>
							<Text style={gstyles.subTitle}>{data.subTitle}</Text>
						</View>
					</View>
					<View style={gstyles.shadow}>
						<CacheableImage style={styles.cardMedia} source={{uri: data.attachedMedia}} />
					</View>
				</View>
			);
		}
	}

	render() {
		return(
			<View>
				{this.uiTitleBar()}
			</View>
		);
	}

}