import { StyleSheet, Dimensions} from 'react-native';
import { color } from '../../../reusable/common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

// Ensure style sheet is given as portrait
if (screenHeight < screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const styles = StyleSheet.create({

	/*
		Contents:
			0. Title Bar
			1. Body
	*/

	// 0. Title Bar
	titleBar: {
		height: screenHeight * 0.15,
		width: screenWidth,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		backgroundColor: color.darkBlue,
		paddingLeft: screenWidth * 0.05,
		paddingRight: screenWidth * 0.05,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	logo: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		resizeMode: 'contain'
	},
	titleBarIconButton: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	titleBarIcon: {
		height: screenHeight * 0.06,
		width: screenHeight * 0.06,
		resizeMode: 'contain',
		tintColor: '#fff'
	},
	container: {
		height: screenHeight * 0.88,
		width: screenWidth,
		backgroundColor: color.lightBlue,
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	scrollView: {
		paddingTop: screenHeight * 0.03
	},
	greetingCard: {
		height: screenHeight * 0.12
	},
	cardRowText: {
		marginLeft: screenWidth * 0.05
	},
	greetingIcon: {
		marginLeft: screenWidth * 0.05
	},
	cardRowHeader: {
		height: screenHeight * 0.1,
        width: screenWidth * 0.9,
        backgroundColor: color.cardBlue,
        flexDirection: 'row',
        paddingLeft: screenWidth * 0.05,
        paddingRight: screenWidth * 0.05,
        alignItems: 'center'
	},
	mediaCardColumn: {
		height: screenHeight * 0.55,
		flexDirection: 'column',
		justifyContent: 'flex-start'
	},
	cardMedia: {
		height: screenHeight * 0.35,
		width: screenWidth * 0.65,
		borderRadius: screenHeight * 0.01,
		marginTop: screenHeight * 0.03
	},

	// Placeholder stuff
    placeholderBlur: {
    	height: screenHeight * 0.4,
    	width: screenWidth * 0.9,
    	borderRadius: screenWidth * 0.03,
    	justifyContent: 'space-around',
    	alignItems: 'center',
    	backgroundColor: '#000000',
    	paddingHorizontal: screenWidth * 0.1,
    	paddingVertical: screenHeight * 0.05
    },
    placeholderActionButton: {
    	height: screenHeight * 0.12,
    	width: screenWidth * 0.3,
    	borderRadius: screenWidth * 0.01,
    	justifyContent: 'center',
    	alignItems: 'center',
    	backgroundColor: color.green
    }

});

export default styles;