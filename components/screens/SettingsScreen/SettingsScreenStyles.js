import { StyleSheet, Dimensions} from 'react-native';
import { color } from '../../reusable/common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

if (screenHeight < screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const styles = StyleSheet.create({

	/*
		Contents:
			0. Title Bar
			1. Body
			2. Greeting Blur
			3. Picker styles
	*/

	// 0. Title Bar
	titleBar: {
		height: screenHeight * 0.15,
		width: screenWidth,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		backgroundColor: color.darkBlue,
		paddingLeft: screenWidth * 0.05,
		paddingRight: screenWidth * 0.05,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	logo: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		resizeMode: 'contain'
	},
	titleBarIconButton: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	titleBarIcon: {
		height: screenHeight * 0.07,
		width: screenHeight * 0.07,
		resizeMode: 'contain',
		tintColor: '#fff'
	},
	confirmIcon: {
		tintColor: color.green
	},

	// 1. Body
	container: {
		height: screenHeight,
		width: screenWidth,
		backgroundColor: color.lightBlue,
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	keyboardScrollView: {
		flex: 1,
		width: screenWidth,
		alignItems: 'center'
	},
	image: {
		height: screenHeight * 0.35,
		width: screenWidth * 0.65,
		alignSelf: 'center',
		marginBottom: screenHeight * 0.03,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3,
		borderRadius: 10
	},
	mainDetailsCard: {
		height: screenHeight * 0.45,
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
		paddingLeft: screenWidth * 0.05
	},
	mainDetailsInputs: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.8,
		marginLeft: screenWidth * 0.03,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginTop: screenHeight * 0.02,
		marginBottom: screenHeight * 0.02
	},
	creatorModeToggle: {
		height: screenHeight * 0.2,
		width: screenWidth * 0.8,
		marginLeft: screenWidth * 0.03,
		justifyContent: 'space-around',
		alignItems: 'flex-start'
	},
	familyDetailsCard: {
		height: screenHeight * 0.35,
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
		paddingLeft: screenWidth * 0.05
	},
	familyDetailsInputs: {
		height: screenHeight * 0.1,
		width: screenWidth * 0.8,
		marginLeft: screenWidth * 0.03,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginTop: screenHeight * 0.02
	},
	textInputContainer: {
		height: screenHeight * 0.07,
		width: screenWidth * 0.7,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginBottom: screenHeight * 0.01
	},
	creatorModeContainer: {
		height: screenHeight * 0.18,
		width: screenWidth * 0.7,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginBottom: screenHeight * 0.01
	},
	textInput: {
		fontFamily: 'Futura',
		fontSize: 20,
		color: '#fff',
		height: screenHeight * 0.05,
		width: screenWidth * 0.7,
		borderBottomWidth: 3,
		borderColor: color.lightBlue
	},
	scrollView: {
		paddingTop: screenHeight * 0.03
	},
	cardRowText: {
		marginLeft: screenWidth * 0.05
	},
	greetingIcon: {
		marginLeft: screenWidth * 0.05
	},
	cardRowHeader: {
		height: screenHeight * 0.15,
        width: screenWidth * 0.9,
        backgroundColor: color.cardBlue,
        flexDirection: 'row',
        paddingLeft: screenWidth * 0.05,
        paddingRight: screenWidth * 0.05,
        alignItems: 'center'
	},
	mediaCardColumn: {
		height: screenHeight * 0.6
	},
	cardMedia: {
		height: screenHeight * 0.35,
		width: screenWidth * 0.65,
		borderRadius: screenHeight * 0.01
	},
	changeProfilePicture: {
		height: screenHeight * 0.25,
		width: screenWidth * 0.8,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	profilePicture: {
		height: screenHeight * 0.15,
		width: screenHeight * 0.15,
		borderRadius: screenHeight * 0.075
	},
	editProfilePictureButton: {
		height: screenHeight * 0.1,
		width: screenWidth * 0.45,
		borderRadius: 10,
		backgroundColor: color.yellow,
		justifyContent: 'center',
		alignItems: 'center'
	},
	familyActionRow: {
		height: screenHeight * 0.15,
		width: screenWidth * 0.8,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	familyStateText: {
		width: screenWidth * 0.4,
		textAlign: 'center'
	},
	familyActionButton: {
		height: screenHeight * 0.1,
		width: screenWidth * 0.35,
		borderRadius: 10,
		backgroundColor: color.yellow,
		justifyContent: 'center',
		alignItems: 'center'
	},

	// 2. Greeting Blur
	greetingBlur: {
		position: 'absolute',
		height: screenHeight,
		width: screenWidth,
		backgroundColor: '#000000',
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingHorizontal: screenWidth * 0.1,
		paddingVertical: screenHeight * 0.1
	},
	greetingLogo: {
		height: screenHeight * 0.2,
		width: screenHeight * 0.2
	},
	greetingContinueButton: {
		height: screenHeight * 0.1,
		width: screenWidth * 0.4,
		borderRadius: screenWidth * 0.01,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.green
	},

	// Picker Styles
	pickerContainer: {
		height: screenHeight * 0.06,
		width: screenWidth * 0.25,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		borderColor: color.lightBlue,
		borderBottomWidth: screenHeight * 0.003
	},
	picker: {
		height: screenHeight * 0.06,
		width: screenWidth * 0.25,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	pickerText: {
		fontFamily: 'Futura',
		fontSize: screenHeight * 0.02,
		color: '#fff'
	},
	pickerChevron: {
		position: 'relative',
		height: screenHeight * 0.03,
		width: screenHeight * 0.03,
		tintColor: '#444',
		top: '0%'
	}

});

export default styles;