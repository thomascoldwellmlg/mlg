import React from 'react';
import { Text,
		 View,
		 ScrollView,
		 FlatList,
		 Image,
		 TextInput,
		 Alert,
		 TouchableOpacity,
		 Switch } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './SettingsScreenStyles';
import gstyles from '../../reusable/GlobalStyles';
import { UserData,
		 UserId,
		 Append,
		 MemberData,
		 FetchFamilyIds,
		 ExternalFamilyData } from '../../../lib/Firebase';
import { UploadImage } from '../../../lib/AWS';
import { ImagePicker, BlurView } from 'expo';
import { Image as CacheableImage} from 'react-native-aws-cache';
import RNPickerSelect from 'react-native-picker-select';
import { years } from '../../reusable/common';
 
export default class SettingsScreen extends React.Component {

	static navigationOptions = {
		header: null
	}

	state = {
		profilePicture: require('../../../assets/icons/png/account.png'),
		name: '',
		username: '',
		email: '',
		dateOfBirth: '',
		backButtonEnabled: true,
		userData: {},
		familyData: {},
		familyId: '',
		familyStateText: 'To join or create a group type in an ID',
		familyIdEditable: true,
		onPressFamilyActionButton: null,
		familyActionButtonText: '',
		creatorModeEnabled: false,
		greetingEnabled: false
	}

	async componentDidMount() {
		// Upon navigation to the settings determine whether we came from 'login' or 'profile'
		// According disable chevron back button
		var navvedFrom = this.props.navigation.state.params.navvedFrom;
		console.log('Navved from ', navvedFrom);
		if (navvedFrom == 'login') {
			this.setState({backButtonEnabled: false});
			this.setState({greetingEnabled: true});
		}
		await this.getUserSettings();
		await this.getFamilySettings();
		var familyIds = await FetchFamilyIds();
		await this.setState({familyIds: familyIds});
	}

	async getUserSettings() {
		var userData = await UserData();
		await this.setState({userData: userData});
		// If coming from profile then all data WILL exist thus set the state of
		// all fields to the user's current data
		var navvedFrom = this.props.navigation.state.params.navvedFrom;
		if (navvedFrom == 'profile') {
			this.setState({
				profilePicture: {uri: userData.profilePicture},
				name: userData.name,
				username: userData.username,
				email: userData.email,
				dateOfBirth: userData.dateOfBirth,
				familyId: userData.familyId,
				creatorModeEnabled: userData.creatorMode
			});
		}
		else {
			this.setState({
				email: userData.email
			});
		}
	}

	async getFamilySettings() {
		var userData = await UserData();
		await this.setState({familyId: userData.familyId});
		console.log(this.state.familyId)
		// Check all possible family states, set UI and action button function appropriately
		if (userData.familyState == 'pending-approval') {
			this.setState({
				familyIdEditable: false,
				familyStateText: 'Awaiting approval',
				onPressFamilyActionButton: () => this.removeJoinFamilyRequest(),
				familyActionButtonText: 'Remove Request'
			});
			this.setState({familyId: userData.pendingFamilyId});
		}
		else if (userData.familyState == 'in-group') {
			this.setState({
				familyIdEditable: false,
				familyStateText: 'You are part of ' + this.state.familyId,
				onPressFamilyActionButton: () => this.leaveFamilyGroup(),
				familyActionButtonText: 'Leave Group'
			});
			this.setState({familyId: userData.familyId});
		}
		else {
			// If none then make editable and set UI to...
			this.setState({
				familyIdEditable: true,
				familyStateText: 'Type in an existing or new family ID',
				onPressFamilyActionButton: null,
				familyActionButtonText: ''
			});
		}
	}

	onPressBackButton() {
		if (this.state.backButtonEnabled == true) {
			this.saveSettings();
		}
	}

	async changeProfilePicture() {
		let photo = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: false,
            aspect: [4, 3],
            exif: true
        });
        if (photo.cancelled != true) {
        	const currentUser = await UserId();
        	const uri = photo.uri;
        	const name = 'profilePicture';
        	const path = 'users/' + currentUser + '/';
        	var profilePictureUrl = await UploadImage(uri, name, path);
        	await Append({profilePicture: profilePictureUrl}, path);
        	this.setState({profilePicture: {uri: profilePictureUrl}});
        }
	}

	async onFamilyIdInputChange(familyId) {
		// Upon the entered family Id changing we need to check whether the family Id exists or not
		// if exists then set mode to send request else set mode to create family group
		await this.setState({familyId});
		if (this.state.familyId != '') {
			var foundMatchingFamilyId = false;
			console.log('current family Id = ', this.state.familyId)
			for (var i = 0; i <= this.state.familyIds.length - 1; i++) {
				if (this.state.familyIds[i] == this.state.familyId) {
					foundMatchingFamilyId = true;
				}
			}
			if (foundMatchingFamilyId == true) {
				this.setState({
					familyIdEditable: true,
					familyStateText: 'Join the group: ' + this.state.familyId,
					onPressFamilyActionButton: () => this.sendJoinFamilyRequest(),
					familyActionButtonText: 'Send Request'
				});
			}
			else {
				this.setState({
					familyIdEditable: true,
					familyStateText: 'Create the group: ' + this.state.familyId,
					onPressFamilyActionButton: () => this.createFamilyGroup(),
					familyActionButtonText: 'Create Group'
				});
			}
		}
		else {
			// If none then make editable and set UI to...
			this.setState({
				familyIdEditable: true,
				familyStateText: 'Type in an existing or new family ID',
				onPressFamilyActionButton: null,
				familyActionButtonText: ''
			});
		}
	}

	async sendJoinFamilyRequest() {
		// Set current user as pending member in target family group
		const currentUser = await UserId();
		const familyPath = 'families/' + this.state.familyId + '/pendingMembers/';
		await Append({[currentUser]: 0}, familyPath);
		// Then set the family state on the current users db, also set familyId to 'pending'
		// giving access to a placeholder family group till approval
		const userPath = 'users/' + currentUser;
		await Append({familyState: 'pending-approval', familyId: 'Pending', pendingFamilyId: this.state.familyId}, userPath);
		// Finally update the UI to reflect the changes
		this.getFamilySettings();
	}

	async createFamilyGroup() {
		// Get current userId and create family group
		const currentUser = await UserId();
		const familyPath = 'families/' + this.state.familyId;
		const data = {
			familyAdmin: currentUser,
			members: { [currentUser]: 0 }
		}
		await Append(data, familyPath);
		// Then update users db to reflect changes
		const userPath = 'users/' + currentUser;
		await Append({familyState: 'in-group', familyId: this.state.familyId}, userPath);
		this.getFamilySettings();
	}

	async removeJoinFamilyRequest() {
		// Set current user to null as pending member in target family group removing from db
		const currentUser = await UserId();
		const familyPath = 'families/' + this.state.userData.pendingFamilyId + '/pendingMembers/';
		await Append({[currentUser]: null}, familyPath);
		// Then set the family state on the current users db, also set familyId to 'pending'
		// giving access to a placeholder family group till approval
		const userPath = 'users/' + currentUser;
		await Append({familyState: 'none', familyId: '', pendingFamilyId: ''}, userPath);
		// Finally update the UI to reflect the changes
		this.setState({familyId: ''});
		this.getFamilySettings();
	}

	async leaveFamilyGroup() {
		await Alert.alert('Warning!',
			              'Are you sure you want to leave the family group?',
			              [{text: 'Confirm', onPress: async () => confirmLeave()}, {text: 'Cancel'}]);
		const confirmLeave = async () => {
			// Get current userId and null member in family group
			const currentUser = await UserId();
			const familyPath = 'families/' + this.state.familyId + '/members/';
			const data = { [currentUser]: null }
			await Append(data, familyPath);
			// Then update users db to reflect changes
			const userPath = 'users/' + currentUser;
			await Append({familyState: 'none', familyId: '', pendingFamilyId: ''}, userPath);
			this.setState({familyId: ''});
			this.getFamilySettings();
		}
		
	}

	async saveSettings() {
		// Set initial flag, if any sections are incomplete set flag to false
		var allowSave = true;
		if (this.state.name == '') {
			allowSave = false;
		}
		if (this.state.familyIdEditable == true) {
			allowSave = false;
		}
		if (allowSave == true) {
			const userId = await UserId();
			const path = 'users/' + userId;
			const data = {
				name: this.state.name,
				username: this.state.username,
				email: this.state.email,
				dateOfBirth: this.state.dateOfBirth,
				creatorMode: this.state.creatorModeEnabled
			}
			// await Append(data, path);
			// Select the appropriate navigation to either the creator or player home screen
			if (this.state.creatorModeEnabled == true) {
				this.props.navigation.navigate('MyProfileScreen');
			}
			else {
				this.props.navigation.navigate('PlayerProfileScreen');
			}
		}
		else {
			Alert.alert('Unable to save. One or more details are incorrect or misisng from your profile.');
		}
	}

	toggleCreatorMode() {
		if (this.state.creatorModeEnabled == true) {
			this.setState({creatorModeEnabled: false});
		}
		else {
			this.setState({creatorModeEnabled: true});
		}
	}

	selectedValue(item) {
		setTimeout(() => this.selectedValue(item), 10);
		return(this.state.dateOfBirth);
	}

	uiTitleBar() {
		return(
			<View style={styles.titleBar}>
				<TouchableOpacity style={styles.titleBarIconButton}
								  onPress={() => this.onPressBackButton()}>
					<View style={gstyles.shadow}>
						<Image style={styles.titleBarIcon} source={require('../../../assets/icons/png/chevron-left.png')} />
					</View>
				</TouchableOpacity>
				<Text style={gstyles.largeText}>Settings</Text>
				<TouchableOpacity style={styles.titleBarIconButton}
								  onPress={() => this.saveSettings()}>
				    <View style={gstyles.shadow}>
						<Image source={require('../../../assets/icons/png/check.png')}
							   style={[styles.titleBarIcon, styles.confirmIcon]} />
				    </View>
			    </TouchableOpacity>
			</View>
		);
	}

	uiProfilePicture() {
		if (this.state.profilePicture == require('../../../assets/icons/png/account.png')) {
			return(
				<Image style={styles.profilePicture} source={this.state.profilePicture} />
			);
		}
		else {
			return(
				<CacheableImage style={styles.profilePicture} source={this.state.profilePicture} allowsUpdate={true}/>
			);
		}
	}

	uiBody() {
		return(
			<KeyboardAwareScrollView extraHeight={150}>
			<View style={styles.keyboardScrollView}>
				<View style={styles.changeProfilePicture}>
					<View style={gstyles.shadow}>
						{this.uiProfilePicture()}
					</View>
					<TouchableOpacity style={styles.editProfilePictureButton} onPress={() => this.changeProfilePicture()}>
						<Text style={gstyles.smallText}>Change Profile Picture</Text>
					</TouchableOpacity>
				</View>
                <View style={[gstyles.cardColumn, styles.mainDetailsCard]}>
                	<Text style={gstyles.smallText}>Main User Details</Text>
                	<View style={styles.mainDetailsInputs}>
                		<TouchableOpacity style={styles.textInputContainer}
                					      onPress={() => this.refs.nameInput.focus()}>
                			<Text style={gstyles.subTitle}>Name</Text>
                			<TextInput style={styles.textInput}
                					   ref='nameInput'
                					   placeholder='Enter your full name'
                					   value={this.state.name}
                					   onChangeText={(name) => this.setState({name})} />
                		</TouchableOpacity>
                		{/*<TouchableOpacity style={styles.textInputContainer}>
                			<Text style={gstyles.subTitle}>Date of Birth</Text>
                			<RNPickerSelect style={{viewContainer: styles.pickerContainer,
	                	    						inputIOSContainer: styles.picker,
	                	    					    inputIOS: styles.pickerText,
	                	    						icon: styles.pickerChevron}}
            	    						selectedValue={this.selectedValue(this.state.dateOfBirth)}
	                	    			    value={this.state.dateOfBirth}
	                	    		        onValueChange={(d, i) => { this.setState({dateOfBirth: d})}}
	            	    					items={years} />
                		</TouchableOpacity>*/}
            		</View>
            		<Text style={gstyles.smallText}>Creator Mode</Text>
            		<View style={styles.creatorModeToggle}>
	            		<View style={styles.creatorModeContainer}>
	            			<Text style={gstyles.subTitle}>
	            			To allow access to building games and uploading photos to the family album enable this mode!
	            			Note this is only recommended for the family of those with dementia or the elderly.
	            			</Text>
	            			<Switch onValueChange={() => this.toggleCreatorMode()}
	            					value={this.state.creatorModeEnabled}/>
	            		</View>
            		</View>
                </View>
                <View style={[gstyles.cardColumn, styles.familyDetailsCard]}>
                	<Text style={gstyles.smallText}>Family Group Details</Text>
                	<View style={styles.familyDetailsInputs}>
                		<TouchableOpacity style={styles.textInputContainer}
                					      onPress={() => this.refs.idInput.focus()}>
                			<Text style={gstyles.subTitle}>My Family Group ID</Text>
                			<TextInput style={styles.textInput}
                					   ref='idInput'
                					   editable={this.state.familyIdEditable}
                					   placeholder='Enter your family group ID'
                					   value={this.state.familyId}
                					   onChangeText={(familyId) => this.onFamilyIdInputChange(familyId)} />
                		</TouchableOpacity>
                	</View>
                	<View style={styles.familyActionRow}>
                		<Text style={[gstyles.mediumText, styles.familyStateText]}>{this.state.familyStateText}</Text>
                		<TouchableOpacity style={styles.familyActionButton} onPress={this.state.onPressFamilyActionButton}>
							<Text style={gstyles.smallText}>{this.state.familyActionButtonText}</Text>
						</TouchableOpacity>
                	</View>
                </View>
            </View>
            </KeyboardAwareScrollView>
		);
	}

	uiBlur() {
		if (this.state.greetingEnabled == true) {
			return(
				<BlurView tint='dark' intensity={90} style={styles.greetingBlur}>
					<View style={gstyles.shadow}>
						<Image style={styles.greetingLogo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
					</View>
					<Text style={gstyles.largeText}>Welcome!</Text>
					<Text style={[gstyles.smallText, {textAlign: 'center'}]}>
					Congratulations! You've nearly finished creating your MLG account -
					all we need now is a lovely photo of you (so family members can recognise you),
					a few personal details and finally to either join a family group (setup by your
					family admin) or create a new family group if you intend to be the family admin!
					</Text>
					<TouchableOpacity style={styles.greetingContinueButton}
									  onPress={() => this.setState({greetingEnabled: false})}>
					    <Text style={gstyles.mediumText}>Continue!</Text>
					</TouchableOpacity>	
				</BlurView>
			);
		}
	}

	render() {
		return(
			<View style={styles.container}>
				{this.uiTitleBar()}
				{this.uiBody()}
				{this.uiBlur()}
			</View>
		);
	}

}