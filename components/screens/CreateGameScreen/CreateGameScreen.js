import React from 'react';
import { View, 
		 Text,
		 Image,
		 TouchableOpacity,
		 TextInput,
		 FlatList,
		 Dimensions,
		 ActivityIndicator,
		 Alert,
		 StyleSheet,
		 Platform,
		 Animated } from 'react-native';
import GridView from 'react-native-super-grid';
import styles from './CreateGameScreenStyles';
import gstyles from '../../reusable/GlobalStyles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { UserData,
		 UserId,
		 MemberData,
		 FamilyData,
		 Append } from '../../../lib/Firebase';
import { getNeatDate } from '../../../lib/Custom';
import { color, months } from '../../reusable/common';
import EasySlideshow from '../../reusable/EasySlideshow/EasySlideshow';
import { Location, FaceDetector, ImageManipulator, BlurView, Camera, ImagePicker, Audio, Video, ScreenOrientation, FileSystem} from 'expo';
import { UploadImage, UploadAudio } from '../../../lib/AWS';
import VoiceRecorder from '../../reusable/VoiceRecorder/VoiceRecorder';
import { sendAddFamilyPhotoNotification } from '../../../lib/Notifications';
import { Image as CacheableImage} from 'react-native-aws-cache';
import CardFlip from 'react-native-card-flip';

const DEMO_DIR = `${FileSystem.documentDirectory}mlgDemoModeAssets/`;

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

const animationImages = [
    {
        image: require('../../../assets/images/animations/questionImage1.jpg'),
        question: require('../../../assets/images/animations/completeQuestion1.png')
    },
    {
        image: require('../../../assets/images/animations/questionImage2.jpg'),
        question: require('../../../assets/images/animations/completeQuestion2.png')
    },
    {
        image: require('../../../assets/images/animations/questionImage3.jpg'),
        question: require('../../../assets/images/animations/completeQuestion3.png')
    }
]

export default class CreateGameScreen extends React.Component {

	state = {
		stage: 'add-image',
		addImageStage: 1,
		enableQuestionNavigationArrows: false,
		enableSectionNavigator: false,
		titleBarText: 'A quick tutorial',
		activeSection: 'a',
		sectionState: {
			a: {
				status: null,
				currentlySelected: styles.selectedSection3rds,
				nav: () => this.navToAddImage(),
				text: 'My Photo'
			},
			b: {
				status: null,
				currentlySelected: styles.unselectedSection3rds,
				nav: () => this.navToAddMainInfo(),
				text: 'Title and Answers'
			},
			c: {
				status: null,
				currentlySelected: styles.unselectedSection3rds,
				nav: () => this.navToAddHint(),
				text: 'Question Hint'
			},
			d: {
				status: null,
				currentlySelected: styles.unselectedSection3rds,
				nav: () => this.navToAdditionalQuestion(),
				text: 'Additional Question'
			}
		},
		addImagePrompt: 'Add a photo|\nChoose a photo of your family at Christmas or your skiing trip to France! \n Landscape photos work best!',
		addImagePhoto: require('../../../assets/images/splash-screen-image.jpg'),
		newImageData: {},
		cameraEnabled: false,
		cameraDirection: Camera.Constants.Type.back,
		enableVoiceRecorder: false,
		voiceRecorderStage: 'start',
		name: '',
		description: '',
		location: '',
		dateTaken: '',
		faceDetectionResults: '',
		isUploading: false,
		isFaceDetecting: true,
		gameData: [
			{
				title: '',
				image: {},
				correctAnswer: '',
				dummyAnswer1: '',
				dummyAnswer2: '',
				dummyAnswer3: '',
				questionHint: '',
				additionalQuestion: ''
			}
		],
		gameTitle: 'Game 1',
		selectedRecipients: [],
		familyMembers: [],
		attachedMessage: '',
		currentQuestion: 0,
		questionHintPrompt: 'Please record a question hint to help your player out if they get stuck!',
		questionHintButtonColor: {backgroundColor: color.red},
		additionalQuestionPrompt: 'Feel free to ask anything else for your players to answer!',
	    additionalQuestionButtonColor: {backgroundColor: color.red},
	    enableMainMenu1: false,
	    enableMainMenu2: false,
	    enableQuestionPreview: false,
	    slideshowData: [],
	    buildTutorialOverlayEnabled: true,
	    videoTutorialEnabled: false,
	    addFamilyPhotoDetailsExpanded: false,
        animationCard: {
            marginLeft: new Animated.Value(0),
            opacity: new Animated.Value(0)
        },
        animationImageIndex: 0,
        demoMode: null
	}

	async componentDidMount() {
		// Check if we are in demo mode
		var demoMode = await Expo.SecureStore.getItemAsync('demo-mode');
		if (demoMode == 'true') {
			console.log('demoMode ==== > ', demoMode)
			this.setState({demoMode: true});
		}
		ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE);
		await this.getFamilyMembers();
		await this.generateSlideshow();
		this.startBuilding();
		this.startSplashScreenAnimation();
		this.setGameTitle();
	}

	startSplashScreenAnimation() {
        this.card.flip();
        // Animations for the starting iamge
        Animated.loop(Animated.sequence([
            Animated.delay(1000),
            Animated.parallel([
                Animated.timing(
                    this.state.animationCard.marginLeft,
                    {
                        toValue: screenWidth * 0.36,
                        duration: 3000,
                        useNativeDriver: false,
                        onComplete: () => {
                        	if (this.card != null) {
	                            this.card.flip();
	                            this.setState({animationCaption: 'Into your games...'});
                            }
                        }
                    }
                ),
                Animated.timing(
                    this.state.animationCard.opacity,
                    {
                        toValue: 1.0,
                        duration: 1500,
                        useNativeDriver: false
                    }
                )
            ]),
            Animated.delay(5000),
            Animated.parallel([
                Animated.timing(
                    this.state.animationCard.marginLeft,
                    {
                        toValue: screenWidth * 0.8,
                        duration: 3000,
                        useNativeDriver: false,
                        onComplete: () => {
                        	if (this.card != null) {
	                            this.card.flip();
	                            this.setState({animationCaption: 'Turning your photos...'});
	                            // Advance or reset the image index so it cycles round
                                if (this.state.animationImageIndex == 2) {
                                    this.setState({animationImageIndex: 0});
                                }
                                else {
                                    this.setState({animationImageIndex: this.state.animationImageIndex + 1});
                                }
                            }
                        }
                    }
                ),
                Animated.timing(
                    this.state.animationCard.opacity,
                    {
                        toValue: 0.0,
                        duration: 1500,
                        useNativeDriver: false,
                        delay: 1500
                    }
                )
            ])
        ])).start();
    }

    async setGameTitle() {
    	if (this.state.demoMode != true) {
	    	var userData = await UserData();
	    	this.setState({gameTitle: userData.name.split(' ')[0] + "'s Game 1"});
    	}
    	else {
	    	this.setState({gameTitle: "John's Game 1"});
    	}
    }

	async getFamilyMembers() {
		// Check if we are in demo mode and retrieve from either firebase or the demo manager
		if (this.state.demoMode != true) {
			// Get the PP, name and uid for each family member for the recipients flatlist
			var familyData = await FamilyData();
			var userId = await UserId();
			var uids = Object.keys(familyData.members);
			// Remove current userId
			await uids.splice(uids.indexOf(userId), 1);
			var familyMembers = [];
			for (var i = 0; i <= uids.length - 1; i++) {
				var memberData = await MemberData(uids[i]);
				familyMembers.push({
					name: memberData.name,
					profilePicture: memberData.profilePicture,
					uid: uids[i],
					selected: false,
					styleStatus: {
						height: screenHeight * 0.07,
						width: screenHeight * 0.07,
						tintColor: color.grey,
						opacity: 0.7
					}
				});
			}
			this.setState({familyMembers: familyMembers});
		}
		else {
			// Get the PP, name and uid for each family member for the recipients flatlist
			var demoData = await FileSystem.readAsStringAsync(DEMO_DIR + 'manager.json');
			demoData = JSON.parse(demoData);
			var familyData = demoData.families.Miller;
			var userId = 'Hr3ApcPo2UWl1jcPjFPpozDj9fr2';
			var uids = Object.keys(familyData.members);
			// Remove current userId
			await uids.splice(uids.indexOf(userId), 1);
			var familyMembers = [];
			for (var i = 0; i <= uids.length - 1; i++) {
				var memberData = demoData.users[uids[i]];
				familyMembers.push({
					name: memberData.name,
					profilePicture: memberData.profilePicture,
					uid: uids[i],
					selected: false,
					styleStatus: {
						height: screenHeight * 0.07,
						width: screenHeight * 0.07,
						tintColor: color.grey,
						opacity: 0.7
					}
				});
			}
			this.setState({familyMembers: familyMembers});
		}
	}

	generateSlideshow() {
		var images = [];
		const gameData = this.state.gameData;
		for (var i = 0; i <= gameData.length - 1; i++) {
			console.log(gameData[i])
			images.push({url: gameData[i].image.mainImageUrl, 
						 title: gameData[i].title,
						 caption: gameData[i].correctAnswer});
		}
		this.setState({slideshowData: images});
		console.log(this.state.slideshowData);
	}

	updateSectionState(section, obj, value) {
		// Method to save lines of code setting the states of the individual sections
		const ref = this.state.sectionState;
		ref[section][obj] = value;
		this.setState({sectionState: ref});
	}

	updateGameState(obj, value) {
		// Method to save lines of code setting the states of the individual sections
		const ref = this.state.gameData;
		ref[this.state.currentQuestion][obj] = value;
		this.setState({gameData: ref});
	}

	async changeSection(nextSection) {
		var stage = this.state.stage;
		if (stage == 'add-image' || stage == 'add-main-info' || stage == 'add-question-hint') {
			await this.updateSectionState(this.state.activeSection, 'currentlySelected', styles.unselectedSection3rds);
			await this.updateSectionState(nextSection, 'currentlySelected', styles.selectedSection3rds);
		}
		else {
			await this.updateSectionState(this.state.activeSection, 'currentlySelected', styles.unselectedSection);
			await this.updateSectionState(nextSection, 'currentlySelected', styles.selectedSection);
		}
		this.setState({activeSection: nextSection});
	}

	navToAddImage() {
		this.changeSection('a');
		this.setState({stage: 'add-image',
					   addImageStage: 1});
		if (this.state.addImagePhoto == require('../../../assets/images/splash-screen-image.jpg')) {
			this.setState({addImagePrompt: 'Add a photo|\nChoose a photo of your family at Christmas or your skiing trip to France! \n Landscape photos work best!'});
		}
		else {
			this.setState({addImagePrompt: 'Change your photo?'});
		}
		this.checkQuestionSectionStatus();
	}

	navToAddMainInfo() {
		this.changeSection('b');
		this.setState({stage: 'add-main-info'});
		this.checkQuestionSectionStatus();
	}

	navToAddHint() {
		this.changeSection('c');
		this.setState({stage: 'add-question-hint'});
		this.checkQuestionSectionStatus();
	}

	navToAdditionalQuestion() {
		this.changeSection('d');
		this.setState({stage: 'add-additional'});
		this.checkQuestionSectionStatus();
	}

	async navToCreatedGame() {
		// Set the stage back to the add image default and then reset the section navigator
		var sections = ['a', 'b', 'c', 'd'];
		var newSectionNames = [
			'My Photo',
			'Title and Answers',
			'Question Hint',
			'Additional Question'
		];
		var newNavDirections = [
			() => this.navToAddImage(),
			() => this.navToAddMainInfo(),
			() => this.navToAddHint(),
			() => this.navToAdditionalQuestion()
		];
		for (var k = 0; k <= sections.length - 1; k++) {
			this.updateSectionState(sections[k], 'text', newSectionNames[k]);
			this.updateSectionState(sections[k], 'nav', newNavDirections[k]);
			this.updateSectionState(sections[k], 'currentlySelected', styles.unselectedSection3rds);
		}
		this.updateSectionState('a', 'currentlySelected', styles.selectedSection3rds);
		await this.setState({stage: 'add-image'});
		this.setState({addImageStage: 1});
		this.checkQuestionSectionStatus();
		this.changeSection('a');
		this.setState({titleBarText: 'Question ' + (this.state.currentQuestion + 1).toString()});
		this.setState({enableQuestionNavigationArrows: true});

	}

	navToAddGameTitle() {
		this.setState({stage: 'game-title'});
		this.checkFinaliseSectionStatus();
		this.changeSection('b');
		this.setState({titleBarText: 'Add Game Title'});
	}

	navToSelectRecipients() {
		this.setState({stage: 'select-recipients'});
		this.checkFinaliseSectionStatus();
		this.changeSection('c');
		this.setState({titleBarText: 'Select Recipients'});
	}

	navToSend() {
		this.setState({stage: 'send'});
		this.checkFinaliseSectionStatus();
		this.changeSection('d');
		this.setState({titleBarText: 'Preview and Send'});
	}

	async checkQuestionSectionStatus() {
		// Check the status of each section - set the status icon appropriately
		if (this.state.addImagePhoto != require('../../../assets/images/splash-screen-image.jpg')) {
			await this.updateSectionState('a', 'status', {backgroundColor: color.green});
		}
		else {
			this.updateSectionState('a', 'status', {backgroundColor: color.grey});
		}
		var mainInfoSections = ['title', 'correctAnswer', 'dummyAnswer1', 'dummyAnswer2', 'dummyAnswer3'];
		var mainInfoComplete = true;
		for (var i = 0; i <= mainInfoSections.length - 1; i++) {
			if (this.state.gameData[this.state.currentQuestion][mainInfoSections[i]] == '') {
				mainInfoComplete = false;
			}
		}
		if (mainInfoComplete == true) {
			this.updateSectionState('b', 'status', styles.sectionStatus);
		}
		else {
			this.updateSectionState('b', 'status', {backgroundColor: color.grey});
		}
		if (this.state.gameData[this.state.currentQuestion].questionHint != '' &&
			this.state.gameData[this.state.currentQuestion].questionHint != null) {
			this.updateSectionState('c', 'status', styles.sectionStatus);
		}
		else {
			this.updateSectionState('c', 'status', {backgroundColor: color.grey});
		}
		if (this.state.gameData[this.state.currentQuestion].additionalQuestion != '' &&
			this.state.gameData[this.state.currentQuestion].additionalQuestion != null) {
			this.updateSectionState('d', 'status', styles.sectionStatus);
		}
		else {
			this.updateSectionState('d', 'status', {backgroundColor: color.grey});
		}
	}

	async checkFinaliseSectionStatus() {
		// Check the status of each section - set the status icon appropriately
		await this.updateSectionState('a', 'status', {backgroundColor: color.green});
		if (this.state.gameTitle != '') {
			this.updateSectionState('b', 'status', styles.sectionStatus);
		}
		else {
			this.updateSectionState('b', 'status', {backgroundColor: color.grey});
		}
		// Check that recipients have been added
		var familyMembers = this.state.familyMembers;
		var recipientsPresent = false;
		for (var i = 0; i <= familyMembers.length - 1; i++) {
			if (familyMembers[i].selected == true) {
				recipientsPresent = true;
			}
		}
		if (recipientsPresent == true) {
			this.updateSectionState('c', 'status', styles.sectionStatus);
		}
		else {
			this.updateSectionState('c', 'status', {backgroundColor: color.grey});
		}
		if (this.state.attachedMessage != '') {
			this.updateSectionState('d', 'status', styles.sectionStatus);
		}
		else {
			this.updateSectionState('d', 'status', {backgroundColor: color.grey});
		}
	}

	checkQuestionHintPresent() {
		if (this.state.gameData[this.state.currentQuestion].questionHint == '' ||
			this.state.gameData[this.state.currentQuestion].questionHint == null) {
			this.setState({questionHintPrompt: 'Please record a question hint to help your player out if they get stuck!',
						   questionHintButtonColor: {backgroundColor: color.red}});
		}
		else {
			this.setState({questionHintPrompt: 'Thanks for recording that hint! To change it press the button below again!',
						   questionHintButtonColor: {backgroundColor: color.yellow}});
		}
	}

	checkAdditionalQuestionPresent() {
		if (this.state.gameData[this.state.currentQuestion].additionalQuestion == '' ||
			this.state.gameData[this.state.currentQuestion].additionalQuestion == null) {
			this.setState({additionalQuestionPrompt: 'Feel free to ask anything else for your players to answer!',
						   additionalQuestionButtonColor: {backgroundColor: color.red}});
		}
		else {
			this.setState({additionalQuestionPrompt: 'Thanks for recording your question! To change it press the button below again!',
						   additionalQuestionButtonColor: {backgroundColor: color.yellow}});
		}
	}

	startBuilding() {
		this.setState({
			enableQuestionNavigationArrows: true,
			enableSectionNavigator: true,
			titleBarText: 'Question 1',
			stage: 'add-image'
		});
	}

	async chooseFromFamilyAlbum() {
		await this.getFamilyAlbum();
	}

	async getFamilyAlbum() {
		if (this.state.demoMode != true) {
			// Use Firebase to get user data 
			var userData = await UserData();
			this.setState({userData: userData});
			// Then using the familyId get the whole family album library
			var familyData = await FamilyData();
			if (familyData.familyAlbum != undefined) {
				var keys = Object.keys(familyData.familyAlbum);
				var familyAlbum = [];
				for (var i = keys.length - 1; i >= 0; i--) {
					var image = familyData.familyAlbum[keys[i]];
					image.index = i;
					familyAlbum.push(image);
				}
				this.setState({familyAlbum: familyAlbum});
				this.setState({addImageStage: 2});
			}
			else {
				this.setState({addImageStage: 1});
				Alert.alert('It appears your family album is currently empty!');
			}
		}
		else {
			var demoData = await FileSystem.readAsStringAsync(DEMO_DIR + 'manager.json');
			demoData = JSON.parse(demoData);
			// Use Firebase to get user data 
			var userData = demoData.users.Hr3ApcPo2UWl1jcPjFPpozDj9fr2;
			this.setState({userData: userData});
			// Then using the familyId get the whole family album library
			var familyData = demoData.families.Miller;
			if (familyData.familyAlbum != undefined) {
				var keys = Object.keys(familyData.familyAlbum);
				var familyAlbum = [];
				for (var i = keys.length - 1; i >= 0; i--) {
					var image = familyData.familyAlbum[keys[i]];
					image.index = i;
					familyAlbum.push(image);
				}
				this.setState({familyAlbum: familyAlbum});
				this.setState({addImageStage: 2});
			}
			else {
				this.setState({addImageStage: 1});
				Alert.alert('It appears your family album is currently empty!');
			}
		}
	}

	async onPressFamilyAlbumPhoto(item) {
		console.log('Family Album Item = ', item);
		await this.updateGameState('image', item);
        this.setState({stage: 'add-main-info'});
        await this.setState({addImagePhoto: {uri: item.mainImageUrl}});
        this.checkQuestionSectionStatus();
        this.changeSection('b');
	}

	async chooseFromCameraRoll() {
		let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: false,
            aspect: [4, 3],
            exif: true
        });
		if (result.cancelled != true) {
			this.setState({newImageData: result});
			this.setState({addImageStage: 3});
			this.getCurrentPhotoData();
			this.setState({addFamilyPhotoDetailsExpanded: false});
		}
	}

	takeNewPhoto() {
		this.setState({cameraEnabled: true});
	}

	rotateCamera() {
		if (this.state.cameraDirection == Camera.Constants.Type.back) {
			this.setState({cameraDirection: Camera.Constants.Type.front});
		}
		else {
			this.setState({cameraDirection: Camera.Constants.Type.back});
		}
	}

	onShutterPress = async () => {
	    if (this.camera) {
	    	var newImageData = await this.camera.takePictureAsync({exif: true});
	    	this.setState({cameraEnabled: false});
	    	this.setState({newImageData: newImageData});
	    	this.setState({addImageStage: 3});
	    	this.getCurrentPhotoData();
	    	this.setState({addFamilyPhotoDetailsExpanded: false});
    	}
	}

	async getCurrentPhotoData() {
		// Now clean up an extract EXIF data of image to get date taken and location (city-wise)
		console.log(this.state.newImageData)
		if (this.state.newImageData.exif.GPSLatitude != undefined) {
			var location = await Location.reverseGeocodeAsync({latitude: this.state.newImageData.exif.GPSLatitude,
															   longitude: this.state.newImageData.exif.GPSLongitude});
			console.log(location)
			if (location.city != undefined && location.country != undefined) {
				var locationString = location.city + ' ' + location.country;
				await this.setState({location: locationString});
			}
		}
		// Get date from exif
		if (this.state.newImageData.exif.DateTimeDigitized != undefined) {
			var date = this.state.newImageData.exif.DateTimeDigitized.split(' ')[0].split(':');
			// Clean into standard string
			var month = months[date[1]];
			var day = date[2];
			var suffix = 'th';
			if (day.split('')[1] == '1') {
				suffix = 'st';
			}
			else if (day.split('')[1] == '2') {
				suffix = 'nd';
			}
			else if (day.split('')[1] == '3') {
				suffix = 'rd';
			}
			if (day.split('')[0] == '0') {
				day = day.split('')[1];
			}
			var displayDate = day + suffix + ' ' + month + ' ' + date[0];
			console.log(displayDate);
	 		this.setState({dateTaken: displayDate});
 		}
 		// Finally run face detection on the image
 		this.runFaceDetection(this.state.newImageData.uri);
	}

	async runFaceDetection(imageUri) {
    	// Method to perform face detection and finally sets the state of it's results
    	// to the faceDetectionResults state
    	const faceDetectorOptions = { mode: FaceDetector.Constants.Mode.fast };
    	var detectionResults = await FaceDetector.detectFacesAsync(imageUri, faceDetectorOptions);
    	console.log(detectionResults);
    	// Now we need to extract an individual image (uri) for each face detected
    	var faces = [];
    	for (var i = 0; i <= detectionResults.faces.length - 1; i ++) {
    		var faceBB = detectionResults.faces[i].bounds;
    		var boundingBoxedCroppedImage = await ImageManipulator.manipulate(
    			detectionResults.image.uri,
    			[{crop: {originX: faceBB.origin.x, 
    					 originY: faceBB.origin.y, 
    					 width: faceBB.size.width,
    					 height: faceBB.size.height}}]
			);
			faces.push(Object.assign({faceUri: boundingBoxedCroppedImage.uri}, detectionResults.faces[i]));
    		this.setState({['faceLabel_' + i.toString()]: ''});
    	}
    	console.log(' FACES ---->>>>>', faces)
    	var faceDetectionResults = {faces: faces, image: detectionResults.image};
    	await this.setState({faceDetectionResults: faceDetectionResults});
    	// Pass flag to enable save function after finishing face detection
    	this.setState({isFaceDetecting: false});
    }

    async saveToFamilyAlbum() {
    	if (this.state.demoMode != true) {
	    	if (this.state.isFaceDetecting == false) {
	    		// Set up the timeout function
	    		var uploadFailed = true;
	    		setTimeout(() => {
	    			if (uploadFailed == true) {
		    			Alert.alert('Image could not be uploaded. Please check your internet connection');
		    			this.setState({isUploading: false, isFaceDetecting: true, addImageStage: 1});
	    			}
	    		}, 12000);
		    	// Tell the whole compoenent we are uploading
		    	this.setState({isUploading: true});
		    	// Firstly upload the main image and the cropped face detction images to AWS
		    	const uniquePhotoId = Date.now().toString();
		    	var familyId = await UserData();
		    	familyId = familyId.familyId;
		    	const awsPath = 'families/' + familyId + '/familyAlbum/' + uniquePhotoId + '/';
		    	var mainImageUrl = await UploadImage(this.state.newImageData.uri,
		    										 'main',
		    										 awsPath);
		    	var faceImageUrls = [];
		    	var faces = this.state.faceDetectionResults.faces;
		    	for (var i = 0; i <= faces.length - 1; i++) {
		    		var faceUrl = await UploadImage(this.state.faceDetectionResults.faces[i].faceUri,
		    										i.toString(),
		    										awsPath);
		    		faceImageUrls.push(faceUrl);
		    	}
		    	// Now we have all the image Urls we need to update the data to the Firebade database
		    	const firebasePath = 'families/' + familyId + '/familyAlbum/' + uniquePhotoId + '/';
		    	// Form an object containing all of the data on each face in the image
		        var faceDataForFirebase = {};
		        for (var j = 0; j <= faceImageUrls.length - 1; j ++) {
		        	faceDataForFirebase[j.toString()] = {
		        		name: this.state['faceLabel_' + j.toString()],
		        		boundingBoxData: faces[j].bounds,
		        		faceImageUrl: faceImageUrls[j]
		        	};
		        }
		        var userId = await UserId();
		        var firebaseData = {mainImageUrl: mainImageUrl,
		        					uploadedBy: userId,
		        					title: this.state.name,
		        					description: this.state.description,
		        				    location: this.state.location,
		        				    dateTaken: this.state.dateTaken,
		        				    faces: faceDataForFirebase}
		        await Append(firebaseData, firebasePath);
		        // When finished uploading the photo to the family album ....
		        await sendAddFamilyPhotoNotification(uniquePhotoId);
		        this.setState({isUploading: false, isFaceDetecting: true});
		        // NAVIGATE TO ADD TITLE AND ANSWERS AND PASS ALONG THE IMAGE'S UNIQUE ID 
		        await this.updateGameState('image', firebaseData);
		        this.setState({stage: 'add-main-info'});
		        await this.setState({addImagePhoto: {uri: mainImageUrl}});
		        this.checkQuestionSectionStatus();
		        this.changeSection('b');
		        this.setState({name: '',
		    			       description: '',
		    			       location: '',
		    			       dateTaken: ''});
		        for (var n = 0; n < 10; n++) {
		        	this.setState({['faceLabel_' + n.toString()]: ''});
		        }
		        uploadFailed = false;
	        }
        }
        else {
        	if (this.state.isFaceDetecting == false) {
        		var demoData = await FileSystem.readAsStringAsync(DEMO_DIR + 'manager.json');
        		demoData = JSON.parse(demoData);
	    		// Set up the timeout function
	    		var uploadFailed = true;
	    		setTimeout(() => {
	    			if (uploadFailed == true) {
		    			Alert.alert('Image could not be uploaded. Please check your internet connection');
		    			this.setState({isUploading: false, isFaceDetecting: true, addImageStage: 1});
	    			}
	    		}, 12000);
		    	// Tell the whole compoenent we are uploading
		    	this.setState({isUploading: true});
		    	// Firstly upload the main image and the cropped face detction images to AWS
		    	const uniquePhotoId = Date.now().toString();
		    	var familyId = 'Miller';
		    	await FileSystem.copyAsync({
		    		from: this.state.newImageData.uri, 
		    		to: DEMO_DIR + uniquePhotoId + '.jpg'
		    	})
		    	var mainImageUrl = DEMO_DIR + uniquePhotoId + '.jpg'
		    	var faceImageUrls = [];
		    	var faces = this.state.faceDetectionResults.faces;
		    	for (var i = 0; i <= faces.length - 1; i++) {
		    		await FileSystem.copyAsync({
			    		from: this.state.faceDetectionResults.faces[i].faceUri, 
			    		to: DEMO_DIR + uniquePhotoId + '_' + i.toString() + '.jpg'
			    	})
		    		var faceUrl = DEMO_DIR + uniquePhotoId + '_' + i.toString() + '.jpg';
		    		faceImageUrls.push(faceUrl);
		    	}
		    	// Now we have all the image Urls we need to update the data to the Firebade database
		    	// Form an object containing all of the data on each face in the image
		        var faceDataForFirebase = [];
		        for (var j = 0; j <= faceImageUrls.length - 1; j ++) {
		        	faceDataForFirebase.push({
		        		name: this.state['faceLabel_' + j.toString()],
		        		boundingBoxData: faces[j].bounds,
		        		faceImageUrl: faceImageUrls[j]
		        	});
		        }
		        var userId = 'Hr3ApcPo2UWl1jcPjFPpozDj9fr2';
		        var firebaseData = {mainImageUrl: mainImageUrl,
		        					uploadedBy: userId,
		        					title: this.state.name,
		        					description: this.state.description,
		        				    location: this.state.location,
		        				    dateTaken: this.state.dateTaken,
		        				    faces: faceDataForFirebase}
		        demoData.families.Miller.familyAlbum[uniquePhotoId] = firebaseData;
		        await FileSystem.writeAsStringAsync(
		        	DEMO_DIR + 'manager.json',
		        	JSON.stringify(demoData)
	        	);
		        this.setState({isUploading: false, isFaceDetecting: true});
		        // NAVIGATE TO ADD TITLE AND ANSWERS AND PASS ALONG THE IMAGE'S UNIQUE ID 
		        await this.updateGameState('image', firebaseData);
		        this.setState({stage: 'add-main-info'});
		        await this.setState({addImagePhoto: {uri: mainImageUrl}});
		        this.checkQuestionSectionStatus();
		        this.changeSection('b');
		        this.setState({name: '',
		    			       description: '',
		    			       location: '',
		    			       dateTaken: ''});
		        for (var n = 0; n < 10; n++) {
		        	this.setState({['faceLabel_' + n.toString()]: ''});
		        }
		        uploadFailed = false;
	        }
        }
    }

    addHintVoiceRecorder() {
    	// Check whether the recording is present or not - set the voice recorder stage appropriately
    	if (this.state.gameData[this.state.currentQuestion].questionHint == '') {
    		this.setState({voiceRecorderStage: 'start'});
    	}
    	else {
    		this.setState({voiceRecorderStage: 'complete'});
    	}
    	this.setState({enableVoiceRecorder: true});
    }

    additionalQuestionVoiceRecorder() {
    	// Check whether the recording is present or not - set the voice recorder stage appropriately
    	if (this.state.gameData[this.state.currentQuestion].additionalQuestion == '') {
    		this.setState({voiceRecorderStage: 'start'});
    	}
    	else {
    		this.setState({voiceRecorderStage: 'complete'});
    	}
    	this.setState({enableVoiceRecorder: true});
    }

    async addNewQuestion() {
    	// If called from main menu - close the menu
    	this.setState({enableMainMenu1: false});
    	// Add question to gameData
    	const currentData = this.state.gameData;
    	const newQuestion = {
    		title: '',
			image: {},
			correctAnswer: '',
			dummyAnswer1: '',
			dummyAnswer2: '',
			dummyAnswer3: '',
			questionHint: '',
			additionalQuestion: ''
    	};
    	await currentData.splice((this.state.currentQuestion + 1), 0, newQuestion);
    	console.log('game data @ addNew - ', currentData);
    	await this.setState({gameData: currentData});
    	// Reset everything back to default
    	// Navigate forwards
    	this.navigateQuestionForwards();
    }

    async removeCurrentQuestion() {
    	// If called from main menu - close the menu
    	this.setState({enableMainMenu1: false});
    	Alert.alert('Warning!', 
    		        'Are you sure you want to delete this question?',
    		         [
    		         	{text: 'Confirm', onPress: () => this.confirmRemove()},
    		         	{text: 'Cancel'}
    		         ]);
    }

    async confirmRemove() {
    	if (this.state.gameData.length >= 2) {
    		if (this.state.currentQuestion == 0) {
    			// Nav forwards and delete the first question
    			await this.navigateQuestionForwards();
    			const currentData = this.state.gameData;
		    	await currentData.splice(0, 1);
		    	await this.setState({gameData: currentData});
		    	this.setState({titleBarText: 'Question 1'});
		    	// Remember all questions now shift back by 1
    			this.setState({currentQuestion: this.state.currentQuestion - 1});
    		}
    		else {
    			// Nav backwards and delete the prev question
    			await this.navigateQuestionBackwards();
    			const currentData = this.state.gameData;
		    	await currentData.splice((this.state.currentQuestion + 1), 1);
		    	await this.setState({gameData: currentData});
    		}
    	}
    	console.log('Post-Remove state - ', this.state.gameData);
    }

    async finaliseGame() {
    	// If called from main menu - close the menu
    	this.setState({enableMainMenu1: false});
    	// Check the status of each section - flag if any are incomplete
    	var allSectionsComplete = true;
    	for (var i = 0; i <= this.state.gameData.length - 1; i++) {
			if (this.state.addImagePhoto == require('../../../assets/images/splash-screen-image.jpg')
				|| this.state.gameData[i].image == null || this.state.gameData[i].image == undefined) {
				allSectionsComplete = false;
			}
			var mainInfoSections = ['title', 'correctAnswer', 'dummyAnswer1', 'dummyAnswer2', 'dummyAnswer3'];
			var mainInfoComplete = true;
			for (var j = 0; j <= mainInfoSections.length - 1; j++) {
				if (this.state.gameData[i][mainInfoSections[j]] == '') {
					allSectionsComplete = false;
				}
			}
			// if (this.state.gameData[i].questionHint == '' ||
			// 	this.state.gameData[i].questionHint == null) {
			// 	// Set flag to still be true as question hint is optional for now
			// 	allSectionsComplete = false;
			// }
		}
		if (allSectionsComplete == true) {
			this.setState({sectionState: {
				a: {
					status: null,
					currentlySelected: styles.unselectedSection,
					nav: () => this.navToAddImage(),
					text: 'My Photo'
				},
				b: {
					status: null,
					currentlySelected: styles.selectedSection,
					nav: () => this.navToAddMainInfo(),
					text: 'Title and Answers'
				},
				c: {
					status: null,
					currentlySelected: styles.unselectedSection,
					nav: () => this.navToAddHint(),
					text: 'Question Hint'
				},
				d: {
					status: null,
					currentlySelected: styles.unselectedSection,
					nav: () => this.navToAdditionalQuestion(),
					text: 'Additional Question'
				}
			}});
			await this.generateSlideshow();
			this.setState({stage: 'game-title'});
			this.setState({enableQuestionNavigationArrows: false});
			this.setState({titleBarText: 'Add Game Title'});
			this.changeSection('b');
			this.checkFinaliseSectionStatus();
			var sections = ['a', 'b', 'c', 'd'];
			var newSectionNames = [
				'Created Game',
				'Add Game Title',
				'Select Recipients',
				'Preview and Send'
			];
			var newNavDirections = [
				() => this.navToCreatedGame(),
				() => this.navToAddGameTitle(),
				() => this.navToSelectRecipients(),
				() => this.navToSend()
			];
			for (var k = 0; k <= sections.length - 1; k++) {
				this.updateSectionState(sections[k], 'text', newSectionNames[k]);
				this.updateSectionState(sections[k], 'nav', newNavDirections[k]);
			}
		}
		else {
			Alert.alert('Please complete all sections for each question before finalising your game!');
		}
    }

    async navigateQuestionForwards() {
    	var questionsLength = this.state.gameData.length;
    	console.log('qLength', questionsLength);
    	console.log(this.state.currentQuestion + 1);
    	if (this.state.currentQuestion + 1 != questionsLength) {
    		await this.setState({currentQuestion: this.state.currentQuestion + 1});
    		await this.navigationCheckUI();
    		console.log('game data @ navForwards - ', this.state.gameData);
    	}
    }

    async navigateQuestionBackwards() {
    	if (this.state.currentQuestion != 0) {
    		await this.setState({currentQuestion: this.state.currentQuestion - 1});
    		await this.navigationCheckUI();
    		console.log('game data @ navForwards - ', this.state.gameData);
    	}
    }

    navigationCheckUI() {
    	this.setState({titleBarText: 'Question ' + (this.state.currentQuestion + 1).toString()});
    	console.log(this.state.gameData[this.state.currentQuestion].image.mainImageUrl);
    	if (this.state.gameData[this.state.currentQuestion].image.mainImageUrl != undefined) {
			// Then set the addImagePhoto to be the stored image
			// Else set it to be the default placeholder
			this.setState({addImagePrompt: 'Change your photo?',
						   addImagePhoto: {uri: this.state.gameData[this.state.currentQuestion].image.mainImageUrl}});
		}
		else {
			// Else set it to be the default placeholder
			this.setState({addImagePrompt: 'Add a photo|\nChoose a photo of your family at Christmas or your skiing trip to France! \n Landscape photos work best!',
						   addImagePhoto: require('../../../assets/images/splash-screen-image.jpg')});
		}
		this.checkQuestionSectionStatus();
		this.navToAddImage();
		this.checkQuestionHintPresent();
		this.checkAdditionalQuestionPresent();
	}

	showMainMenu() {
		var stage = this.state.stage;
		if (stage == 'game-title' || stage == 'select-recipients' || stage == 'send') {
			this.setState({enableMainMenu2: true});
		}
		else {
			this.setState({enableMainMenu1: true});
		}
	}

	async onToggleRecipient(index, item) {
		console.log('familyMember 3', item);
		var selectedStatus = this.state.familyMembers[index].selected;
		if (selectedStatus == true) {
			var members = this.state.familyMembers;
			members[index].selected = false;
			members[index].styleStatus = {
				height: screenHeight * 0.07,
				width: screenHeight * 0.07,
				tintColor: color.grey,
				opacity: 0.7
			};
			await this.setState({familyMembers: members});
		}
		else {
			var members = this.state.familyMembers;
			members[index].selected = true;
			members[index].styleStatus = {
				height: screenHeight * 0.07,
				width: screenHeight * 0.07,
				tintColor: color.green
			};
			await this.setState({familyMembers: members});
		}
	}

	async uploadGame() {
		// Check to see if all game details e.g. game title and selected recipients are filled in
		if (this.state.gameTitle != '') {
			const familyMembers = this.state.familyMembers;
			var recipientUids = [];
			for (var i = 0; i <= familyMembers.length - 1; i++) {
				if (familyMembers[i].selected == true) {
					recipientUids.push(familyMembers[i].uid);
				}
			}
			console.log(recipientUids.length);
			if (recipientUids.length == 0) {
				await Alert.alert('Warning!', 
								  'You haven\'t selected any recipients. The game will still be saved but not sent to anyone. Continue?',
								  [
								  	{text: 'Confirm', onPress: () => this.confirmUpload(recipientUids)},
								  	{text: 'Cancel'}
								  ]);
			}
			else {
				this.confirmUpload(recipientUids);
			}
		}
		else {
			Alert.alert('Warning!', 'Please give your game a title before uploading!');
		}
	}

	async confirmUpload(recipientUids) {
		console.log(recipientUids)
		if (this.state.demoMode != true) {
			// Set up the timeout function
			var uploadFailed = true;
			setTimeout(() => {
				if (uploadFailed == true) {
					Alert.alert('The game could not be uploaded. Please check your internet connection and try again!');
					this.setState({isUploading: false});
				}
			}, 12000);
			// Create a unique game ID to reference to
			const uniqueGameId = Date.now().toString();
			const userId = await UserId();
			// Display the uploading blur while we upload the data to AWS and Firebase
			this.setState({isUploading: true});
			// One by one, for each recorded question hint and then subsequestly additional question
			// pass the uri to AWS AudioUpload and replace with the recieved audioUrl
			const gameData = this.state.gameData;
			var audioUrl;
			for (var j = 0; j <= gameData.length - 1; j++) {
				if (gameData[j].questionHint != '') {
					audioUrl = await UploadAudio(gameData[j].questionHint,
									             'questionHint',
									             'users/' + userId + '/createdGames/' + uniqueGameId + '/' + j.toString() + '/');
					gameData[j].questionHint = audioUrl;
				}
				if (gameData[j].additionalQuestion != '') {
					audioUrl = await UploadAudio(gameData[j].additionalQuestion,
									             'additionalQuestion',
									             'users/' + userId + '/createdGames/' + uniqueGameId + '/' + j.toString() + '/');
					gameData[j].additionalQuestion = audioUrl;
				}
			}
			var date = await getNeatDate();
			var firebasePath = 'users/' + userId + '/createdGames';
			var game = { 
				[uniqueGameId]: {
					title: this.state.gameTitle,
					attachedMessage: this.state.attachedMessage,
					questions: this.state.gameData,
					recipients: recipientUids,
					uploadDate: date
				}
			}
			await Append(game, firebasePath);
			// Append all recipient databases to contain reference to the received game
			for (var k = 0; k <= recipientUids.length - 1; k++) {
				await Append({[uniqueGameId]: userId}, 'users/' + recipientUids[k] + '/receivedGames/');
			}
			uploadFailed = false;
			// Nav back to Home when complete
			// Reset the UI back to portrait upon returning to the home screen
			await ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT);
			this.props.navigation.navigate('CreatedGamesScreen');
		}
		else {
			// Get the demo data ready for appending
			var demoData = await FileSystem.readAsStringAsync(DEMO_DIR + 'manager.json');
			demoData = JSON.parse(demoData);
			
			// Create a unique game ID to reference to
			const uniqueGameId = Date.now().toString();
			const userId = 'Hr3ApcPo2UWl1jcPjFPpozDj9fr2';
			// Display the uploading blur while we upload the data to AWS and Firebase
			this.setState({isUploading: true});
			// One by one, for each recorded question hint and then subsequestly additional question
			// pass the uri to AWS AudioUpload and replace with the recieved audioUrl
			const gameData = this.state.gameData;
			var audioUrl;
			for (var j = 0; j <= gameData.length - 1; j++) {
				if (gameData[j].questionHint != '') {
					await FileSystem.moveAsync({
						from: gameData[j].questionHint,
						to: DEMO_DIR + uniqueGameId + '_' + j.toString()  + '_' + 'questionHint.caf'
					});
					audioUrl = DEMO_DIR + uniqueGameId + '_' + j.toString()  + '_' + 'questionHint.caf'
					gameData[j].questionHint = audioUrl;
				}
				if (gameData[j].additionalQuestion != '') {
					await FileSystem.moveAsync({
						from: gameData[j].additionalQuestion,
						to: DEMO_DIR + uniqueGameId + '_' + j.toString() + '_' + 'additionalQuestion.caf'
					});
					audioUrl = DEMO_DIR + uniqueGameId + '_' + j.toString() + '_' + 'additionalQuestion.caf'
					gameData[j].additionalQuestion = audioUrl;
				}
			}
			var date = await getNeatDate();
			var firebasePath = 'users/' + userId + '/createdGames';
			var game = {
				title: this.state.gameTitle,
				attachedMessage: this.state.attachedMessage,
				questions: this.state.gameData,
				recipients: recipientUids,
				uploadDate: date
			}
			demoData.users[userId].createdGames[uniqueGameId] = game;
			// Append all recipient databases to contain reference to the received game
			for (var k = 0; k <= recipientUids.length - 1; k++) {
				demoData.users[recipientUids[k]].receivedGames[uniqueGameId] = userId;
			}
			await FileSystem.writeAsStringAsync(
				DEMO_DIR + 'manager.json',
				JSON.stringify(demoData)
			);
			uploadFailed = false;
			// Nav back to Home when complete
			// Reset the UI back to portrait upon returning to the home screen
			await ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT);
			this.props.navigation.navigate('CreatedGamesScreen', {demoMode: true});
		}
	}

	getFaceInputRefString(index) {
		var string = 'faceInput' + index.toString();
		return(string);
	}

	async continueWithPreviousImage() {
		var prevImage = this.state.gameData[this.state.currentQuestion - 1].image;
		await this.updateGameState('image', prevImage);
        this.setState({stage: 'add-main-info'});
        await this.setState({addImagePhoto: {uri: prevImage.mainImageUrl}});
        this.checkQuestionSectionStatus();
        this.changeSection('b');
	}

	uiTitleBar() {
		return(
			<View style={styles.titleBar}>
				<TouchableOpacity style={gstyles.shadow}
								  onPress={() => Alert.alert('Warning!', 
								  						     'Are you sure you want to quit to home?',
								  						     [
								  						     	{text: 'Confirm', onPress: () => this.props.navigation.navigate('CreatedGamesScreen', {demoMode: true})},
								  						     	{text: 'Cancel'}
								  						     ])}>
					<Image style={styles.logo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
				</TouchableOpacity>
				{this.uiTitleBarMid()}
				<TouchableOpacity style={[styles.leftTitleBarArrow, gstyles.shadow]} onPress={() => this.showMainMenu()}>
					<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/menu.png')} />
				</TouchableOpacity>
			</View>
		);
	}

	uiTitleBarMid() {
		if (this.state.enableQuestionNavigationArrows == true) {
			return(
				<View style={styles.titleBarMid}>
					<TouchableOpacity style={[styles.leftTitleBarArrow, gstyles.shadow]} onPress={() => this.navigateQuestionBackwards()}>
						<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/chevron-left.png')} />
					</TouchableOpacity>
					<Text style={gstyles.largeText}>{this.state.titleBarText}</Text>
					<TouchableOpacity style={[styles.leftTitleBarArrow, gstyles.shadow]} onPress={() => this.navigateQuestionForwards()}>
						<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/chevron-right.png')} />
					</TouchableOpacity>
				</View>
			);
		}
		else {
			return(
				<View style={styles.titleBarMid}>
					<Text style={gstyles.largeText}>{this.state.titleBarText}</Text>
				</View>
			);
		}
	}

	uiSectionNavigator() {
		const sectionState = this.state.sectionState;
		if (this.state.enableSectionNavigator == true) {
			var stage = this.state.stage;
			//
			//
			// Special UI change - as in alpha we are leaving out the 'additional question' section
			// the UI has changed to that the firsat section nav is in thirds and the later completion
			// section nav is in fourths - this will revert after Alpha and can be condesned back to the
			// original format.
			//
			//
			if (stage == 'add-image' || stage == 'add-main-info' || stage == 'add-question-hint') {
				return(
					<View style={styles.sectionNavigator}>
						<TouchableOpacity style={[styles.sectionRow3rds, sectionState.a.currentlySelected]}
										  onPress={sectionState.a.nav}>
							<View style={[styles.sectionStatusCircle, sectionState.a.status]} />
							<View style={styles.sectionText}>
								<Text style={[gstyles.smallText, {textAlign: 'center'}]}>{sectionState.a.text}</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity style={[styles.sectionRow3rds, sectionState.b.currentlySelected]}
										  onPress={sectionState.b.nav}>
							<View style={[styles.sectionStatusCircle, sectionState.b.status]} />
							<View style={styles.sectionText}>
								<Text style={[gstyles.smallText, {textAlign: 'center'}]}>{sectionState.b.text}</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity style={[styles.sectionRow3rds, sectionState.c.currentlySelected]}
										  onPress={sectionState.c.nav}>
							<View style={[styles.sectionStatusCircle, sectionState.c.status]} />
							<View style={styles.sectionText}>
								<Text style={[gstyles.smallText, {textAlign: 'center'}]}>{sectionState.c.text}</Text>
							</View>
						</TouchableOpacity>
						{/*<TouchableOpacity style={[styles.sectionRow, sectionState.d.currentlySelected]}
										  onPress={sectionState.d.nav}>
							<View style={[styles.sectionStatusCircle, sectionState.d.status]} />
							<View style={styles.sectionText}>
								<Text style={[gstyles.smallText, {textAlign: 'center'}]}>{sectionState.d.text}</Text>
							</View>
						</TouchableOpacity> */}
					</View>
				);
			}
			else {
				return(
					<View style={styles.sectionNavigator}>
						<TouchableOpacity style={[styles.sectionRow, sectionState.a.currentlySelected, styles.createdGameSection]}
										  onPress={sectionState.a.nav}>
							<View style={[styles.sectionStatusCircle, sectionState.a.status]} />
							<View style={styles.sectionText}>
								<Text style={[gstyles.smallText, {textAlign: 'center'}]}>{sectionState.a.text}</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity style={[styles.sectionRow, sectionState.b.currentlySelected]}
										  onPress={sectionState.b.nav}>
							<View style={[styles.sectionStatusCircle, sectionState.b.status]} />
							<View style={styles.sectionText}>
								<Text style={[gstyles.smallText, {textAlign: 'center'}]}>{sectionState.b.text}</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity style={[styles.sectionRow, sectionState.c.currentlySelected]}
										  onPress={sectionState.c.nav}>
							<View style={[styles.sectionStatusCircle, sectionState.c.status]} />
							<View style={styles.sectionText}>
								<Text style={[gstyles.smallText, {textAlign: 'center'}]}>{sectionState.c.text}</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity style={[styles.sectionRow, sectionState.d.currentlySelected]}
										  onPress={sectionState.d.nav}>
							<View style={[styles.sectionStatusCircle, sectionState.d.status]} />
							<View style={styles.sectionText}>
								<Text style={[gstyles.smallText, {textAlign: 'center'}]}>{sectionState.d.text}</Text>
							</View>
						</TouchableOpacity>
					</View>
				);
			}
		}
		else {
			return(null);
		}
	}

	uiBody() {
		if (this.state.stage == 'tutorial') {
			return(<View>{this.uiTutorial()}</View>);
		}
		else if (this.state.stage == 'sample') {
			return(<View>{this.uiSample()}</View>);
		}
		else if (this.state.stage == 'add-image') {
			return(<View>{this.uiAddImage()}</View>);
		}
		else if (this.state.stage == 'add-main-info') {
			return(<View>{this.uiAddTitleAndAnswers()}</View>);
		}
		else if (this.state.stage == 'add-question-hint') {
			return(<View>{this.uiAddQuestionHint()}</View>);
		}
		else if (this.state.stage == 'add-additional') {
			return(<View>{this.uiAdditionalQuestion()}</View>);
		}
		else if (this.state.stage == 'game-title') {
			return(<View>{this.uiGameTitle()}</View>);
		}
		else if (this.state.stage == 'select-recipients') {
			return(<View>{this.uiSelectRecipients()}</View>);
		}
		else {
			return(<View>{this.uiSend()}</View>);
		}
	}

	uiTutorial() {
		return(
			<View style={styles.tutorialBody}>
				<View style={[gstyles.cardRow, styles.tutorialSteps]}>
					<View style={styles.stepColumn}>
						<Text style={gstyles.mediumText}>Step 1</Text>
						<Image style={gstyles.largeIcon} source={require('../../../assets/icons/png/image-plus.png')} />
						<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>
							Add a photo. This will be the basis for your question!
						</Text>
					</View>
					<View style={styles.stepColumn}>
						<Text style={gstyles.mediumText}>Step 2</Text>
						<Image style={gstyles.largeIcon} source={require('../../../assets/icons/png/text.png')} />
						<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>
							Let us know the correct answer, some false ones and a question hint!
						</Text>
					</View>
					<View style={styles.stepColumn}>
						<Text style={gstyles.mediumText}>Step 3</Text>
						<Image style={gstyles.largeIcon} source={require('../../../assets/icons/png/send.png')} />
						<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>
							Then finally preview the game and send it off to be played!
						</Text>
					</View>
				</View>
				<View style={styles.tutorialButtons}>
					<TouchableOpacity style={[gstyles.shadow, styles.seeSampleGameButton]}
									  onPress={() => this.setState({stage: 'sample', titleBarText: 'Sample Game'})}>
						<Text style={gstyles.smallText}>See a sample game?</Text>
					</TouchableOpacity>
					<TouchableOpacity style={[gstyles.shadow, styles.startBuildButton]}
									  onPress={() => this.startBuilding()}>
						<Text style={[gstyles.smallText, {textAlign: 'center'}]}>Ready to build a game?</Text>
					</TouchableOpacity>
				</View>
			</View>
		);
	}

	uiSample() {
		return(
			<View style={styles.sampleBody}>
				<View style={gstyles.shadow}>
					<Video isLooping={true} style={styles.sampleVideo} source={{uri: 'https://s3-eu-west-1.amazonaws.com/testingmlg/sample.mp4'}} shouldPlay={true}/>
				</View>
				<TouchableOpacity style={styles.fab}
								  onPress={() => this.startBuilding()}> 
					<Image style={gstyles.smallIcon} source={require('../../../assets/icons/png/arrow-right.png')} />
				</TouchableOpacity>
			</View>
		);
	}

	uiAddImage() {
		if (this.state.addImageStage == 1) {
			return(
				<View style={styles.addImage1Body}>
					<Text>
						<Text style={[gstyles.mediumText, styles.addImagePrompt]}>{this.state.addImagePrompt.split('|')[0]}</Text>
						<Text>{"\n"}</Text>
						<Text style={[gstyles.subTitle, styles.addImagePrompt]}>{this.state.addImagePrompt.split('|')[1]}</Text>
					</Text>
					{this.uiAddImageContent()}
					<View style={styles.addImageButtons}>
						<TouchableOpacity style={[gstyles.cardColumn, styles.addImageButton]}
										  onPress={() => this.chooseFromFamilyAlbum()}>
							<Text style={[gstyles.smallText, styles.addImageButtonText]}>Choose from Family Album</Text>
						</TouchableOpacity>
						<TouchableOpacity style={[gstyles.cardColumn, styles.addImageButton, styles.greenHighlight]}
										  onPress={() => this.chooseFromCameraRoll()}>
							<Text style={[gstyles.smallText, styles.addImageButtonText]}>Choose from Camera Roll</Text>
						</TouchableOpacity>
						<TouchableOpacity style={[gstyles.cardColumn, styles.addImageButton]}
										  onPress={() => this.takeNewPhoto()}>
							<Text style={[gstyles.smallText, styles.addImageButtonText]}>Take a new photo</Text>
						</TouchableOpacity>
					</View>
				</View>
			);
		}
		else if (this.state.addImageStage == 2) {
			return(
				<View>
					<Text style={[gstyles.smallText, styles.familyAlbumPrompt]}>Select a photo</Text>
					<GridView itemDimension={screenWidth * 0.23}
							  items={this.state.familyAlbum}
							  style={styles.familyAlbumGridContainer}
							  renderItem={(item) => <TouchableOpacity onPress={() => this.onPressFamilyAlbumPhoto(item)}
							  	  			    					  style={styles.gridViewItem}>
									  		  			<Image style={styles.gridViewItem}
									  	  		               source={{uri: item.mainImageUrl}} />
							  		  				</TouchableOpacity>
							  }/>

				</View>
			);
		}
		else {
			return(
				<KeyboardAwareScrollView extraHeight={0}>
	                {this.uiFamilyPhotoDetails()}
	            </KeyboardAwareScrollView>
			);
		}
	}

	uiFamilyPhotoDetails() {
		if (this.state.addFamilyPhotoDetailsExpanded == true) {
			return(
				<View style={styles.keyboardScrollView}>
					<View style={[gstyles.shadow, styles.familyAlbumImage]}>
	                	<Image style={styles.familyAlbumImage} source={{uri: this.state.newImageData.uri}} />
	                	<TouchableOpacity style={styles.changePhotoOverlayButton}
	                					  onPress={() => this.setState({addImageStage: 1})}>
		                	<BlurView style={styles.changePhotoOverlay} intensity={90} tint='dark'>
		                		<Text style={gstyles.subTitle}>Change your photo?</Text>
		                	</BlurView>
	                	</TouchableOpacity>
	                </View>
					<View style={[gstyles.cardColumn, styles.familyAlbumMainDetailsCard]}>
	                	<Text style={gstyles.smallText}>Main Photo Details</Text>
	                	<View style={styles.familyAlbumDetailsInputs}>
	                		<TouchableOpacity style={styles.familyAlbumTextInputContainer}
								  			  onPress={() => this.refs.nameInput.focus()}>
	                			<Text style={gstyles.subTitle}>Name</Text>
	                			<TextInput style={styles.familyAlbumTextInput}
	                					   ref='nameInput'
	                					   placeholder='Enter a name for your photo'
	                					   value={this.state.name}
	                					   onChangeText={(name) => this.setState({name})} />
	                		</TouchableOpacity>
	                		{/*<TouchableOpacity style={styles.familyAlbumTextInputContainer}
								              onPress={() => this.refs.descriptionInput.focus()}>
	                			<Text style={gstyles.subTitle}>Description</Text>
	                			<TextInput style={styles.familyAlbumTextInput}
	                					   ref='descriptionInput'
	                					   placeholder='Enter a short description'
	                					   value={this.state.description}
	                					   onChangeText={(description) => this.setState({description})} />
	                		</TouchableOpacity>*/}
	                		<TouchableOpacity style={styles.familyAlbumTextInputContainer}
								  			  onPress={() => this.refs.locationInput.focus()}>
	                			<Text style={gstyles.subTitle}>Location</Text>
	                			<TextInput style={styles.familyAlbumTextInput}
	                					   ref='locationInput'
	                					   placeholder='Enter the photo location'
	                					   value={this.state.location}
	                					   onChangeText={(location) => this.setState({location})} />
	                		</TouchableOpacity>
	                		<TouchableOpacity style={styles.familyAlbumTextInputContainer}
								  			  onPress={() => this.refs.dateInput.focus()}>
	                			<Text style={gstyles.subTitle}>Date Taken</Text>
	                			<TextInput style={styles.familyAlbumTextInput}
	                					   ref='dateInput'
	                					   placeholder='Enter the date taken'
	                					   value={this.state.dateTaken}
	                					   onChangeText={(dateTaken) => this.setState({dateTaken})} />
	                		</TouchableOpacity>
	                	</View>
	                </View>
	                {this.uiFaceDetectionCard()}
                </View>
			);
		}
		else {
			return(
				<View style={styles.keyboardScrollView}>
					<View style={[gstyles.shadow, styles.familyAlbumImage]}>
	                	<Image style={styles.familyAlbumImage} source={{uri: this.state.newImageData.uri}} />
	                	<TouchableOpacity style={styles.changePhotoOverlayButton}
	                					  onPress={() => this.setState({addImageStage: 1})}>
		                	<BlurView style={styles.changePhotoOverlay} intensity={90} tint='dark'>
		                		<Text style={gstyles.subTitle}>Change your photo?</Text>
		                	</BlurView>
	                	</TouchableOpacity>
	                </View>
					<TouchableOpacity style={[gstyles.cardColumn, styles.familyAlbumMainDetailsCardCollapsed]}
									  onPress={() => this.setState({addFamilyPhotoDetailsExpanded: true})}>
	                		<Text style={gstyles.smallText}>Add some info to this photo?</Text>
	                </TouchableOpacity>
                </View>
			);
		}
	}

	uiAddImageContent() {
		if (this.state.currentQuestion == 0) {
			if (this.state.addImagePhoto != require('../../../assets/images/splash-screen-image.jpg')) {
				return(
					<View style={gstyles.shadow}>
						<Image style={styles.addImagePhoto} source={this.state.addImagePhoto} />
					</View>
				);
			}
			else {
				return(
					<View style={gstyles.shadow}>
						<Video isLooping={true} style={styles.addImagePhoto} source={{uri: 'https://s3-eu-west-1.amazonaws.com/testingmlg/exampleImages.mp4'}} shouldPlay={true}/>
					</View>
				);
			}
		}
		else {
			if (this.state.gameData[this.state.currentQuestion].image.mainImageUrl != null) {
				return(
					<View style={[gstyles.shadow, styles.addImagePhoto]}>
						<Image style={styles.addImagePhoto} source={this.state.addImagePhoto} />
					</View>
				);
			}
			else if (this.state.gameData[this.state.currentQuestion - 1].image.mainImageUrl != null) {
				return(
					<View style={[gstyles.shadow, styles.addImagePhoto]}>
						<Image style={styles.addImagePhoto} source={{uri: this.state.gameData[this.state.currentQuestion - 1].image.mainImageUrl}} />
						<TouchableOpacity style={styles.changePhotoOverlayButton}
	                					  onPress={() => this.continueWithPreviousImage()}>
		                	<BlurView style={styles.changePhotoOverlay} intensity={90} tint='dark'>
		                		<Text style={gstyles.subTitle}>Keep using this photo?</Text>
		                	</BlurView>
	                	</TouchableOpacity>
					</View>
				);
			}
			else {
				return(
					<View style={gstyles.shadow}>
						<Video isLooping={true} style={styles.addImagePhoto} source={{uri: 'https://s3-eu-west-1.amazonaws.com/testingmlg/exampleImages.mp4'}} shouldPlay={true}/>
					</View>
				);
			}
		}
	}

	uiFaceDetectionCard() {
		if (this.state.faceDetectionResults != '') {
			return(
				<View style={[gstyles.cardColumn, styles.familyAlbumMainDetailsCard, {height: 100 + (this.state.faceDetectionResults.faces.length * 125)}]}>
            		<Text style={gstyles.smallText}>Face Detection</Text>
            		<View style={[styles.familyAlbumDetailsInputs, {flex: 1}]}>
	            		<FlatList data={this.state.faceDetectionResults.faces}
						          keyExtractor={(item, index) => index.toString()}
						          scrollEnabled={false}
	                              renderItem={(item) => <View style={styles.faceDetectionRow}>
	                              							<View style={gstyles.shadow}>
	                              								<CacheableImage style={[gstyles.profilePicturePortrait, styles.faceDetectionProfilePictures]} source={{uri: item.item.faceUri}} />
	                              							</View>
	                              							<TouchableOpacity style={styles.familyAlbumTextInputContainer}
								  											  onPress={() => this.itemRefs['faceInput' + item.index].focus()}>
									                		   <Text style={gstyles.subTitle}>Who is this?</Text>
									                		   <TextInput style={[styles.familyAlbumTextInput, styles.faceDetectionInputs]}
									                		   		      ref={(ref) => this.itemRefs = {...this.itemRefs, ['faceInput' + item.index]: ref}}
									                					  placeholder='Their name is...'
									                					  value={this.state['faceLabel_' + item.index.toString()]}
				                                 					      onChangeText={(text) => this.setState({['faceLabel_' + item.index.toString()]: text})} />
									                	    </TouchableOpacity>
								                	    </View>}/>
            	    </View>
            	</View>
			);
		}
		else {
			return(null);
		}
	}

	uiCamera() {
		if (this.state.cameraEnabled == true) {
			return(
				<Camera type={this.state.cameraDirection} style={styles.camera} ref={ref => {this.camera = ref;}}>
					<BlurView style={styles.cameraActionBar} tint="dark" intensity={90}>
						<TouchableOpacity style={styles.cameraShutterButton} onPress={() => this.rotateCamera()}>
							<View style={gstyles.shadow}>
								<Image style={gstyles.smallIcon} source={require('../../../assets/icons/png/rotate-3d.png')} />
							</View>
						</TouchableOpacity>
						<TouchableOpacity style={styles.cameraShutterButton} onPress={() => this.onShutterPress()}>
							<View style={gstyles.shadow}>
								<Image style={gstyles.largeIcon} source={require('../../../assets/icons/png/camera-iris.png')} />
							</View>
						</TouchableOpacity>
						<TouchableOpacity style={styles.cameraShutterButton} onPress={() => this.chooseFromCameraRoll()}>
							<View style={gstyles.shadow}>
								<Image style={gstyles.smallIcon} source={require('../../../assets/icons/png/image-multiple.png')} />
							</View>
						</TouchableOpacity>
					</BlurView>
				</Camera>
			);
		}
		else {
			return(null);
		}
	}

	uiUploadingBlur() {
		if (this.state.isUploading == true) {
			return(
				<BlurView tint="dark" intensity={90} style={styles.uploadingBlur}>
					<Image style={styles.uploadingLogo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
					<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>
						Saving to your secure Family Cloud...
					</Text>
					<ActivityIndicator size="large" color='#3498db' />
				</BlurView>
			);
		}
		else {
			return(null);
		}
	}

	uiSaveFAB() {
		if (this.state.stage == 'add-image' && this.state.addImageStage == 3) {
			return(
				<TouchableOpacity style={styles.fab}
								  onPress={() => this.saveToFamilyAlbum()}>
				    <Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/check.png')} />
				</TouchableOpacity>
			);
		}
		else {
			return(null);
		}
	}

	uiAddTitleAndAnswers() {
		return(
			<View>
			<KeyboardAwareScrollView extraHeight={150}>
			<View style={styles.mainInfoBody}>
				<TouchableOpacity style={styles.questionTitleInputContainer}
								  onPress={() => this.refs.titleInput.focus()}>
        			<Text style={gstyles.subTitle}>What is your question?</Text>
        			<TextInput style={styles.questionTitleInput}
        					   ref='titleInput'
        					   placeholder='Enter a question here'
        					   value={this.state.gameData[this.state.currentQuestion].title}
        					   onChangeText={(title) => this.updateGameState('title', title)}
        					   onSubmitEditing={() => this.refs.correctAnswerInput.focus()}
        					   returnKeyType='next' />
        		</TouchableOpacity>
        		<View style={[gstyles.shadow, styles.mainInfoImage]}>
        			{this.uiAddTandAsPhoto()}
        			<TouchableOpacity style={styles.changePhotoOverlayButton}
                					  onPress={() => this.navToAddImage()}>
	                	<BlurView style={styles.changePhotoOverlay} intensity={90} tint='dark'>
	                		<Text style={gstyles.subTitle}>Change your photo?</Text>
	                	</BlurView>
                	</TouchableOpacity>
    			</View>
    			<View style={styles.answerRow}>
    				<TouchableOpacity style={styles.answerInputContainer}
								      onPress={() => this.refs.correctAnswerInput.focus()}>
	        			<Text style={gstyles.subTitle}>Correct Answer</Text>
	        			<TextInput style={[styles.answerInput, styles.correctAnswerUnderline]}
	        					   ref='correctAnswerInput'
	        					   placeholder='The correct answer is...'
	        					   value={this.state.gameData[this.state.currentQuestion].correctAnswer}
	        					   onChangeText={(answer) => this.updateGameState('correctAnswer', answer)}
	        					   onSubmitEditing={() => this.refs.dummyAnswerInput1.focus()}
	        					   returnKeyType='next' />
	        		</TouchableOpacity>
	        		<TouchableOpacity style={styles.answerInputContainer}
								  	  onPress={() => this.refs.dummyAnswerInput1.focus()}>
	        			<Text style={gstyles.subTitle}>False Answer 1</Text>
	        			<TextInput style={styles.answerInput}
	        					   ref='dummyAnswerInput1'
	        					   placeholder='A false answer is...'
	        					   value={this.state.gameData[this.state.currentQuestion].dummyAnswer1}
	        					   onChangeText={(answer) => this.updateGameState('dummyAnswer1', answer)}
	        					   onSubmitEditing={() => this.refs.dummyAnswerInput2.focus()}
	        					   returnKeyType='next' />
	        		</TouchableOpacity>
    			</View>
    			<View style={styles.answerRow}>
    				<TouchableOpacity style={styles.answerInputContainer}
								      onPress={() => this.refs.dummyAnswerInput2.focus()}>
	        			<Text style={gstyles.subTitle}>False Answer 2</Text>
	        			<TextInput style={styles.answerInput}
	        			           ref='dummyAnswerInput2'
	        					   placeholder='A false answer is...'
	        					   value={this.state.gameData[this.state.currentQuestion].dummyAnswer2}
	        					   onChangeText={(answer) => this.updateGameState('dummyAnswer2', answer)}
	        					   onSubmitEditing={() => this.refs.dummyAnswerInput3.focus()}
	        					   returnKeyType='next' />
	        		</TouchableOpacity>
	        		<TouchableOpacity style={styles.answerInputContainer}
								      onPress={() => this.refs.dummyAnswerInput3.focus()}>
	        			<Text style={gstyles.subTitle}>False Answer 3</Text>
	        			<TextInput style={styles.answerInput}
	        			           ref='dummyAnswerInput3'
	        					   placeholder='A false answer is...'
	        					   value={this.state.gameData[this.state.currentQuestion].dummyAnswer3}
	        					   onChangeText={(answer) => this.updateGameState('dummyAnswer3', answer)}
	        					   onSubmitEditing={() => this.navToAddHint()}
	        					   returnKeyType='next' />
	        		</TouchableOpacity>
    			</View>
			</View>
			</KeyboardAwareScrollView>
				<TouchableOpacity style={styles.fab}
								  onPress={() => this.setState({enableQuestionPreview: true})}>
					<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/chevron-right.png')} />
				</TouchableOpacity>
			</View>
		);
	}

	uiAddTandAsPhoto() {
		if (this.state.demoMode != true) {
			return(
				<CacheableImage style={styles.mainInfoImage} source={{uri: this.state.gameData[this.state.currentQuestion].image.mainImageUrl}} />
			);
		}
		else {
			return(
				<Image style={styles.mainInfoImage} source={{uri: this.state.gameData[this.state.currentQuestion].image.mainImageUrl}} />
			);
		}
	}

	uiAddQuestionHint() {
		return(
			<View style={styles.questionHintBody}>
				<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>{this.state.questionHintPrompt}</Text>
				<Text style={gstyles.subTitle}>Your correct answer is: <Text style={{color: '#fff'}}>{this.state.gameData[this.state.currentQuestion].correctAnswer}</Text></Text>
				<TouchableOpacity style={[styles.questionHintButton, this.state.questionHintButtonColor, gstyles.shadow]}
								  onPress={() => this.addHintVoiceRecorder()}>
					<View style={styles.questionHintIcon} />
					<Text style={gstyles.smallText}>Record question hint</Text>
				</TouchableOpacity>
				<TouchableOpacity style={styles.fab}
								  onPress={() => this.setState({enableQuestionPreview: true})}>
					<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/chevron-right.png')} />
				</TouchableOpacity>
			</View>
		);
	}

	uiAdditionalQuestion() {
		return(
			<View style={styles.additionalQuestionBody}>
				<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>{this.state.additionalQuestionPrompt}</Text>
				<Text style={gstyles.subTitle}>For example: "What was your 18th birthday like grandma?"</Text>
				<TouchableOpacity style={[styles.additionalQuestionButton, this.state.additionalQuestionButtonColor, gstyles.shadow]}
								  onPress={() => this.additionalQuestionVoiceRecorder()}>
					<View style={styles.additionalQuestionIcon} />
					<Text style={gstyles.smallText}>Ask additional question</Text>
				</TouchableOpacity>
				<TouchableOpacity style={styles.fab}
								  onPress={() => this.addNewQuestion()}>
					<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/plus.png')} />
				</TouchableOpacity>
			</View>
		);
	}

	uiVoiceRecorder() {
		if (this.state.enableVoiceRecorder == true) {
			return(<VoiceRecorder parent={this} recordingType={this.state.stage} stagePreset={this.state.voiceRecorderStage} />);
		}
		else {
			return(null);
		}
	}

	uiMainMenu1() {
		if (this.state.enableMainMenu1 == true) {
			return(
				<BlurView style={styles.mainMenuBlur} intensity={90} tint='dark'>
					<View style={gstyles.shadow}>
						<Image style={styles.mainMenuLogo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
					</View>
					<TouchableOpacity style={styles.mainMenuButton}
								      onPress={() => this.addNewQuestion()}>
				        <Text style={gstyles.smallText}>Add New Question</Text>
			        </TouchableOpacity>
			        <TouchableOpacity style={[styles.mainMenuButton, {backgroundColor: color.red}]}
								      onPress={() => this.removeCurrentQuestion()}>
				        <Text style={gstyles.smallText}>Remove Question</Text>
			        </TouchableOpacity>
			        <TouchableOpacity style={[styles.mainMenuButton, {backgroundColor: color.yellow}]}
								      onPress={() => this.setState({enableQuestionPreview: true})}>
				        <Text style={gstyles.smallText}>Preview Question</Text>
			        </TouchableOpacity>
			        <TouchableOpacity style={[styles.mainMenuButton, {backgroundColor: color.darkBlue}]}
								      onPress={() => this.finaliseGame()}>
				        <Text style={gstyles.smallText}>Finalise Game</Text>
			        </TouchableOpacity>
			        <TouchableOpacity style={[styles.closeIcon, gstyles.shadow]} 
			        				  onPress={() => this.setState({enableMainMenu1: false, enableMainMenu2: false})}>
                    	<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/arrow-left.png')} />
                    	<Text style={gstyles.subTitle}>Back</Text>
                	</TouchableOpacity>
                	<TouchableOpacity style={[styles.helpButton, gstyles.shadow]} 
			        				  onPress={() => this.setState({videoTutorialEnabled: true, enableMainMenu1: false})}>
                    	<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/help-circle-outline.png')} />
                    	<Text style={gstyles.subTitle}>Help</Text>
                	</TouchableOpacity>
				</BlurView>
			);
		}
		else {
			return(null);
		}
	}

	uiPreviewQuestion() {
		if (this.state.enableQuestionPreview == true) {
			return(
				<BlurView style={styles.previewBlur} intensity={90} tint='dark'>
					<Text style={gstyles.mediumText}>Here is your question so far...</Text>
					<View style={[gstyles.shadow, styles.previewContainer]}>
						<View style={[gstyles.shadow, styles.previewNavBar]}>
							<View style={gstyles.shadow}>
								<Image style={styles.previewLogo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
							</View>
							<Text style={gstyles.smallText}>Question {this.state.currentQuestion + 1}</Text>
							<Image style={gstyles.smallIcon} source={require('../../../assets/icons/png/menu.png')} />
						</View>
						<Text style={gstyles.smallText}>{this.state.gameData[this.state.currentQuestion].title}</Text>
						<View style={gstyles.shadow}>
							{this.uiPreviewQuestionImage()}
						</View>
						<View style={styles.previewButtonRow}>
							<View style={[gstyles.shadow, styles.previewButton]}>
								<Text style={styles.previewButtonText}>{this.state.gameData[this.state.currentQuestion].correctAnswer}</Text>
							</View>
							<View style={[gstyles.shadow, styles.previewButton]}>
								<Text style={styles.previewButtonText}>{this.state.gameData[this.state.currentQuestion].dummyAnswer1}</Text>
							</View>
							<View style={[gstyles.shadow, styles.previewButton]}>
								<Text style={styles.previewButtonText}>{this.state.gameData[this.state.currentQuestion].dummyAnswer2}</Text>
							</View>
							<View style={[gstyles.shadow, styles.previewButton]}>
								<Text style={styles.previewButtonText}>{this.state.gameData[this.state.currentQuestion].dummyAnswer3}</Text>
							</View>
						</View>
					</View>
					<Text style={gstyles.smallText}>What would you like to do next?</Text>
					<View style={styles.previewActionButtons}>
						<TouchableOpacity style={styles.previewActionButton}
									      onPress={() => {
									      	this.navToAddHint();
									      	this.setState({enableQuestionPreview: false});
									      }}>
					        <Text style={[gstyles.smallText, {textAlign: 'center'}]}>Add Question Hint</Text>
				        </TouchableOpacity>
				        <TouchableOpacity style={[styles.previewActionButton, {backgroundColor: color.red}]}
									      onPress={() => Alert.alert('Woah! You found a beta feature - we are working hard to bring this feature soon!')}>
					        <Text style={[gstyles.smallText, {textAlign: 'center'}]}>Auto Generate{"\n"}a Question</Text>
				        </TouchableOpacity>
				        <TouchableOpacity style={[styles.previewActionButton, {backgroundColor: color.yellow}]}
									      onPress={() => {
									      	this.addNewQuestion();
									      	this.setState({enableQuestionPreview: false});
									      }}>
					        <Text style={[gstyles.smallText, {textAlign: 'center'}]}>Add Normal Question</Text>
				        </TouchableOpacity>
				        <TouchableOpacity style={[styles.previewActionButton, {backgroundColor: color.darkBlue}]}
									      onPress={() => {
									      	this.finaliseGame();
									      	this.setState({enableQuestionPreview: false});
									      }}>
					        <Text style={[gstyles.smallText, {textAlign: 'center'}]}>Finalise Game</Text>
				        </TouchableOpacity>
					</View> 
			        <TouchableOpacity style={[styles.closeIcon, gstyles.shadow]} 
			        				  onPress={() => this.setState({enableQuestionPreview: false})}>
                    	<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/close.png')} />
                	</TouchableOpacity>
				</BlurView>
			);
		}
		else {
			return(null);
		}
	}

	uiPreviewQuestionImage() {
		if (this.state.demoMode != true) {
			return(
				<CacheableImage style={styles.previewImage} source={{uri: this.state.gameData[this.state.currentQuestion].image.mainImageUrl}} />
			);
		}
		else {
			return(
				<Image style={styles.previewImage} source={{uri: this.state.gameData[this.state.currentQuestion].image.mainImageUrl}} />
			);
		}
	}

	uiGameTitle() {
		return(
			<View style={styles.addTitleBody}>
				<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>Let's give your game a title so your family know what it is about!</Text>
				<View style={styles.gameTitleTextInputContainer}>
        		   <Text style={gstyles.subTitle}>Game Title</Text>
        		   <TextInput style={styles.gameTitleTextInput}
        					  placeholder='Enter a title for your game...'
        					  value={this.state.gameTitle}
     					      onChangeText={(text) => this.setState({gameTitle: text})} />
        	    </View>
        	    <TouchableOpacity style={styles.fab}
								  onPress={() => this.navToSelectRecipients()}>
					<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/chevron-right.png')} />
				</TouchableOpacity>
			</View>
		);
	}

	uiSelectRecipients() {
		// Check if it should render a place holder or the present content
		if (this.state.familyMembers.length != 0 && this.state.familyMembers.length != undefined) {
			return(
				<View style={styles.recipientsBody}>
					<View style={styles.recipientsPrompt}>
						<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>
							Select members from your family group to send the game to:
						</Text>
					</View>
					<View style={[gstyles.cardColumn, styles.attachedMessageCard]}>
						<Text style={gstyles.subTitle}>Send a message with your game...</Text>
						<TextInput style={styles.attachedMessageTextInput}
								   multiline={true}
	        					   placeholder='Please enter your message here...'
	        					   value={this.state.attachedMessage}
	     					       onChangeText={(text) => this.setState({attachedMessage: text})} />
					</View>
					<View style={[gstyles.cardColumn, styles.recipientsCard]}>
						<FlatList data={this.state.familyMembers}
						          keyExtractor={(item, index) => index.toString()}
						          scrollEnabled={true}
						          extraData={this.state}
	                              renderItem={(item) => <TouchableOpacity style={styles.memberRow}
	                              										  onPress={() => this.onToggleRecipient(item.index, item.item)}>
	                              							<View style={gstyles.shadow}>
	                              								{this.uiSelectRecipientsPP(item)}
	                              							</View>
	                              							<View style={styles.memberName}>
	                              								<Text style={gstyles.smallText}>{item.item.name}</Text>
                              								</View>
	                              							<Image style={[item.item.styleStatus]} source={require('../../../assets/icons/png/check.png')}/>
								                	    </TouchableOpacity>}/>
					</View>
					<TouchableOpacity style={styles.fab}
									  onPress={() => this.navToSend()}>
						<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/chevron-right.png')} />
					</TouchableOpacity>
				</View>
			);
		}
		else {
			return(
				<View style={styles.recipientsBody}>
					<View style={styles.recipientsPrompt}>
						<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>
							Select members from your family group to send the game to:
						</Text>
					</View>
					<View style={[gstyles.cardColumn, styles.recipientsCard]}>
						<Text style={[gstyles.smallText, {textAlign: 'center'}]}>
						It appears you have not added any family members yet!{"\n"}
						Your game will still be saved but not sent to anyone.
						</Text>
					</View>
					<TouchableOpacity style={styles.fab}
									  onPress={() => this.navToSend()}>
						<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/chevron-right.png')} />
					</TouchableOpacity>
				</View>
			);
		}
	}

	uiSelectRecipientsPP(item) {
		if (this.state.demoMode != true) {
			return(
				<CacheableImage style={styles.profilePictures} source={{uri: item.item.profilePicture}} />
			);
		}
		else {
			return(
				<Image style={styles.profilePictures} source={{uri: item.item.profilePicture}} />
			);
		}
	}

	uiSend() {
		return(
			<View style={styles.sendBody}>
				<View style={styles.sendTitle}>
					<Text style={gstyles.mediumText}>Game Title: {this.state.gameTitle}</Text>
				</View>
				<View style={styles.sendBodyPreviewRow}>
					<View style={gstyles.shadow}>
						<EasySlideshow data={this.state.slideshowData} demoMode={this.state.demoMode}/>
					</View>
					<View style={[gstyles.cardColumn, styles.sendStatsCard]}>
						<Text style={gstyles.subTitle}>Number of questions: {this.state.gameData.length}</Text>
					</View>
				</View>
				<View style={styles.sendBodyActionRow}>
					<TouchableOpacity style={[styles.sendActionButton1, gstyles.shadow]}
							  		  onPress={() => this.navToCreatedGame()}>
						<Text style={gstyles.smallText}>Add more questions?</Text>
					</TouchableOpacity>
					<Text style={gstyles.mediumText}>Or</Text>
					<TouchableOpacity style={[styles.sendActionButton2, gstyles.shadow]}
							  		  onPress={() => this.uploadGame()}>
						<Text style={gstyles.smallText}>Send my game!</Text>
					</TouchableOpacity>
				</View>
			{/*<TouchableOpacity style={[styles.fab, {end: '18%', bottom: '4%'}]}
							  onPress={() => this.uploadGame()}>
				<Image style={gstyles.smallIcon} source={require('../../../assets/icons/png/send.png')} />
			</TouchableOpacity>*/}
			</View>
		);
	}

	uiMainMenu2() {
		if (this.state.enableMainMenu2 == true) {
			return(
				<BlurView style={styles.mainMenuBlur} intensity={90} tint='dark'>
					<View style={gstyles.shadow}>
						<Image style={styles.mainMenuLogo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
					</View>
			        <TouchableOpacity style={[styles.closeIcon, gstyles.shadow]} 
			        				  onPress={() => this.setState({enableMainMenu1: false, enableMainMenu2: false})}>
                    	<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/close.png')} />
                	</TouchableOpacity>
				</BlurView>
			);
		}
		else {
			return(null);
		}
	}

	uiBuildTutorial() {
		if (this.state.buildTutorialOverlayEnabled == true) {
			return(
				<BlurView style={styles.buildTutorialBlur} intensity={90} tint='dark'>
					<TouchableOpacity style={styles.buildTutorialBack} 
								      onPress={() => this.props.navigation.navigate('CreatedGamesScreen')}>
					    <Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/arrow-left.png')} />
					    <Text style={gstyles.subTitle}>Back</Text>
			        </TouchableOpacity>
					<View style={[gstyles.shadow, styles.buildTutorialLogo]}>
						<Image style={styles.buildTutorialLogo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
					</View>
					<View style={styles.buildTutorialProcesses}>
						<Animated.View style={[styles.animatedView, this.state.animationCard]}>
                            <CardFlip ref={(card) => this.card = card} style={styles.animatedCard} duration={1200}>
                                <Image style={styles.animatedImage1} 
                                       source={animationImages[this.state.animationImageIndex].image}
                                       transform={[{scaleX: -1}]} />
                                <Image style={styles.animatedImage2} 
                                       source={animationImages[this.state.animationImageIndex].question}
                                       transform={[{scaleX: -1}]} />
                            </CardFlip>
                        </Animated.View>
					</View>
					<Text>
						<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>
						Memory Lane Games let's you turn family photos into
						a family game!{"\n"}
						</Text>
						<Text style={[gstyles.subTitle, {textAlign: 'center'}]}>
						{"\n"}If you want a quick tutorial then press the
						'tutorial' button below or if you feel confident
						enough press 'start' to get building!
						</Text>
					</Text>
					<View style={styles.buildTutorialButtons}>
						<TouchableOpacity style={[styles.buildTutorialSeeVideoButton, gstyles.shadow]} 
				        				  onPress={() => this.setState({buildTutorialOverlayEnabled: false,
				        				  							    videoTutorialEnabled: true})}>
	                    	<Text style={gstyles.smallText}>See video tutorial!</Text>
	                	</TouchableOpacity>
				        <TouchableOpacity style={[styles.buildTutorialStartButton, gstyles.shadow]} 
				        				  onPress={() => this.setState({buildTutorialOverlayEnabled: false})}>
	                    	<Text style={gstyles.smallText}>Start creating!</Text>
	                	</TouchableOpacity>
                	</View>
				</BlurView>
			);
		}
		else {
			return(null);
		}
	}

	uiVideoTutorial() {
		if (this.state.videoTutorialEnabled == true) {
			return(
				<BlurView style={styles.buildTutorialBlur} intensity={90} tint='dark'>
					<View style={[gstyles.shadow, styles.videoTutorialLogo]}>
						<Image style={styles.videoTutorialLogo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
					</View>
					<Text style={gstyles.mediumText}>Creating a Memory Lane Game</Text>
					<View style={gstyles.shadow}>
						<Video isLooping={true} style={styles.tutorialVideo} source={{uri: 'https://s3-eu-west-1.amazonaws.com/testingmlg/sample.mp4'}} shouldPlay={true}/>
					</View>
			        <TouchableOpacity style={[styles.buildTutorialStartButton, gstyles.shadow]} 
			        				  onPress={() => this.setState({videoTutorialEnabled: false})}>
                    	<Text style={gstyles.smallText}>Continue!</Text>
                	</TouchableOpacity>
                	<TouchableOpacity style={styles.launchTrailerButton}>
                		<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/exit-to-app.png')} />
                	</TouchableOpacity>
				</BlurView>
			);
		}
		else {
			return(null);
		}
	}

	render() {
		return(
			<View style={styles.container}>
				{this.uiTitleBar()}
				<View style={styles.body}>
					{this.uiSectionNavigator()}
					{this.uiBody()}
				</View>
				{this.uiCamera()}
				{this.uiSaveFAB()}
				{this.uiUploadingBlur()}
				{this.uiVoiceRecorder()}
				{this.uiMainMenu1()}
				{this.uiPreviewQuestion()}
				{this.uiMainMenu2()}
				{this.uiBuildTutorial()}
				{this.uiVideoTutorial()}
			</View>
		);
	}

}