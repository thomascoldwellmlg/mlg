import { StyleSheet, Dimensions } from 'react-native';
import { color } from '../../reusable/common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

if (screenHeight > screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const sf = 0.82;

const styles = StyleSheet.create({

	/*
		Contents:
			0. General
			1. Tutorial
			2. Sample
			3. Add Image
			3.5 Add Family Album Photo
			4. Add Title and Answers
			5. Add Question Hint
			6. Add Additional Question

			7. Add Game Title
			8. Select Recipients
			9. Add Message and Send

			10. Camera
			11. Main Menus
			12. Question Preview
			13. Build Tutorial
	*/

	// 0. General
	container: {
		height: screenHeight,
		width: screenWidth,
		backgroundColor: color.lightBlue
	},
	titleBar: {
		height: screenHeight * 0.16,
		width: screenWidth,
		backgroundColor: color.darkBlue,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	titleBarMid: {
		height: screenHeight * 0.16,
		width: screenWidth * 0.55,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	logo: {
		height: screenHeight * 0.11,
		width: screenHeight * 0.11
	},
	body: {
		height: screenHeight * 0.84,
		width: screenWidth,
		flexDirection: 'row',
		justifyContent: 'flex-start'
	},
	sectionNavigator: {
		height: screenHeight * 0.84,
		width: screenWidth * 0.2,
		backgroundColor: color.darkBlue
	},
	sectionRow: {
		height: screenHeight * 0.21,
		width: screenWidth * 0.2,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	sectionRow3rds: {
		height: screenHeight * 0.28,
		width: screenWidth * 0.2,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	sectionStatusCircle: {
		height: screenHeight * 0.03,
		width: screenHeight * 0.03,
		borderRadius: screenHeight * 0.015,
		backgroundColor: color.grey
	},
	sectionText: {
		height: screenHeight * 0.21,
		width: screenWidth * 0.13,
		alignItems: 'center',
		justifyContent: 'center'
	},
	selectedSection: {
		height: screenHeight * 0.21,
		width: screenWidth * 0.2,
		backgroundColor: color.lightBlue,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	selectedSection3rds: {
		height: screenHeight * 0.28,
		width: screenWidth * 0.2,
		backgroundColor: color.lightBlue,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	unselectedSection: {
		height: screenHeight * 0.21,
		width: screenWidth * 0.2,
		backgroundColor: color.darkBlue
	},
	unselectedSection3rds: {
		height: screenHeight * 0.28,
		width: screenWidth * 0.2,
		backgroundColor: color.darkBlue
	},
	sectionStatus: {
		backgroundColor: color.green
	},
	createdGameSection: {
		backgroundColor: color.yellow
	},
	stage: {
		height: screenHeight * 0.84,
		width: screenWidth * 0.8,
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	fab: {
		position: 'absolute',
		height: screenHeight * 0.14,
		width: screenHeight * 0.14,
		borderRadius: screenHeight * 0.07,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.green,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3,
		end: '4%',
		bottom: '8%'
	},

	// 1. Tutorial
	tutorialBody: {
		height: screenHeight * 0.84,
		width: screenWidth,
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingTop: screenHeight * 0.05,
		paddingBottom: screenHeight * 0.05
	},
	tutorialSteps: {
		height: screenHeight * 0.4,
		width: screenWidth * 0.9,
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	stepColumn: {
		height: screenHeight * 0.35,
		width: screenWidth * 0.22,
		alignItems: 'center',
		justifyContent: 'space-around'
	},
	tutorialButtons: {
		height: screenHeight * 0.2,
		width: screenWidth * 0.9,
		justifyContent: 'space-around',
		alignItems: 'center',
		flexDirection: 'row'
	},
	seeSampleGameButton: {
		height: screenHeight * 0.18,
		width: screenWidth * 0.35,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.yellow,
		borderRadius: screenWidth * 0.01
	},
	startBuildButton: {
		height: screenHeight * 0.18,
		width: screenWidth * 0.35,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.green,
		borderRadius: screenWidth * 0.01,
		paddingLeft: screenWidth * 0.03,
		paddingRight: screenWidth * 0.03
	},

	// 2. Sample
	sampleBody: {
		height: screenHeight * 0.8375,
		width: screenWidth,
		justifyContent: 'space-around',
		alignItems: 'center',
		backgroundColor: color.lighBlue,
		paddingTop: screenHeight * 0.05,
		paddingBottom: screenHeight * 0.15,
		marginTop: screenHeight * 0.0025
	},
	sampleVideo: {
		height: screenHeight * 0.6,
		width: screenWidth * 0.8
	},

	// 3. Add Image
	addImage1Body: {
		height: screenHeight * 0.84,
		width: screenWidth * 0.8,
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingTop: screenHeight * 0.02
	},
	addImagePrompt: {
		textAlign: 'center',
		width: screenWidth * 0.5
	},
	addImagePhoto: {
		height: screenHeight * 0.38,
		width: screenWidth * 0.5,
		borderRadius: screenWidth * 0.01
	},
	addImageButtons: {
		height: screenHeight * 0.23,
		width: screenWidth * 0.8,
		justifyContent: 'space-around',
		alignItems: 'center',
		flexDirection: 'row',
		paddingTop: screenHeight * 0.03
	},
	addImageButton: {
		height: screenHeight * 0.15,
		width: screenWidth * 0.22,
		paddingLeft: screenWidth * 0.03,
		paddingRight: screenWidth * 0.03
	},
	addImageButtonText: {
		textAlign: 'center'
	},
	gridViewItem: {
    	height: screenWidth * 0.21,
    	width: screenWidth * 0.21,
    	alignSelf: 'center',
    	marginBottom: screenHeight * 0.015,
    	backgroundColor: '#ccc',
    	shadowColor: 'black',
    	shadowOpacity: 0.3,
    	shadowOffset: {
    		width: 0,
    		height: 5
    	}
    },
    familyAlbumGridContainer: {
    	width: screenWidth * 0.78,
    	alignSelf: 'center'
    },
    familyAlbumPrompt: {
    	textAlign: 'center',
    	alignSelf: 'center',
    	paddingTop: screenHeight * 0.04,
    	paddingBottom: screenHeight * 0.04
    },
    changePhotoOverlayButton: {
    	position: 'absolute',
    	bottom: 0,
    	height: screenHeight * 0.06,
    	width: screenWidth * 0.5,
    	justifyContent: 'center',
    	alignItems: 'flex-start'
    },
    changePhotoOverlay: {
    	height: screenHeight * 0.06,
    	width: screenWidth * 0.5,
    	justifyContent: 'center',
    	alignItems: 'flex-start',
    	paddingLeft: screenWidth * 0.02,
    	borderBottomLeftRadius: screenWidth * 0.01,
    	borderBottomRightRadius: screenWidth * 0.01
    },

	// 3.5 Add Family Album Photo
	keyboardScrollView: {
		flex: 1,
		width: screenWidth * 0.8,
		paddingTop: screenHeight * 0.03
	},
	familyAlbumImage: {
		height: screenHeight * 0.4,
		width: screenWidth * 0.5,
		alignSelf: 'center',
		marginBottom: screenHeight * 0.03,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3,
		borderRadius: 10
	},
	familyAlbumMainDetailsCard: {
		height: screenHeight * 0.6,
		width: screenWidth * 0.7,
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
		paddingLeft: screenWidth * 0.05,
		alignSelf: 'center'
	},
	familyAlbumMainDetailsCardCollapsed: {
		height: screenHeight * 0.2,
		width: screenWidth * 0.4,
		alignItems: 'center',
		justifyContent: 'center',
		alignSelf: 'center'
	},
	familyAlbumDetailsInputs: {
		height: screenHeight * 0.5,
		width: screenWidth * 0.8,
		marginLeft: screenWidth * 0.03,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginTop: screenHeight * 0.02
	},
	familyAlbumTextInputContainer: {
		height: screenHeight * 0.1,
		width: screenWidth * 0.52,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginBottom: screenHeight * 0.01
	},
	familyAlbumTextInput: {
		fontFamily: 'Futura',
		fontSize: 20,
		color: '#fff',
		height: screenHeight * 0.05,
		width: screenWidth * 0.52,
		borderBottomWidth: 3,
		borderColor: color.lightBlue
	},
	faceDetectionInputs: {
		width: screenWidth * 0.4
	},
	faceDetectionRow: {
		flexDirection: 'row',
		height: screenHeight * 0.18,
	},
	faceDetectionProfilePictures: {
		marginRight: screenWidth * 0.03
	},
	uploadingBlur: {
		height: screenHeight,
		width: screenWidth,
		paddingVertical: screenHeight * 0.3,
		position: 'absolute',
		backgroundColor: '#000',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	uploadingLogo: {
		height: screenHeight * 0.15,
		width: screenHeight * 0.15
	},

	// 4. Add Title and Answers
	mainInfoBody: {
		flex: 1,
		width: screenWidth * 0.8,
		paddingTop: screenHeight * 0.05
	},
	questionTitleInputContainer: {
		height: screenHeight * 0.1,
		width: screenWidth * 0.7,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginBottom: screenHeight * 0.03,
		alignSelf: 'center'
	},
	questionTitleInput: {
		fontFamily: 'Futura',
		fontSize: 20,
		color: '#fff',
		height: screenHeight * 0.05,
		width: screenWidth * 0.7,
		borderBottomWidth: 3,
		borderColor: color.darkBlue
	},
	mainInfoImage: {
		height: screenHeight * 0.4,
		width: screenWidth * 0.5,
		alignSelf: 'center',
		marginBottom: screenHeight * 0.03,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3,
		borderRadius: 10
	},
	answerRow: {
		height: screenHeight * 0.15,
		width: screenWidth * 0.7,
		flexDirection: 'row',
		justifyContent: 'flex-start',
		alignItems: 'center',
		alignSelf: 'center'
	},
	answerInputContainer: {
		height: screenHeight * 0.1,
		width: screenWidth * 0.25,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		alignSelf: 'center',
		marginRight: screenWidth * 0.06
	},
	correctAnswerUnderline: {
		borderColor: color.darkGreen
	},
	answerInput: {
		fontFamily: 'Futura',
		fontSize: 20,
		color: '#fff',
		height: screenHeight * 0.05,
		width: screenWidth * 0.25,
		borderBottomWidth: 3,
		borderColor: color.darkBlue
	},

	// 5. Add Question Hint
	questionHintBody: {
		height: screenHeight * 0.84,
		width: screenWidth * 0.8,
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingTop: screenHeight * 0.15,
		paddingBottom: screenHeight * 0.25,
		paddingLeft: screenWidth * 0.05,
		paddingRight: screenWidth * 0.05
	},
	questionHintButton: {
		height: screenHeight * 0.18,
		width: screenWidth * 0.35,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		backgroundColor: color.red,
		borderRadius: screenWidth * 0.01,
		paddingLeft: screenWidth * 0.02,
		paddingRight: screenWidth * 0.02
	},
	questionHintIcon: {
		height: screenHeight * 0.04,
		width: screenHeight * 0.04,
		borderRadius: screenHeight * 0.02,
		backgroundColor: '#fff'
	},

	// 6. Additional question
	additionalQuestionBody: {
		height: screenHeight * 0.84,
		width: screenWidth * 0.8,
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingTop: screenHeight * 0.15,
		paddingBottom: screenHeight * 0.25,
		paddingLeft: screenWidth * 0.05,
		paddingRight: screenWidth * 0.05
	},
	additionalQuestionButton: {
		height: screenHeight * 0.18,
		width: screenWidth * 0.35,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		backgroundColor: color.red,
		borderRadius: screenWidth * 0.01,
		paddingLeft: screenWidth * 0.02,
		paddingRight: screenWidth * 0.02
	},
	additionalQuestionIcon: {
		height: screenHeight * 0.04,
		width: screenHeight * 0.04,
		borderRadius: screenHeight * 0.02,
		backgroundColor: '#fff'
	},

	// 7. Add Game Title
	addTitleBody: {
		height: screenHeight * 0.84,
		width: screenWidth * 0.8,
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingLeft: screenWidth * 0.05,
		paddingRight: screenWidth * 0.05,
		paddingBottom: screenHeight * 0.48,
		paddingTop: screenHeight * 0.05
	},
	gameTitleTextInputContainer: {
		height: screenHeight * 0.1,
		width: screenWidth * 0.52,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginBottom: screenHeight * 0.01
	},
	gameTitleTextInput: {
		fontFamily: 'Futura',
		fontSize: 20,
		color: '#fff',
		height: screenHeight * 0.05,
		width: screenWidth * 0.52,
		borderBottomWidth: 3,
		borderColor: color.darkBlue
	},

	// 8. Select Recipients
	recipientsBody: {
		height: screenHeight * 0.84,
		width: screenWidth * 0.8,
		alignItems: 'center',
		justifyContent: 'space-around'
	},
	recipientsPrompt: {
		height: screenHeight * 0.15,
		width: screenWidth * 0.6,
		justifyContent: 'center',
		alignItems: 'center'
	},
	recipientsCard: {
		height: screenHeight * 0.4,
		width: screenWidth * 0.65
	},
	memberRow: {
		height: screenHeight * 0.15,
		width: screenWidth * 0.55,
		alignSelf: 'center',
		flexDirection: 'row',
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	profilePictures: {
		height: screenHeight * 0.12,
		width: screenHeight * 0.12,
		borderRadius: screenHeight * 0.06
	},
	memberName: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.3,
		justifyContent: 'center',
		alignItems: 'flex-start',
		marginLeft: screenWidth * 0.05,
		marginRight: screenWidth * 0.04
	},
	unselectedRecipient: {
		height: screenHeight * 0.08,
		width: screenHeight * 0.08,
		tintColor: color.grey,
		opacity: 0.7
	},
	selectedRecipient: {
		height: screenHeight * 0.08,
		width: screenHeight * 0.08,
		tintColor: color.green
	},
	attachedMessageCard: {
		height: screenHeight * 0.2,
		width: screenWidth * 0.65,
		justifyContent: 'space-between',
		alignItems: 'flex-start'
	},
	attachedMessageTextInput: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.6,
		fontFamily: 'Futura',
		fontSize: 20,
		color: '#fff',
		borderBottomWidth: 3,
		borderColor: color.lightBlue
	},

	// 9. Send
	sendBody: {
		height: screenHeight * 0.84,
		width: screenWidth * 0.8,
		paddingHorizontal: screenWidth * 0.04
	},
	sendTitle: {
		alignSelf: 'center',
		height: screenHeight * 0.12,
		width: screenWidth * 0.7,
		justifyContent: 'center'
	},
	sendBodyPreviewRow: {
		alignSelf: 'center',
		height: screenHeight * 0.4,
		width: screenWidth * 0.7,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	sendStatsCard: {
		height: screenHeight * 0.25,
		width: screenWidth * 0.26,
		marginBottom: 0
	},
	sendBodyActionRow: {
		alignSelf: 'center',
		height: screenHeight * 0.3,
		width: screenWidth * 0.7,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	sendActionButton1: {
		height: screenHeight * 0.15,
		width: screenWidth * 0.25,
		borderRadius: screenWidth * 0.01,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.yellow
	},
	sendActionButton2: {
		height: screenHeight * 0.15,
		width: screenWidth * 0.25,
		borderRadius: screenWidth * 0.01,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.green
	},

	// 10. Camera
	camera: {
    	position: 'absolute',
    	height: screenHeight,
    	width: screenWidth,
    	justifyContent: 'flex-end'
    },
    cameraActionBar: {
    	height: screenHeight * 0.1,
    	width: screenWidth,
    	justifyContent: 'space-around',
    	alignItems: 'center',
    	flexDirection: 'row',
    	backgroundColor: '#000',
    	opacity: 0.7
    },
    cameraRotateButton: {
    	height: screenHeight * 0.1,
    	width: screenHeight * 0.1,
    	justifyContent: 'center',
    	alignItems: 'center'
    },
    cameraShutterButton: {
    	height: screenHeight * 0.08,
    	width: screenHeight * 0.08,
    	borderRadius: screenHeight * 0.1,
    	marginLeft: screenWidth * 0.25,
    	marginRight: screenWidth * 0.25,
    	justifyContent: 'center',
    	alignItems: 'center'
    },

    // 11. Main Menus
    mainMenuBlur: {
    	height: screenHeight,
		width: screenWidth,
		position: 'absolute',
		backgroundColor: '#000',
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingTop: screenHeight * 0.1,
		paddingBottom: screenHeight * 0.05
    },
    mainMenuLogo: {
    	height: screenHeight * 0.2,
    	width: screenHeight * 0.2,
    	marginBottom: screenHeight * 0.05
    },
    mainMenuButton: {
    	height: screenHeight * 0.12,
    	width: screenWidth * 0.35,
    	justifyContent: 'center',
    	alignItems: 'center',
    	borderRadius: screenWidth * 0.01,
    	backgroundColor: color.green
    },
    closeIcon: {
		position: 'absolute',
		right: '5%',
		top: '5%',
		alignItems: 'center'
	},

	// 12. Preview Question
	previewBlur: {
    	height: screenHeight,
		width: screenWidth,
		position: 'absolute',
		backgroundColor: '#000',
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingVertical: screenHeight * 0.03
    },
    previewContainer: {
    	height: screenWidth * 0.55 * sf,
    	width: screenWidth * 0.73 * sf,
    	alignItems: 'center',
    	backgroundColor: color.lightBlue
    },
    previewNavBar: {
    	height: screenHeight * 0.117 * sf,
    	width: screenWidth * 0.73 * sf,
    	flexDirection: 'row',
    	justifyContent: 'space-between',
    	alignItems: 'center',
    	backgroundColor: color.darkBlue,
    	paddingLeft: screenWidth * 0.03 * sf,
    	paddingRight: screenWidth * 0.03 * sf,
    	marginBottom: screenHeight * 0.03 * sf
    },
    previewLogo: {
    	height: screenHeight * 0.08 * sf,
    	width: screenHeight * 0.08 * sf
    },
    previewImage: {
    	height: screenHeight * 0.3 * sf,
		width: screenWidth * 0.35 * sf,
		alignSelf: 'center',
		marginBottom: screenHeight * 0.02 * sf,
		borderRadius: screenWidth * 0.02 * sf,
		marginTop: screenHeight * 0.015 * sf,
		marginBottom: screenHeight * 0.03 * sf
    },
    previewButtonRow: {
    	height: screenHeight * 0.15 * sf,
    	width: screenWidth * 0.7 * sf,
    	flexDirection: 'row',
    	justifyContent: 'space-around',
    	alignItems: 'center'
    },
    previewButton: {
    	height: screenHeight * 0.12 * sf,
    	width: screenWidth * 0.15 * sf,
    	justifyContent: 'center',
    	alignItems: 'center',
    	borderRadius: screenWidth * 0.01 * sf,
    	backgroundColor: color.cardBlue
    },
    previewButtonText: {
    	fontFamily: 'Futura',
    	fontSize: screenHeight * 0.025 * sf,
    	color: '#fff',
    	textAlign: 'center'
    },
    previewActionButtons: {
    	height: screenHeight * 0.15,
    	width: screenWidth * 0.9,
    	flexDirection: 'row',
    	justifyContent: 'space-around',
    	alignItems: 'center'
    },
    previewActionButton: {
    	height: screenHeight * 0.15,
    	width: screenWidth * 0.2,
    	justifyContent: 'center',
    	alignItems: 'center',
    	borderRadius: screenWidth * 0.01,
    	backgroundColor: color.green,
    	paddingHorizontal: screenWidth * 0.02
    },

    // 13. Build Tutorial
    buildTutorialBlur: {
    	height: screenHeight,
		width: screenWidth,
		position: 'absolute',
		backgroundColor: '#000',
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingHorizontal: screenWidth * 0.2,
		paddingVertical: screenHeight * 0.04
    },
    buildTutorialBack: {
    	position: 'absolute',
    	top: screenHeight * 0.04,
    	left: screenWidth * 0.04,
    	justifyContent: 'center',
    	alignItems: 'center'
    },
    buildTutorialLogo: {
    	height: screenHeight * 0.14,
    	width: screenHeight * 0.14
    },
    buildTutorialProcesses: {
    	height: screenHeight * 0.3,
    	width: screenWidth * 0.9,
    	flexDirection: 'row',
    	justifyContent: 'flex-start',
    	alignItems: 'center'
    },
    buildTutorialProcessVideo: {
    	height: screenHeight * 0.3,
		width: screenWidth * 0.3
    },
    buildTutorialButtons: {
    	height: screenHeight * 0.12,
    	width: screenWidth * 0.8,
    	flexDirection: 'row',
    	justifyContent: 'space-around',
    	alignItems: 'center'
    },
    buildTutorialSeeVideoButton: {
    	height: screenHeight * 0.12,
    	width: screenWidth * 0.3,
    	borderRadius: screenWidth * 0.01,
    	justifyContent: 'center',
    	alignItems: 'center',
    	backgroundColor: color.yellow
    },
    buildTutorialStartButton: {
    	height: screenHeight * 0.12,
    	width: screenWidth * 0.3,
    	borderRadius: screenWidth * 0.01,
    	justifyContent: 'center',
    	alignItems: 'center',
    	backgroundColor: color.green
    },
    videoTutorialLogo: {
    	position: 'absolute',
    	height: screenHeight * 0.14,
    	width: screenHeight * 0.14,
    	left: '5%',
    	top: '3%'
    },
    tutorialVideo: {
    	height: screenHeight * 0.5,
		width: screenWidth * 0.667
    },
    helpButton: {
		position: 'absolute',
		left: '5%',
		top: '5%',
		alignItems: 'center'
	},
	launchTrailerButton: {
		position: 'absolute',
		right: '6%',
    	top: '6%'
	},
	animatedView: {
		height: screenHeight * 0.3,
		width: screenWidth * 0.35,
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	animatedCard: {
		height: screenHeight * 0.3,
		width: screenWidth * 0.35,
		justifyContent: 'center',
		alignItems: 'center'
	},
	animatedImage1: {
		height: screenHeight * 0.3,
		width: screenWidth * 0.35,
		resizeMode: 'contain'
	},
	animatedImage2: {
		height: screenHeight * 0.3,
		width: screenWidth * 0.35,
		resizeMode: 'contain'
	},

	// Extra styles
	greenHighlight: {
		backgroundColor: color.green,
		height: screenHeight * 0.17,
		width: screenWidth * 0.22
	}

});

export default styles;