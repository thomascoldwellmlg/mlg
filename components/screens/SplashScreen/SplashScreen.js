//Import React Components
import React from 'react';
import {View, 
        Text, 
        Image, 
        TextInput, 
        TouchableOpacity, 
        Alert, 
        StyleSheet, 
        Dimensions, 
        KeyboardAvoidingView,
        Animated,
        Platform,
        Easing,
        ActivityIndicator} from 'react-native';
import { Button } from '../../reusable/rui';
import Expo from 'expo';
import {Permissions, BlurView, ScreenOrientation} from 'expo';
import * as firebase from 'firebase';
import styles from './SplashScreenStyles';
import gstyles from '../../reusable/GlobalStyles';
import { UserData,
         UserId,
         UserEmail,
         Append } from '../../../lib/Firebase';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CardFlip from 'react-native-card-flip';
import SeeHowAnimation from '../../reusable/SeeHowAnimation/SeeHowAnimation';

var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

// Ensure style sheet is given as portrait
if (screenHeight < screenWidth) {
    var temp = screenHeight;
    screenHeight = screenWidth;
    screenWidth = temp;
}

const animationImages = [
    {
        image: require('../../../assets/images/animations/questionImage1.jpg'),
        question: require('../../../assets/images/animations/completeQuestion1.png')
    },
    {
        image: require('../../../assets/images/animations/questionImage2.jpg'),
        question: require('../../../assets/images/animations/completeQuestion2.png')
    },
    {
        image: require('../../../assets/images/animations/questionImage3.jpg'),
        question: require('../../../assets/images/animations/completeQuestion3.png')
    }
]

export default class SplashScreen extends React.Component {

    state = {
        renderState: 'main',
        permissionsGranted: true,
        showTutorial: true,
        animationCard: {
            marginLeft: new Animated.Value(0),
            opacity: new Animated.Value(0)
        },
        animationCaption: 'Turning your photos...',
        animationImageIndex: 0,
        buffering: false
    }

    async componentDidMount() {
        // Lock the screen orientation to portrait mode
        await this.getPermissions();
        await Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);
        // Fetch the saved user login credentials from Secure Store
        let userName =  await Expo.SecureStore.getItemAsync('userName');
        let userPassword =  await Expo.SecureStore.getItemAsync('userPassword');
        await this.setState({username: userName});
        await this.setState({password: userPassword});
        if (this.state.username != null) {
        // Try to automatically log the user in with these saved credentials
        //await this.autoLogin();
        }
        else {
        // Else the user is registering for the first time - do nothing and let them select
        // if they want to login to an existing account or register for one.
        }
        // Start animation for 'image into game'
        this.startSplashScreenAnimation();
    }

    startSplashScreenAnimation() {
        this.card.flip();
        // Animations for the starting iamge
        Animated.loop(Animated.sequence([
            Animated.delay(1000),
            Animated.parallel([
                Animated.timing(
                    this.state.animationCard.marginLeft,
                    {
                        toValue: screenWidth * 0.14,
                        duration: 3000,
                        useNativeDriver: false,
                        onComplete: () => {
                            if (this.card != null) {
                                this.card.flip();
                                this.setState({animationCaption: 'Into your games...'});
                            }
                        }
                    }
                ),
                Animated.timing(
                    this.state.animationCard.opacity,
                    {
                        toValue: 1.0,
                        duration: 1500,
                        useNativeDriver: false
                    }
                )
            ]),
            Animated.delay(5000),
            Animated.parallel([
                Animated.timing(
                    this.state.animationCard.marginLeft,
                    {
                        toValue: screenWidth * 0.9,
                        duration: 3000,
                        useNativeDriver: false,
                        onComplete: () => {
                            if (this.card != null) {
                                this.card.flip();
                                this.setState({animationCaption: 'Turning your photos...'});
                                // Advance or reset the image index so it cycles round
                                if (this.state.animationImageIndex == 2) {
                                    this.setState({animationImageIndex: 0});
                                }
                                else {
                                    this.setState({animationImageIndex: this.state.animationImageIndex + 1});
                                }
                            }
                        }
                    }
                ),
                Animated.timing(
                    this.state.animationCard.opacity,
                    {
                        toValue: 0.0,
                        duration: 1500,
                        useNativeDriver: false,
                        delay: 1500
                    }
                )
            ])
        ])).start();
    }

    async getPermissions() {
        //Get all Permissions for the App
        this.setState({permissionsGranted: true});
        const PERMISSIONS = [Permissions.CAMERA, Permissions.CAMERA_ROLL, Permissions.AUDIO_RECORDING, Permissions.LOCATION];
        for (var i = 0; i <= PERMISSIONS.length - 1; i++) {
            const { status } = await Permissions.askAsync(PERMISSIONS[i]);
            if (status !== 'granted') {
                Alert.alert('Permissions must be granted for MLG to run. Please go into Settings > MLG  and enable all permisisons and launch the app again!');
                this.setState({permissionsGranted: false});
            }
        }
    }

    navToLogin() {
        // Check permissions have been granted before continuing
        if (this.state.permissionsGranted == true) {
            this.props.navigation.navigate('LoginScreen');
        }
        else {
            this.getPermissions();
        }
    }

    async navToDemo() {
        // Check permissions have been granted before continuing
        if (this.state.permissionsGranted == true) {
            await Expo.SecureStore.setItemAsync('demo-mode', 'true');
            this.props.navigation.navigate('CreatedGamesScreen');
        }
        else {
            this.getPermissions();
        }
    }

    uiMain() {
        if (this.state.renderState == 'main') {
            return(
                <BlurView style={styles.tutorialBlur} intensity={90} tint='dark'>
                    <Image style={styles.tutorialLogo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
                    {/*<Text style={gstyles.largeText}>Memory Lane Games</Text>*/}
                    <View style={styles.tutorialGIF}>
                        <Animated.View style={[styles.animatedView, this.state.animationCard]}>
                            <Text style={gstyles.mediumText}>{this.state.animationCaption}</Text>
                            <CardFlip ref={(card) => this.card = card} style={styles.animatedCard} duration={1200}>
                                <Image style={styles.animatedImage1} 
                                       source={animationImages[this.state.animationImageIndex].image}
                                       transform={[{scaleX: -1}]} />
                                <Image style={styles.animatedImage2} 
                                       source={animationImages[this.state.animationImageIndex].question}
                                       transform={[{scaleX: -1}]} />
                            </CardFlip>
                        </Animated.View>
                    </View>
                    {/*<Text style={[gstyles.mediumText, {textAlign: 'center'}]}>Turning family photos into family games!</Text>*/}
                    <View style={styles.tutorialButtonRow}>
                        <TouchableOpacity style={[gstyles.shadow, styles.tutorialDemoButton]}
                                          onPress={() => this.navToDemo()}>
                            <Text style={gstyles.smallText}>Try a demo?</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[gstyles.shadow, styles.tutorialHowButton]}
                                          onPress={ async () => {
                                            await ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE);
                                            this.setState({renderState: 'see-how'});
                                          }}>
                            <Text style={[gstyles.smallText, {textAlign: 'center'}]}>See how{"\n"} it works?</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[gstyles.shadow, styles.tutorialContinueButton]}
                                          onPress={() => this.navToLogin()}>
                            <Text style={[gstyles.smallText, {textAlign: 'center'}]}>Login{"\n"}or register!</Text>
                        </TouchableOpacity>
                    </View>
                </BlurView>
            );
        }
    }

    uiSeeHow() {
        if (this.state.renderState == 'see-how') {
            return(
                <View style={styles.seeHowContainer}>
                    <View style={gstyles.shadow}>
                        <View style={styles.seeHowTitleBar}>
                            <View style={gstyles.shadow}>
                                <Image style={styles.seeHowLogo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
                            </View>
                            <Text style={gstyles.largeText}>See How It Works!</Text>
                            <TouchableOpacity style={styles.seeHowBackButton}
                                              onPress={ async () => {
                                                await ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT);
                                                this.setState({renderState: 'main'});
                                                this.startSplashScreenAnimation();
                                              }}>
                                <Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/arrow-left.png')} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.seeHowBody}>
                        <View style={gstyles.shadow}>
                            <SeeHowAnimation />
                        </View>
                    </View>
                </View>
            );
        }
    }

    render() {
        return(
            <View>
                {this.uiMain()}
                {this.uiSeeHow()}
            </View>
        );
    }
}


