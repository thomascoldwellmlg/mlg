import React from 'react';
import {View,
		Image,
		Text,
		TouchableOpacity,
		Animated,
		Alert} from 'react-native';
import styles from './CreatorHomeNavBarStyles';
import Expo from 'expo';

export default class CreatorHomeNavBar extends React.Component {

	state = {
		activeStyle: [null, null, null, styles.active, null],
		offset: 0,
		index: 2
	}

	async componentDidMount() {
		// Set up navigation listener to refresh screen every time
		const willFocusNavListener = this.props.navigation.addListener('willFocus', () => {
			this.updateUi();
			console.log('Changed Tab');
		});
	}

	componentWillReceiveProps(props) {
		const oldState = this.props.navigation.state;
	    const oldRoute = oldState.routes[oldState.index];
	    const oldParams = oldRoute.params;
	    const wasVisible = !oldParams || oldParams.visible;

	    const newState = props.navigation.state;
	    const newRoute = newState.routes[newState.index];
	    const newParams = newRoute.params;
	    const isVisible = !newParams || newParams.visible;

	    if (wasVisible && !isVisible) {
	        this.setState({offset: -150});
	    } else if (isVisible && !wasVisible) {
	        this.setState({offset: 0});
	    }

	    // Put in to get rid of disappearing tab bar during demo mode where isVisible ===> undefined
	    if (isVisible == undefined) {
	    	this.setState({offset: 0})
	    }

	    if (wasVisible == undefined && isVisible == false) {
	    	this.setState({offset: -150})
	    }
	    console.log(wasVisible, isVisible);
	}

	componentDidUpdate() {
		// Check whether any navigation was triggered outside of the nav bar (e.g. another button)
	    // consequently update the ui
	    if (this.state.index != this.props.navigation.state.index) {
	    	this.updateUi();
	    	this.setState({index: this.props.navigation.state.index});
    	}
	}

	async changeTab(navTo) {
		// Navigates to new screen
		var demoMode = await Expo.SecureStore.getItemAsync('demo-mode');
		console.log("DemoMode ==", demoMode);
		if (demoMode == 'true') {
			if (navTo != 'ActivityScreen' && navTo != 'MyProfileScreen') {
				await this.props.navigation.navigate(navTo, {demoMode: true});
				this.updateUi();
			}
			else {
				Alert.alert('Hey there!', 'You are currently in demo mode! To access these sections of the app you will need to create an account!');
			}
		}
		else {
			await this.props.navigation.navigate(navTo);
			this.updateUi();
		}
	}

	updateUi() {
		// Changes active tab highlight
		var index = this.props.navigation.state.index;
		console.log(index);

		if (index == 0) {
			this.setState({activeStyle: [styles.active, null, null, null, null]});
		}
		else if (index == 1) {
			this.setState({activeStyle: [null, styles.active, null, null, null]});
		}
		else if (index == 2) {
			this.setState({activeStyle: [null, null, styles.active, null, null]});
		}
		else if (index == 3){
			this.setState({activeStyle: [null, null, null, styles.active, null]});
		}
		else if (index == 4){
			this.setState({activeStyle: [null, null, null, null, styles.active]});
		}
		else {
			this.setState({activeStyle: [null, null, null, styles.active, null]});
		}
	}

	render() {

		return(
			<View style={[styles.container, {bottom: this.state.offset}]}>

				<TouchableOpacity style={[styles.inactive, this.state.activeStyle[0]]}
								  onPress={() => this.changeTab('ActivityScreen')}>
					<Image source={require('../../../assets/icons/png/newspaper.png')}
						   style={styles.icon} />
				    <Text style={styles.label}>Activity Feed</Text>
			    </TouchableOpacity>

			    <TouchableOpacity style={[styles.inactive, this.state.activeStyle[1]]}
								  onPress={() => this.changeTab('FamilyGamesScreen')}>
					<Image source={require('../../../assets/icons/png/gift.png')}
						   style={styles.icon} />
				    <Text style={styles.label}>Received Games</Text>
			    </TouchableOpacity>

			    <TouchableOpacity style={[styles.inactive, this.state.activeStyle[2]]}
								  onPress={() => this.changeTab('CreatedGamesScreen')}>
					<Image source={require('../../../assets/icons/png/brush.png')}
						   style={styles.icon} />
				    <Text style={styles.label}>My Games</Text>
			    </TouchableOpacity>

			    <TouchableOpacity style={[styles.inactive, this.state.activeStyle[3]]}
								  onPress={() => this.changeTab('FamilyAlbumScreen')}>
					<Image source={require('../../../assets/icons/png/image-multiple.png')}
						   style={styles.icon} />
				    <Text style={styles.label}>Family Album</Text>
			    </TouchableOpacity>

			    <TouchableOpacity style={[styles.inactive, this.state.activeStyle[4]]}
								  onPress={() => this.changeTab('MyProfileScreen')}>
					<Image source={require('../../../assets/icons/png/account.png')}
						   style={styles.icon} />
				    <Text style={styles.label}>My Profile</Text>
			    </TouchableOpacity>

			</View>
		);
	}

} 