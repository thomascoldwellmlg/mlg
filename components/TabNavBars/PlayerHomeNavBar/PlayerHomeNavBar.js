import React from 'react';
import {View,
		Image,
		Text,
		TouchableOpacity,
		Animated} from 'react-native';
import styles from './PlayerHomeNavBarStyles';

export default class PlayerHomeNavBar extends React.Component {

	state = {
		activeStyle: [null, null, styles.active, null, null],
		offset: 0
	}

	componentDidMount() {
		// Set up navigation listener to refresh screen every time
		const willFocusNavListener = this.props.navigation.addListener('willFocus', () => {
			this.updateUi();
			console.log('Changed Tab');
		});
	}

	componentWillReceiveProps(props) {
		const oldState = this.props.navigation.state;
	    const oldRoute = oldState.routes[oldState.index];
	    const oldParams = oldRoute.params;
	    const wasVisible = !oldParams || oldParams.visible;

	    const newState = props.navigation.state;
	    const newRoute = newState.routes[newState.index];
	    const newParams = newRoute.params;
	    const isVisible = !newParams || newParams.visible;

	    if (wasVisible && !isVisible) {
	        this.setState({offset: -150});
	    } else if (isVisible && !wasVisible) {
	        this.setState({offset: 0});
	    }
	}

	async changeTab(navTo) {
		// Navigates to new screen
		await this.props.navigation.navigate(navTo);
		this.updateUi();
	}

	updateUi() {
		// Changes active tab highlight
		var index = this.props.navigation.state.index;

		if (index == 0) {
			this.setState({activeStyle: [styles.active, null, null]});
		}
		else if (index == 1) {
			this.setState({activeStyle: [null, styles.active, null]});
		}
		else {
			this.setState({activeStyle: [null, null, styles.active]});
		}
	}

	render() {

		return(
			<View style={[styles.container, {bottom: this.state.offset}]}>

			    <TouchableOpacity style={[styles.inactive, this.state.activeStyle[0]]}
								  onPress={() => this.changeTab('PlayerGamesScreen')}>
				    <Text style={styles.label}>My Games</Text>
			    </TouchableOpacity>

			    <TouchableOpacity style={[styles.inactive, this.state.activeStyle[1]]}
								  onPress={() => this.changeTab('PlayerFamilyAlbumScreen')}>
				    <Text style={styles.label}>Family Album</Text>
			    </TouchableOpacity>

			    <TouchableOpacity style={[styles.inactive, this.state.activeStyle[2]]}
								  onPress={() => this.changeTab('PlayerProfileScreen')}>
				    <Text style={styles.label}>My Profile</Text>
			    </TouchableOpacity>

			</View>
		);
	}

} 