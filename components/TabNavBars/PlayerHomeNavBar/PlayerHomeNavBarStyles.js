import { StyleSheet, Dimensions} from 'react-native';
import { color } from '../../reusable/common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

if (screenHeight < screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const styles = StyleSheet.create({

	container: {
		height: screenHeight * 0.12,
		width: screenWidth,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		backgroundColor: color.darkBlue,
		position: 'absolute',
		bottom: '0%'
	},
	inactive: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.33,
		justifyContent: 'space-around',
		alignItems: 'center',
		padding: screenHeight * 0.01
	},
	active: {
		backgroundColor: color.lightBlue, 
		shadowColor: 'black',
		shadowOpacity: 0.3,
		shadowOffset: { height: 0 }
	},
	icon: {
		height: screenHeight * 0.05,
		width: screenHeight * 0.05,
		resizeMode: 'contain',
		tintColor: '#fff'
	},
	label: {
		fontFamily: 'Futura',
		fontSize: 32,
		color: '#fff',
		textAlign: 'center'
	}

});

export default styles;