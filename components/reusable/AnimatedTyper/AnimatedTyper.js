/*
	Written by Thomas Coldwell

	Props:
	style - the generic style prop
	text - the full string to be typed out
	speed - the update speed between each character getting printed out
	delayStart - to delay the start of the animation by a number of ms
	delayEnd - to delay the end of the aniamtion by a number of ms if loop is enabled
	loop - whether to loop the animation after its first run

*/

import React from 'react';
import { Animated,
		 Text } from 'react-native';


export default class AnimatedTyper extends React.Component {

	state = {
		percentage: new Animated.Value(0.0),
		text: []
	}

	async componentDidMount() {
		await this.buildTextArray();
		this.startAnimation();
	}

	buildTextArray() {
		var text = this.props.text.split("");
		var array = [];
		var input = [];
		for (var i = 1; i <= text.length; i++) {
			array.push(text.slice(0, i).join(""));
			input.push((i - 1)/(text.length));
		}
		this.setState({text: array,
					   inputRange: input});
		console.log(array, input)
	}

	startAnimation() {
		Animated.sequence([
			Animated.timing(
				this.state.percentage,
				{
					toValue: 1.0,
					duration: this.props.speed
				}
			)
		]).start(() => {
			console.log("run")
			this.startAnimation();
		});
	}

	render() {
		const index = this.state.percentage.interpolate({
			inputRange: [0.0, 0.1, 0.2, 1.0],
			outputRange: [0, 1, 2, 4]
		})
		console.log(index)
		return(
			<Animated.Text style={this.props.style}>
				{this.state.text[index]}
			</Animated.Text>
		);
	}

}
