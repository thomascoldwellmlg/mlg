import React from 'react';
import { Text,
		 View,
		 FlatList,
		 TouchableOpacity,
		 Animated,
		 Dimensions } from 'react-native';
import styles from './SeeHowAnimationStyles';
import gstyles from '../GlobalStyles';
import { BlurView } from 'expo';
import AnimatedTyper from '../AnimatedTyper/AnimatedTyper';
import { color } from '../../reusable/common';
const timer = require('react-native-timer');

var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

// Ensure style sheet is given as landscape
if (screenHeight > screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}


export default class SeeHowAnimation extends React.Component {

	state = {
		sectionScrollCards: [
			{
				title: 'Create a\nFamily Group',
				onPress: () => this.goToStep(0),
				section: 'create-family-group'
			},
			{
				title: 'Step 2',
				onPress: () => this.goToStep(1),
				section: 'select-photo'
			},
			{
				title: 'Step 3',
				onPress: () => this.goToStep(2),
				section: 'create-question'
			},
			{
				title: 'Step 4',
				onPress: () => this.goToStep(3),
				section: 'create-family-group'
			},
			{
				title: 'Step 5',
				onPress: () => this.goToStep(4),
				section: 'create-family-group'
			},
			{
				title: 'Step 6',
				onPress: () => this.goToStep(5),
				section: 'create-family-group'
			},
			{
				title: 'Step 7',
				onPress: () => this.goToStep(6),
				section: 'create-family-group'
			},
			{
				title: 'Step 8',
				onPress: () => this.goToStep(7),
				section: 'create-family-group'
			}
		],
		section: 'create-family-group',
		nullAnimation: new Animated.Value(0.0),
		s1SurnameTypingIndex: 0,
		s1SurnameAndJoinOpacity: new Animated.Value(0.0),
		s1JoinButtonOpacity: new Animated.Value(1.0),
		s1SurnameLength: new Animated.Value(screenWidth * 0.2),
		s1SurnameAndJoinYPos: new Animated.Value(screenHeight * 0.375)
	}

	componentDidMount() {
		this.startS1Animations();
	}

	// Functionality

	goToStep(index) {
		console.log(index)
		if (index == 0) {
			this.setState({section: 'create-family-group'});
			this.startS1Animations();
		}
		else {
			this.resetS1Animations();
		}
		if (index == 1) {
			this.setState({section: 'select-photo'});
			this.startS2Animations();
		}
		if (index == 2) {
			this.setState({section: 'select-photo'});
			this.startS2Animations();
		}
		if (index == 3) {
			this.setState({section: 'select-photo'});
			this.startS2Animations();
		}
		if (index == 4) {
			this.setState({section: 'select-photo'});
			this.startS2Animations();
		}
		if (index == 5) {
			this.setState({section: 'select-photo'});
			this.startS2Animations();
		}

	}

	// UI Animations

	startTypingS1Surname() {
		timer.clearInterval(this)
		var index = 0;
		var max = 11;
		timer.setInterval(this, 'uno', () => {
			console.log("running")
			if (this.state.section == 'create-family-group') {
				this.setState({s1SurnameTypingIndex: index});
				index ++;
			}
			else {
				timer.clearInterval(this)
			}
		}, 200);
	}

	startS1Animations() {
		// Reset all animated Values
		this.resetS1Animations();
		// Start Typing animation
		timer.clearTimeout(this)
		timer.setTimeout(this, 'a', () => {
			this.startTypingS1Surname();
		}, 1000)
		// Start main animation loop
		Animated.sequence([
			Animated.delay(1000),
			Animated.delay(2200),
			Animated.timing(
				this.state.s1JoinButtonOpacity,
				{
					toValue: 0.0,
					duration: 1000
				}
			),
			Animated.parallel([
				Animated.timing(
					this.state.s1SurnameLength,
					{
						toValue: screenWidth * 0.3,
						duration: 1000
					}
				),
				Animated.timing(
					this.state.s1SurnameAndJoinOpacity,
					{
						toValue: 1.0,
						duration: 500,
						delay: 500
					}
				)
			]),
			Animated.delay(300),
			Animated.timing(
				this.state.s1SurnameAndJoinYPos,
				{
					toValue: screenHeight * 0.1,
					duration: 2000
				}
			)
		]).start(() => {
			// Callback to check to see if we should continue looping the animation or to let it stop
			if (this.state.section == 'create-family-group') {
				this.startS1Animations();
			}
		});
	}

	startS2Animations() {

	}

	// Animation Resets

	resetS1Animations() {
		timer.clearInterval(this)
		this.setState({s1SurnameTypingIndex: 0})
		this.state.s1SurnameAndJoinOpacity.setValue(0.0);
		this.state.s1JoinButtonOpacity.setValue(1.0);
		this.state.s1SurnameLength.setValue(screenWidth * 0.2);
		this.state.s1SurnameAndJoinYPos.setValue(screenHeight * 0.375);
	}
	
	// UI Components

	uiCreateFamilyGroup() {
		const familyId = 'Coldwell72'
		if (this.state.section == 'create-family-group') {
			return(
				<Animated.View style={[styles.s1SurnameAndJoin, 
									   {backgroundColor: this.state.s1SurnameAndJoinOpacity.interpolate({inputRange: [0.0, 1.0],
																										 outputRange: [color.darkBlue + '00', color.darkBlue + 'FF']})},
									   {marginTop: this.state.s1SurnameAndJoinYPos}
									   ]}>
					<Animated.View style={[styles.s1Surname, {width: this.state.s1SurnameLength}]}>
						<Text style={gstyles.smallText}>{familyId.split("").slice(0, this.state.s1SurnameTypingIndex).join("")}</Text>
					</Animated.View>
					<Animated.View style={[styles.s1JoinButton, {opacity: this.state.s1JoinButtonOpacity}]}>
						<Text style={gstyles.smallText}>Join!</Text>
					</Animated.View>
				</Animated.View>
			);
		}
		else {
			return(<View />);
		}
	}

	uiSectionCard(item) {
		if (item.section == this.state.section) {
			return(
				<TouchableOpacity style={[gstyles.shadow, styles.sectionScrollCardCurrent]}
								  onPress={item.onPress}>
					<Text style={[gstyles.smallText, {textAlign: 'center'}]}>{item.title}</Text>
				</TouchableOpacity>
			);
		}
		else {
			return(
				<TouchableOpacity style={[gstyles.shadow, styles.sectionScrollCard]}
								  onPress={item.onPress}>
					<Text style={[gstyles.smallText, {textAlign: 'center'}]}>{item.title}</Text>
				</TouchableOpacity>
			);
		}
	}

	render() {
		return(
			<View style={styles.container}>
				<View style={styles.animationContainer}>
					{this.uiCreateFamilyGroup()}
				</View>
				<BlurView style={styles.sectionScrollBarContainer} intensity={90} tint='dark'>
					<FlatList style={styles.sectionScroll}
							  horizontal={true}
							  data={this.state.sectionScrollCards}
							  keyExtractor={(item, index) => index.toString()}
	                          renderItem={({item}) => this.uiSectionCard(item)}/>
				</BlurView>
			</View>
		);
	}
}