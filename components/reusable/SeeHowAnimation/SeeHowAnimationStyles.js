import React from 'react';
import {StyleSheet, 
        Dimensions} from 'react-native';
import { color } from '../../reusable/common';

var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

// Ensure style sheet is given as landscape
if (screenHeight > screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const styles = StyleSheet.create({

	/*
		Contents:
			0. General
			1. Step 1 (s1)
	*/


	// 0. General
	container: {
		height: screenHeight * 0.75,
		width: screenWidth * 0.75,
		justifyContent: 'flex-start',
		alignItems: 'center',
		backgroundColor: color.lightGrey
	},
	animationContainer: {
		height: screenHeight * 0.6,
		width: screenWidth * 0.75,
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	sectionScrollBarContainer: {
		height: screenHeight * 0.15,
		width: screenWidth * 0.75,
		alignItems: 'center',
		backgroundColor: '#000000'
	},
	sectionScroll: {
		height: screenHeight * 0.15,
		width: screenWidth * 0.75,
		paddingHorizontal: screenWidth * 0.03
	},
	sectionScrollCard: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.2,
		backgroundColor: color.grey,
		alignSelf: 'center',
		justifyContent: 'center',
		alignItems: 'center',
		marginRight: screenWidth * 0.03,
		borderRadius: screenWidth * 0.01
	},
	sectionScrollCardCurrent: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.2,
		backgroundColor: color.darkBlue,
		alignSelf: 'center',
		justifyContent: 'center',
		alignItems: 'center',
		marginRight: screenWidth * 0.03,
		borderRadius: screenWidth * 0.01
	},

	// 1. Step 1 (s1)
	s1SurnameAndJoin: {
		height: screenHeight * 0.1,
		width: screenWidth * 0.3,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		backgroundColor: color.darkBlue,
		borderRadius: screenWidth * 0.01
	},
	s1Surname: {
		height: screenHeight * 0.08,
		width: screenWidth * 0.2,
		justifyContent: 'center',
		alignItems: 'center',
		// backgroundColor: '#ccc'
	},
	s1JoinButton: {
		height: screenHeight * 0.08,
		width: screenWidth * 0.1,
		backgroundColor: color.green,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: screenWidth * 0.01
	}

});

export default styles;