import React from 'react';
import { StyleSheet,
		 View,
		 FlatList,
		 Text,
		 Image,
		 TouchableOpacity,
		 Dimensions } from 'react-native';
import { BlurView } from 'expo';
import gstyles from '../GlobalStyles';
import { Image as CacheableImage } from 'react-native-aws-cache';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

if (screenHeight > screenWidth) {
    var temp = screenHeight;
    screenHeight = screenWidth;
    screenWidth = temp;
}

export default class EasySlideshow extends React.Component {

	state = {
		captionTitle: this.props.data[0].title,
		captionSubTitle: this.props.data[0].caption,
		url: this.props.data[0].url,
		index: 0
	}

	slideLeft() {
		var data = this.props.data;
		var index = this.state.index;
		if (this.state.index != 0) {
			this.setState({
				captionTitle: data[index - 1].title,
				captionSubTitle: data[index - 1].caption,
				url: data[index - 1].url,
				index: (index - 1)
			});
		}
	}

	slideRight() {
		var data = this.props.data;
		var index = this.state.index;
		if (this.state.index != this.props.data.length - 1) {
			this.setState({
				captionTitle: data[index + 1].title,
				captionSubTitle: data[index + 1].caption,
				url: data[index + 1].url,
				index: (index + 1)
			});
		}
	}

	uiCurrentImage() {
		if (this.props.demoMode != true) {
			return(
				<CacheableImage style={styles.image} source={{uri: this.state.url}} />
			);
		}
		else {
			return(
				<Image style={styles.image} source={{uri: this.state.url}} />
			);
		}
	}

	render() {
		return(
			<View style={styles.container}>
				{this.uiCurrentImage()}
				<TouchableOpacity onPress={() => this.slideLeft()}
							      style={styles.slideLeftButton}>
			        <Image style={styles.slideArrowIcon} source={require('../../../assets/icons/png/chevron-left.png')} />
		        </TouchableOpacity>
		        <TouchableOpacity onPress={() => this.slideRight()}
							      style={styles.slideRightButton}>
			        <Image style={styles.slideArrowIcon} source={require('../../../assets/icons/png/chevron-right.png')} />
		        </TouchableOpacity>
		        <BlurView style={styles.captionBlur} tint='dark' intensity={70}>
		        	<Text style={[gstyles.smallText, styles.captionTitle]}>{this.state.captionTitle}</Text>
		        	<Text style={[gstyles.subTitle, styles.captionSubTitle]}>{this.state.captionSubTitle}</Text>
		        </BlurView>
		        <BlurView style={styles.indexBlur} tint='dark' intensity={90}>
		        	<Text style={[gstyles.smallText, styles.captionTitle]}>{(this.state.index + 1).toString()}</Text>
		        </BlurView>
			</View>
		);
	}
}



const styles = StyleSheet.create({
	container: {
		height: screenHeight * 0.336,
		width: screenWidth * 0.42,
		backgroundColor: '#000000'
	},
	flatlist: {

	},
	image: {
		height: screenHeight * 0.336,
		width: screenWidth * 0.42
	},
	slideLeftButton: {
		position: 'absolute',
		left: '3%',
		top: '40%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	slideRightButton: {
		position: 'absolute',
		right: '3%',
		top: '40%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	slideArrowIcon: {
		tintColor: '#fff'
	},
	captionBlur: {
		position: 'absolute',
		bottom: '0%',
		height: screenHeight * 0.08,
		width: screenWidth * 0.42,
		backgroundColor: '#000000',
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		paddingLeft: screenWidth * 0.02,
		paddingVertical: screenHeight * 0.01
	},
	captionTitle: {
		fontSize: screenHeight * 0.023
	},
	captionSubTitle: {
		fontSize: screenHeight * 0.02
	},
	indexBlur: {
		position: 'absolute',
		top: '0%',
		height: screenHeight * 0.06,
		width: screenHeight * 0.06,
		backgroundColor: '#000000',
		justifyContent: 'center',
		alignItems: 'center',
		borderBottomRightRadius: screenWidth * 0.01
	},
});