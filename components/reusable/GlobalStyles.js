import React from 'react';
import {StyleSheet, 
        Dimensions} from 'react-native';
import { color } from './common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

// Alwyas set front size to a constant of the device - in this case the longest dimension always
var fontRef = Dimensions.get('window').height;
if (screenWidth > fontRef) {
    fontRef = screenWidth;
}

if (screenHeight < screenWidth) {
    var temp = screenHeight;
    screenHeight = screenWidth;
    screenWidth = temp;
}

const gstyles = StyleSheet.create({
    
    largeText: {
    	fontFamily: 'Futura',
    	fontSize: fontRef * 0.04,
    	color: '#fff'
    },
    mediumText: {
    	fontFamily: 'Futura',
    	fontSize: fontRef * 0.03,
    	color: '#fff'
    },
    smallText: {
    	fontFamily: 'Futura',
    	fontSize: fontRef * 0.022,
    	color: '#fff'
    },
    subTitle: {
        fontFamily: 'Futura',
        fontSize: fontRef * 0.02,
        color: color.lightGrey
    },

    largeIcon: {
        height: 72,
        width: 72,
        tintColor: '#fff'
    },
    mediumIcon: {
        height: 64,
        width: 64,
        tintColor: '#fff'
    },
    smallIcon: {
        height: 48,
        width: 48,
        tintColor: '#fff'
    },

    bold: {
        fontWeight: 'bold'
    },

    greenFab: {
        height: 120,
        width: 120,
        borderRadius: 60,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: color.green,
        position: 'absolute',
        end: '2%',
        bottom: '2%',
        shadowOffset: { height: 3 },
        shadowColor: 'black',
        shadowOpacity: 0.3,
    },

    cardColumn: {
        height: screenHeight * 0.15,
        width: screenWidth * 0.9,
        backgroundColor: color.cardBlue,
        justifyContent: 'space-around',
        alignItems: 'center',
        padding: 20,
        borderRadius: 20,
        shadowOffset: { height: 3 },
        shadowColor: 'black',
        shadowOpacity: 0.3,
        marginBottom: screenHeight * 0.03
    },
    cardRow: {
        height: screenHeight * 0.15,
        width: screenWidth * 0.9,
        backgroundColor: color.cardBlue,
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: screenWidth * 0.05,
        paddingRight: screenWidth * 0.05,
        borderRadius: 20,
        shadowOffset: { height: 3 },
        shadowColor: 'black',
        shadowOpacity: 0.3,
        marginBottom: screenHeight * 0.03
    },

    profilePicturePortrait: {
        height: screenHeight * 0.09,
        width: screenHeight * 0.09,
        borderRadius: screenHeight * 0.045,
        shadowOffset: { height: 10 },
        shadowColor: 'black',
        shadowOpacity: 0.3
    },

    logoPortrait: {
        height: 96,
        width: 96
    },

    shadow: {
        shadowOffset: { height: 3 },
        shadowColor: 'black',
        shadowOpacity: 0.4,
    }
    
});

export default gstyles;