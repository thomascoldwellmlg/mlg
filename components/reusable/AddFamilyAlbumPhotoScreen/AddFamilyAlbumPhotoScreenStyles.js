import { StyleSheet, Dimensions} from 'react-native';
import { color } from '../common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({

	/*
		Contents:
			0. General
			1. Title Bar
			2. Body
	*/

	// 0. General
	container: {
		height: screenHeight,
		width: screenWidth,
		backgroundColor: color.lightBlue,
		justifyContent: 'flex-start',
		alignItems: 'center'
	},

	// 1. Title Bar
	titleBar: {
		height: screenHeight * 0.15,
		width: screenWidth,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		backgroundColor: color.darkBlue,
		paddingLeft: screenWidth * 0.05,
		paddingRight: screenWidth * 0.05,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3
	},
	logo: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		resizeMode: 'contain'
	},
	titleBarIconButton: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	titleBarIcon: {
		height: screenHeight * 0.06,
		width: screenHeight * 0.06,
		resizeMode: 'contain',
		tintColor: '#fff'
	},

	// 2. Body
	keyboardScrollView: {
		flex: 1,
		width: screenWidth,
		alignItems: 'center',
		paddingTop: screenHeight * 0.03
	},
	image: {
		height: screenHeight * 0.35,
		width: screenWidth * 0.65,
		alignSelf: 'center',
		marginBottom: screenHeight * 0.03,
		shadowOffset: { height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.3,
		borderRadius: 10
	},
	mainDetailsCard: {
		height: screenHeight * 0.45,
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
		paddingLeft: screenWidth * 0.05
	},
	mainDetailsInputs: {
		height: screenHeight * 0.35,
		width: screenWidth * 0.8,
		marginLeft: screenWidth * 0.03,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginTop: screenHeight * 0.02
	},
	textInputContainer: {
		height: screenHeight * 0.07,
		width: screenWidth * 0.7,
		justifyContent: 'space-around',
		alignItems: 'flex-start',
		marginBottom: screenHeight * 0.01
	},
	textInput: {
		fontFamily: 'Futura',
		fontSize: 20,
		color: '#fff',
		height: screenHeight * 0.05,
		width: screenWidth * 0.7,
		borderBottomWidth: 3,
		borderColor: color.lightBlue
	},
	faceDetectionInputs: {
		width: screenWidth * 0.5
	},
	faceDetectionRow: {
		flexDirection: 'row',
		height: screenHeight * 0.12,
	},
	profilePictures: {
		marginRight: screenWidth * 0.03
	},

	blur: {
		height: screenHeight,
		width: screenWidth,
		paddingVertical: screenHeight * 0.3,
		position: 'absolute',
		backgroundColor: '#000',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	uploadingLogo: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1
	},
	loadingGif: {
		height: screenHeight * 0.1,
		width: screenHeight * 0.1
	}

});

export default styles;