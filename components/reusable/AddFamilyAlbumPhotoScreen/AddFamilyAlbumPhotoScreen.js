import React from 'react';
import { Text,
		 View,
		 ScrollView,
		 FlatList,
		 Image,
		 TouchableOpacity,
		 Dimensions,
		 TextInput,
		 Alert,
		 ActionSheetIOS,
		 ActivityIndicator,
		 KeyboardAvoidingView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './AddFamilyAlbumPhotoScreenStyles';
import gstyles from '../GlobalStyles';
import { UserData,
		 UserId,
		 MemberData,
		 FamilyData,
		 Append } from '../../../lib/Firebase';
import { color, months } from '../common';
import { Location, FaceDetector, ImageManipulator, BlurView, FileSystem} from 'expo';
import { UploadImage } from '../../../lib/AWS';
import { sendAddFamilyPhotoNotification } from '../../../lib/Notifications';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

const DEMO_DIR = `${FileSystem.documentDirectory}mlgDemoModeAssets/`;

export default class AddFamilyAlbumPhotoScreen extends React.Component {

	state = {
		imageData: {},
		name: '',
		description: '',
		location: '',
		dateTaken: '',
		faceDetectionResults: '',
		isUploading: false,
		isFaceDetecting: true,
		demoMode: null
	}

	_scrollToInput (reactNode: any) {
	  	// Add a 'scroll' ref to your ScrollView
	 	this.scroll.scrollToFocusedInput(reactNode);
	}

	async componentDidMount() {
		// Check if we are in demo mode
		var demoMode = await Expo.SecureStore.getItemAsync('demo-mode');
		if (demoMode == 'true') {
			console.log('demoMode ==== > ', demoMode)
			this.setState({demoMode: true});
		}
		this.getCurrentPhotoData();
		// Set up navigation listener to refresh screen every time
		const willFocusNavListener = this.props.navigation.addListener('willFocus', () => {
			console.log('Navigated to Add Family Photo Screen!');
			this.getCurrentPhotoData();
		});
	}

	async getCurrentPhotoData() {
		this.setState({name: '',
    			       description: '',
    			       location: '',
    			       dateTaken: ''});
        for (var n = 0; n < 10; n++) {
        	this.setState({['faceLabel_' + n.toString()]: ''});
        }
		// Get the passed image data and set it as a state to be accessed easily
		this.setState({imageData: this.props.navigation.state.params.data});
		// Now clean up an extract EXIF data of image to get date taken and location (city-wise)
		console.log(this.state.imageData)
		if (this.state.imageData.exif != undefined) {
			if (this.state.imageData.exif.GPSLatitude != undefined) {
				var location = await Location.reverseGeocodeAsync({latitude: this.state.imageData.exif.GPSLatitude,
																   longitude: this.state.imageData.exif.GPSLongitude});
				console.log(location)
				if (location.city != undefined && location.country != undefined) {
					var locationString = location.city + ' ' + location.country;
					await this.setState({location: locationString});
				}
			}
			// Get date from exif
			if (this.state.imageData.exif.DateTimeDigitized != undefined) {
				var date = this.state.imageData.exif.DateTimeDigitized.split(' ')[0].split(':');
				// Clean into standard string
				var month = months[date[1]];
				var day = date[2];
				var suffix = 'th';
				if (day.split('')[1] == '1') {
					suffix = 'st';
				}
				else if (day.split('')[1] == '2') {
					suffix = 'nd';
				}
				else if (day.split('')[1] == '3') {
					suffix = 'rd';
				}
				if (day.split('')[0] == '0') {
					day = day.split('')[1];
				}
				var displayDate = day + suffix + ' ' + month + ' ' + date[0];
				console.log(displayDate);
		 		this.setState({dateTaken: displayDate});
	 		}
 		}
 		// Finally run face detection on the image
 		this.runFaceDetection(this.state.imageData.uri);
	}

	async runFaceDetection(imageUri) {
    	// Method to perform face detection and finally sets the state of it's results
    	// to the faceDetectionResults state
    	const faceDetectorOptions = { mode: FaceDetector.Constants.Mode.fast };
    	console.log('Is detecting faces');
    	var detectionResults = await FaceDetector.detectFacesAsync(imageUri, faceDetectorOptions);
    	console.log('finished detecting faces', detectionResults);
    	// Now we need to extract an individual image (uri) for each face detected
    	var faces = [];
    	for (var i = 0; i <= detectionResults.faces.length - 1; i ++) {
    		var faceBB = detectionResults.faces[i].bounds;
    		var boundingBoxedCroppedImage = await ImageManipulator.manipulate(
    			detectionResults.image.uri,
    			[{crop: {originX: faceBB.origin.x, 
    					 originY: faceBB.origin.y, 
    					 width: faceBB.size.width,
    					 height: faceBB.size.height}}]
			);
			faces.push(Object.assign({faceUri: boundingBoxedCroppedImage.uri}, detectionResults.faces[i]));
    		this.setState({['faceLabel_' + i.toString()]: ''});
    	}
    	console.log(' FACES ---->>>>>', faces)
    	var faceDetectionResults = {faces: faces, image: detectionResults.image};
    	await this.setState({faceDetectionResults: faceDetectionResults});
    	// Pass flag to enable save function after finishing face detection
    	this.setState({isFaceDetecting: false});
    }

    async saveToFamilyAlbum() {
		// Check whether we are in demo mode, if not upload via AWS else save data to local doc dir and append demoModeAssets.json
		if (this.state.demoMode != true) {
			if (this.state.isFaceDetecting == false) {
	    		// Set up the timeout function
	    		var uploadFailed = true;
	    		setTimeout(() => {
	    			if (uploadFailed == true) {
		    			Alert.alert('Image could not be uploaded. Please check your internet connection');
		    			this.setState({isUploading: false, isFaceDetecting: true});
			        	this.props.navigation.navigate('FamilyAlbumScreen');
		        	}
	    		}, 12000);
		    	// Tell the whole compoenent we are uploading
		    	this.setState({isUploading: true});
		    	// Firstly upload the main image and the cropped face detction images to AWS
		    	const uniquePhotoId = Date.now().toString();
		    	var familyId = await UserData();
		    	familyId = familyId.familyId;
		    	const awsPath = 'families/' + familyId + '/familyAlbum/' + uniquePhotoId + '/';
		    	var mainImageUrl = await UploadImage(this.state.imageData.uri,
		    										 'main',
		    										 awsPath);
		    	var faceImageUrls = [];
		    	var faces = this.state.faceDetectionResults.faces;
		    	for (var i = 0; i <= faces.length - 1; i++) {
		    		var faceUrl = await UploadImage(this.state.faceDetectionResults.faces[i].faceUri,
		    										i.toString(),
		    										awsPath);
		    		faceImageUrls.push(faceUrl);
		    	}
		    	// Now we have all the image Urls we need to update the data to the Firebade database
		    	const firebasePath = 'families/' + familyId + '/familyAlbum/' + uniquePhotoId + '/';
		    	// Form an object containing all of the data on each face in the image
		        var faceDataForFirebase = {};
		        for (var j = 0; j <= faceImageUrls.length - 1; j ++) {
		        	faceDataForFirebase[j.toString()] = {
		        		name: this.state['faceLabel_' + j.toString()],
		        		boundingBoxData: faces[j].bounds,
		        		faceImageUrl: faceImageUrls[j]
		        	};
		        }
		        var userId = await UserId();
		        var firebaseData = {mainImageUrl: mainImageUrl,
		        					uploadedBy: userId,
		        					title: this.state.name,
		        					description: this.state.description,
		        				    location: this.state.location,
		        				    dateTaken: this.state.dateTaken,
		        				    faces: faceDataForFirebase}
		        await Append(firebaseData, firebasePath);
		        // Finally navigate back to the family album and send a notification
		        await sendAddFamilyPhotoNotification(uniquePhotoId);
		        uploadFailed = false;
		        this.setState({isUploading: false, isFaceDetecting: true});
		        this.props.navigation.navigate('FamilyAlbumScreen');
	        }
        }
        else {
        	if (this.state.isFaceDetecting == false) {
	        	var demoData = await FileSystem.readAsStringAsync(DEMO_DIR + 'manager.json');
        		demoData = JSON.parse(demoData);
	    		// Set up the timeout function
	    		var uploadFailed = true;
	    		setTimeout(() => {
	    			if (uploadFailed == true) {
		    			Alert.alert('Image could not be uploaded. Please check your internet connection');
		    			this.setState({isUploading: false, isFaceDetecting: true, addImageStage: 1});
	    			}
	    		}, 12000);
		    	// Tell the whole compoenent we are uploading
		    	this.setState({isUploading: true});
		    	// Firstly upload the main image and the cropped face detction images to AWS
		    	const uniquePhotoId = Date.now().toString();
		    	var familyId = 'Miller';
		    	await FileSystem.copyAsync({
		    		from: this.state.imageData.uri, 
		    		to: DEMO_DIR + uniquePhotoId + '.jpg'
		    	})
		    	var mainImageUrl = DEMO_DIR + uniquePhotoId + '.jpg'
		    	var faceImageUrls = [];
		    	var faces = this.state.faceDetectionResults.faces;
		    	for (var i = 0; i <= faces.length - 1; i++) {
		    		await FileSystem.copyAsync({
			    		from: this.state.faceDetectionResults.faces[i].faceUri, 
			    		to: DEMO_DIR + uniquePhotoId + '_' + i.toString() + '.jpg'
			    	})
		    		var faceUrl = DEMO_DIR + uniquePhotoId + '_' + i.toString() + '.jpg';
		    		faceImageUrls.push(faceUrl);
		    	}
		    	// Now we have all the image Urls we need to update the data to the Firebade database
		    	// Form an object containing all of the data on each face in the image
		        var faceDataForFirebase = [];
		        for (var j = 0; j <= faceImageUrls.length - 1; j ++) {
		        	faceDataForFirebase.push({
		        		name: this.state['faceLabel_' + j.toString()],
		        		boundingBoxData: faces[j].bounds,
		        		faceImageUrl: faceImageUrls[j]
		        	});
		        }
		        var userId = 'Hr3ApcPo2UWl1jcPjFPpozDj9fr2';
		        var firebaseData = {mainImageUrl: mainImageUrl,
		        					uploadedBy: userId,
		        					title: this.state.name,
		        					description: this.state.description,
		        				    location: this.state.location,
		        				    dateTaken: this.state.dateTaken,
		        				    faces: faceDataForFirebase}
		        demoData.families.Miller.familyAlbum[uniquePhotoId] = firebaseData;
		        await FileSystem.writeAsStringAsync(
		        	DEMO_DIR + 'manager.json',
		        	JSON.stringify(demoData)
	        	);
	        	uploadFailed = false;
	        	// Edit JSON file for demo mode assets to reflect the change
	        	this.setState({isUploading: false, isFaceDetecting: true});
		        this.props.navigation.navigate('FamilyAlbumScreen');
	        }
        }
    }

	uiTitleBar() {
		return(
			<View style={styles.titleBar}>
				<View style={gstyles.shadow}>
					<Image style={styles.logo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
				</View>
				<Text style={gstyles.largeText}>Add Family Photo</Text>
				<TouchableOpacity style={styles.titleBarIconButton}
								  onPress={() => this.props.navigation.navigate('FamilyAlbumScreen')}>
					<Image source={require('../../../assets/icons/png/close.png')}
						   style={styles.titleBarIcon} />
			    </TouchableOpacity>
			</View>
		);
	}

	uiBody() {
		return(
			<KeyboardAwareScrollView extraHeight={150}>
			<View style={styles.keyboardScrollView}>
				<View style={gstyles.shadow}>
                	<Image style={styles.image} source={{uri: this.state.imageData.uri}} />
                </View>
                <View style={[gstyles.cardColumn, styles.mainDetailsCard]}>
                	<Text style={gstyles.smallText}>Main Photo Details</Text>
                	<View style={styles.mainDetailsInputs}>
                		<TouchableOpacity style={styles.textInputContainer}
                		                  onPress={() => this.refs.nameInput.focus()}>
                			<Text style={gstyles.subTitle}>Name</Text>
                			<TextInput style={styles.textInput}
                					   ref='nameInput'
                					   placeholder='Enter a name for your photo'
                					   value={this.state.name}
                					   onChangeText={(name) => this.setState({name})} />
                		</TouchableOpacity>
                		{/*<TouchableOpacity style={styles.textInputContainer}
                		                  onPress={() => this.refs.descriptionInput.focus()}>
                			<Text style={gstyles.subTitle}>Description</Text>
                			<TextInput style={styles.textInput}
                					   ref='descriptionInput'
                					   placeholder='Enter a short description'
                					   value={this.state.description}
                					   onChangeText={(description) => this.setState({description})} />
                		</TouchableOpacity>*/}
                		<TouchableOpacity style={styles.textInputContainer}
                		                  onPress={() => this.refs.locationInput.focus()}>
                			<Text style={gstyles.subTitle}>Location</Text>
                			<TextInput style={styles.textInput}
                					   ref='locationInput'
                					   placeholder='Enter the photo location'
                					   value={this.state.location}
                					   onChangeText={(location) => this.setState({location})} />
                		</TouchableOpacity>
                		<TouchableOpacity style={styles.textInputContainer}
                		                  onPress={() => this.refs.dateInput.focus()}>
                			<Text style={gstyles.subTitle}>Date Taken</Text>
                			<TextInput style={styles.textInput}
                					   ref='dateInput'
                					   placeholder='Enter the date taken'
                					   value={this.state.dateTaken}
                					   onChangeText={(dateTaken) => this.setState({dateTaken})} />
                		</TouchableOpacity>
                	</View>
                </View>
                {this.uiFaceDetectionCard()}
            </View>
            </KeyboardAwareScrollView>
		);
	}

	uiFaceDetectionCard() {
		if (this.state.faceDetectionResults != '') {
			return(
				<View style={[gstyles.cardColumn, styles.mainDetailsCard, {height: 100 + (this.state.faceDetectionResults.faces.length * 115)}]}>
            		<Text style={gstyles.smallText}>Face Detection</Text>
            		<View style={[styles.mainDetailsInputs, {flex: 1}]}>
	            		<FlatList data={this.state.faceDetectionResults.faces}
						          keyExtractor={(item, index) => index.toString()}
						          scrollEnabled={false}
	                              renderItem={(item) => <View style={styles.faceDetectionRow}>
	                              							<View style={gstyles.shadow}>
	                              								<Image style={[gstyles.profilePicturePortrait, styles.profilePictures]} source={{uri: item.item.faceUri}} />
	                              							</View>
	                              							<TouchableOpacity style={styles.textInputContainer}
	                              											  onPress={() => this.itemRefs['faceInput' + item.index.toString()].focus()}>
									                		   <Text style={gstyles.subTitle}>Who is this?</Text>
									                		   <TextInput style={[styles.textInput, styles.faceDetectionInputs]}
									                		   		      ref={(ref) => this.itemRefs = {...this.itemRefs, ['faceInput' + item.index.toString()]: ref}}
									                					  placeholder='Their name is...'
									                					  value={this.state['faceLabel_' + item.index.toString()]}
				                                 					      onChangeText={(text) => this.setState({['faceLabel_' + item.index.toString()]: text})} />
									                	    </TouchableOpacity>
								                	    </View>}/>
            	    </View>
            	</View>
			);
		}
		else {
			return(null);
		}
	}

	uiSaveFAB() {
		return(
			<TouchableOpacity style={gstyles.greenFab}
							  onPress={() => this.saveToFamilyAlbum()}>
			    <Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/check.png')} />
			</TouchableOpacity>
		);
	}

	uiUploadingBlur() {
		if (this.state.isUploading == true) {
			return(
				<BlurView tint="dark" intensity={90} style={styles.blur}>
					<Image style={styles.uploadingLogo} source={require('../../../assets/images/Memory-Lane-Games.png')} />
					<Text style={[{textAlign: 'center'}, gstyles.mediumText]}>Saving to your secure Family Cloud...</Text>
					<ActivityIndicator size="large" color='#3498db' />
				</BlurView>
			);
		}
		else {
			return(null);
		}
	}

	render() {
		return(
			<View style={styles.container}>
				{this.uiTitleBar()}
				{this.uiBody()}
				{this.uiSaveFAB()}
				{this.uiUploadingBlur()}
			</View>
		);
	}

}