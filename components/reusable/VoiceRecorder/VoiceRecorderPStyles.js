import { Dimensions, StyleSheet } from 'react-native';
import { color } from '../common';

var screenHeight = Dimensions.get('window').height;
var screenWidth = Dimensions.get('window').width;

if (screenHeight < screenWidth) {
	var temp = screenHeight;
	screenHeight = screenWidth;
	screenWidth = temp;
}

const pstyles = StyleSheet.create({

	container: {
		height: screenHeight,
		width: screenWidth,
		position: 'absolute',
		backgroundColor: '#000',
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingTop: screenHeight * 0.1,
		paddingBottom: screenHeight * 0.1
	},
	closeIcon: {
		position: 'absolute',
		right: '5%',
		top: '5%'
	},
	durationCounter: {

	},
	controlRow: {
		height: screenHeight * 0.2,
		width: screenWidth * 0.9,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	button: {
		height: screenHeight * 0.18,
		width: screenWidth * 0.35,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: color.green,
		borderRadius: screenWidth * 0.01
	},
	subButton: {
		height: screenHeight * 0.12,
		width: screenWidth * 0.2,
		backgroundColor: color.cardBlue
	}

});

export default pstyles;