import React from 'react';
import { View,
         Text,
         Image,
         TouchableOpacity } from 'react-native';
import { Audio, BlurView } from 'expo';
import lstyles from './VoiceRecorderStyles';
import pstyles from './VoiceRecorderPStyles';
import gstyles from '../GlobalStyles';
import { color } from '../common';

export default class VoiceRecorder extends React.Component {

	constructor(props) {
        super(props);
        this.recording = null;
    }

    componentDidMount() {
        // Get the correct styles ===>>>
        if (this.props.recordingType == 'voice-message') {
            this.setState({styles: pstyles});
        }
        else {
            this.setState({styles: lstyles});
        }
    	this.setState({stage: this.props.parent.state.voiceRecorderStage});
    	// Check if the recording is already present in the parent
    	var currentQuestion = this.props.parent.state.currentQuestion;
        if (this.props.recordingType == 'add-question-hint' || this.props.recordingType == 'add-additional') {
        	if (this.props.recordingType == 'add-question-hint') {
        		var section = 'questionHint';
        	}
        	else if (this.props.recordingType == 'add-additional') {
        		var section = 'additionalQuestion'
        	}
        	if (this.props.parent.state.gameData[currentQuestion][section] != '' && 
        		this.props.parent.state.gameData[currentQuestion][section] != undefined &&
        		this.props.parent.state.gameData[currentQuestion][section] != null) {
        		// Set the URI from the gameData to the current recording's URI
        		this.recording = {_uri: ''};
        		this.recording._uri = this.props.parent.state.gameData[currentQuestion][section];
        		this.setState({isDoneRecording: true});
        	}
        }
        if (this.props.recordingType == 'additional-question' && this.props.parent.state.additionalQuestionReply != '') {
            this.recording = {_uri: ''};
            this.recording._uri = this.props.parent.state.additionalQuestionReply;
            this.setState({isDoneRecording: true});
            this.setState({stage: 'complete'});
        }
        if (this.props.recordingType == 'complete' && this.props.parent.state.replyMessage != '') {
            this.recording = {_uri: ''};
            this.recording._uri = this.props.parent.state.replyMessage;
            this.setState({isDoneRecording: true});
            this.setState({stage: 'complete'});
        }
    }

    state = {
    	recordingDuration: 0,
        recording: null,
        isRecording: false,
        isDoneRecording: false,
        stage: 'start',
        styles: ''
    }

    async onPressRecord() {
        this.setState({stage: 'recording'});
        await Audio.setAudioModeAsync({
            allowsRecordingIOS: true,
            interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
            playsInSilentModeIOS: true,
            shouldDuckAndroid: true,
            interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
        });
        if (this.recording !== null) {
            this.recording.setOnRecordingStatusUpdate(null);
            this.recording = null;
        }
        const recording = new Audio.Recording();
        await recording.prepareToRecordAsync(JSON.parse(JSON.stringify(Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY)));
        recording.setOnRecordingStatusUpdate(this.updateRecordingUI);
        this.recording = recording;
        await this.recording.startAsync(); // Will call this._updateScreenForRecordingStatus to update the screen.
    }

    // Method called every time the recording object updates in some way 'status' is simply a variable for this
    // object and through it all other objects such as isRecording can be accessed
    updateRecordingUI = status => {
        // Convert from millis to seconds
        var durationSeconds = Math.round(status.durationMillis / 1000);
        console.log('Updating UI', status);
        // Update the counter with the lastest duration value
        this.setState({recordingDuration: durationSeconds});
        this.setState({isRecording: status.isRecording,
                       isDoneRecording: status.isDoneRecording});
        // Defines a maximum recording duration in seconds
        var maxRecordingDurationSeconds = 30;
        if (durationSeconds >= maxRecordingDurationSeconds && status.isRecording == true) {
            this.recording.stopAndUnloadAsync();
        }
    }

    async onPressFinishRecording() {
        this.setState({stage: 'complete'});
        if (this.state.isRecording == true) {
            // Stop the recording
            await this.recording.stopAndUnloadAsync();
            console.log(this.recording);
        }
    }

    async onPressPlaybackRecording() {
    	console.log(this.recording)
        if (this.state.isDoneRecording == true && this.recording != null) {
            // Get the URI of the newly recorded clip
            var recordingURI = this.recording._uri;
            console.log('uri - ', recordingURI);
            // Creates a new Expo audio sound object for us to play back and eventually save as a file
            // and upload
            const finalRecordedMessage = new Audio.Sound();
            await finalRecordedMessage.loadAsync({ uri: recordingURI });
            finalRecordedMessage.playAsync();
        }
    }

    async onPressClearRecording() {
        // Resets ther recording object back to null and the counter back to 0
        if (this.state.isRecording != true) {
            this.recording = null;
            this.setState({recordingDuration: 0});
        }
        this.setState({stage: 'start'});
        // Pass the recording back to the parent component
        if (this.props.recordingType == 'add-question-hint') {
        	this.props.parent.updateGameState('questionHint', null);
        	this.props.parent.checkQuestionHintPresent();
        }
        if (this.props.recordingType == 'add-additional') {
        	this.props.parent.updateGameState('additionalQuestion', null);
        	this.props.parent.checkAdditionalQuestionPresent();
        }
        if (this.props.recordingType == 'additional-question') {
            this.props.parent.setState({additionalQuestionReply: ''});
            this.props.parent.checkAdditionalQuestionPresent();
        }
        if (this.props.recordingType == 'complete') {
            this.props.parent.setState({replyMessage: ''});
            this.props.parent.checkGameReplyPresent();
        }
    }

    async onPressConfirmRecording() {
        // Pass the recording back to the parent component
        if (this.props.recordingType == 'add-question-hint') {
        	await this.props.parent.updateGameState('questionHint', this.recording._uri);
        	this.props.parent.checkQuestionHintPresent();
        }
        if (this.props.recordingType == 'add-additional') {
        	await this.props.parent.updateGameState('additionalQuestion', this.recording._uri);
        	this.props.parent.checkAdditionalQuestionPresent();
        }
        if (this.props.recordingType == 'additional-question') {
            await this.props.parent.setState({additionalQuestionReply: this.recording._uri});
            this.props.parent.checkAdditionalQuestionPresent();
        }
        if (this.props.recordingType == 'complete') {
            await this.props.parent.setState({replyMessage: this.recording._uri});
            this.props.parent.checkGameReplyPresent();
        }
        if (this.props.recordingType == 'voice-message') {
            await this.props.parent.setState({recordingUri: this.recording._uri});
            await this.props.parent.uploadAndSendVoiceMessage();
            await this.props.parent.getNewMessages();
            await this.props.parent.setState({recordingUri: ''});
        }
        this.props.parent.setState({enableVoiceRecorder: false});
    }

    exitVoiceRecorder() {
        if (this.props.recordingType == 'add-question-hint' || this.props.recordingType == 'add-additional') {
        	this.props.parent.checkAdditionalQuestionPresent();
            this.props.parent.checkQuestionHintPresent();
        }
        if (this.props.recordingType == 'additional-question' || this.props.recordingType == 'complete') {
            this.props.parent.checkAdditionalQuestionPresent();
            this.props.parent.checkGameReplyPresent();
        }
    	this.props.parent.setState({enableVoiceRecorder: false});
    }

	render() {
        var styles = this.state.styles;
        if (this.recording == null) {
            return(
                <BlurView style={styles.container} intensity={90} tint='dark'>
                    <Text style={gstyles.mediumText}>Click below to start recording!</Text>
                    <View style={styles.controlRow}>
                        <TouchableOpacity style={styles.button}
                                          onPress={() => this.onPressRecord()}>
                            <Text style={gstyles.smallText}>Start recording!</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={[styles.closeIcon, gstyles.shadow]} onPress={() => this.exitVoiceRecorder()}>
                    	<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/close.png')} />
                	</TouchableOpacity>
                </BlurView>
            );
        }
        if (this.state.stage == 'recording') {
            return(
                <BlurView style={styles.container} intensity={90} tint='dark'>
                    <Text style={gstyles.mediumText}>Recording your message...</Text>
                    <Text style={gstyles.largeText}>Time: {this.state.recordingDuration} seconds</Text>
                    <View style={styles.controlRow}>
                        <TouchableOpacity style={[styles.button, {backgroundColor: color.red}]}
                                          onPress={() => this.onPressFinishRecording()}>
                            <Text style={gstyles.smallText}>Stop recording!</Text>
                        </TouchableOpacity>
                    </View>
                </BlurView>            
            );
        }
        if (this.state.stage == 'complete') {
            return(
               <BlurView style={styles.container} intensity={90} tint='dark'>
                    <Text style={gstyles.mediumText}>Click below to start recording!</Text>
                    <View style={styles.controlRow}>
                        <TouchableOpacity style={[styles.button, styles.subButton]}
                                          onPress={() => this.onPressPlaybackRecording()}>
                            <Text style={gstyles.smallText}>Playback</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.button}
                                          onPress={() => this.onPressConfirmRecording()}>
                            <Text style={gstyles.mediumText}>Confirm</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.button, styles.subButton]}
                                          onPress={() => this.onPressClearRecording()}>
                            <Text style={gstyles.smallText}>Clear</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={[styles.closeIcon, gstyles.shadow]} onPress={() => this.exitVoiceRecorder()}>
                    	<Image style={gstyles.mediumIcon} source={require('../../../assets/icons/png/close.png')} />
                	</TouchableOpacity>
                </BlurView>
            );

        }
	}

}